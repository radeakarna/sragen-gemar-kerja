<?php

class M_geleri extends CI_Model {
    var $table         = 'galeri t1'; //nama tabel dari database
    var $column_order  = array(null, 't1.judul', 't1.deskripsi', 't2.id', 't1.diubah_pada'); //field yang ada di table user
    var $column_search = array('t1.judul', 't1.deskripsi'); //field yang diizin untuk pencarian
    var $order         = array('t2.nama' => 'asc'); // default order
    var $select        = "t1.id, t1.judul, t1.foto_thumbnail_path, t1.deskripsi, t2.nama as nama_album, t1.diubah_pada";
    var $lokasi        = "uploads/images/album/";

    public function __construct() {
        parent::__construct();
    }

    function get_data($album = null){
        $list = $this->get_datatables($album); 
        $data = array();
        $no = $_GET['start'];
        foreach($list as $field){
            $no++;
            $row     = array();
            $row[]   = $no;
            $row[]   = '<img class="rounded" src="'.base_url($field->foto_thumbnail_path).'" alt="'.$field->deskripsi.'" width="80%" />';
            $row[]   = $field->deskripsi;
            $row[]   = $field->nama_album;
            $row[]   = tanggal_format($field->diubah_pada);
            $action  = '';
            $action .= '<button href="#" class="btn btn-sm btn-rounded btn-outline-primary edit_button" data="'.strToHex($field->id).'"> <i class="fa fa-pencil"></i> Edit</button> '; 
            $action .= '<button href="#" class="btn btn-sm btn-rounded btn-outline-danger delete_button" data="'.strToHex($field->id).'"> <i class="fa fa-trash"></i> Hapus</button>';
            $row[] = $action;

            $data[] = $row;
        }

        $output = array(
            "draw"              => $_GET['draw'],
            "recordsTotal"      => $this->count_all($album),
            "recordsFiltered"   => $this->count_filtered($album),
            "data"              => $data,
        );
        //output dalam format JSON
        return $output;
    }

    private function _get_datatables_query($album = null) {

        $this->db->select($this->select);
        $this->db->from($this->table);
        $this->db->join('album t2', 't1.album_id = t2.id', 'left');
        $this->db->where(['t1.dihapus_pada' => null]);
        $this->db->where(['t2.dihapus_pada' => null]);
        $this->db->where('t2.status', '1');

        if($album != null){
            $this->db->where('t2.slug', $album);    
        }


        $i = 0;

        foreach ($this->column_search as $item) { // looping awal
            if($_GET['search']['value']) { // jika datatable mengirimkan pencarian dengan metode POST

                if($i===0) // looping awal
                {
                    $this->db->group_start();
                    $this->db->like($item, $_GET['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_GET['search']['value']);
                }

                if(count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }

        if(isset($_GET['order']))
        {
            $this->db->order_by($this->column_order[$_GET['order']['0']['column']], $_GET['order']['0']['dir']);
        }
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables($album = null) {
        $this->_get_datatables_query($album);
        if($_GET['length'] != -1)
        $this->db->limit($_GET['length'], $_GET['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered($album = null) {
        $this->_get_datatables_query($album);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all($album = null) {
        $this->_get_datatables_query($album);
        return $this->db->count_all_results();
    } 

    function get_where_id($id=null) {
        return $this->db->where('id', $id)->get($this->table)->row();
    }

    public function fileUpload($id)
    {
        //Upload file
        $config=[
            'upload_path'   => $this->mdir($id),
            // 'file_name'     => $id,
            'allowed_types' => 'gif|jpg|png|jpeg',
            'max_size'      => 20000,
            'encrypt_name'  => TRUE,
        
        ];

        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('file')){

            $json['status']  = false;
            $json['message'] = 'Gagal mengupload gambar';

        }else{

            // thump
            $this->image
                ->load($this->upload->data('full_path'))
                ->resize_crop(368, 207) // panjang x lebar
                ->set_jpeg_quality(100)
                ->save_pa("", "-thumb", FALSE);

            // large
            $this->image
                ->load($this->upload->data('full_path'))
                ->resize_crop(1110, 624) // panjang x lebar
                ->set_jpeg_quality(100)
                ->save_pa("", "-large", FALSE);

            $location = $this->lokasi.$id.'/';
            $json['status']        = true;
            $json['file_name']     = $this->upload->data('file_name');
            $json['file_path']     = $this->upload->data('file_path');  
            $json['full_path']     = $this->upload->data('full_path');  
            $json['location']      = $location;
            $json['full_location'] = $location.$this->upload->data('file_name');
            $json['file_thumb']    = $this->upload->data('raw_name').'-thumb'.$this->upload->data('file_ext');
            $json['file_large']    = $this->upload->data('raw_name').'-large'.$this->upload->data('file_ext');
        }

        return $json; 

    }

    //upload file 
    public function mdir($folder = "") {
        $structure = $this->lokasi.$folder;
        if (!file_exists($structure)) {
            @mkdir($structure, 0777, true);
            @fwrite(fopen($structure . "/index.html", "w"), "Directory access is forbidden.");
        }
        return $structure;
    }

    public function remdir($folder = '') {
        $path  = $this->lokasi.$folder;
        $files = @glob($path . '/*');
        foreach ($files as $file) {
            @is_dir($file) ? remove_dir($file) : @unlink($file);
        }
        @rmdir($path);
        return $path;
    }


}