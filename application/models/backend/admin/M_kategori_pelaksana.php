<?php

defined('BASEPATH') or exit('No direct script access allowed');

class M_kategori_pelaksana extends CI_Model
{

    var $table         = 'kategori_unit t1'; //nama tabel dari database
    var $column_order  = [null, 't1.nama', 't1.status', 't1.slug', 't1.dibuat_pada', null]; //field yang ada di table user
    var $column_search = ['t1.nama', 't1.status', 't1.dibuat_pada'];//field yang diizin untuk pencarian
    var $order         = ['t1.id' => 'DESC']; // default order
    var $select        = "t1.id, t1.nama, t1.status, t1.slug, t1.dibuat_pada, t1.deskripsi, t1.jenis";

    public function list_data()
    {
        $list = $this->get_datatables();
        $data = array();
        $no = $_GET['start']+1; 
        foreach ($list as $v) {

            $x = [];
            $x[] = $no++;
            $x[] = $v->nama;
            // $x[] = '<span class="badge badge-info">'.$v->jenis.'</span>';
            $x[] = tanggal_format($v->dibuat_pada);

            $status = '';

            switch ($v->status) {
                case 1:
                    $status = '
                        <label class="css-control css-control-sm css-control-success css-switch">
                            <input type="checkbox" class="css-control-input tombol_ubah_status" data-id="' . strToHex($v->id) . '" data-status="0" checked>
                            <span class="css-control-indicator"></span> Aktif
                        </label>        
                    ';
                    break;
                default:
                    $status = '
                        <label class="css-control css-control-sm css-control-success css-switch">
                            <input type="checkbox" class="css-control-input tombol_ubah_status" data-id="' . strToHex($v->id) . '" data-status="1">
                            <span class="css-control-indicator"></span> Nonaktif
                        </label>
                    ';
            }
            
            $x[] = $status;
            $x[] = $v->deskripsi;

            $aksi = '';

            $aksi .= '<button type="button" class="btn btn-circle btn-outline-warning mr-5 mb-5 tombol_ubah" data="' . strToHex($v->id) . '" title="Edit Kategori Berita ' . $v->nama . '">';
            $aksi .= '<i class="fa fa-edit"></i>';
            $aksi .= '</button>';

            $aksi .= '<button type="button" class="btn btn-circle btn-outline-danger mr-5 mb-5 tombol_hapus" data="' . strToHex($v->id) . '" data-title="' . $v->nama . '" title="Hapus Kategori Berita ' . $v->nama . '">';
            $aksi .= '<i class="fa fa-trash-o"></i>';
            $aksi .= '</button>';

            $x[] = $aksi;

            $data[] = $x;

        }
 
        $output = array(
            "draw"              => $_GET['draw'],
            "recordsTotal"      => $this->count_all(),
            "recordsFiltered"   => $this->count_filtered(),
            "data"              => $data,
        );
        return $output;
    }

    //kebutuhabn server side
	private function _get_datatables_query(){
        $this->db->select($this->select);
        $this->db->from($this->table);
        $this->db->where('t1.dihapus_pada is NULL');
        
        $i = 0;
     
        foreach ($this->column_search as $item) // looping awal
        {
            if($_GET['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
            {
                 
                if($i===0) // looping awal
                {
                    $this->db->group_start(); 
                    $this->db->like($item, $_GET['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_GET['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) 
                    $this->db->group_end(); 
            }
            $i++;
        }
         
        if(isset($_GET['order'])) 
        {
            $this->db->order_by($this->column_order[$_GET['order']['0']['column']], $_GET['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    public function get_datatables()
    {
        $this->_get_datatables_query();
        if($_GET['length'] != -1)
        $this->db->limit($_GET['length'], $_GET['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->_get_datatables_query();
        return $this->db->count_all_results();
    }
}
