<?php

defined('BASEPATH') or exit('No direct script access allowed');

class M_manajemen_web extends CI_Model
{

    var $table         = 'website'; //nama tabel dari database
    var $column_order  = [null, 'judul', 'dibuat_pada', null]; //field yang ada di table user
    var $column_search = ['judul', 'dibuat_pada']; //field yang diizin untuk pencarian
    var $order         = ['id' => 'ASC']; // default order
    var $select        = "id, judul, dibuat_pada";
    var $lokasi        = "uploads/images/website/";

    public function list_data()
    {
        $list = $this->get_datatables();
        $data = array();
        $no = $_GET['start'] + 1;
        foreach ($list as $v) {

            $x = [];
            $x[] = $no++;
            $x[] = $v->judul;

            $x[] = tanggal_format($v->dibuat_pada);

            $aksi = '';

            $aksi .= '<button type="button" class="btn btn-circle btn-outline-warning mr-5 mb-5 tombol_ubah" data="' . strToHex($v->id) . '" title="Edit Berita ' . $v->judul . '">';
            $aksi .= '<i class="fa fa-edit"></i>';
            $aksi .= '</button>';

            $aksi .= '<button type="button" class="btn btn-circle btn-outline-danger mr-5 mb-5 tombol_hapus" data="' . strToHex($v->id) . '" data-judul="' . $v->judul . '" title="Hapus Berita ' . $v->judul . '">';
            $aksi .= '<i class="fa fa-trash-o"></i>';
            $aksi .= '</button>';

            $x[] = $aksi;

            $data[] = $x;
        }

        $output = array(
            "draw"              => $_GET['draw'],
            "recordsTotal"      => $this->count_all(),
            "recordsFiltered"   => $this->count_filtered(),
            "data"              => $data,
        );
        return $output;
    }

    //kebutuhabn server side
    private function _get_datatables_query()
    {
        $this->db->select($this->select);
        $this->db->from($this->table);
        $this->db->where('dihapus_pada is NULL');

        $i = 0;

        foreach ($this->column_search as $item) // looping awal
        {
            if ($_GET['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
            {

                if ($i === 0) // looping awal
                {
                    $this->db->group_start();
                    $this->db->like($item, $_GET['search']['value']);
                } else {
                    $this->db->or_like($item, $_GET['search']['value']);
                }

                if (count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }

        if (isset($_GET['order'])) {
            $this->db->order_by($this->column_order[$_GET['order']['0']['column']], $_GET['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_GET['length'] != -1)
            $this->db->limit($_GET['length'], $_GET['start']);
        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->_get_datatables_query();
        return $this->db->count_all_results();
    }

    public function fileUpload($id)
    {
        //Upload file
        $config = [
            'upload_path'   => "uploads/images/website/",
            'file_nama'     => $id,
            'allowed_types' => 'gif|jpg|png|jpeg',
            'max_size'      => 20000,
        ];

        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('logo_dark')) {

            $json['status']  = true;
            $json['message'] = $this->upload->display_errors();
            $json['logo_dark']['status'] = false;
        } else {

            // thump
            $this->image
                ->load($this->upload->data('full_path'))
                ->resize_crop(368, 207) // panjang x lebar
                ->set_jpeg_quality(100)
                ->save_pa("", "-thumb", FALSE);

            // large
            $this->image
                ->load($this->upload->data('full_path'))
                ->resize_crop(1110, 624) // panjang x lebar
                ->set_jpeg_quality(100)
                ->save_pa("", "-large", FALSE);

            $location = $this->lokasi . '/';
            $json['status']        = true;
            $json['logo_dark']['status'] = true;
            $json['logo_dark']['file_name']     = $this->upload->data('file_name');
            $json['logo_dark']['file_path']     = $this->upload->data('file_path');
            $json['logo_dark']['full_path']     = $this->upload->data('full_path');
            $json['logo_dark']['location']      = $location;
            $json['logo_dark']['full_location'] = $location . $this->upload->data('file_name');
            $json['logo_dark']['file_thumb']    = $this->upload->data('raw_name') . '-thumb' . $this->upload->data('file_ext');
            $json['logo_dark']['file_large']    = $this->upload->data('raw_name') . '-large' . $this->upload->data('file_ext');
        }

        $config = [
            'upload_path'   => "uploads/images/website/",
            'file_nama'     => $id,
            'allowed_types' => 'gif|jpg|png|jpeg',
            'max_size'      => 20000,
        ];

        if (!$this->upload->do_upload('logo_white')) {

            $json['status']  = true;
            $json['message'] = $this->upload->display_errors();
            $json['logo_white']['status'] = false;
        } else {

            // thump
            $this->image
                ->load($this->upload->data('full_path'))
                ->resize_crop(368, 207) // panjang x lebar
                ->set_jpeg_quality(100)
                ->save_pa("", "-thumb", FALSE);

            // large
            $this->image
                ->load($this->upload->data('full_path'))
                ->resize_crop(1110, 624) // panjang x lebar
                ->set_jpeg_quality(100)
                ->save_pa("", "-large", FALSE);

            $location = $this->lokasi . '/';
            $json['status']        = true;
            $json['logo_white']['status'] = true;
            $json['logo_white']['file_name']     = $this->upload->data('file_name');
            $json['logo_white']['file_path']     = $this->upload->data('file_path');
            $json['logo_white']['full_path']     = $this->upload->data('full_path');
            $json['logo_white']['location']      = $location;
            $json['logo_white']['full_location'] = $location . $this->upload->data('file_name');
            $json['logo_white']['file_thumb']    = $this->upload->data('raw_name') . '-thumb' . $this->upload->data('file_ext');
            $json['logo_white']['file_large']    = $this->upload->data('raw_name') . '-large' . $this->upload->data('file_ext');
        }

        $config = [
            'upload_path'   => "uploads/images/website/",
            'file_nama'     => $id,
            'allowed_types' => 'gif|jpg|png|jpeg',
            'max_size'      => 20000,
        ];

        if (!$this->upload->do_upload('logo_footer')) {

            $json['status']  = true;
            $json['message'] = $this->upload->display_errors();
            $json['logo_footer']['status'] = false;
        } else {

            // thump
            $this->image
                ->load($this->upload->data('full_path'))
                ->resize_crop(368, 207) // panjang x lebar
                ->set_jpeg_quality(100)
                ->save_pa("", "-thumb", FALSE);

            // large
            $this->image
                ->load($this->upload->data('full_path'))
                ->resize_crop(1110, 624) // panjang x lebar
                ->set_jpeg_quality(100)
                ->save_pa("", "-large", FALSE);

            $location = $this->lokasi . '/';
            $json['status']        = true;
            $json['logo_footer']['status'] = true;
            $json['logo_footer']['file_name']     = $this->upload->data('file_name');
            $json['logo_footer']['file_path']     = $this->upload->data('file_path');
            $json['logo_footer']['full_path']     = $this->upload->data('full_path');
            $json['logo_footer']['location']      = $location;
            $json['logo_footer']['full_location'] = $location . $this->upload->data('file_name');
            $json['logo_footer']['file_thumb']    = $this->upload->data('raw_name') . '-thumb' . $this->upload->data('file_ext');
            $json['logo_footer']['file_large']    = $this->upload->data('raw_name') . '-large' . $this->upload->data('file_ext');
        }

        return $json;
    }

    public function detail($id)
    {
        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->where('dihapus_pada is NULL');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    //upload file 
    public function mdir($folder = "")
    {
        $structure = $this->lokasi . $folder;
        if (!file_exists($structure)) {
            @mkdir($structure, 0777, true);
            @fwrite(fopen($structure . "/index.html", "w"), "Directory access is forbidden.");
        }
        return $structure;
    }

    public function remdir($folder = '')
    {
        $path  = $this->lokasi . $folder;
        $files = @glob($path . '/*');
        foreach ($files as $file) {
            @is_dir($file) ? rmdir($file) : @unlink($file);
        }
        @rmdir($path);
        return $path;
    }
}
