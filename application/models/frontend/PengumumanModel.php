<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PengumumanModel extends CI_Model {
  var $table = 'pengumuman t1'; //nama tabel dari database
  public function __construct()
  {
    parent::__construct();
  }

  public function tampil($limit = null, $where = null){
    $this->db->select('t1.judul, t1.konten, t1.tanggal_pablis, t1.slug');
    $this->db->from($this->table);
    $this->db->where(['t1.dihapus_pada' => NULL, 't1.status' => '1']);
    $this->db->order_by('t1.id', 'DESC');

    if($limit != null){
      $this->db->limit($limit);
    }

    if($where != null){
      $this->db->where($where);
    }

    return $this->db->get()->result();
  }

  public function tampilPagination($limit =null, $ofset =null, $where = null){
    $this->db->select('t1.judul, t1.konten, t1.tanggal_pablis, t1.slug');
    $this->db->from($this->table);
    $this->db->where(['t1.dihapus_pada' => NULL, 't1.status' => '1']);
    $this->db->order_by('t1.id', 'DESC');

    if($limit != null){
      $this->db->limit($limit, $ofset);
    }
    
    if($where != null){
      $this->db->where($where);
    }
    return $this->db->get()->result();
  }

  public function detail($slug = null, $where=null)
  {
    $this->db->select('t1.judul, t1.konten, t1.tanggal_pablis, t1.slug');
    $this->db->from($this->table);
    $this->db->where(['t1.dihapus_pada' => NULL, 't1.status' => '1']);
    $this->db->order_by('t1.id', 'DESC');
    if($slug != null){
      $this->db->where(['t1.slug' => $slug]);
    }
    if($where != null){
      $this->db->where($where);
    }
    $this->db->order_by('t1.id', 'DESC');
    
    return $this->db->get()->row();
  }

  public function totalData($where = null)
  {
    $this->db->select('t1.judul, t1.konten, t1.tanggal_pablis, t1.slug');
    $this->db->from($this->table);
    $this->db->where(['t1.dihapus_pada' => NULL, 't1.status' => '1']);
    $this->db->order_by('t1.id', 'DESC');
    if($where != null){
      $this->db->where($where);
    }
    $this->db->order_by('news.id', 'DESC');
    return $this->db->count_all_results();
  }

  public function searchTampilPagination($limit =null, $ofset =null, $search = null){
    $this->db->select('t1.judul, t1.konten, t1.tanggal_pablis, t1.slug');
    $this->db->from($this->table);
    $this->db->where(['t1.dihapus_pada' => NULL, 't1.status' => '1']);
    $this->db->order_by('t1.id', 'DESC');
    if($limit != 0){
      $this->db->limit($limit, $ofset);
    }
    if($search != null){
      $this->db->like ('t1.judul', $search);
      $this->db->or_like('t1.dibuat_pada', $search);
    }
    return $this->db->get()->result();
  }

  public function searchTotalData($search = null)
  {
    $this->db->select('t1.judul, t1.konten, t1.tanggal_pablis, t1.slug');
    $this->db->from($this->table);
    $this->db->where(['t1.dihapus_pada' => NULL, 't1.status' => '1']);
    $this->db->order_by('t1.id', 'DESC');
    if($search != null){
      $this->db->like ('t1.judul', $search);
      $this->db->or_like('t1.dibuat_pada', $search);
    }
    return $this->db->count_all_results();
  }

}