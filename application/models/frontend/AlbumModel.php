<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AlbumModel extends CI_Model {
  var $table = 'album t1'; //nama tabel dari database
  public function __construct()
  {
    parent::__construct();
  }

  public function tampil($limit = null, $where = null){
    $this->db->select('t1.slug, t1.nama, t2.foto_thumbnail_path');
    $this->db->from($this->table);
    $this->db->join('galeri t2', 't1.id = t2.album_id', 'left');
    $this->db->where(['t1.dihapus_pada' => NULL, 't1.status' => '1']);
    $this->db->group_by("t1.id");
    $this->db->order_by('t1.id', 'DESC');

    if($limit != null){
      $this->db->limit($limit);
    }

    if($where != null){
      $this->db->where($where);
    }

    return $this->db->get()->result();
  }

  public function tampilPagination($limit =null, $ofset =null, $where = null){
    $this->db->select('t1.slug, t1.nama, t2.foto_thumbnail_path, t2.deskripsi, t1.dibuat_pada');
    $this->db->from($this->table);
    $this->db->join('galeri t2', 't1.id = t2.album_id', 'left');
    $this->db->where(['t1.dihapus_pada' => NULL, 't1.status' => '1']);
    $this->db->group_by("t1.id");
    $this->db->order_by('t1.id', 'DESC');

    if($limit != null){
      $this->db->limit($limit, $ofset);
    }
    
    if($where != null){
      $this->db->where($where);
    }
    return $this->db->get()->result();
  }

  public function detail($slug = null, $where=null)
  {
    $this->db->select('t1.slug, t1.nama, t2.foto_thumbnail_path');
    $this->db->from($this->table);
    $this->db->join('galeri t2', 't1.id = t2.album_id', 'left');
    $this->db->where(['t1.dihapus_pada' => NULL, 't1.status' => '1']);
    $this->db->group_by("t1.id");
    $this->db->order_by('t1.id', 'DESC');
    if($slug != null){
      $this->db->where(['t1.slug' => $slug]);
    }
    if($where != null){
      $this->db->where($where);
    }
    $this->db->order_by('t1.id', 'DESC');
    
    return $this->db->get()->row();
  }

  public function totalData($where = null)
  {
    $this->db->select('t1.slug, t1.nama, t2.foto_thumbnail_path');
    $this->db->from($this->table);
    $this->db->join('galeri t2', 't1.id = t2.album_id', 'left');
    $this->db->where(['t1.dihapus_pada' => NULL, 't1.status' => '1']);
    $this->db->group_by("t1.id");
    $this->db->order_by('t1.id', 'DESC');
    if($where != null){
      $this->db->where($where);
    }
    $this->db->order_by('news.id', 'DESC');
    return $this->db->count_all_results();
  }

}