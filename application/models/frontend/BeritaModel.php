<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BeritaModel extends CI_Model {
  var $table = 'berita t1'; //nama tabel dari database
  public function __construct()
  {
    parent::__construct();
  }

  public function tampil($limit = null, $where = null){
    $this->db->select('t1.judul, t1.konten, t1.foto_thumbnail_path, t1.tanggal_pablis, t1.slug, t2.slug as slug_kategori');
    $this->db->from($this->table);
    $this->db->join('kategori_berita t2', 't2.id = t1.kategori_id', 'left');
    $this->db->where(['t1.dihapus_pada' => NULL, 't1.status' => '1']);
    $this->db->where('t2.status', '1');
    $this->db->where('t2.jenis !=', 'halaman');
    $this->db->where('t2.dihapus_pada is NULL');
    $this->db->order_by('t1.id', 'DESC');

    if($limit != null){
      $this->db->limit($limit);
    }

    if($where != null){
      $this->db->where($where);
    }

    return $this->db->get()->result();
  }

  public function tampilPagination($limit =null, $ofset =null, $where = null){
    $this->db->select('t1.judul, t1.konten, t1.foto_thumbnail_path, t1.tanggal_pablis, t1.slug, t2.slug as slug_kategori');
    $this->db->from($this->table);
    $this->db->join('kategori_berita t2', 't2.id = t1.kategori_id', 'left');
    $this->db->where(['t1.dihapus_pada' => NULL, 't1.status' => '1']);
    $this->db->where('t2.status', '1');
    $this->db->where('t2.jenis !=', 'halaman');
    $this->db->where('t2.dihapus_pada is NULL');
    $this->db->order_by('t1.id', 'DESC');

    if($limit != null){
      $this->db->limit($limit, $ofset);
    }
    
    if($where != null){
      $this->db->where($where);
    }
    return $this->db->get()->result();
  }

  public function detail($slug = null, $where=null)
  {
    $this->db->select('t1.judul, t1.konten, t1.foto_besar_path, t1.tanggal_pablis, t1.slug, t2.slug as slug_kategori, t2.nama as nama_kategori');
    $this->db->from($this->table);
    $this->db->join('kategori_berita t2', 't2.id = t1.kategori_id', 'left');
    $this->db->where(['t1.dihapus_pada' => NULL, 't1.status' => '1']);
    $this->db->where('t2.status', '1');
    $this->db->where('t2.jenis !=', 'halaman');
    $this->db->where('t2.dihapus_pada is NULL');
    $this->db->order_by('t1.id', 'DESC');
    if($slug != null){
      $this->db->where(['t1.slug' => $slug]);
    }
    if($where != null){
      $this->db->where($where);
    }
    $this->db->order_by('t1.id', 'DESC');
    
    return $this->db->get()->row();
  }

  public function totalData($where = null)
  {
    $this->db->select('t1.judul, t1.konten, t1.foto_thumbnail_path, t1.tanggal_pablis, t1.slug, t2.slug as slug_kategori');
    $this->db->from($this->table);
    $this->db->join('kategori_berita t2', 't2.id = t1.kategori_id', 'left');
    $this->db->where(['t1.dihapus_pada' => NULL, 't1.status' => '1']);
    $this->db->where('t2.status', '1');
    $this->db->where('t2.jenis !=', 'halaman');
    $this->db->where('t2.dihapus_pada is NULL');
    $this->db->order_by('t1.id', 'DESC');
    if($where != null){
      $this->db->where($where);
    }
    $this->db->order_by('news.id', 'DESC');
    return $this->db->count_all_results();
  }

  // public function tagTampilPagination($limit ='', $ofset ='', $where = ''){
  //   $this->db->select('news.*');
  //   $this->db->from($this->table);
  //   $this->db->join('category', 'news.id = category.news_id', 'left');
  //   $this->db->join('category', 'category.id = category.category_id', 'left');
  //   $this->db->join('tag_news', 'news.id = tag_news.news_id', 'left');
  //   $this->db->join('tag', 'tag.id = tag_news.tag_id', 'left');
  //   $this->db->where(['news.deleted_at' => NULL, 'news.status' => '1']);
  //   $this->db->where('category.aktif', '1');
  //   $this->db->where('category.deleted_at is NULL');
  //   $this->db->order_by('news.id', 'DESC');
  //   if($limit != 0){
  //     $this->db->limit($limit, $ofset);
  //   }
  //   if($where != 0){
  //     $this->db->where($where);
  //   }
  //   return $this->db->get()->result();
  // }

  // public function tagTotalData($where = '')
  // {
  //   $this->db->select('news.*');
  //   $this->db->from($this->table);
  //   $this->db->join('category', 'news.id = category.news_id', 'left');
  //   $this->db->join('category', 'category.id = category.category_id', 'left');
  //   $this->db->join('tag_news', 'news.id = tag_news.news_id', 'left');
  //   $this->db->join('tag', 'tag.id = tag_news.tag_id', 'left');
  //   $this->db->where(['news.deleted_at' => NULL, 'news.status' => '1']);
  //   $this->db->where('category.aktif', '1');
  //   $this->db->where('category.deleted_at is NULL');
  //   if($where != ''){
  //     $this->db->where($where);
  //   }
  //   $this->db->order_by('news.id', 'DESC');
  //   return $this->db->count_all_results();
  // }

  public function searchTampilPagination($limit =null, $ofset =null, $search = null){
    $this->db->select('t1.judul, t1.konten, t1.foto_thumbnail_path, t1.tanggal_pablis, t1.slug, t2.slug as slug_kategori');
    $this->db->from($this->table);
    $this->db->join('kategori_berita t2', 't2.id = t1.kategori_id', 'left');
    $this->db->where(['t1.dihapus_pada' => NULL, 't1.status' => '1']);
    $this->db->where('t2.status', '1');
    $this->db->where('t2.jenis !=', 'halaman');
    $this->db->where('t2.dihapus_pada is NULL');
    $this->db->order_by('t1.id', 'DESC');
    if($limit != 0){
      $this->db->limit($limit, $ofset);
    }
    if($search != null){
      $this->db->like ('t1.judul', $search);
      $this->db->or_like('t2.nama', $search);
      $this->db->or_like('t1.dibuat_pada', $search);
    }
    return $this->db->get()->result();
  }

  public function searchTotalData($search = null)
  {
    $this->db->select('t1.judul, t1.konten, t1.foto_thumbnail_path, t1.tanggal_pablis, t1.slug, t2.slug as slug_kategori');
    $this->db->from($this->table);
    $this->db->join('kategori_berita t2', 't2.id = t1.kategori_id', 'left');
    $this->db->where(['t1.dihapus_pada' => NULL, 't1.status' => '1']);
    $this->db->where('t2.status', '1');
    $this->db->where('t2.jenis !=', 'halaman');
    $this->db->where('t2.dihapus_pada is NULL');
    $this->db->order_by('t1.id', 'DESC');
    if($search != null){
      $this->db->like ('t1.judul', $search);
      $this->db->or_like('t2.nama', $search);
      $this->db->or_like('t1.dibuat_pada', $search);
    }
    return $this->db->count_all_results();
  }

}