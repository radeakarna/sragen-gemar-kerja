<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GaleriModel extends CI_Model {
  var $table = 'galeri t1'; //nama tabel dari database
  public function __construct()
  {
    parent::__construct();
  }

  public function tampil($limit = null, $where = null){
    $this->db->select('t1.judul, t1.foto_thumbnail_path, t1.foto_besar_path, t1.deskripsi, t1.dibuat_pada');
    $this->db->from($this->table);
    $this->db->where(['t1.dihapus_pada' => NULL, 't1.status' => '1']);
    $this->db->order_by('t1.id', 'DESC');

    if($limit != null){
      $this->db->limit($limit);
    }

    if($where != null){
      $this->db->where($where);
    }

    return $this->db->get()->result();
  }

  public function tampilPagination($limit =null, $ofset =null, $where = null){
    $this->db->select('t1.judul, t1.foto_thumbnail_path, t1.foto_besar_path, t1.deskripsi, t1.dibuat_pada, t2.nama as album');
    $this->db->from($this->table);
    $this->db->join('album t2', 't2.id = t1.album_id');
    $this->db->where(['t1.dihapus_pada' => NULL, 't1.status' => '1']);
    $this->db->order_by('t1.id', 'DESC');

    if($limit != null){
      $this->db->limit($limit, $ofset);
    }
    
    if($where != null){
      $this->db->where($where);
    }
    return $this->db->get()->result();
  }

  public function detail($slug = null, $where=null)
  {
    $this->db->select('t1.judul, t1.foto_thumbnail_path, t1.foto_besar_path, t1.deskripsi, t1.dibuat_pada');
    $this->db->from($this->table);
    $this->db->join('album t2', 't2.id = t1.album_id');
    $this->db->where(['t1.dihapus_pada' => NULL, 't1.status' => '1']);
    $this->db->order_by('t1.id', 'DESC');
    if($slug != null){
      $this->db->where(['t1.slug' => $slug]);
    }
    if($where != null){
      $this->db->where($where);
    }
    $this->db->order_by('t1.id', 'DESC');
    
    return $this->db->get()->row();
  }

  public function totalData($where = null)
  {
    $this->db->select('t1.judul, t1.foto_thumbnail_path, t1.foto_besar_path, t1.deskripsi, t1.dibuat_pada');
    $this->db->from($this->table);
    $this->db->join('album t2', 't2.id = t1.album_id');
    $this->db->where(['t1.dihapus_pada' => NULL, 't1.status' => '1']);
    $this->db->order_by('t1.id', 'DESC');
    if($where != null){
      $this->db->where($where);
    }
    $this->db->order_by('news.id', 'DESC');
    return $this->db->count_all_results();
  }

}