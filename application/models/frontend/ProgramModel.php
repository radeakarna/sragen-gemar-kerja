<?php
class ProgramModel extends CI_Model {
	var $table = 'program t1'; //nama tabel dari database

    public function tampil($limit = null, $where = null)
    {
        $this->db->select('t1.judul, t1.slug, t1.icon_path, t1.konten');
        $this->db->from($this->table);
        $this->db->where('dihapus_pada is NULL');
        $this->db->where('status', '1');
        $this->db->order_by('t1.id', 'DESC');
        if($limit != null){
            $this->db->limit($limit);
        }
        if($where != null){
        $this->db->where($where);
        }
        return $this->db->get()->result();
    }
}
