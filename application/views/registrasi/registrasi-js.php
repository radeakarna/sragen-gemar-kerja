<script type="text/javascript">
    let status = true;
    $(document).on('change','#perusahaan_umum', function(){
        $('#usaha').val($(this).val());
    });
    
    function cek_required_tambah() {
        status = true;
        $('#form_tambah .required').each(function(){
            if( $(this).val() == "" ){
              toastr.error( 'Ada inputan yang belum terisi', 'Gagal', { timeOut: 2000, fadeOut: 2000 });
              status = false;
            }
        });
        return status;
    }
    
    $(document).on('change','#tambah_jenis_kelamin', function(){
        $('#jsk').val($(this).val());
        alert($(this).val());
    });
    
    $(document).on('change','#tambah_status_kerja', function(){
        $('#sttp').val($(this).val());
    });
    
    $('#form_tambah').submit(function(e) {
        e.preventDefault();

        if (cek_required_tambah() == true) {
            let data = new FormData(this);
            var url  = "<?= base_url($uri_segment.'store') ?>";
            data.append(csrf.token_name, csrf.hash); 
            console.log(url);
            $.ajax({
                url: url,
                type: "POST",
                data: data,
                dataType : "JSON",
                processData: false,
                contentType: false,
                cache: false,
                async: false,
                beforeSend:function(){
                    $('#buttom_tambah').text("proses...");
                },
                success: function(data) {
                    if (data.status == true) {
                    
                        $('#modal_tambah').modal('hide');
                        $('#email_umum').val("");
                        $('#perusahaan_umum').val("");
                        $('#tambah_role').val(3);
                        $('#password').val("");
                        $('#confirm_password').val("");
                        $('#buttom_tambah').text("Simpan"); 
                        toastr.success( 'User berhasil ditambahkan', 'Berhasil', { timeOut: 2000, fadeOut: 2000 });
                    } else { 
                        $('#error_username').html(data.username);
                        $('#error_email').html(data.role);
                        $('#error_perusahaan').html(data.perusahaan_id);
                        $('#error_confirm_password').html(data.confirm_password);
                        $('#buttom_tambah').text("Simpan");
                        
                        $('#error_ktp').html(respon.ktp);
                        $('#error_nim').html(respon.nim);
                       
                        $('#error_jenis_kelamin').html(respon.jsk);
                        $('#error_hp').html(respon.hp);
                        $('#error_status_kerja').html(respon.sttp);
                        $('#error_pendidikan_terakhir').html(respon.pendidikan_terakhir);
                        $('#error_bpjs').html(respon.bpjs);
                
                        toastr.error( 'Periksa Inputan Anda', { timeOut: 2000, fadeOut: 2000 });
                    }                  

                    csrf.token_name = data.csrf.token_name;
                    csrf.hash = data.csrf.hash; 
                    
                },
                error: function () {
                    toastr.error( 'Periksa Inputan Anda', { timeOut: 2000, fadeOut: 2000 });
                    location.reload();
                }
            });
        } 
    });
    

</script>