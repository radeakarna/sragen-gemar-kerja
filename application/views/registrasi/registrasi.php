<!doctype html>
<!--[if lte IE 9]>     <html lang="en" class="no-focus lt-ie10 lt-ie10-msg"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en" class="no-focus"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

        <!-- <title><?= @$this->config->item('head_title') ?></title>  -->
        <title>Login | Gemar Kerja</title> 


        <!-- Open Graph Meta -->
        <meta name="description" content="<?= @$this->config->item('head_description') ?>">
        <meta name="author" content="Dit. Akses Pembiayaan | Kemenparekraf">
        <meta name="robots" content="noindex, nofollow">
        <meta property="og:title" content="<?= @$this->config->item('head_title') ?>">
        <meta property="og:site_name" content="<?= @$this->config->item('head_site_name') ?>">
        <meta property="og:description" content="<?= @$this->config->item('head_description') ?>">
        <meta property="og:type" content="website">
        <meta property="og:url" content="">
        <meta property="og:image" content="">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="<?= base_url('assets/') ?>logo.png">
        <link rel="icon" type="image/png" sizes="192x192" href="<?= base_url('assets/') ?>logo.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?= base_url('assets/') ?>logo.png">
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- Codebase framework -->
        <link rel="stylesheet" id="css-main" href="<?= base_url('assets/') ?>backend/css/codebase.min.css">

        <!-- Custom Kemenpar -->
        <link rel="stylesheet" href="<?= base_url('assets/') ?>backend/css/custom.css">

        <!-- You can include a specific file from css/themes/ folder to alter the default color theme of the template. eg: -->
        <!-- <link rel="stylesheet" id="css-theme" href="<?= base_url('assets/') ?>backend/css/themes/flat.min.css"> -->
        <!-- END Stylesheets -->
      <?php @$toastr ? $this->load->view('_templates/css/toastr'):''; ?> 
    </head>
    <body>
        <!-- Page Container -->
        <!--
            Available classes for #page-container:

        GENERIC

            'enable-cookies'                            Remembers active color theme between pages (when set through color theme helper Codebase() -> uiHandleTheme())

        SIDEBAR & SIDE OVERLAY

            'sidebar-r'                                 Right Sidebar and left Side Overlay (default is left Sidebar and right Side Overlay)
            'sidebar-mini'                              Mini hoverable Sidebar (screen width > 991px)
            'sidebar-o'                                 Visible Sidebar by default (screen width > 991px)
            'sidebar-o-xs'                              Visible Sidebar by default (screen width < 992px)
            'sidebar-inverse'                           Dark themed sidebar

            'side-overlay-hover'                        Hoverable Side Overlay (screen width > 991px)
            'side-overlay-o'                            Visible Side Overlay by default

            'side-scroll'                               Enables custom scrolling on Sidebar and Side Overlay instead of native scrolling (screen width > 991px)

        HEADER

            ''                                          Static Header if no class is added
            'page-header-fixed'                         Fixed Header

        HEADER STYLE

            ''                                          Classic Header style if no class is added
            'page-header-modern'                        Modern Header style
            'page-header-inverse'                       Dark themed Header (works only with classic Header style)
            'page-header-glass'                         Light themed Header with transparency by default
                                                        (absolute position, perfect for light images underneath - solid light background on scroll if the Header is also set as fixed)
            'page-header-glass page-header-inverse'     Dark themed Header with transparency by default
                                                        (absolute position, perfect for dark images underneath - solid dark background on scroll if the Header is also set as fixed)

        MAIN CONTENT LAYOUT

            ''                                          Full width Main Content if no class is added
            'main-content-boxed'                        Full width Main Content with a specific maximum width (screen width > 1200px)
            'main-content-narrow'                       Full width Main Content with a percentage width (screen width > 1200px)
        -->
        <div id="page-container" class="main-content-boxed">
            <!-- Main Container -->
            <main id="main-container">
                <!-- Page Content -->
                <div class="bg-image">
                    <div class="row mx-0 bg-black-op">
                        <div class="hero-static col-md-6 col-xl-6 d-none d-md-flex align-items-md-end">
                            <div class="p-30 invisible" data-toggle="appear">
                                <p class="font-size-h4 font-w600 text-white mb-0">
                                    Registrasi User
                                </p>
                                <p class="font-italic text-white-op mb-0">
                                    Hak Cipta &copy; <span class="js-year-copy"></span>
                                </p>
                            </div>
                        </div>
                        <div class="hero-static col-md-6 col-xl-6 d-flex align-items-center bg-white invisible overflow-hidden" data-toggle="appear" data-class="animated fadeInRight">
                            <div class="content content-full">
                                <!-- Header -->
                                <div class="px-30 py-10">
                                    <a class="link-effect font-w700" href="index.html">
                                        <span class="h5 text-primary-dark">Website Gemar Kerja</span>
                                    </a>
                                    <h1 class="h3 font-w700 mt-30 mb-10">Registrasi User</h1>
                                    <!-- <h2 class="h5 font-w400 text-muted mb-0">Please sign in</h2> -->
                                </div>
                          
                                <!-- Sign In Form -->
                                <!-- jQuery Validation (.js-validation-signin class is initialized in js/pages/op_auth_signin.js) -->
                                <!-- For more examples you can check out https://github.com/jzaefferer/jquery-validation -->
                                <form id="form_tambah">
                                    <div class="block-content">

                                        <div class="form-group"> 
                                            <div class="row">

                                                <div class="col-sm-12">
                                                    <label>Email</label>
                                                    <input type="email" id="email_umum" class="form-control" name="email_umum" required/> 
                                                     <span id="error_email"></span>
                                                </div>
                                                <div class="col-sm-12">
                                                    <label>Username</label>
                                                    <input type="text" id="user_umum" name="user_umum" class="form-control" required />
                                                    <span id="error_username"></span>
                                                </div>
                                                
                                                <div class="col-sm-12">
                                                    <label>Perusahaan</label>
                                                    <select name="perusahaan" class="form-control" id="perusahaan_umum" required>
                                                        <?=$perusahaan?>
                                                    </select>
                                                     <span id="error_perusahaan"></span>
                                                     <input type="hidden" name="usaha" id="usaha" />
                                                </div>
                                                                                                
                                                <input type="hidden" name="data_role" id="data_role" value="3">
                                                
                                                <div class="col-sm-12">
                                                    <label>Password</label>
                                                    <input type="password" id="password" class="form-control" name="password" required/> 
                                                </div>
                                                <div class="col-sm-12">
                                                    <label>Ulangi Password</label>
                                                    <input type="password" id="confirm_password" class="form-control" name="confirm_password" required/> 
                                                    <span id="error_confirm_password"></span>
                                                </div>
                                            </div> 
                                        </div> 
                                    </div>

                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary-dark" id='buttom_tambah'>Simpan</button>
                                    </div>
                                </form>
                                <!-- END Sign In Form -->
                            </div>
                            <img src="<?= base_url('assets/') ?>logo.png" class="img-overlay-login">
                        </div>
                    </div>
                </div>
                <!-- END Page Content -->
            </main>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->

        <!-- Codebase Core JS -->
        <script src="<?= base_url('assets/') ?>backend/js/core/jquery.min.js"></script>
        <script src="<?= base_url('assets/') ?>backend/js/core/bootstrap.bundle.min.js"></script>
        <script src="<?= base_url('assets/') ?>backend/js/core/jquery.slimscroll.min.js"></script>
        <script src="<?= base_url('assets/') ?>backend/js/core/jquery.scrollLock.min.js"></script>
        <script src="<?= base_url('assets/') ?>backend/js/core/jquery.appear.min.js"></script>
        <script src="<?= base_url('assets/') ?>backend/js/core/jquery.countTo.min.js"></script>
        <script src="<?= base_url('assets/') ?>backend/js/core/js.cookie.min.js"></script>
        <script src="<?= base_url('assets/') ?>backend/js/codebase.js"></script>

        <!-- Page JS Plugins -->
        <script src="<?= base_url('assets/') ?>backend/js/plugins/jquery-validation/jquery.validate.min.js"></script>

        <!-- Page JS Code -->
        <script src="<?= base_url('assets/') ?>backend/js/pages/op_auth_signin.js"></script>
        <script type="text/javascript">
            const csrf = {
                token_name: '<?=$this->security->get_csrf_token_name()?>',
                hash: '<?=$this->security->get_csrf_hash()?>'
            } 
        </script>
            <?php
            
                @$toastr ? $this->load->view('_templates/js/toastr'):'';
               @$sweet_alert ? $this->load->view('_templates/js/sweet-alert'):'';
               @$leaflet ? $this->load->view('_templates/js/leaflet'):'';
               @$select2 ? $this->load->view('_templates/js/select2'):'';
               @$dropzone ? $this->load->view('_templates/js/dropzone'):'';
               @$highchart ? $this->load->view('_templates/js/highchart'):'';
               @$datatables_js ? $this->load->view('_templates/js/datatables'):'';
               @$sumernote ? $this->load->view('_templates/js/sumernote'):'';
               @$slick ? $this->load->view('_templates/js/slick'):'';
               @$script ? $this->load->view($script):'';
             ?>
    
        
        <script type="text/javascript">
            $('#email_umum').val("");
            $('#password').val("");
        </script>
    </body>
</html>
