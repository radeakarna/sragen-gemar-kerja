<script type="text/javascript">
    let status = true;
    const base_url = '<?= base_url() ?>';

    // Add the following code if you want the name of the file appear on select
    $(".custom-file-input").on("change", function() {
        let fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName); 
    });

    $(document).ready(function(){
        detail('<?= $id ?>');
    });

    $('#form_ubah').submit(function(e) {
        e.preventDefault();

        let data = new FormData(this);
        var url = "<?php echo base_url($uri_segment.'update') ?>"; 
        data.append(csrf.token_name, csrf.hash); 

        $('#buttom_simpan').text("proses...");
        $('#buttom_simpan').attr("disabled", true);

        let post  = $.ajax({
            url         : url,
            type        : 'POST',
            data        : data,
            dataType    : 'json',
            processData : false,
            contentType : false,
            cache       : false,
            async       : false,
        });
        post.done(function(respon){
            $('#buttom_simpan').text("Simpan Data");
            $('#buttom_simpan').attr("disabled", false);
            
            if (respon.status == true) {
                toastr.success( 'Data berhasil ditambahkan', 'Berhasil', { timeOut: 2000, fadeOut: 2000 });
                setTimeout(() => window.location.href = base_url+'backend/admin/slider_utama', 2500);
            } else {
                $('#file_error').html(respon.file);
                $('#nama_error').html(respon.nama);
                $('#deskripsi_error').html(respon.deskripsi);
                $('#status_error').html(respon.status);
                toastr.error( 'Periksa Inputan Anda', { timeOut: 2000, fadeOut: 2000 });
                $('#buttom_simpan').attr("disabled", false);
            }                  

            csrf.token_name = respon.csrf.token_name;
            csrf.hash = respon.csrf.hash; 
        });
        post.fail(function(respon){
            $('#buttom_simpan').attr("disabled", false);
            toastr.error( 'Ada inputan yang belum terisi', 'Gagal', { timeOut: 2000, fadeOut: 2000 });
            // location.reload();
        });
        post.always(function () {
            $('#buttom_simpan').html('Simpan Data');
        });
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            let reader = new FileReader();

            reader.onload = function (e) {
                $('#tampil_kover').attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    // jQuery(function () {
    //     // Init page helpers (Summernote + CKEditor + SimpleMDE plugins)
    //     Codebase.helpers(['summernote']);
    // });

    function detail(id){
        let url  = "<?= base_url($uri_segment.'detail') ?>";
        let data = {id : id};
        let get  = $.get(url, data);
        get.done(function(respon){
            $('#id').val(id);
            $('#nama').val(respon.data.nama);
            // get_kategori(respon.data.kategori_id);
            $('#deskripsi').val(respon.data.deskripsi);
            $('input:radio[name="status"]').filter('[value="'+respon.data.status+'"]').attr('checked', true);
            $('#tampil_kover').attr('src', base_url+respon.data.foto_path);
        });
        get.fail(function(respon){
            toastr.error( 'Periksa koneksi internet', 'Gagal', { timeOut: 2000, fadeOut: 2000 });
            // location.reload();
        });
    }
</script>