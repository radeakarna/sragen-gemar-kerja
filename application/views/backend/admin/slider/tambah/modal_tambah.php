<div id="modal_tambah_kategori" class="modal" id="modal-normal" tabindex="-1" role="dialog" aria-labelledby="modal-small" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title">Modal Tambah</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
                    </div>
                </div>
                <form id="form_tambah_kategori" method="POST" action="<?= base_url('backend/admin/kategori_berita/store') ?>">
                    <div class="block-content">

                        <div class="form-group">
                            <label for="name">Nama*</label>
                            <input name="nama" type="text" class="form-control" required>
                            <span id="nama_error"></span>
                        </div>
                        <div class="form-group">
                            <label>Jenis* </label>
                            <select class="form-control" name="jenis">
                                <option value="berita" selected>Berita</option>
                            </select>
                            <span id="jenis_error"></span>
                        </div>
                        <div class="form-group">
                            <label>Deskripsi* </label>
                            <textarea class="form-control" name="deskripsi" rows="5" required></textarea>
                            <span id="deskripsi_error"></span>
                        </div>
                        <div class="form-group">
                            <label>Status* </label>
                            <ul class="list-unstyled mb-0">
                                <li class="d-inline-block mr-2 mb-1">
                                    <fieldset>
                                        <div class="radio">
                                            <input type="radio" name="status" id="radio1" value="1" checked>
                                            <label for="radio1">Tampilkan</label>
                                        </div>
                                    </fieldset>
                                </li>
                                <li class="d-inline-block mr-2 mb-1">
                                    <fieldset>
                                        <div class="radio">
                                            <input type="radio" name="status" id="radio2" value="0">
                                            <label for="radio2">Sembunyikan</label>
                                        </div>
                                    </fieldset>
                                </li>
                            </ul>
                            <span id="status_error"></span>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal">Tutup</button>
                        <button type="submit" id='buttom_tambah' class="btn btn-info">Simpan Data</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>