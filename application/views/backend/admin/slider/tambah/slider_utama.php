    <div class="container-fluid px-md-5"> 
        <h2 class="content-heading"><?= @$page_title?></h2>

        <form id="form_tambah" method="post" action="<?= base_url($uri_segment.'store') ?>">
        <div class="row">
            <div class="col-lg-12">
                <!-- Dynamic Table Full -->
                <div class="block custom">
                    <div class="block-header block-header-default" style="background-color: white;">
                        <h3 class="block-title"> <small><?= @$detail_page_title; ?></small></h3>
                        <div class="float-right">
                            <!-- iki disik isine tombol tambah -->
                            <?php echo $btn_kembali ?>
                        </div>
                    </div>
                    <div class="block-content block-content-full"> 
                        <div class="row" id="basic-table">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-content">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label>Judul*</label>
                                                <input type="text" id="nama" class="form-control" name="nama" required/>
                                                <span id="nama_error"></span>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="email-id-vertical">Deskripsi*</label>
                                                <textarea id="deskripsi" class="form-control" name="deskripsi" rows="6"></textarea>
                                                <span id="deskripsi_error"></span>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="first-name-vertical">Foto*</label>
                                                <div class="custom-file">
                                                    <input type="file" id="kover" class="custom-file-input" name="file" onchange="readURL(this);" required/>
                                                    <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                                </div>
                                                <span id="file_error"></span>
                                            </div>
                                            <img id="tampil_kover" class="rounded mx-auto d-block bg-body-light" src="<?= base_url('assets/img/no-camera.png') ?>" alt="your image" width="50%" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full block-content-sm bg-body-light font-size-sm">
                        <button type="submit" id="buttom_tambah" class="btn btn-primary">Simpan Data</button>
                    </div>
                </div> 
            </div>
        </div>
        </form> 
        
    </div>
</main>