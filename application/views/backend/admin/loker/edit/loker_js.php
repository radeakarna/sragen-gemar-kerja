<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>

<script type="text/javascript">
    let status = true;
    const base_url = '<?= base_url() ?>';

    // Add the following code if you want the name of the file appear on select
    $(".custom-file-input").on("change", function() {
        let fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName); 
    });

     $('#tambah_mulai_sewa, #tambah_jatuh_tempo').datetimepicker({
        useSeconds: true
    });
    
     // Mencari selilish dua tanggal dengan momen js
     let getFormattedDateDiff = function(date_initial, date_finel) {
        let b = moment(date_initial),
            a = moment(date_finel),
            intervals = ['years', 'months', 'weeks', 'days', 'hours'],
            out = [];
        let time = {}

        for(let i=0; i<intervals.length; i++){
            let diff = a.diff(b, intervals[i]);
            b.add(diff, intervals[i]);
            // out.push(diff + ' ' + intervals[i]);
            time[intervals[i]] = diff
        }
        // return out.join(', ');
        return time
    };
    
     let showDiff = data => {
        const object = data;
        let data_value = '';
        for (const [key, value] of Object.entries(object)) {
            if( value != 0 ){
                const extention = { 'years': 'tahun', 'months' : 'bulan', 'weeks' : 'minggu' , 'days' : 'hari', 'hours' : 'jam' }
                data_value += ' '+value+' '+ extention[key]
            }
        }
        return data_value;
    }
    
    $('#tambah_mulai_sewa').change(function(){
        let start_date  = $(this).val()
        let end_date    = $('#tambah_jatuh_tempo').val()
        let time        = getFormattedDateDiff( new Date(start_date), new Date(end_date) )
        $('#tambah_jangka_waktu').val( showDiff(time) )
    })

    $('#tambah_jatuh_tempo').change(function(){
        let start_date = $('#tambah_mulai_sewa').val()
        let end_date   = $(this).val()
        let time       = getFormattedDateDiff( new Date(start_date), new Date(end_date) )
        $('#tambah_jangka_waktu').val( showDiff(time) )
    })
    
    $(document).ready(function(){
        detail('<?= $id ?>');
    });

    $('#form_ubah').submit(function(e) {
        e.preventDefault();

        tinyMCE.triggerSave();

        let data = new FormData(this);
        var url = "<?php echo base_url($uri_segment.'update') ?>"; 
        data.append(csrf.token_name, csrf.hash); 

        $('#buttom_simpan').text("proses...");
        $('#buttom_simpan').attr("disabled", true);

        let post  = $.ajax({
            url         : url,
            type        : 'POST',
            data        : data,
            dataType    : 'json',
            processData : false,
            contentType : false,
            cache       : false,
            async       : false,
        });
        post.done(function(respon){
            $('#buttom_simpan').text("Simpan Data");
            $('#buttom_simpan').attr("disabled", false);
            
            if (respon.status == true) {
                toastr.success( 'Data berhasil diubah', 'Berhasil', { timeOut: 2000, fadeOut: 2000 });
                setTimeout(() => window.location.href = base_url+'backend/admin/loker', 2500);
            } else {
                $('#nama_error').html(respon.nama);
                $('#npwp_error').html(respon.npwp);
                $('#deskripsi_error').html(respon.deskripsi);
                $('#alamat_error').html(respon.alamat);
                $('#status_error').html(respon.status);
                toastr.error( 'Periksa Inputan Anda', { timeOut: 2000, fadeOut: 2000 });
                $('#buttom_simpan').attr("disabled", false);
            }                  

            csrf.token_name = respon.csrf.token_name;
            csrf.hash = respon.csrf.hash; 
        });
        post.fail(function(respon){
            $('#buttom_simpan').attr("disabled", false);
            toastr.error( 'Ada inputan yang belum terisi', 'Gagal', { timeOut: 2000, fadeOut: 2000 });
            // location.reload();
        });
        post.always(function () {
            $('#buttom_simpan').html('Simpan Data');
        });
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            let reader = new FileReader();

            reader.onload = function (e) {
                $('#tampil_kover').attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    // jQuery(function () {
    //     // Init page helpers (Summernote + CKEditor + SimpleMDE plugins)
    //     Codebase.helpers(['summernote']);
    // });

    function detail(id){
        let url  = "<?= base_url($uri_segment.'detail') ?>";
        let data = {id : id};
        let get  = $.get(url, data);
        get.done(function(respon){
            
            $('#id').val(id);
            $('#ubah_judul').val(respon.data.judul);
            $('#ubah_perusahaan').val(respon.data.perusahaan_id);
            $('#tambah_mulai_sewa').val(respon.data.tanggal_mulai);
            $('#tambah_jatuh_tempo').val(respon.data.tanggal_selesai);
            $('#tambah_jangka_waktu').val(respon.data.jangka_waktu);
            // get_kategori(respon.data.kategori_id);
           // $('#npwp').val(respon.data.npwp);
           // $('#alamat').val(respon.data.alamat);
            //$('#deskripsi').val(respon.data.deskripsi);
            tinymce.get("deskripsi").setContent(respon.data.deskripsi);
        });
        get.fail(function(respon){
            toastr.error( 'Periksa koneksi internet', 'Gagal', { timeOut: 2000, fadeOut: 2000 });
            // location.reload();
        });
    }
</script>

<script src="<?= base_url() ?>plugins/tinymce/tinymce.min.js"></script>
<script>
    tinymce.init({
        selector: "#deskripsi",theme: "modern",height: 300,
        plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
            "table contextmenu directionality emoticons paste textcolor"
    ],
    toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
    toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code ",
    image_advtab: true ,
    external_filemanager_path:"<?= base_url() ?>plugins/filemanager/",
    filemanager_title:"Responsive Filemanager" ,
    external_plugins: { "filemanager" : "<?= base_url() ?>plugins/filemanager/plugin.min.js"}
    });
</script> 