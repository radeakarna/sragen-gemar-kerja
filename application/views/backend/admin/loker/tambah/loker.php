<div class="container-fluid px-md-5"> 
    <h2 class="content-heading"><?= @$page_title ?></h2>

    <form id="form_tambah" method="post" action="<?= base_url($uri_segment . 'store') ?>">
        <div class="row">
            <div class="col-lg-12">
                <!-- Dynamic Table Full -->
                <div class="block custom">
                    <div class="block-header block-header-default" style="background-color: white;">
                        <h3 class="block-title"> <small><?= @$detail_page_title; ?></small></h3>
                        <div class="float-right">
                            <!-- iki disik isine tombol tambah -->
                            <?php echo $btn_kembali ?>
                        </div>
                    </div>
                    <div class="block-content block-content-full"> 
                        <div class="row" id="basic-table">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-content">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label>Judul*</label>
                                                <input type="text" id="tambah_judul" class="form-control" name="tambah_judul" required />
                                                <span id="error_tambah_judul"></span>
                                            </div>
                                        </div>

                                        <div class="col-sm-12 row">
                                            <label class="col-12" for="kec_id">Perusahaan</label>
                                            <div class="col-md-12">
                                                <select class="js-select2 form-control" id="tambah_perushaan" name="tambah_perusahaan" style="width:100%;">
                                                   <?=$pil_perusahaan?>
                                                </select>
                                            </div>
                                            <div id="error_tambah_perusahaan" class="form-text text-muted text-danger"></div>
                                        </div>
                                        
                                        <div class="col-12 mt-3">
                                            <label>Jangka Waktu*</label>
                                            <div class="col-lg-12">

                                                <div class="row">
                                                    <div style="padding: 0 !important; margin: 0 !important;" class="col-md-9 no-gutters">
                                                        <div class="form-group">
                                                            <div class="input-daterange input-group">
                                                                
                                                                <input type="text" class="form-control" name="mulai_sewa" id="tambah_mulai_sewa" autocomplete="off" placeholder="Tanggal Mulai Sewa" value="<?= date('Y-m-d') ?>T16:00" required/>
                                                                <span id="error_tambah_waktu_sewa"></span>
                                                                
                                                                <div class="input-group-prepend input-group-append">
                                                                    <span class="input-group-text font-w600">to</span>
                                                                </div>
                                                                <input type="text" class="form-control" name="jatuh_tempo" id="tambah_jatuh_tempo" autocomplete="off" placeholder="Tanggal Jatuh Tempo" value="<?= date('Y-m-d') ?>T16:00" required/>
                                                                <span id="error_tambah_waktu_jatuh_tempo"></span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3" style="padding: 0 !important; margin: 0 !important;">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control" name="jangka_waktu" id="tambah_jangka_waktu" placeholder="Jangka Waktu" readonly required/>
                                                        </div>
                                                        <div id="error_tambah_jangka_waktu" class="form-text text-muted text-danger"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="email-id-vertical">Deskripsi Loker*</label>
                                                <textarea id="deskripsi" class="form-control" name="tambah_deskripsi" rows="6"></textarea>
                                                <span id="error_tambah_deskripsi"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full block-content-sm bg-body-light font-size-sm">
                        <button type="submit" id="buttom_tambah" class="btn btn-primary">Simpan Data</button>
                    </div>
                </div> 
            </div>
        </div>
    </form> 

</div>
</main>