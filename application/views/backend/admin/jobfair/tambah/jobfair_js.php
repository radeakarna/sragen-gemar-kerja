<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>

<script type="text/javascript">
    let status = true;

    // Add the following code if you want the name of the file appear on select
    $(".custom-file-input").on("change", function() {
        let fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });

   
    
     // Mencari selilish dua tanggal dengan momen js
     let getFormattedDateDiff = function(date_initial, date_finel) {
        let b = moment(date_initial),
            a = moment(date_finel),
            intervals = ['years', 'months', 'weeks', 'days', 'hours'],
            out = [];
        let time = {}

        for(let i=0; i<intervals.length; i++){
            let diff = a.diff(b, intervals[i]);
            b.add(diff, intervals[i]);
            // out.push(diff + ' ' + intervals[i]);
            time[intervals[i]] = diff
        }
        // return out.join(', ');
        return time
    };
    
    $(document).ready(function(){
        $('#tambah_mulai_sewa, #tambah_jatuh_tempo').datetimepicker({
           useSeconds: true
       });
    });
    
     let showDiff = data => {
        const object = data;
        let data_value = '';
        for (const [key, value] of Object.entries(object)) {
            if( value != 0 ){
                const extention = { 'years': 'tahun', 'months' : 'bulan', 'weeks' : 'minggu' , 'days' : 'hari', 'hours' : 'jam' }
                data_value += ' '+value+' '+ extention[key]
            }
        }
        return data_value;
    }
    
    $('#tambah_mulai_sewa').change(function(){
        let start_date  = $(this).val()
        let end_date    = $('#tambah_jatuh_tempo').val()
        let time        = getFormattedDateDiff( new Date(start_date), new Date(end_date) )
        $('#tambah_jangka_waktu').val( showDiff(time) )
    })

    $('#tambah_jatuh_tempo').change(function(){
        let start_date = $('#tambah_mulai_sewa').val()
        let end_date   = $(this).val()
        let time       = getFormattedDateDiff( new Date(start_date), new Date(end_date) )
        $('#tambah_jangka_waktu').val( showDiff(time) )
    })
    
    $('#form_tambah').submit(function(e) {
        e.preventDefault();

        tinyMCE.triggerSave();
        
        $('#buttom_tambah').text("proses...");
        $('#buttom_tambah').attr("disabled", true);

        let data = new FormData(this);
        var url  = $(this).attr('action');
        data.append(csrf.token_name, csrf.hash); 
        
        let post  = $.ajax({
            url         : url,
            type        : 'POST',
            data        : data,
            dataType    : 'json',
            processData : false,
            contentType : false,
            cache       : false,
            async       : false,
        });
        post.done(function(respon){
            if (respon.status == true) {
                $('#buttom_tambah').text("Simpan Data"); 
                toastr.success( 'Data berhasil ditambahkan', 'Berhasil', { timeOut: 2000, fadeOut: 2000 });
                setTimeout(() => window.location.reload(), 2500);
            } else { 
                $('#buttom_tambah').text("Simpan Data");
                $('#error_tambah_judul').html(respon.judul);
                $('#error_tambah_waktu_sewa').html(respon.waktu_sewa);
                $('#error_tambah_waktu_jatuh_tempo').html(respon.waktu_jatuh_tempo);
                $('#error_tambah_kec_id').html(respon.provinsi_id);
                $('#error_tambah_kab_id').html(respon.kecamatan_id);
                $('#error_tambah_kel_id').html(respon.kelurahan_id);
                $('#error_tambah_alamat').html(respon.perusahaan);
                $('#error_tambah_deskripsi').html(respon.deskripsi);
                
                $('#status_error').html(respon.status);
                $('#buttom_tambah').attr("disabled", false);
                toastr.error( 'Periksa Inputan Anda', { timeOut: 2000, fadeOut: 2000 });
            }                  

            csrf.token_name = respon.csrf.token_name;
            csrf.hash = respon.csrf.hash; 
        });
        post.fail(function(respon){
            toastr.error( 'Ada inputan yang belum terisi', 'Gagal', { timeOut: 2000, fadeOut: 2000 });
            // location.reload();
        });
        post.always(function () {
            $('#buttom_tambah').html('Simpan Data');
        });
    });
    
//     $('#form_tambah').submit(function(e) {
//        e.preventDefault();
//
//        $('#buttom_tambah').text("proses...");
//        $('#buttom_tambah').attr("disabled", true);
//
//        let data = new FormData(this);
//        var url  = $(this).attr('action');
//        data.append(csrf.token_name, csrf.hash); 
//        
//        let post  = $.ajax({
//            url         : url,
//            type        : 'POST',
//            data        : data,
//            dataType    : 'json',
//            processData : false,
//            contentType : false,
//            cache       : false,
//            async       : false,
//        });
//        post.done(function(respon){
//            console.log(respon);
//        });
//       
//    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#tampil_kover').attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    
    
    
    function gantiprov(){
        let prov = $('#tambah_prov_id').val();
        
        
        let url  = '<?php echo base_url($uri_segment); ?>loadpilkab';
        
        let data = {prov : prov};
        let get  = $.get(url, data);
        get.done(function(respon){
            
            var html = "";             
            for (var i = 0; i < respon.length; i++) {
                html += "<option value='"+respon[i].id+"'>"+respon[i].nama+"</option>";
            }
           // $("#kategori_id").html(html);
            $('#tambah_kab_id').append(html);
            $('#tambah_kec_id').val(0);
        });
        get.fail(function(respon){
            toastr.error( 'Periksa koneksi internet', 'Gagal', { timeOut: 2000, fadeOut: 2000 });
            // location.reload();
        });
        
       
        
    }
    
    function gantikab(){
        let kab = $('#tambah_kab_id').val();
        
        let url  = '<?php echo base_url($uri_segment); ?>loadpilkec';
        
        let data = {kab : kab};
        let get  = $.get(url, data);
        get.done(function(respon){
           
            
            var html = "";             
            for (var i = 0; i < respon.length; i++) {
                html += "<option value='"+respon[i].id+"'>"+respon[i].kecamatan_nama+"</option>";
            }
           // $("#kategori_id").html(html);
            $('#tambah_kec_id').append(html);
        });
        get.fail(function(respon){
            toastr.error( 'Periksa koneksi internet', 'Gagal', { timeOut: 2000, fadeOut: 2000 });
            // location.reload();
        });
        
       // $('#adahkec').load(urlkeload);
    }

    jQuery(function () {
        // Init page helpers (Summernote + CKEditor + SimpleMDE plugins)
        Codebase.helpers(['summernote']);
    });
</script>

<script src="<?= base_url() ?>plugins/tinymce/tinymce.min.js"></script>
<script>
    tinymce.init({
        selector: "#deskripsi",theme: "modern",height: 300,
        plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
            "table contextmenu directionality emoticons paste textcolor"
    ],
    toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
    toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code ",
    image_advtab: true ,
    external_filemanager_path:"<?= base_url() ?>plugins/filemanager/",
    filemanager_title:"Responsive Filemanager" ,
    external_plugins: { "filemanager" : "<?= base_url() ?>plugins/filemanager/plugin.min.js"}
    });
</script> 