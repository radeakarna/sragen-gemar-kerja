<script type="text/javascript">
    let status = true;
    
     
        
    $(document).ready(function() {
        load_table();
        
         
    });
   
    
    function load_table() {
        $('#table-data').dataTable({
            destroy: true,
            processing: true,
            serverSide: true,
            ajax: {
                url: $('#table-data').data('url'),
                type: 'GET',
                dataType: 'JSON',
                data: function(d) {
                    d[csrf.token_name] = csrf.hash;
                }
            },
            drawCallback: function(res) {
                csrf.token_name = res.json.csrf.token_name;
                csrf.hash = res.json.csrf.hash;
            },
            language: {
                processing: '<div class="spinner-border""></div>'
            },
            order: [],
            columnDefs: [{
                    targets: [0, 4],
                    orderable: false
                },
                {
                    targets: [0, 4],
                    className: 'text-center'
                }

            ],
            scrollX: true
        });
        
        
    }

    $('#show_data').on('click', '.tombol_ubah', function() {
        let id = $(this).attr('data');
        window.location.href = "<?= base_url('backend/admin/jobfair/edit/') ?>" + id;

    });

    // Menampilkan model 
    $('#show_data').on('click', '.tombol_hapus', function() {
        let id = $(this).attr('data');
        Swal.fire({
            title: 'Apakah Anda Yakin',
            text: "Anda akan menghapus data ini",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, hapus',
            cancelButtonText: 'Tidak',
            confirmButtonClass: 'btn btn-warning',
            cancelButtonClass: 'btn btn-danger ml-1',
            buttonsStyling: false,
        }).then(function(result) {

            if (result.value) {
                let data = {
                    [csrf.token_name]: csrf.hash,
                    id: id
                };
                let url = "<?= base_url($uri_segment . 'delete') ?>";
                let post = $.post(url, data);
                post.done(function(respon) {
                    toastr.success('Data berhasil dihapus');
                    load_table();

                    csrf.token_name = respon.csrf.token_name;
                    csrf.hash = respon.csrf.hash;
                });
                post.fail(function(respon) {
                    toastr.error('Periksa koneksi internet', 'Gagal', {
                        timeOut: 2000,
                        fadeOut: 2000
                    });
                });
            }

        });

    });

    // Menampilkan model 
    $('#show_data').on('change', '.tombol_ubah_status', function() {
        let id = $(this).data('id');
        let status = $(this).data('status');
        Swal.fire({
            title: 'Apakah Anda Yakin',
            text: "Ingin Mengubah Status Data",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, Ubah',
            cancelButtonText: 'Tidak',
            confirmButtonClass: 'btn btn-warning',
            cancelButtonClass: 'btn btn-danger ml-1',
            buttonsStyling: false,
        }).then(function(result) {

            if (result.value) {
                let data = {
                    [csrf.token_name]: csrf.hash,
                    status: status
                };
                let url = "<?= base_url($uri_segment . 'ubahStatus/') ?>" + id;
                let post = $.post(url, data);
                post.done(function(respon) {
                    toastr.success('Berhasil Mengubah Status');
                    load_table();

                    csrf.token_name = respon.csrf.token_name;
                    csrf.hash = respon.csrf.hash;
                });
                post.fail(function(respon) {
                    toastr.error('Periksa koneksi internet', 'Gagal', {
                        timeOut: 2000,
                        fadeOut: 2000
                    });
                });
            }
            load_table();
        });

    });
</script>