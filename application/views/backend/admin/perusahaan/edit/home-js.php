<script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/esri-leaflet/2.4.1/esri-leaflet-debug.js"></script>
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css" />
<script type="text/javascript">
    let status = true;
    const base_url = '<?= base_url() ?>';

    // Add the following code if you want the name of the file appear on select
    $(".custom-file-input").on("change", function() {
        let fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName); 
    });

    $(document).ready(function(){
        detail('<?= $id ?>');
        $(":input").inputmask();
    });
    
      let map;
    $(document).ready(function(){
       
        init_map();
    }); 

    let init_map = () => {
        map = L.map('map', {
            attributionControl: false,
            zoomControl: true
        }).setView([-7.610432, 110.816037], 17); 

        setInterval(function () {
           map.invalidateSize();
        }, 100);
        let basemap = {
            google_hybrid: L.tileLayer('http://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}', {
                maxZoom: 20,
                subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
            }).addTo(map),
            google_roadmap: L.tileLayer('http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}', {
                maxZoom: 20,
                subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
            }),
            google_satellite: L.tileLayer('http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}', {
                maxZoom: 20,
                subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
            }),
            google_terrain: L.tileLayer('http://{s}.google.com/vt/lyrs=p&x={x}&y={y}&z={z}', {
                maxZoom: 20,
                subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
            }),
            osm: L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                maxZoom: 19,
            }),
            esri_world_imagery: L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
                maxZoom: 17
            }),
            esri_world_street_map: L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}'),
            esri_world_topo_map: L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}'),
            citra_satelit: L.esri.imageMapLayer({
                url: 'https://portal.ina-sdi.or.id/arcgis/rest/services/CITRASATELIT/JawaBaliNusra_2015_ImgServ1/ImageServer',
                attribution: 'Badan Informasi Geospasial'
            }),
            peta_rbi: L.esri.dynamicMapLayer({
                url: 'https://portal.ina-sdi.or.id/arcgis/rest/services/IGD/RupabumiIndonesia/MapServer',
                attribution: 'Badan Informasi Geospasial'
            }),
            peta_rbi_opensource: L.tileLayer.wms('http://palapa.big.go.id:8080/geoserver/gwc/service/wms', {
                maxZoom: 20,
                layers: "basemap_rbi:basemap",
                format: "image/png",
                attribution: 'Badan Informasi Geospasial'
            })

        }
        L.control.layers(basemap).addTo(map);
		
		mark = L.marker([-7.610432, 110.816037]).addTo(map);

        map.on('click', addMarker);
		
		function addMarker(e){

            // if (mark != undefined) {
            //     map.removeLayer(mark); 
            //     $('#latitude').val(e.latlng.lat);
            //     $('#longitude').val(e.latlng.lng); 
            // };

            if (typeof mark != 'undefined') mark.remove(map); 
            $('#latitude').val(e.latlng.lat);
            $('#longitude').val(e.latlng.lng);
						// $('#latitude, #longitude').valid();		
            // updateKegiatan('latitude');
            // updateKegiatan('longitude');
            mark = L.marker(e.latlng, {
                draggable: true
            });
            mark.on('drag', function(e) {
                let latlng = e.target._latlng;
                $('#latitude').val(latlng.lat);
                $('#longitude').val(latlng.lng);
                // updateKegiatan('latitude');
                // updateKegiatan('longitude');
            })
            mark.on('dragend', function(e) {
							// $('#latitude, #longitude').valid();
                let latlng = e.target._latlng;
                // updateKegiatan('latitude');
                // updateKegiatan('longitude');
            })
			// mark = L.marker([e.latlng.lat, e.latlng.lng]).addTo(map);
            mark.addTo(map);
        }

    }
    
    
    function gantiprov(provs_det=null){
        let prov;
        if(provs_det !== 0 || provs_det !== null){
            prov = provs_det;
        } 
        
        if(provs_det == null){
            prov = $('#tambah_prov_id').val();
            
        }
     
        let url  = '<?php echo base_url($uri_segment); ?>loadpilkab';
        
        let data = {prov : prov};
        let get  = $.get(url, data);
        get.done(function(respon){
            console.log(respon);
            var html =  "<option value='0'>Pilih Kabupaten</option>";             
            for (var i = 0; i < respon.length; i++) {
                if(provs_det !== null || provs_det !== 0){
                    html += "<option value='"+respon[i].id+"' selected>"+respon[i].nama+"</option>";
                } else {
                    html += "<option value='"+respon[i].id+"'>"+respon[i].nama+"</option>";
                }
            }
           // $("#kategori_id").html(html);
            $('#tambah_kab_id').html(html);
            if(provs_det == null){
                $('#tambah_kab_id').val(0);
                $('#tambah_kec_id').html("<option value='0'>Pilih Kecamatan</option>");
            } 
            
            
        });
        get.fail(function(respon){
            toastr.error( 'Periksa koneksi internet', 'Gagal', { timeOut: 2000, fadeOut: 2000 });
            // location.reload();
        });
        
       
        
    }
     
    function gantikab(kabupaten){
        let kab;
        if(kabupaten !== 0 || kabupaten !== null){
            kab = kabupaten;
        } 
        
        if(kabupaten == null) {
            kab = $('#tambah_kab_id').val();
        }
        
           
        let url  = '<?php echo base_url($uri_segment); ?>loadpilkec';
        
        let data = {kab : kab};
        let get  = $.get(url, data);
        get.done(function(respon){
           
            
            var html = "";             
            for (var i = 0; i < respon.length; i++) {
                var html =  "<option value='0'>Pilih Kecamatan</option>";  
                if(kabupaten !== null || kabupaten == 0){
                    html += "<option value='"+respon[i].id+"' selected>"+respon[i].kecamatan_nama+"</option>";
                } else {
                    html += "<option value='"+respon[i].id+"'>"+respon[i].kecamatan_nama+"</option>";
                }
            }
           // $("#kategori_id").html(html);
         
            $('#tambah_kec_id').html(html);
            
            if(kabupaten == null){
                $('#tambah_kec_id').val(0);
            } 
        });
        get.fail(function(respon){
            toastr.error( 'Periksa koneksi internet', 'Gagal', { timeOut: 2000, fadeOut: 2000 });
            // location.reload();
        });
        
       // $('#adahkec').load(urlkeload);
    }

    $('#form_ubah').submit(function(e) {
        e.preventDefault();

        tinyMCE.triggerSave();

        let data = new FormData(this);
        var url = "<?php echo base_url($uri_segment.'update') ?>"; 
        data.append(csrf.token_name, csrf.hash); 

        $('#buttom_simpan').text("proses...");
        $('#buttom_simpan').attr("disabled", true);

        let post  = $.ajax({
            url         : url,
            type        : 'POST',
            data        : data,
            dataType    : 'json',
            processData : false,
            contentType : false,
            cache       : false,
            async       : false,
        });
        post.done(function(respon){
            $('#buttom_simpan').text("Simpan Data");
            $('#buttom_simpan').attr("disabled", false);
            
            if (respon.status == true) {
                toastr.success( 'Data berhasil diubah', 'Berhasil', { timeOut: 2000, fadeOut: 2000 });
                setTimeout(() => window.location.href = base_url+'backend/admin/perusahaan', 2500);
            } else {
                $('#nama_error').html(respon.nama);
                $('#npwp_error').html(respon.npwp);
                $('#deskripsi_error').html(respon.deskripsi);
                $('#alamat_error').html(respon.alamat);
                $('#status_error').html(respon.status);
                $('#error_tambah_provinsi').html(respon.provinsi_id);
                $('#error_tambah_kab_id').html(respon.kabupaten_id);
                $('#error_tambah_kec_id').html(respon.kecamatan_id);
                toastr.error( 'Periksa Inputan Anda', { timeOut: 2000, fadeOut: 2000 });
                $('#buttom_simpan').attr("disabled", false);
            }                  

            csrf.token_name = respon.csrf.token_name;
            csrf.hash = respon.csrf.hash; 
        });
        post.fail(function(respon){
            $('#buttom_simpan').attr("disabled", false);
            toastr.error( 'Ada inputan yang belum terisi', 'Gagal', { timeOut: 2000, fadeOut: 2000 });
            // location.reload();
        });
        post.always(function () {
            $('#buttom_simpan').html('Simpan Data');
        });
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            let reader = new FileReader();

            reader.onload = function (e) {
                $('#tampil_kover').attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    // jQuery(function () {
    //     // Init page helpers (Summernote + CKEditor + SimpleMDE plugins)
    //     Codebase.helpers(['summernote']);
    // });

    function detail(id){
        let url  = "<?= base_url($uri_segment.'detail') ?>";
        let data = {id : id};
        let get  = $.get(url, data);
        get.done(function(respon){
            $('#id').val(id);
            $('#nama').val(respon.data.nama);
            // get_kategori(respon.data.kategori_id);
            $('#npwp').val(respon.data.npwp);
            $('#alamat').val(respon.data.alamat);
            //$('#deskripsi').val(respon.data.deskripsi);
            $('#latitude').val(respon.data.latitude);
            $('#tambah_prov_id').val(respon.data.provinsi_id);
            $('#tambah_kab_id').val(respon.data.kabupaten_id);
            gantiprov(respon.data.provinsi_id);
            gantikab(respon.data.kabupaten_id);
            
            
            $('#tambah_kec_id').val(respon.data.kecamatan_id);
            $('#longitude').val(respon.data.longitude);
            $('#tampil_kover').attr('src', base_url+respon.data.foto_thumbnail_path);
            tinymce.get("deskripsi").setContent(respon.data.deskripsi);
        });
        get.fail(function(respon){
            toastr.error( 'Periksa koneksi internet', 'Gagal', { timeOut: 2000, fadeOut: 2000 });
            // location.reload();
        });
    }
</script>

<script src="<?= base_url() ?>plugins/tinymce/tinymce.min.js"></script>
<script>
    tinymce.init({
        selector: "#deskripsi",theme: "modern",height: 300,
        plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
            "table contextmenu directionality emoticons paste textcolor"
    ],
    toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
    toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code ",
    image_advtab: true ,
    external_filemanager_path:"<?= base_url() ?>plugins/filemanager/",
    filemanager_title:"Responsive Filemanager" ,
    external_plugins: { "filemanager" : "<?= base_url() ?>plugins/filemanager/plugin.min.js"}
    });
</script> 