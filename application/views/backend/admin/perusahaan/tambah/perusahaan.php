<div class="container-fluid px-md-5"> 
    <h2 class="content-heading"><?= @$page_title ?></h2>

    <form id="form_tambah" method="post" action="<?= base_url($uri_segment . 'store') ?>">
        <div class="row">
            <div class="col-lg-12">
                <!-- Dynamic Table Full -->
                <div class="block custom">
                    <div class="block-header block-header-default" style="background-color: white;">
                        <h3 class="block-title"> <small><?= @$detail_page_title; ?></small></h3>
                        <div class="float-right">
                            <!-- iki disik isine tombol tambah -->
                            <?php echo $btn_kembali ?>
                        </div>
                    </div>
                    <div class="block-content block-content-full"> 
                        <div class="row" id="basic-table">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-content">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label>Nama*</label>
                                                <input type="text" id="nama" class="form-control" name="nama" required />
                                                <span id="nama_error"></span>
                                            </div>
                                        </div>

                                        <div class="col-12">
                                            <div class="form-group">
                                                <label>NPWP*</label>
                                                <input type="text" id="npwp" class="form-control" name="npwp"  data-inputmask="'mask': '99.999.999.9-999.999'" required />
                                                <span id="npwp_error"></span>
                                            </div>
                                        </div>

                                        <div class="col-12">
                                            <div class="form-group">
                                                <label>Alamat*</label>
                                                <input type="text" id="alamat" class="form-control" name="alamat" required/>
                                                <span id="alamat_error"></span>
                                            </div>
                                        </div>
                                        
                                         <div class="row mb-3">
                                             <div class="col-md-4">
                                                <div class="col-sm-12 row">
                                                    <label class="col-12" for="kec_id">Provinsi</label>
                                                    <div class="col-md-12">
                                                        <select class="js-select2 form-control" id="tambah_prov_id" name="prov_id" onchange="gantiprov();" style="width:100%;">
                                                            <?=$provinsi?>
                                                        </select>
                                                    </div>
                                                    <div id="error_tambah_kel_id" class="form-text text-muted text-danger"></div>
                                                </div>
                                            </div>
                                            
                                             <div class="col-md-4">
                                                <div class="col-sm-12 row" id="adahkab">
                                                    <label class="col-12" for="kec_id">Kabupaten</label>
                                                    <div class="col-md-12">
                                                        <select class="js-select2 form-control" id="tambah_kab_id" name="kab_id" style="width:100%;" onchange="gantikab();">
                                                            <option value="0">Pilih Kabupaten</option>
                                                        </select>
                                                    </div>
                                                    <div id="error_tambah_kab_id" class="form-text text-muted text-danger"></div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-4">
                                                <div class="col-sm-12 row" id="adahkec">
                                                    <label class="col-md-12" for="kec_id">Kecamatan</label>
                                                    <div class="col-md-12">
                                                        <select class="js-select2 form-control col-md-12" id="tambah_kec_id" name="kec_id">
                                                            <option value="0">Pilih Kecamatan</option>
                                                        </select>
                                                    </div>
                                                    <div id="error_tambah_kec_id" class="form-text text-muted text-danger"></div>
                                                </div>
                                            </div>

                                           
                                        </div>

                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="email-id-vertical">Deskripsi Perusahaan*</label>
                                                <textarea id="deskripsi" class="form-control" name="deskripsi" rows="6"></textarea>
                                                <span id="deskripsi_error"></span>
                                            </div>
                                        </div>

                                        <div class="block-content block-content-full">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div id="map" style="height: 285px"></div>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label style="display:none;" for="latitude">Latitude<span class="text-danger">*</span></label>
                                                                <input id="latitude" class="form-control required" type="hidden" name="latitude">
                                                            </div>
                                                            <div id="error_tambah_latitude" class="form-text text-muted text-danger"></div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label style="display:none;" for="longitude">Longitude<span class="text-danger">*</span></label>
                                                                <input id="longitude" class="form-control required" type="hidden" name="longitude">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="error_tambah_longitude" class="form-text text-muted text-danger"></div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="card">
                                                        <div class="card-content">
                                                            <div class="block">
                                                                <div class="block-header bg-info" style="background-color: white !important;">
                                                                    <h3 class="block-title"> <small class="text-black">Gambar Sampul</small></h3>
                                                                </div>
                                                                <div class="block-content">
                                                                    <div class="row">
                                                                        <div class="col-12">
                                                                            <div class="form-group">
                                                                                <label for="first-name-vertical">Kover*</label>
                                                                                <div class="custom-file">
                                                                                    <input type="file" id="kover" class="custom-file-input" name="file" onchange="readURL(this);" required/>
                                                                                    <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                                                                </div>
                                                                                <span id="file_error"></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="block-content block-content-full block-content-sm bg-body-light font-size-sm">
                                                                    <img id="tampil_kover" class="rounded mx-auto d-block" src="<?= base_url('assets/no-img.png') ?>" alt="your image" width="40%" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="block-content block-content-full block-content-sm bg-body-light font-size-sm">
                            <button type="submit" id="buttom_tambah" class="btn btn-primary">Simpan Data</button>
                        </div>
                    </div> 
                </div>
            </div>
    </form> 

</div>
</main>