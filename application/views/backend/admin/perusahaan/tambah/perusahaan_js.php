<script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/esri-leaflet/2.4.1/esri-leaflet-debug.js"></script>
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css" />
<script type="text/javascript">
    let status = true;

    // Add the following code if you want the name of the file appear on select
    $(".custom-file-input").on("change", function() {
        let fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });
    $(document).ready(function(){
        //$("#npwp").mask("99.999.999.9-999.999");
        $(":input").inputmask();
       // $('#npwp')
    });
    
    let map;
    $(document).ready(function(){
       
        init_map();
    }); 

    let init_map = () => {
        map = L.map('map', {
            attributionControl: false,
            zoomControl: true
        }).setView([-7.610432, 110.816037], 17); 

        setInterval(function () {
           map.invalidateSize();
        }, 100);
        let basemap = {
            google_hybrid: L.tileLayer('http://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}', {
                maxZoom: 20,
                subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
            }).addTo(map),
            google_roadmap: L.tileLayer('http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}', {
                maxZoom: 20,
                subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
            }),
            google_satellite: L.tileLayer('http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}', {
                maxZoom: 20,
                subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
            }),
            google_terrain: L.tileLayer('http://{s}.google.com/vt/lyrs=p&x={x}&y={y}&z={z}', {
                maxZoom: 20,
                subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
            }),
            osm: L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                maxZoom: 19,
            }),
            esri_world_imagery: L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
                maxZoom: 17
            }),
            esri_world_street_map: L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}'),
            esri_world_topo_map: L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}'),
            citra_satelit: L.esri.imageMapLayer({
                url: 'https://portal.ina-sdi.or.id/arcgis/rest/services/CITRASATELIT/JawaBaliNusra_2015_ImgServ1/ImageServer',
                attribution: 'Badan Informasi Geospasial'
            }),
            peta_rbi: L.esri.dynamicMapLayer({
                url: 'https://portal.ina-sdi.or.id/arcgis/rest/services/IGD/RupabumiIndonesia/MapServer',
                attribution: 'Badan Informasi Geospasial'
            }),
            peta_rbi_opensource: L.tileLayer.wms('http://palapa.big.go.id:8080/geoserver/gwc/service/wms', {
                maxZoom: 20,
                layers: "basemap_rbi:basemap",
                format: "image/png",
                attribution: 'Badan Informasi Geospasial'
            })

        }
        L.control.layers(basemap).addTo(map);
		
		mark = L.marker([-7.610432, 110.816037]).addTo(map);

        map.on('click', addMarker);
		
		function addMarker(e){

            // if (mark != undefined) {
            //     map.removeLayer(mark); 
            //     $('#latitude').val(e.latlng.lat);
            //     $('#longitude').val(e.latlng.lng); 
            // };

            if (typeof mark != 'undefined') mark.remove(map); 
            $('#latitude').val(e.latlng.lat);
            $('#longitude').val(e.latlng.lng);
						// $('#latitude, #longitude').valid();		
            // updateKegiatan('latitude');
            // updateKegiatan('longitude');
            mark = L.marker(e.latlng, {
                draggable: true
            });
            mark.on('drag', function(e) {
                let latlng = e.target._latlng;
                $('#latitude').val(latlng.lat);
                $('#longitude').val(latlng.lng);
                // updateKegiatan('latitude');
                // updateKegiatan('longitude');
            })
            mark.on('dragend', function(e) {
							// $('#latitude, #longitude').valid();
                let latlng = e.target._latlng;
                // updateKegiatan('latitude');
                // updateKegiatan('longitude');
            })
			// mark = L.marker([e.latlng.lat, e.latlng.lng]).addTo(map);
            mark.addTo(map);
        }

    }
    
    function gantiprov(){
        let prov = $('#tambah_prov_id').val();
        
        
        let url  = '<?php echo base_url($uri_segment); ?>loadpilkab';
        
        let data = {prov : prov};
        let get  = $.get(url, data);
        get.done(function(respon){
            
            var html = "";             
            for (var i = 0; i < respon.length; i++) {
                html += "<option value='"+respon[i].id+"'>"+respon[i].nama+"</option>";
            }
           // $("#kategori_id").html(html);
            $('#tambah_kab_id').append(html);
            $('#tambah_kec_id').val(0);
        });
        get.fail(function(respon){
            toastr.error( 'Periksa koneksi internet', 'Gagal', { timeOut: 2000, fadeOut: 2000 });
            // location.reload();
        });
        
       
        
    }
    
    function gantikab(){
        let kab = $('#tambah_kab_id').val();
        
        let url  = '<?php echo base_url($uri_segment); ?>loadpilkec';
        
        let data = {kab : kab};
        let get  = $.get(url, data);
        get.done(function(respon){
           
            
            var html = "";             
            for (var i = 0; i < respon.length; i++) {
                html += "<option value='"+respon[i].id+"'>"+respon[i].kecamatan_nama+"</option>";
            }
           // $("#kategori_id").html(html);
            $('#tambah_kec_id').append(html);
        });
        get.fail(function(respon){
            toastr.error( 'Periksa koneksi internet', 'Gagal', { timeOut: 2000, fadeOut: 2000 });
            // location.reload();
        });
        
       // $('#adahkec').load(urlkeload);
    }
    
    $('#form_tambah').submit(function(e) {
        e.preventDefault();

        tinyMCE.triggerSave();
        
        $('#buttom_tambah').text("proses...");
        $('#buttom_tambah').attr("disabled", true);

        let data = new FormData(this);
        var url  = $(this).attr('action');
        data.append(csrf.token_name, csrf.hash); 
        
        let post  = $.ajax({
            url         : url,
            type        : 'POST',
            data        : data,
            dataType    : 'json',
            processData : false,
            contentType : false,
            cache       : false,
            async       : false,
        });
        post.done(function(respon){
            if (respon.status == true) {
                $('#buttom_tambah').text("Simpan Data"); 
                toastr.success( 'Data berhasil ditambahkan', 'Berhasil', { timeOut: 2000, fadeOut: 2000 });
                setTimeout(() => window.location.reload(), 2500);
            } else { 
                $('#buttom_tambah').text("Simpan Data");
                $('#nama_error').html(respon.nama);
                $('#npwp_error').html(respon.npwp);
                $('#deskripsi_error').html(respon.deskripsi);
                $('#alamat_error').html(respon.alamat);
                $('#status_error').html(respon.status);
                $('#error_tambah_latitude').html(respon.latitude);
                $('#error_tambah_longitude').html(respon.longitude);               
                $('#buttom_tambah').attr("disabled", false);
                toastr.error( 'Periksa Inputan Anda', { timeOut: 2000, fadeOut: 2000 });
            }                  

            csrf.token_name = respon.csrf.token_name;
            csrf.hash = respon.csrf.hash; 
        });
        post.fail(function(respon){
            toastr.error( 'Ada inputan yang belum terisi', 'Gagal', { timeOut: 2000, fadeOut: 2000 });
            // location.reload();
        });
        post.always(function () {
            $('#buttom_tambah').html('Simpan Data');
        });
    });
    
//     $('#form_tambah').submit(function(e) {
//        e.preventDefault();
//
//        $('#buttom_tambah').text("proses...");
//        $('#buttom_tambah').attr("disabled", true);
//
//        let data = new FormData(this);
//        var url  = $(this).attr('action');
//        data.append(csrf.token_name, csrf.hash); 
//        
//        let post  = $.ajax({
//            url         : url,
//            type        : 'POST',
//            data        : data,
//            dataType    : 'json',
//            processData : false,
//            contentType : false,
//            cache       : false,
//            async       : false,
//        });
//        post.done(function(respon){
//            console.log(respon);
//        });
//       
//    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#tampil_kover').attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    jQuery(function () {
        // Init page helpers (Summernote + CKEditor + SimpleMDE plugins)
        Codebase.helpers(['summernote']);
    });
</script>

<script src="<?= base_url() ?>plugins/tinymce/tinymce.min.js"></script>
<script>
    tinymce.init({
        selector: "#deskripsi",theme: "modern",height: 300,
        plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
            "table contextmenu directionality emoticons paste textcolor"
    ],
    toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
    toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code ",
    image_advtab: true ,
    external_filemanager_path:"<?= base_url() ?>plugins/filemanager/",
    filemanager_title:"Responsive Filemanager" ,
    external_plugins: { "filemanager" : "<?= base_url() ?>plugins/filemanager/plugin.min.js"}
    });
</script> 