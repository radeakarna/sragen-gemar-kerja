<script type="text/javascript">
    let status = true;
    const base_url = '<?= base_url() ?>';

    // Add the following code if you want the name of the file appear on select
    $(".custom-file-input").on("change", function() {
        let fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });

    $(document).ready(function(){
        get_kategori('<?= $id ?>');
        detail('<?= $id ?>');
    });

    $( "#tambah_kategori_berita" ).click(function() {
        $('#modal_tambah_kategori').modal('show');
    });

    $('#form_tambah_kategori').submit(function(e) {
        e.preventDefault();

        let data = new FormData(this);
        let url  = "<?= base_url('backend/admin/kategori_berita/store') ?>";
        data.append(csrf.token_name, csrf.hash); 

        $('#buttom_tambah_kategori').text("proses...");

        let post  = $.ajax({
            url         : url,
            type        : 'POST',
            data        : data,
            dataType    : 'json',
            processData : false,
            contentType : false,
            cache       : false,
            async       : false,
        });
        post.done(function(respon){
            if (respon.status == true) {
                $('#modal_tambah_kategori').modal('hide');
                get_kategori();
                $('#buttom_tambah_kategori').text("Simpan"); 
                toastr.success( 'Data berhasil ditambahkan', 'Berhasil', { timeOut: 2000, fadeOut: 2000 });
                // setTimeout(() => window.location.reload(), 2500);
            } else { 
                $('#buttom_tambah_kategori').text("Simpan");
                toastr.error( 'Periksa Inputan Anda, nama tidak boleh sama', { timeOut: 2000, fadeOut: 2000 });
            }                  

            csrf.token_name = respon.csrf.token_name;
            csrf.hash = respon.csrf.hash; 
        });
        post.fail(function(respon){
            toastr.error( 'Ada inputan yang belum terisi', 'Gagal', { timeOut: 2000, fadeOut: 2000 });
            location.reload();
        });
        post.always(function () {
            $('#buttom_tambah_kategori').html('Simpan Data');
        });
    });


    function get_kategori(id = null){
        let url  = "<?= base_url('backend/admin/kategori_unit/show_id') ?>";
        let data = { jenis : 'berita', id: id };
        let get  = $.get(url, data);
        
        get.done(function(respon){
//            let html = "<option value=''></option>";             
//            for (let i = 0; i < respon.length; i++) {
//                let selected = '';
//                if(respon[i].id){
//                    selected = 'selected';
//                } 
//                
//                html += "<option value='"+respon[i].id+"'>"+respon[i].nama+"</option>";
//                
//            }
            $("#kategori_id").html(respon);
        });
        get.fail(function(respon){
            toastr.error( 'Periksa koneksi internet', 'Gagal', { timeOut: 2000, fadeOut: 2000 });
            // location.reload();
        });
    }

    
    $('#form_ubah').submit(function(e) {
        e.preventDefault();

        tinyMCE.triggerSave();
        let data = new FormData(this);
        let url  = $(this).attr('action');
        // data.append(csrf.token_name, csrf.hash);
        
        $('#buttom_simpan').text("proses...");
        $('#buttom_simpan').attr("disabled", true);

        let post  = $.ajax({
            url         : url,
            type        : 'POST',
            data        : data,
            dataType    : 'json',
            processData : false,
            contentType : false,
            cache       : false,
            async       : false,
            beforeSend: function(){
                // Show image container
                $('body').addClass('oy-hid');
                $("#preloader").show();
            },
        });
        post.done(function(respon){
            
            if (respon.status == true) {
                toastr.success( 'Data berhasil ditambahkan', 'Berhasil', { timeOut: 2000, fadeOut: 2000 });
                setTimeout(() => window.location.href = base_url+'backend/admin/berita', 2500);
            } else {

                $("body").removeClass("oy-hid");
                $("#preloader").hide();

                $('#kategori_error').html(respon.kategori_id);
                $('#file_error').html(respon.file);
                $('#judul_error').html(respon.judul);
                $('#konten_error').html(respon.konten);
                $('#status_error').html(respon.status);
                toastr.error( 'Periksa Inputan Anda', { timeOut: 2000, fadeOut: 2000 });
            }                  

            csrf.token_name = respon.csrf.token_name;
            csrf.hash = respon.csrf.hash; 

            $('#buttom_simpan').text("Simpan Data");
            $('#buttom_simpan').attr("disabled", false);
        });
        post.fail(function(respon){
            toastr.error( 'Ada inputan yang belum terisi', 'Gagal', { timeOut: 2000, fadeOut: 2000 });
            location.reoad();
        });

    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            let reader = new FileReader();

            reader.onload = function (e) {
                $('#tampil_kover').attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    function detail(id){
        let url  = "<?= base_url($uri_segment.'detail') ?>";
        let data = {id : id};
        let get  = $.get(url, data);
        get.done(function(respon){
            $('#id').val(id);
            $('#judul').val(respon.data.judul);
            // get_kategori(respon.data.kategori_id);
            //setTimeout(() => $('#kategori_id').val(respon.data.kategori_id), 500);
            
            $('input:radio[name="status"]').filter('[value="'+respon.data.status+'"]').attr('checked', true);
            $('#tampil_kover').attr('src', base_url+respon.data.foto_thumbnail_path);
            //$('textarea#konten').html(respon.data.konten);
            //tinymce.get("textarea#konten").setContent(respon.data.konten);
            tinymce.activeEditor.setContent(respon.data.konten);
        });
        get.fail(function(respon){
            toastr.error( 'Periksa koneksi internet', 'Gagal', { timeOut: 2000, fadeOut: 2000 });
            // location.reload();
        });
    }

</script>

<script src="<?= base_url() ?>plugins/tinymce/tinymce.min.js"></script>
<script>
    tinymce.init({
        selector: "#konten",theme: "modern",height: 220,
        plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
            "table contextmenu directionality emoticons paste textcolor"
    ],
    toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
    toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code",
    image_advtab: true ,
    auto_focus:true,
    draggable_modal: true,
    elementpath: false,
    media_live_embeds: true,
    extended_valid_elements: "iframe[src|width|height|name|align], embed[width|height|name|flashvars|src|bgcolor|align|play|loop|quality|allowscriptaccess|type|pluginspage]",
    external_filemanager_path:"<?= base_url() ?>/plugins/filemanager/",
    filemanager_title:"Responsive Filemanager" ,
    external_plugins: { "filemanager" : "<?= base_url() ?>/plugins/filemanager/plugin.min.js"}
    });
</script> 