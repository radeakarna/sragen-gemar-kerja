
    <div class="container-fluid px-md-5"> 
        <h2 class="content-heading"><?= @$page_title?></h2>
        <!-- id="form_ubah" -->
        
        <form method="post" id="form_ubah" action="<?= base_url($uri_segment.'update') ?>" enctype="multipart/form-data">
        <input type="hidden" name="<?= $this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>">
        <div class="row">
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-content">
                        <div class="block">
                            <div class="block-header bg-info" style="background-color: white !important;">
                                <h3 class="block-title"> <small class="text-black">Kategori Berita</small></h3>
                                <div class="float-right">
                                    <button type="button" id="tambah_kategori_berita" class="btn btn-link">+ tambah kategori</button>
                                </div>
                            </div>
                            <div class="block-content">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="first-name-vertical">Kategori*</label>
                                            <select class="custom-select" id="kategori_id" name="kategori_id" required>
                                            </select>
                                            <span id="kategori_error"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="block-content block-content-full block-content-sm bg-body-light font-size-sm">
                                <span class="text-danger">*Silahkan pilih kategori berita</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-content">
                        <div class="block">
                            <div class="block-header bg-info" style="background-color: white !important;">
                                <h3 class="block-title"> <small class="text-black">Thumbnile</small></h3>
                            </div>
                            <div class="block-content">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="first-name-vertical">Kover*</label>
                                            <div class="custom-file">
                                                <input type="file" id="kover" class="custom-file-input" name="file" onchange="readURL(this);"/>
                                                <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                            </div>
                                            <span id="file_error"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="block-content block-content-full block-content-sm bg-body-light font-size-sm">
                                <img id="tampil_kover" class="rounded mx-auto d-block" src="<?= base_url('assets/img/no-camera.png') ?>" alt="your image" width="100%" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8">
                <!-- Dynamic Table Full -->
                <div class="block custom">
                    <div class="block-header block-header-default" style="background-color: white !important;">
                        <h3 class="block-title"> <small><?= @$detail_page_title; ?></small></h3>
                        <div class="float-right">
                            <!-- iki disik isine tombol tambah -->
                            <?php echo $btn_kembali ?>

                        </div>
                    </div>
                    <div class="block-content block-content-full"> 
                        <div class="row" id="basic-table">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-content">
                                        <div class="col-12" hidden>
                                            <div class="form-group">
                                                <label>ID*</label>
                                                <input type="text" id="id" class="form-control" name="id" required>
                                                <span id="id_error"></span>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label>Judul*</label>
                                                <input type="text" id="judul" class="form-control" name="judul">
                                                <span id="judul_error"></span>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="email-id-vertical">Konten*</label>
                                                <textarea id="konten" class="form-control" name="konten" rows="6" required><?= $berita->konten ?></textarea>
                                                <span id="konten_error"></span>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="password2">Status* </label>
                                                <ul class="list-unstyled mb-0">
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="status" id="radio1" value="1" checked>
                                                                <label for="radio1">Tampikan</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="status" id="radio2" value="0">
                                                                <label for="radio2">Sembunyikan</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                </ul>
                                                <span id="status_error"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full block-content-sm bg-body-light font-size-sm">
                        <button type="submit" id="buttom_simpan" class="btn btn-primary">Simpan Data</button>
                    </div>
                </div> 
            </div>
        </div>
        </form> 
        
    </div>
</main>