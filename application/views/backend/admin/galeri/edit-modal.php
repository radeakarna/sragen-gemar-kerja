<div id="edit_model" class="modal" id="modal-normal" tabindex="-1" role="dialog" aria-labelledby="modal-small" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary">
                    <h3 class="block-title">Modal Ubah</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
                    </div>
                </div>
                <form id="edit_form">
                    <div class="block-content">

                        <div class="form-group"> 
                            <div class="row">

                                <div class="col-sm-12" hidden>
                                    <label>Id</label>
                                    <input type="text" id="edit_id" class="form-control" name="id" > 
                                </div> 
                                
                                <div class="col-sm-12">
                                    <label>Kategori*</label>
                                    <select name="kategori_id" id="edit_kategori_id" class="form-control">
                                    </select>
                                </div>

                                <div class="col-sm-12">
                                    <label for="first-name-vertical">Kover</label>
                                    <div class="custom-file">
                                        <input type="file" id="edit_file" class="custom-file-input" name="file" onchange="editReadURL(this);" />
                                        <label class="custom-file-label" for="inputGroupFile01">Pilih file</label>
                                    </div>
                                    <div id="show_edit_error_file"></div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group"> 
                                        <img id="edit_show_image" class="rounded mx-auto d-block" src="<?= base_url('assets/img/no_image.png') ?>" alt="your image" width="40%" />
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <label>Keterangan*</label>
                                    <textarea id="edit_description" class="form-control" name="description" rows="3" required></textarea>
                                </div>

                                <div class="col-sm-12">
                                    <label for="password2">Status* </label>
                                    <ul class="list-unstyled mb-0">
                                        <li class="d-inline-block mr-2 mb-1">
                                            <fieldset>
                                                <div class="radio">
                                                    <input type="radio" name="status" id="customRadio1" value="1" checked>
                                                    <label for="radio1">Tampikan</label>
                                                </div>
                                            </fieldset>
                                        </li>
                                        <li class="d-inline-block mr-2 mb-1">
                                            <fieldset>
                                                <div class="radio">
                                                    <input type="radio" name="status" id="customRadio2" value="0">
                                                    <label for="radio2">Sembunyikan</label>
                                                </div>
                                            </fieldset>
                                        </li>
                                    </ul>
                                </div>

                            </div> 
                        </div> 
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary" id='edit_button'>Simpan</button>
                    </div>
                </form> 
            </div> 
        </div>
    </div>
</div>