<script type="text/javascript">
    let status = true;
    const base_url = '<?= base_url() ?>';

    $(document).ready(function(){
        load_table();
    }); 
  // Add the following code if you want the name of the file appear on select
    $(".custom-file-input").on("change", function() {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#add_show_image').attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    function editReadURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#edit_show_image').attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }


    function load_table(){ 
        $('#table-data').dataTable({
            destroy: true,
            processing: true,
            serverSide: true,
            ajax: {
                url: $('#table-data').data('url'),
                type: 'GET',
                dataType: 'JSON',
                data: function(d){
                    d[csrf.token_name] = csrf.hash;
                }
            },
            drawCallback: function(res) {
                csrf.token_name = res.json.csrf.token_name;
                csrf.hash = res.json.csrf.hash;
            },
            language: {
                processing: '<div class="spinner-border""></div>'
            },
            order: [],
            columnDefs: [{
                    targets: [0, 5],
                    orderable: false
                },
                {
                    targets: [0, 5],
                    className: 'text-center'
                }

            ],
            scrollX: true
        });
         
    }

    // function cek_required_tambah() {
    //     status = true;
    //     $('#add_form .required').each(function(){
    //         if( $(this).val() == "" ){
    //           toastr.error( 'Ada inputan yang belum terisi', 'Gagal', { timeOut: 2000, fadeOut: 2000 });
    //           status = false;
    //         }
    //     });
    //     return status;
    // }

    // function cek_required_ubah() {
    //     status = true;
    //     $('#form_ubah .required').each(function(){
    //         if( $(this).val() == "" ){
    //           toastr.error( 'Ada inputan yang belum terisi', 'Gagal', { timeOut: 2000, fadeOut: 2000 });
    //           status = false;
    //         }
    //     });
    //     return status;
    // }

    $('#add_form').submit(function(e) {
        e.preventDefault();

        let data = new FormData(this);
        var url  = "<?= base_url($uri_segment.'store') ?>";
        data.append(csrf.token_name, csrf.hash); 

        $.ajax({
            url         : url,
            type        : "POST",
            data        : data,
            dataType    : "JSON",
            processData : false,
            contentType : false,
            cache       : false,
            async       : false,

            beforeSend:function(){
                $('#add_button').attr("disabled", true);
                $('#add_button').text("proses...");
            },
            success: function(data) {
                $('#add_button').attr("disabled", false);
                if (data.status == true) {
                    load_table(); 
                    $('#modal_tambah').modal('hide');
                    // $('#add_name').val("");
                    // $('#add_description').html("");
                    $('#add_button').text("Simpan"); 
                    toastr.success( 'Data berhasil ditambahkan', 'Berhasil', { timeOut: 2000, fadeOut: 2000 });
                    location.reload();
                } else { 
                    $('#show_add_error_file').html(data.file);
                    $('#add_button').text("Simpan");
                    toastr.error( 'Periksa Inputan Anda', { timeOut: 2000, fadeOut: 2000 });
                }                  

                csrf.token_name = data.csrf.token_name;
                csrf.hash = data.csrf.hash; 
                
            },
            error: function () {
                toastr.error( 'Periksa Inputan Anda', { timeOut: 2000, fadeOut: 2000 });
                location.reload();
            }
        });
    });

    $('#edit_form').submit(function(e) {
        e.preventDefault();

        let data = new FormData(this);
        var url  = "<?= base_url($uri_segment.'store') ?>";
        data.append(csrf.token_name, csrf.hash); 

        $.ajax({
            url         : url,
            type        : "POST",
            data        : data,
            dataType    : "JSON",
            processData : false,
            contentType : false,
            cache       : false,
            async       : false,
            beforeSend:function(){
                $('#edit_button').attr("disabled", true);
                $('#edit_button').text("proses...");
            },
            success: function(data) {
                $('#edit_button').attr("disabled", false);
                $('#edit_button').text("Simpan");
                if (data.status == true) {
                    load_table(); 
                    toastr.success( 'Data berhasil diubah', 'Berhasil', { timeOut: 2000, fadeOut: 2000 });
                    $('#edit_model').modal('hide');
                } else { 
                    toastr.error( 'Periksa Inputan Anda', { timeOut: 2000, fadeOut: 2000 });
                    $('#show_edit_error_file').html(data.file);
                    // location.reload();
                }                  

                csrf.token_name = data.csrf.token_name;
                csrf.hash = data.csrf.hash; 
                
            },
            error: function () {
                toastr.error( 'Periksa Inputan Anda', { timeOut: 2000, fadeOut: 2000 });
                location.reload();
            }
        });
    });

    $('#add_button').click(function(){
        $('#add_modal').modal('show');
    });

    $('#table-data').on('click','.edit_button',function(){
        let id = $(this).attr('data'); 
        $.ajax({
            url: "<?= base_url($uri_segment.'detail') ?>",
            type: 'POST',
            dataType: 'JSON',
            data: {
                id:id,
                [csrf.token_name]: csrf.hash
            },
            success: function(respon){
                if (respon.status == true) {
                    $('#edit_id').val(respon.data.id);
                    $('#edit_description').html(respon.data.deskripsi); 
                    $('#edit_kategori_id').val(respon.data.album_id);
                    $('#edit_show_image').attr('src', base_url+respon.data.thumbnile);
                    if(respon.data.is_active == '0'){
                        $('#customRadio2').attr('checked', true);
                    }else{
                        $('#customRadio1').attr('checked', true);
                    }
                    $('#edit_model').modal('show');
                } else {
                    toastr.error( 'Koneksi Bermasalah', { timeOut: 2000, fadeOut: 2000 }); 
                }
                csrf.token_name = respon.csrf.token_name;
                csrf.hash = respon.csrf.hash;
                
            },
            error: function () {
                toastr.error( 'Periksa Inputan Anda', { timeOut: 2000, fadeOut: 2000 });
                // location.reload();
            }
        }); 
    });

    // // Menampilkan model 

    $('#table-data').on('click','.delete_button',function(){
        var id = $(this).attr('data');
        Swal.fire({
          title: 'Apakah Anda Yakin',
          text: "Anda akan menghapus data ini",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Ya, hapus',
          cancelButtonText: 'Tidak',
          confirmButtonClass: 'btn btn-warning',
          cancelButtonClass: 'btn btn-danger ml-1',
          buttonsStyling: false,
        }).then(function (result) {

          if (result.value) {
            $.ajax({
                url: "<?= base_url($uri_segment.'delete/') ?>"+id,
                type: "POST",
                data: 
                    {
                        [csrf.token_name]: csrf.hash
                    }
                ,
                dataType : "JSON", 
                success: function(data) { 
                    toastr.success('Data berhasil dihapus');
                    load_table();
                    $('#table-data').DataTable().ajax.reload();

                    csrf.token_name = data.csrf.token_name;
                    csrf.hash = data.csrf.hash;              
                },
            });
            
          }
        });
        
    }); 

    $(document).ready(function(){

        let url  = "<?= base_url('backend/admin/album/show') ?>";
        let get  = $.get(url);
        get.done(function(respon){
            let html = "<option value=''></option>";             
            for (let i = 0; i < respon.length; i++) {
                html += "<option value='"+respon[i].id+"'>"+respon[i].nama+"</option>";
            }
            $("#add_kategori_id").html(html);
            $("#edit_kategori_id").html(html);
        },);
        get.fail(function(respon){
            toastr.error('Gagal!, Terjadi kesalahan.');
        },);
    });
</script>