<div id="modal_ubah" class="modal" id="modal-normal" tabindex="-1" role="dialog" aria-labelledby="modal-small" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary">
                    <h3 class="block-title">Modal Ubah</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
                    </div>
                </div>
                <form id="form_ubah" method="POST" action="<?= base_url($uri_segment.'update') ?>">
                    <div class="block-content">

                        <div class="form-group" hidden>
                            <label for="id">Id*</label>
                            <input name="id" type="text" id="ubah_id" class="form-control" required>
                            <span id="edit_id_error"></span>
                        </div>
                        <div class="form-group">
                            <label for="name">Nama*</label>
                            <input name="nama" type="text" id="edit_nama" class="form-control" required>
                            <span id="edit_name_error"></span>
                        </div>
                        <div class="form-group" hidden>
                            <label>Jenis* </label>
                            <select id="edit_jenis" class="form-control" name="jenis">
                                <option value="halaman">Halaman</option>
                                <option value="berita">Berita</option>
                            </select>
                            <span id="edit_jenis_error"></span>
                        </div>
                        <div class="form-group">
                            <label>Deskripsi* </label>
                            <textarea id="edit_deskripsi" class="form-control" name="deskripsi" rows="5" required></textarea>
                            <span id="edit_deskripsi_error"></span>
                        </div>
                        <div class="form-group">
                            <label>Status* </label>
                            <ul class="list-unstyled mb-0">
                                <li class="d-inline-block mr-2 mb-1">
                                    <fieldset>
                                        <div class="custom-control custom-radio">
                                            <input type="radio" class="custom-control-input" name="status" id="customRadio1" value="1">
                                            <label class="custom-control-label" for="customRadio1">Tampikan</label>
                                        </div>
                                    </fieldset>
                                </li>
                                <li class="d-inline-block mr-2 mb-1">
                                    <fieldset>
                                        <div class="custom-control custom-radio">
                                            <input type="radio" class="custom-control-input" name="status" id="customRadio2" value="0">
                                            <label class="custom-control-label" for="customRadio2">Sembunyikan</label>
                                        </div>
                                    </fieldset>
                                </li>
                            </ul>
                            <span id="edit_status_error"></span>
                        </div>

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary" id='buttom_ubah'>Simpan Data</button>
                    </div>
                </form> 
            </div> 
        </div>
    </div>
</div>