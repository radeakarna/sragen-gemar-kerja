    <div class="container-fluid px-md-5"> 
        <h2 class="content-heading"><?= @$page_title?></h2>

        <!-- Dynamic Table Full -->
        <div class="block custom">
            <div class="block-header block-header-default">
                <h3 class="block-title"> <small><?= @$detail_page_title; ?></small></h3>
                <div class="float-right">
                    <!-- iki disik isine tombol tambah -->
                    <?php echo $btn_kembali ?>
                </div>
            </div>
            <div class="block-content block-content-full"> 
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-content">
                                <div class="modal-body">
                                  <form id="form_profile" method="post"> 
                                    <div class="col-8 offset-md-2" hidden>
                                        <label>ID</label>
                                        <input type="text" id="id" class="form-control" name="id"> 
                                    </div>
                                    <div class="col-8 offset-md-2" hidden>
                                        <label>thumbnail</label>
                                        <input type="text" id="thumbnail_lama" class="form-control" name="thumbnail_lama"> 
                                    </div>
                                    <div class="col-8 offset-md-2">
                                        <label>Judul </label>
                                        <input type="text" id="judul" class="form-control" name="judul"> 
                                    </div>
                                    <div class="col-8 offset-md-2">
                                        <br><div id="gambar"></div><br>
                                        <label>Thumbnail </label>
                                        <small>Tidak perlu diisi jika tidak diganti</small>
                                        <input type="file" id="thumbnail" class="form-control" name="thumbnail" onchange="return fileValidation()"> 
                                        <small>File max : 3Mb</small>
                                    </div>
                                    <div class="col-8 offset-md-2">
                                        <label>Link Youtube</label>
                                        <input type="text" id="yt" class="form-control" name="yt"> 
                                    </div>
                                    <div class="col-8 offset-md-2">
                                        <div class="form-group">
                                            <label>Deskripsi </label>
                                            <textarea name="deskripsi" id="deskripsi" class="form-control js-summernote" rows="6"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <button type="submit" name="submit" id="buttom_simpan" class="btn btn-primary">Simpan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </div>
</main>