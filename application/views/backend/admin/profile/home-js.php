<script type="text/javascript">
    $(document).ready(function(){
        get_data();

    });

    function fileValidation() {
        var fileInput = document.getElementById('thumbnail'); 

        var filePath = fileInput.value; 
        var allowedExtensions = /(\.jpg|\.jpeg|\.png)$/i; 
        if (!allowedExtensions.exec(filePath)) { 
         toastr.error( 'File harus berupa gambar!', 'Gagal', { timeOut: 2000, fadeOut: 2000 });
         fileInput.value = ''; 
         return false; 
     } else {
        var file_size = $('#thumbnail')[0].files[0].size; 
        var filePath = $()

        if(file_size>3000000) {
            toastr.error( 'Ukuran file terlalu besar!', 'Gagal', { timeOut: 2000, fadeOut: 2000 });
            return false;
        } 
        return true;
    }  
}

const get_data = () => {

    var id = ""; 
    let data = { id:id };
    var uri = "<?= base_url('uploads/images/profile/')?>"
    var url = "<?= base_url($uri_segment.'get_one')?>";
    let get = $.get(url, data);
    get.done(function(res){
        if (res.status == true) {
            var img = res.data.thumbnail

            $('#id').val(res.data.id)
            $('#judul').val(res.data.judul)
            $('#thumbnail_lama').val(res.data.thumbnail) 
            $("#gambar").html('<a href="' + uri + img + '" target="_blank"> <img src="' + uri + img + '" width="50%"> </a>')

            $('textarea#deskripsi').summernote('code', res.data.deskripsi);
            $('#yt').val(res.data.yt)
        } else {
            toastr.error( 'Koneksi Bermasalah', { timeOut: 2000, fadeOut: 2000 }); 
        }
    })
}

$('#form_profile').submit(function(e) {
    e.preventDefault();

    let data = new FormData(this);
    var url  = "<?= base_url($uri_segment.'store')?>"
    data.append(csrf.token_name, csrf.hash); 

    $('#buttom_simpan').text("proses...");
    $('#buttom_simpan').attr("disabled", true);

    let post = $.ajax({
        url: url,
        type: 'post',
        data        : data,
        dataType    : 'json',
        processData : false,
        contentType : false,
        cache       : false,
        async       : false,
    });
    post.done(function(respon) {
        if (respon.status == true) {
            get_data();
            $('#buttom_simpan').text("Simpan"); 
            toastr.success( 'Data berhasil disimpan', 'Berhasil', { timeOut: 2000, fadeOut: 2000 });
            setTimeout(() => window.location.reload(), 2500);
        } 
        csrf.token_name = respon.csrf.token_name;
        csrf.hash = respon.csrf.hash; 
    });
    post.fail(function(respon) {
       toastr.error( 'Ada inputan yang belum terisi', 'Gagal', { timeOut: 2000, fadeOut: 2000 });
       location.reload();
   });
});

jQuery(function () {
        // Init page helpers (Summernote + CKEditor + SimpleMDE plugins)
        Codebase.helpers(['summernote']);
    }); 



</script>