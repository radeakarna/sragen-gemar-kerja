<script type="text/javascript">
    let status = true;
    $(document).ready(function(){
        load_table();
    }); 

    $('#tombol_import').click(function(){ 
        $('#modal_import').modal('show');
    });

    function load_table(){
        let url = $('#table-data').data('url');
        $('#table-data').dataTable({
            destroy: true,
            processing: true,
            serverSide: true,
            ajax: {
                url: url,
                type: 'GET',
                dataType: 'JSON',
                data: function(d){
                    d[csrf.token_name] = csrf.hash;
                }
            },
            drawCallback: function(res) {
                csrf.token_name = res.json.csrf.token_name;
                csrf.hash = res.json.csrf.hash;
            },
            language: {
                processing: '<div class="spinner-border""></div>'
            },
            order: [],
            columnDefs: [{
                    targets: [0, 5],
                    orderable: false
                },
                {
                    targets: [0, 5],
                    className: 'text-center'
                }

            ],
            scrollX: true
        });
         
    }

    $('#form_tambah').submit(function(e) {
        e.preventDefault();

        let data = new FormData(this);
        var url  = $(this).attr('action');
        data.append(csrf.token_name, csrf.hash); 

        $('#buttom_tambah').text("proses...");
        $('#buttom_tambah').attr("disabled", true);
        let post  = $.ajax({
            url         : url,
            type        : 'POST',
            data        : data,
            dataType    : 'json',
            processData : false,
            contentType : false,
            cache       : false,
            async       : false,
        });
        post.done(function(respon){
            if (respon.status == true) {
                $('#modal_tambah').modal('hide');
                load_table();
                $('#buttom_tambah').text("Simpan"); 
                toastr.success( 'Data berhasil ditambahkan', 'Berhasil', { timeOut: 2000, fadeOut: 2000 });
                setTimeout(() => window.location.reload(), 2500);
            } else { 
                $('#buttom_tambah').text("Simpan");
                toastr.error( 'Periksa Inputan Anda, nama tidak boleh sama', { timeOut: 2000, fadeOut: 2000 });
            }                  

            csrf.token_name = respon.csrf.token_name;
            csrf.hash = respon.csrf.hash; 
        });
        post.fail(function(respon){
            toastr.error( 'Ada inputan yang belum terisi', 'Gagal', { timeOut: 2000, fadeOut: 2000 });
            location.reload();
        });
        post.always(function () {
            $('#buttom_tambah').html('Simpan Data');
        });
    });

    $('#form_ubah').submit(function(e) {
        e.preventDefault();

        let data = new FormData(this);
        var url  = $(this).attr('action');
        data.append(csrf.token_name, csrf.hash); 

        $('#button_simpan').html('proses..');
        let post  = $.ajax({
            url         : url,
            type        : 'POST',
            data        : data,
            dataType    : 'json',
            processData : false,
            contentType : false,
            cache       : false,
            async       : false,
        });
        post.done(function(respon){
            csrf.token_name = respon.csrf.token_name;
            csrf.hash = respon.csrf.hash;
            if(respon.status == true){
                $('#modal_ubah').modal('hide');
                toastr.success( 'Data berhasil ditambahkan', 'Berhasil', { timeOut: 2000, fadeOut: 2000 });
                setTimeout(() => window.location.reload(), 2500);
            }else{            
                toastr.error( 'Periksa kembali inputan nda', 'Gagal', { timeOut: 2000, fadeOut: 2000 });
            }
        });
        post.fail(function(respon){
            toastr.error( 'Ada inputan yang belum terisi', 'Gagal', { timeOut: 2000, fadeOut: 2000 });
            location.reload();
        });
        post.always(function () {
            $('#button_simpan').html('Simpan Data');
        });

    });

    $('#tombol_tambah').click(function(){
        $('#modal_tambah').modal('show');
    });

    $('#show_data').on('click','.tombol_ubah',function(){
        var id = $(this).attr('data');

        let data = { id:id };
        var url  = "<?= base_url($uri_segment.'detail') ?>";
        let get  = $.get(url, data);
        get.done(function(respon){
            if (respon.status == true) {
                $('#ubah_id').val(respon.data.id);
                $('#edit_nama').val(respon.data.nama);
                $('#edit_jenis').val(respon.data.jenis);
                $('#edit_deskripsi').html(respon.data.deskripsi);
                $('input:radio[name="status"]').filter('[value="'+respon.data.status+'"]').attr('checked', true);

                $('#modal_ubah').modal('show');
            } else {
                toastr.error( 'Koneksi Bermasalah', { timeOut: 2000, fadeOut: 2000 }); 
            }
        });
        get.fail(function(respon){
            toastr.error( 'Periksa koneksi internet', 'Gagal', { timeOut: 2000, fadeOut: 2000 });
            location.reload();
        });

    });

    // Menampilkan model 
    $('#show_data').on('click','.tombol_hapus',function(){
        let id = $(this).attr('data');
        Swal.fire({
          title: 'Apakah Anda Yakin',
          text: "Anda akan menghapus data ini",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Ya, hapus',
          cancelButtonText: 'Tidak',
          confirmButtonClass: 'btn btn-warning',
          cancelButtonClass: 'btn btn-danger ml-1',
          buttonsStyling: false,
        }).then(function (result) {

          if (result.value) {
            let data = {[csrf.token_name]: csrf.hash};
            let url  = "<?= base_url($uri_segment.'delete/') ?>"+id;
            let post  = $.post(url, data);
            post.done(function(respon){
                toastr.success('Data berhasil dihapus');
                load_table();

                csrf.token_name = respon.csrf.token_name;
                csrf.hash = respon.csrf.hash;              
            });
            post.fail(function(respon){
                toastr.error( 'Periksa koneksi internet', 'Gagal', { timeOut: 2000, fadeOut: 2000 });
            });
          }
          
        });
        
    }); 

    // Menampilkan model 
    $('#show_data').on('change','.tombol_ubah_status',function(){
        let id      = $(this).data('id');
        let status  = $(this).data('status');
        Swal.fire({
          title: 'Apakah Anda Yakin',
          text: "Anda Mengubah Status Data",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Ya, Ubah',
          cancelButtonText: 'Tidak',
          confirmButtonClass: 'btn btn-warning',
          cancelButtonClass: 'btn btn-danger ml-1',
          buttonsStyling: false,
        }).then(function (result) {

          if (result.value) {
            let data = {[csrf.token_name]: csrf.hash, status:status};
            let url  = "<?= base_url($uri_segment.'ubahStatus/') ?>"+id;
            let post  = $.post(url, data);
            post.done(function(respon){
                toastr.success('Status berhasil diubah');

                csrf.token_name = respon.csrf.token_name;
                csrf.hash = respon.csrf.hash;              
            });
            post.fail(function(respon){
                toastr.error( 'Periksa koneksi internet', 'Gagal', { timeOut: 2000, fadeOut: 2000 });
            });
          }
          load_table();
        });
        
    });
</script>