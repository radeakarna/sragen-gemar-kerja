<script type="text/javascript">
    let status = true;
    const base_url = '<?= base_url() ?>';

    // Add the following code if you want the name of the file appear on select
    $(".custom-file-input").on("change", function() {
        let fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });

    $(document).ready(function(){
        detail($('#id').val());
    });

    $('#form_ubah').submit(function(e) {
        e.preventDefault();

        let data = new FormData(this);
        let url  = $(this).attr('action');
        data.append(csrf.token_name, csrf.hash); 

        $('#buttom_simpan').text("proses...");
        $('#buttom_simpan').attr("disabled", true);

        let post  = $.ajax({
            url         : url,
            type        : 'POST',
            data        : data,
            dataType    : 'json',
            processData : false,
            contentType : false,
            cache       : false,
            async       : false,
        });
        post.done(function(respon){
            $('#buttom_simpan').text("Simpan Data");
            $('#buttom_simpan').attr("disabled", false);
            
            if (respon.status == true) {
                toastr.success( 'Data berhasil ditambahkan', 'Berhasil', { timeOut: 2000, fadeOut: 2000 });
                setTimeout(() => window.location.href = base_url+'backend/admin/manajemen_web', 2500);
            } else {
                $('#logo_dark_error').html(respon.logo_dark);
                $('#logo_white_error').html(respon.logo_white);
                $('#logo_footer_error').html(respon.logo_footer);
                $('#judul_error').html(respon.judul);
                $('#deskripsi_error').html(respon.deskripsi);
                toastr.error( 'Periksa Inputan Anda', { timeOut: 2000, fadeOut: 2000 });
            }                  

            csrf.token_name = respon.csrf.token_name;
            csrf.hash = respon.csrf.hash; 
        });
        post.fail(function(respon){
            toastr.error( 'Ada inputan yang belum terisi', 'Gagal', { timeOut: 2000, fadeOut: 2000 });
            // location.reload();
        });
        post.always(function () {
            $('#buttom_simpan').html('Simpan Data');
        });
    });

    function readURLDark(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#tampil_kover_dark').attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    function readURLWhite(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#tampil_kover_white').attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    function readURLFooter(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#tampil_kover_footer').attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    function detail(id){
        let url  = "<?= base_url($uri_segment.'detail') ?>";
        let data = {id : id};
        let get  = $.get(url, data);
        get.done(function(respon){
            console.log(respon)
            $('#id').val(id)
            $('#judul').val(respon.data.judul)
            $('#deskripsi').val(respon.data.deskripsi)
            $('#judul_pendek').val(respon.data.judul_pendek)
            $('#facebook').val(respon.data.facebook)
            $('#twitter').val(respon.data.twitter)
            $('#instagram').val(respon.data.instagram)
        
            $('#alamat').val(respon.data.alamat)
            $('#no_hp').val(respon.data.no_hp)
            $('#copyright').val(respon.data.copyright)
            if(respon.data.logo_dark){
                $('#tampil_kover_dark').attr('src', base_url+respon.data.logo_dark)
            }

            if(respon.data.logo_white){
                $('#tampil_kover_white').attr('src', base_url+respon.data.logo_white)
            }

            if(respon.data.logo_footer){
                $('#tampil_kover_footer').attr('src', base_url+respon.data.logo_footer)
            }
        });
        get.fail(function(respon){
            toastr.error( 'Periksa koneksi internet', 'Gagal', { timeOut: 2000, fadeOut: 2000 });
            // location.reload();
        });
    }
</script>