    <div class="container-fluid px-md-5">
        <h2 class="content-heading"><?= @$page_title ?></h2>

        <form id="form_ubah" method="post" action="<?= base_url($uri_segment . 'update') ?>">
            <div class="row">
                <div class="col-lg-12">
                    <!-- Dynamic Table Full -->
                    <div class="block custom">
                        <div class="block-header block-header-default" style="background-color: white !important;">
                            <h3 class="block-title"> <small><?= @$detail_page_title; ?></small></h3>
                            <div class="float-right">
                                <!-- iki disik isine tombol tambah -->
                                <?php echo $btn_kembali ?>
                            </div>
                        </div>
                        <div class="block-content block-content-full">
                            <div class="row" id="basic-table">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-content">
                                            <div class="col-12" hidden>
                                                <div class="form-group">
                                                    <label>ID*</label>
                                                    <input type="text" id="id" class="form-control" name="id" value="<?php echo $id ?>" required>
                                                    <span id="id_error"></span>
                                                </div>
                                            </div>


                                            <div class="col-12">
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <label>Judul</label>
                                                            <input type="text" id="judul" class="form-control" name="judul">
                                                            <span id="judul_error"></span>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Judul Pendek</label>
                                                            <input type="text" id="judul_pendek" class="form-control" name="judul_pendek">
                                                            <span id="judul_pendek_error"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <label for="email-id-vertical">Deskripsi</label>
                                                            <textarea id="deskripsi" class="form-control js-summernote" name="deskripsi" rows="9" required></textarea>
                                                            <span id="konten_error"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Sosial Media</label>
                                                            <hr>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <button type="button" class="btn btn-secondary">
                                                                        <i class="fa fa-facebook"></i>
                                                                    </button>
                                                                </div>
                                                                <input type="text" class="form-control" id="facebook" name="facebook" placeholder="facebook">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <button type="button" class="btn btn-secondary">
                                                                        <i class="fa fa-twitter"></i>
                                                                    </button>
                                                                </div>
                                                                <input type="text" class="form-control" id="twitter" name="twitter" placeholder="twitter">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <button type="button" class="btn btn-secondary">
                                                                        <i class="fa fa-instagram"></i>
                                                                    </button>
                                                                </div>
                                                                <input type="text" class="form-control" id="instagram" name="instagram" placeholder="instagram">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Logo Dark</label>
                                                            <div class="custom-file">
                                                                <input type="file" id="kover" class="custom-file-input" name="logo_dark" onchange="readURLDark(this);" />
                                                                <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                                            </div>
                                                            <span id="file_error"></span>
                                                        </div>
                                                        <img id="tampil_kover_dark" class="tampil_kover rounded mx-auto d-block bg-body-light" src="<?= base_url('assets/Illustrasi1.png') ?>" alt="your image" width="80%" />
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Logo White</label>
                                                            <div class="custom-file">
                                                                <input type="file" id="kover" class="custom-file-input" name="logo_white" onchange="readURLWhite(this);" />
                                                                <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                                            </div>
                                                            <span id="file_error"></span>
                                                        </div>
                                                        <img id="tampil_kover_white" class="tampil_kover rounded mx-auto d-block bg-body-light" src="<?= base_url('assets/Illustrasi1.png') ?>" alt="your image" width="80%" />
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Logo Footer</label>
                                                            <div class="custom-file">
                                                                <input type="file" id="kover" class="custom-file-input" name="logo_footer" onchange="readURLFooter(this);" />
                                                                <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                                            </div>
                                                            <span id="file_error"></span>
                                                        </div>
                                                        <img id="tampil_kover_footer" class="tampil_kover rounded mx-auto d-block bg-body-light" src="<?= base_url('assets/Illustrasi1.png') ?>" alt="your image" width="80%" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12"><br>
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <label for="email-id-vertical">Alamat</label>
                                                            <textarea id="alamat" class="form-control js-summernote" name="alamat" rows="5" required></textarea>
                                                            <span id="alamat_error"></span>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Nomor hp</label>
                                                            <input type="text" id="no_hp" class="form-control" name="no_hp">
                                                            <span id="no_hp_error"></span>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Copyright</label>
                                                            <input type="text" id="copyright" class="form-control" name="copyright">
                                                            <span id="copyright_error"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="block-content block-content-full block-content-sm bg-body-light font-size-sm">
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="submit" id="buttom_simpan" class="btn btn-primary float-right">Simpan Data</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>

    </div>
    </main>