<script type="text/javascript">
    let status = true;

    // Add the following code if you want the name of the file appear on select
    $(".custom-file-input").on("change", function() {
        let fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });

    $('#form_tambah').submit(function(e) {
        e.preventDefault();

        $('#buttom_tambah').text("proses...");
        $('#buttom_tambah').attr("disabled", true);

        let data = new FormData(this);
        var url  = $(this).attr('action');
        data.append(csrf.token_name, csrf.hash); 
        
        let post  = $.ajax({
            url         : url,
            type        : 'POST',
            data        : data,
            dataType    : 'json',
            processData : false,
            contentType : false,
            cache       : false,
            async       : false,
        });
        post.done(function(respon){
            if (respon.status == true) {
                $('#buttom_tambah').text("Simpan Data"); 
                toastr.success( 'Data berhasil ditambahkan', 'Berhasil', { timeOut: 2000, fadeOut: 2000 });
                setTimeout(() => window.location.reload(), 2500);
            } else { 
                $('#buttom_tambah').text("Simpan Data");
                $('#logo_dark_error').html(respon.logo_dark);
                $('#logo_white_error').html(respon.logo_white);
                $('#logo_footer_error').html(respon.logo_footer);
                $('#judul_error').html(respon.judul);
                $('#konten_error').html(respon.konten);
                $('#status_error').html(respon.status);
                $('#buttom_tambah').attr("disabled", false);
                toastr.error( 'Periksa Inputan Anda', { timeOut: 2000, fadeOut: 2000 });
            }                  

            csrf.token_name = respon.csrf.token_name;
            csrf.hash = respon.csrf.hash; 
        });
        post.fail(function(respon){
            toastr.error( 'Ada inputan yang belum terisi', 'Gagal', { timeOut: 2000, fadeOut: 2000 });
            // location.reload();
        });
        post.always(function () {
            $('#buttom_tambah').html('Simpan Data');
        });
    });

    function readURLDark(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#tampil_kover_dark').attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    function readURLWhite(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#tampil_kover_white').attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    function readURLFooter(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#tampil_kover_footer').attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    jQuery(function () {
        // Init page helpers (Summernote + CKEditor + SimpleMDE plugins)
        Codebase.helpers(['summernote']);
    });
</script>