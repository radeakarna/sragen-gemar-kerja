    <div class="container-fluid px-md-5"> 
        <h2 class="content-heading"><?= @$page_title?></h2>

        <form id="form_tambah" method="post" action="<?= base_url($uri_segment.'store') ?>">
        <div class="row">
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-content">
                        <div class="block">
                            <div class="block-header bg-info" style="background-color: white !important;">
                                <h3 class="block-title"> <small class="text-black">Logo Dark</small></h3>
                            </div>
                            <div class="block-content">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="first-name-vertical">Kover*</label>
                                            <div class="custom-file">
                                                <input type="file" id="kover" class="custom-file-input" name="logo_dark" onchange="readURLDark(this);" required/>
                                                <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                            </div>
                                            <span id="file_error"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="block-content block-content-full block-content-sm bg-body-light font-size-sm">
                                <img id="tampil_kover_dark" class="tampil_kover rounded mx-auto d-block" src="<?= base_url('assets/img/no-camera.png') ?>" alt="your image" width="100%" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <div class="block">
                            <div class="block-header bg-info" style="background-color: white !important;">
                                <h3 class="block-title"> <small class="text-black">Logo White</small></h3>
                            </div>
                            <div class="block-content">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="first-name-vertical">Kover*</label>
                                            <div class="custom-file">
                                                <input type="file" id="kover" class="custom-file-input" name="logo_white" onchange="readURLWhite(this);" required/>
                                                <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                            </div>
                                            <span id="file_error"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="block-content block-content-full block-content-sm bg-body-light font-size-sm">
                                <img id="tampil_kover_white" class="tampil_kover rounded mx-auto d-block" src="<?= base_url('assets/img/no-camera.png') ?>" alt="your image" width="100%" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <div class="block">
                            <div class="block-header bg-info" style="background-color: white !important;">
                                <h3 class="block-title"> <small class="text-black">Logo Footer</small></h3>
                            </div>
                            <div class="block-content">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="first-name-vertical">Kover*</label>
                                            <div class="custom-file">
                                                <input type="file" id="kover" class="custom-file-input" name="logo_footer" onchange="readURLFooter(this);" required/>
                                                <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                            </div>
                                            <span id="file_error"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="block-content block-content-full block-content-sm bg-body-light font-size-sm">
                                <img id="tampil_kover_footer" class="tampil_kover rounded mx-auto d-block" src="<?= base_url('assets/img/no-camera.png') ?>" alt="your image" width="100%" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8">
                <!-- Dynamic Table Full -->
                <div class="block custom">
                    <div class="block-header block-header-default" style="background-color: white;">
                        <h3 class="block-title"> <small><?= @$detail_page_title; ?></small></h3>
                        <div class="float-right">
                            <!-- iki disik isine tombol tambah -->
                            <?php echo $btn_kembali ?>

                        </div>
                    </div>
                    <div class="block-content block-content-full"> 
                        <div class="row" id="basic-table">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-content">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label>Judul*</label>
                                                <input type="text" id="judul" class="form-control" name="judul" required/>
                                                <span id="judul_error"></span>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="email-id-vertical">Deskripsi*</label>
                                                <textarea id="deskripsi" class="form-control" name="deskripsi" rows="6"></textarea>
                                                <span id="deskripsi_error"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full block-content-sm bg-body-light font-size-sm">
                        <button type="submit" id="buttom_tambah" class="btn btn-primary">Simpan Data</button>
                    </div>
                </div> 
            </div>
        </div>
        </form> 
        
    </div>
</main>