<!-- Page Content -->
<div class="container-fluid px-md-5 py-md-5">
    <div class="row gutters-tiny invisible" data-toggle="appear">
        <!-- Row #1 -->
        <div class="col-6 col-xl-3">
            <a class="block block-link-shadow text-right" href="javascript:void(0)">
                <div class="block-content block-content-full clearfix">
                    <div class="float-left mt-10 d-none d-sm-block">
                        <i class="si si-bag fa-3x text-body-bg-dark"></i>
                    </div>
                    <div class="font-size-h3 font-w600" data-toggle="countTo" data-speed="1000" data-to=<?= $jumberita ?>>0</div>
                    <div class="font-size-sm font-w600 text-uppercase text-muted">Berita</div>
                </div>
            </a>
        </div>
        <div class="col-6 col-xl-3">
            <a class="block block-link-shadow text-right" href="javascript:void(0)">
                <div class="block-content block-content-full clearfix">
                    <div class="float-left mt-10 d-none d-sm-block">
                        <i class="si si-wallet fa-3x text-body-bg-dark"></i>
                    </div>
                    <div class="font-size-h3 font-w600"><span data-toggle="countTo" data-speed="1000" data-to=100>0</span></div>
                    <div class="font-size-sm font-w600 text-uppercase text-muted">Pengumuman</div>
                </div>
            </a>
        </div>
        <div class="col-6 col-xl-3">
            <a class="block block-link-shadow text-right" href="javascript:void(0)">
                <div class="block-content block-content-full clearfix">
                    <div class="float-left mt-10 d-none d-sm-block">
                        <i class="si si-envelope-open fa-3x text-body-bg-dark"></i>
                    </div>
                    <div class="font-size-h3 font-w600" data-toggle="countTo" data-speed="1000" data-to=<?= $jumgaleri ?>>0</div>
                    <div class="font-size-sm font-w600 text-uppercase text-muted">Galeri</div>
                </div>
            </a>
        </div>
        <div class="col-6 col-xl-3">
            <a class="block block-link-shadow text-right" href="javascript:void(0)">
                <div class="block-content block-content-full clearfix">
                    <div class="float-left mt-10 d-none d-sm-block">
                        <i class="si si-users fa-3x text-body-bg-dark"></i>
                    </div>
                    <div class="font-size-h3 font-w600" data-toggle="countTo" data-speed="1000" data-to=<?= $jumusers ?>>0</div>
                    <div class="font-size-sm font-w600 text-uppercase text-muted">User</div>
                </div>
            </a>
        </div>
        <!-- END Row #1 -->
    </div>
    <!-- <div class="row gutters-tiny invisible" data-toggle="appear">
        <div class="col-md-12">
            <div class="block">
                <div class="block-header">
                    <h3 class="block-title">
                        Admin <small>Panel</small>
                    </h3>
                </div>
                <div class="block-content block-content-full" style="background-image: url('<?= base_url('assets/backend/') ?>img/photos/photo34@2x.jpg');">
                    <div class="container">
                        <h2 class="h1 font-w700 text-white mb-5">Sistem Informasi Kemenpar AP</h1>
                            <h3 class="h5 text-white-op">Silahkan pilih menu pada bagian kiri</h3>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                    </div>
                </div>
            </div>
        </div>
    </div> -->


    <!-- <div class="row gutters-tiny invisible" data-toggle="appear"> -->
    <!-- Row #5 -->
    <!-- <div class="col-6 col-md-4 col-xl-2">
            <a class="block block-link-shadow text-center" href="be_pages_generic_inbox.html">
                <div class="block-content ribbon ribbon-bookmark ribbon-success ribbon-left">
                    <div class="ribbon-box">15</div>
                    <p class="mt-5">
                        <i class="si si-envelope-letter fa-3x"></i>
                    </p>
                    <p class="font-w600">Inbox</p>
                </div>
            </a>
        </div>
        <div class="col-6 col-md-4 col-xl-2">
            <a class="block block-link-shadow text-center" href="be_pages_generic_profile.html">
                <div class="block-content">
                    <p class="mt-5">
                        <i class="si si-user fa-3x"></i>
                    </p>
                    <p class="font-w600">Profile</p>
                </div>
            </a>
        </div>
        <div class="col-6 col-md-4 col-xl-2">
            <a class="block block-link-shadow text-center" href="be_pages_forum_categories.html">
                <div class="block-content ribbon ribbon-bookmark ribbon-primary ribbon-left">
                    <div class="ribbon-box">3</div>
                    <p class="mt-5">
                        <i class="si si-bubbles fa-3x"></i>
                    </p>
                    <p class="font-w600">Forum</p>
                </div>
            </a>
        </div>
        <div class="col-6 col-md-4 col-xl-2">
            <a class="block block-link-shadow text-center" href="be_pages_generic_search.html">
                <div class="block-content">
                    <p class="mt-5">
                        <i class="si si-magnifier fa-3x"></i>
                    </p>
                    <p class="font-w600">Search</p>
                </div>
            </a>
        </div>
        <div class="col-6 col-md-4 col-xl-2">
            <a class="block block-link-shadow text-center" href="be_comp_charts.html">
                <div class="block-content">
                    <p class="mt-5">
                        <i class="si si-bar-chart fa-3x"></i>
                    </p>
                    <p class="font-w600">Live Stats</p>
                </div>
            </a>
        </div>
        <div class="col-6 col-md-4 col-xl-2">
            <a class="block block-link-shadow text-center" href="javascript:void(0)">
                <div class="block-content">
                    <p class="mt-5">
                        <i class="si si-settings fa-3x"></i>
                    </p>
                    <p class="font-w600">Settings</p>
                </div>
            </a>
        </div> -->
    <!-- END Row #5 -->
    <!-- </div>
</div> -->
    <!-- END Page Content -->