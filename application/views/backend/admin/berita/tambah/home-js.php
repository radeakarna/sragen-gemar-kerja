<script type="text/javascript">
    let status = true;

    // Add the following code if you want the name of the file appear on select
    $(".custom-file-input").on("change", function() {
        let fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });

    $(document).ready(function(){
        get_kategori();
    });

    $( "#tambah_kategori_berita" ).click(function() {
        $('#modal_tambah_kategori').modal('show');
    });

    $('#form_tambah_kategori').submit(function(e) {
        e.preventDefault();

        let data = new FormData(this);
        let url  = $(this).attr('action');
        data.append(csrf.token_name, csrf.hash); 

        $('#buttom_tambah_kategori').text("proses...");
        let post  = $.ajax({
            url         : url,
            type        : 'POST',
            data        : data,
            dataType    : 'json',
            processData : false,
            contentType : false,
            cache       : false,
            async       : false,
        });
        post.done(function(respon){
            if (respon.status == true) {
                $('#modal_tambah_kategori').modal('hide');
                get_kategori();
                $('#buttom_tambah_kategori').text("Simpan"); 
                toastr.success( 'Data berhasil ditambahkan', 'Berhasil', { timeOut: 2000, fadeOut: 2000 });
            } else { 
                $('#buttom_tambah_kategori').text("Simpan");
                toastr.error( 'Periksa Inputan Anda, nama tidak boleh sama', { timeOut: 2000, fadeOut: 2000 });
            }                  

            csrf.token_name = respon.csrf.token_name;
            csrf.hash = respon.csrf.hash; 
        });
        post.fail(function(respon){
            toastr.error( 'Ada inputan yang belum terisi', 'Gagal', { timeOut: 2000, fadeOut: 2000 });
            location.reload();
        });
        post.always(function () {
            $('#buttom_tambah_kategori').html('Simpan Data');
        });
    });


    function get_kategori(){
        let url  = "<?= base_url('backend/admin/kategori/show') ?>";
        let data = {jenis : 'berita'};
        let get  = $.get(url, data);
        get.done(function(respon){
            var html = "<option value=''></option>";             
            for (var i = 0; i < respon.length; i++) {
                html += "<option value='"+respon[i].id+"'>"+respon[i].nama+"</option>";
            }
            $("#kategori_id").html(html);
        });
        get.fail(function(respon){
            toastr.error( 'Periksa koneksi internet', 'Gagal', { timeOut: 2000, fadeOut: 2000 });
            // location.reload();
        });
    }
    
    $( "#buttom_tambah" ).click(function() {
        $('body').addClass('oy-hid');
        $("#preloader").show();
    });

    $('#form_tambah').submit(function(e) {
        e.preventDefault();
        tinyMCE.triggerSave();
        $('#buttom_tambah').text("proses...");
        $('#buttom_tambah').attr("disabled", true);

        let data = new FormData(this);
        let url  = $(this).attr('action');
        data.append(csrf.token_name, csrf.hash); 
        
        let post  = $.ajax({
            url         : url,
            type        : 'POST',
            data        : data,
            dataType    : 'json',
            processData : false,
            contentType : false,
            cache       : false,
            async       : false,
            beforeSend: function(){
                // Show image container
                $('body').addClass('oy-hid');
                $("#preloader").show();
            },
        });
        post.done(function(respon){
            if (respon.status == true) {
                $('#buttom_tambah').text("Simpan Data"); 
                toastr.success( 'Data berhasil ditambahkan', 'Berhasil', { timeOut: 2000, fadeOut: 2000 });
                setTimeout(() => window.location.reload(), 2500);
            } else { 

                $("body").removeClass("oy-hid");
                $("#preloader").hide();

                $('#buttom_tambah').text("Simpan Data");
                $('#kategori_error').html(respon.kategori_id);
                $('#file_error').html(respon.file);
                $('#judul_error').html(respon.judul);
                $('#konten_error').html(respon.konten);
                $('#status_error').html(respon.status);
                $('#buttom_tambah').attr("disabled", false);
                toastr.error( 'Periksa Inputan Anda', { timeOut: 2000, fadeOut: 2000 });
            }                  

            csrf.token_name = respon.csrf.token_name;
            csrf.hash = respon.csrf.hash; 
        });
        post.fail(function(respon){
            toastr.error( 'Ada inputan yang belum terisi', 'Gagal', { timeOut: 2000, fadeOut: 2000 });
            location.reload();
        });
        post.always(function () {
            $('#buttom_tambah').html('Simpan Data');
        });
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#tampil_kover').attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    // jQuery(function () {
    //     // Init page helpers (Summernote + CKEditor + SimpleMDE plugins)
    //     Codebase.helpers(['summernote']);
    // });
</script>

<script src="<?= base_url() ?>plugins/tinymce/tinymce.min.js"></script>
<script>
    tinymce.init({
        selector: "#konten",theme: "modern",height: 300,
        plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
            "table contextmenu directionality emoticons paste textcolor"
    ],
    toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
    toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code ",
    image_advtab: true ,
    external_filemanager_path:"<?= base_url() ?>plugins/filemanager/",
    filemanager_title:"Responsive Filemanager" ,
    external_plugins: { "filemanager" : "<?= base_url() ?>plugins/filemanager/plugin.min.js"}
    });
</script> 