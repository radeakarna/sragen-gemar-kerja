<script type="text/javascript">
    let status = true;
    $(document).ready(function(){
        load_table();
        $('#hide_show_data').hide();
        $('#edit_hide_show_data').show();
    }); 

    // $('#tombol_import').click(function(){ 
    //     $('#modal_import').modal('show');
    // });
    
    $(document).on('change', "#perusahaan_umum", function(){
        $('#usaha').val($(this).val());
    });
    
    $(document).on('change', "#perusahaan_umum_ubah", function(){
        $('#usaha_umum').val($(this).val());
    });
    
    // proses import
    $('#form_import_excel').submit(function(e) {
        let data = new FormData(this);
        var url  = "<?php echo base_url($uri_segment.'import_excel') ?>";
        data.append(csrf.token_name, csrf.hash); 

        e.preventDefault();
        $.ajax({
            url: url,
            type: "POST",
            data: data,
            dataType : "JSON",
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            beforeSend:function(){
                $('#button_simpan').text("proses...");
            },
            success: function(data) { 
                if (data.status == true) {
                    $('#button_simpan').text("Simpan");
                    $("#modal_import").modal('hide');

                    $('#upload_file_excel').val("");
                    toastr.success( 'Data berhasil ditambahkan', 'Berhasil', { timeOut: 2000, fadeOut: 2000 });

                    load_table(); 
                }else{
                    toastr.error( 'Perika kembali data yang anda inputkan', 'Gagal', { timeOut: 2000, fadeOut: 2000 });
                    $('#button_simpan').text("Simpan");
                }

                csrf.token_name = data.csrf.token_name;
                csrf.hash = data.csrf.hash; 
            },
        });
    }); 

    function load_table(){ 
        $('#table-data').dataTable({
            destroy: true,
            processing: true,
            serverSide: true,
            ajax: {
                url: '<?= base_url($uri_segment.'get_data') ?>',
                type: 'GET',
                dataType: 'JSON',
                data: function(d){
                    d[csrf.token_name] = csrf.hash;
                }
            },
            drawCallback: function(res) {
                csrf.token_name = res.json.csrf.token_name;
                csrf.hash = res.json.csrf.hash;
            },
            language: {
                processing: '<div class="spinner-border""></div>'
            },
            order: [],
            columnDefs: [{
                    targets: [0, 4],
                    orderable: false
                },
                {
                    targets: [0, 4],
                    className: 'text-center'
                }

            ],
            scrollX: true
        });
         
    }

    function cek_required_tambah() {
        status = true;
        $('#form_tambah .required').each(function(){
            if( $(this).val() == "" ){
              toastr.error( 'Ada inputan yang belum terisi', 'Gagal', { timeOut: 2000, fadeOut: 2000 });
              status = false;
            }
        });
        return status;
    }

    $(document).on('change', '#tambah_role', function(){
        console.log($(this).val());
        $('#name_role').val($(this).val());
    });
    
    function cek_required_ubah() {
        status = true;
        $('#form_ubah .required').each(function(){
            if( $(this).val() == "" ){
              toastr.error( 'Ada inputan yang belum terisi', 'Gagal', { timeOut: 2000, fadeOut: 2000 });
              status = false;
            }
        });
        return status;
    }

    $('#form_tambah').submit(function(e) {
        e.preventDefault();

        if (cek_required_tambah() == true) {
            let data = new FormData(this);
            var url  = "<?= base_url($uri_segment.'store') ?>";
            data.append(csrf.token_name, csrf.hash); 

            $.ajax({
                url: url,
                type: "POST",
                data: data,
                dataType : "JSON",
                processData: false,
                contentType: false,
                cache: false,
                async: false,
                beforeSend:function(){
                    $('#buttom_tambah').text("proses...");
                },
                success: function(data) {
                    if (data.status == true) {
                        load_table(); 
                        $('#modal_tambah').modal('hide');
                        $('#tambah_email').val("");
                        $('#tambah_username').val("");
                        $('#tambah_role').val(0);
                        $('#password').val("");
                        $('#confirm_password').val("");
                        $('#usaha').val("");
                        $('#buttom_tambah').text("Simpan"); 
                        toastr.success( 'Data berhasil ditambahkan', 'Berhasil', { timeOut: 2000, fadeOut: 2000 });
                    } else { 
                        $('#error_username').html(data.username);
                        $('#error_role').html(data.role);
                        $('#error_perusahaan').html(data.usaha);
                        $('#error_confirm_password').html(data.confirm_password);
                        $('#buttom_tambah').text("Simpan");
                        toastr.error( 'Periksa Inputan Anda', { timeOut: 2000, fadeOut: 2000 });
                    }                  

                    csrf.token_name = data.csrf.token_name;
                    csrf.hash = data.csrf.hash; 
                    
                },
                error: function () {
                    toastr.error( 'Periksa Inputan Anda', { timeOut: 2000, fadeOut: 2000 });
                    location.reload();
                }
            });
        } 
    });

    $('#form_ubah').submit(function(e) {
        e.preventDefault();
        
        if (cek_required_ubah() == true) {
            let data = new FormData(this);
            var url  = "<?php echo base_url($uri_segment.'update') ?>";
            data.append(csrf.token_name, csrf.hash); 

            $.ajax({
                url: url,
                type: "POST",
                data: data,
                dataType : "JSON",
                processData: false,
                contentType: false,
                cache: false,
                async: false,
                beforeSend:function(){
                    $('#buttom_ubah').text("proses...");
                },
                success: function(data) {
                    if (data.status == true) {
                        load_table(); 
                        toastr.success( 'Data berhasil diubah', 'Berhasil', { timeOut: 2000, fadeOut: 2000 });
                        $('#modal_ubah').modal('hide');
                        $('#ubah_email').val("");
                        $('#ubah_username').val("");
                        $('#ubah_password').val("");
                        $('#perusahaan_umum_ubah').val("");
                        $('#usaha').val("");
                        $('#ubah_confirm_password').val("");
                        $('#buttom_ubah').text("Simpan"); 
                    } else { 
                        $('#buttom_ubah').text("Simpan");
                        $('#error_edit_username').html(data.username);
                        $('#error_edit_confirm_password').html(data.confirm_password);
                        $('#error_perusahaan').html(data.perusahaan_id);
                        toastr.error( 'Periksa Inputan Anda', { timeOut: 2000, fadeOut: 2000 });
                        location.reload();
                    }                  

                    csrf.token_name = data.csrf.token_name;
                    csrf.hash = data.csrf.hash; 
                    
                },
                error: function () {
                    toastr.error( 'Periksa Inputan Anda', { timeOut: 2000, fadeOut: 2000 });
                    location.reload();
                }
            });
        } 
    });

    $('#tombol_tambah').click(function(){
        $('#modal_tambah').modal('show');
        // get_penduduk();
    });

    $('#show_data').on('click','.tombol_ubah',function(){
        var id = $(this).attr('data'); 

        $.ajax({
            url: "<?= base_url($uri_segment.'detail') ?>",
            type: 'POST',
            dataType: 'JSON',
            data: {
                id:id,
                [csrf.token_name]: csrf.hash
            },
            success: function(data){
                if (data.status == true) {
                    $('#ubah_id').val(data.data.id);
                    $('#ubah_email').val(data.data.email);
                    $('#ubah_username').val(data.data.username);
                    $('#ubah_role_dt').val(data.data.id_role);
                    // get_penduduk(data.data.id_penduduk);
                    // alert("Hai");
                    if(data.id_role !== 1){
                        $('#usaha_umum').val(0);
                        $('#ubah_usaha_d').hide();
                        $("#perusahaan_umum_ubah").attr("required", false);
                    }
                    $('#ubah_role').val(data.data.id_role);
                    $('#edit_nik').val(data.data.id_penduduk);
                    $('#edit_no_kk').val(data.data.id_penduduk);
                    $('#perusahaan_umum_ubah').val(data.data.perusahaan_id);
                    $('#modal_ubah').modal('show');
                } else {
                    toastr.error( 'Koneksi Bermasalah', { timeOut: 2000, fadeOut: 2000 }); 
                }
                csrf.token_name = data.csrf.token_name;
                csrf.hash = data.csrf.hash; 
                
            },
            error: function () {
                toastr.error( 'Periksa Inputan Anda', { timeOut: 2000, fadeOut: 2000 });
                // location.reload();
            }
        }); 
    });

    // Menampilkan model 

    $('#show_data').on('click','.tombol_hapus',function(){
        var id = $(this).attr('data');
        Swal.fire({
          title: 'Apakah Anda Yakin',
          text: "Anda akan menghapus data ini",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Ya, hapus',
          cancelButtonText: 'Tidak',
          confirmButtonClass: 'btn btn-warning',
          cancelButtonClass: 'btn btn-danger ml-1',
          buttonsStyling: false,
        }).then(function (result) {

          if (result.value) {
            $.ajax({
                url: "<?php echo base_url($uri_segment.'delete/') ?>"+id,
                type: "POST",
                data: 
                    {
                        [csrf.token_name]: csrf.hash
                    }
                ,
                dataType : "JSON", 
                success: function(data) { 
                    toastr.success('Data berhasil dihapus');
                    load_table();
                    $('#table-data').DataTable().ajax.reload();

                    csrf.token_name = data.csrf.token_name;
                    csrf.hash = data.csrf.hash;              
                },
            });
            
          }
        });
        
    }); 

    function cek_table(){
        $.ajax({url: "<?php echo base_url($uri_segment.'cek_table/') ?>"+$('#cek_table').val(), success: function(result){
        $("#show_alert").html(result);
      }});
    }

    $("#cek_table").change(function(){
        $.ajax({url: "<?php echo base_url($uri_segment.'cek_table/') ?>"+$('#cek_table').val(), success: function(result){
        $("#show_alert").html(result);
      }});
    });

    // function get_penduduk(id = ''){
    //     var url  = "<?=  base_url($uri_segment.'get_penduduk') ?>";
    //     var data = {id_penduduk : id};
    //     let get = $.get(url, data);
    //     get.done(function(respon){
    //         var html = "<option value=''></option>";             
    //         for (var i = 0; i < respon.length; i++) {
    //             var selected = '';
    //             if(id == respon[i].id){
    //                 selected = 'selected';
    //             }
    //             html += "<option value='"+respon[i].id+"' "+selected+">"+respon[i].nama+"</option>";
    //         }
    //         $("#id_penduduk").html(html);
    //         $("#ubah_id_penduduk").html(html);
    //     }, "json");
    //     get.fail(function(respon){
    //         toastr.error('Gagal!, Terjadi kesalahan.');
    //     }, "json");
    // }
</script>
<script>
    jQuery(function () {
        // Init page helpers (BS Datepicker + BS Colorpicker + BS Maxlength + Select2 + Masked Input + Range Sliders + Tags Inputs plugins)
        Codebase.helpers(['select2']);
    });

    // validation match password tambah data
    var password = document.getElementById("password")
    , confirm_password = document.getElementById("confirm_password");

    function validatePassword(){
        if(password.value != confirm_password.value) {
        confirm_password.setCustomValidity("Passwords tidak sama");
        } else {
        confirm_password.setCustomValidity('');
        }
    }
    // end

    // validation match password ubah data
    password.onchange = validatePassword;
    confirm_password.onkeyup = validatePassword;

    var edit_password = document.getElementById("ubah_password")
    , edit_confirm_password = document.getElementById("ubah_confirm_password");

    function editValidatePassword(){
        if(edit_password.value != edit_confirm_password.value) {
            edit_confirm_password.setCustomValidity("Passwords tidak sama");
        } else {
            edit_confirm_password.setCustomValidity('');
        }
    }
    // end

    edit_password.onchange = editValidatePassword;
    edit_confirm_password.onkeyup = editValidatePassword;

    $( "#id_penduduk" ).change(function() {
        var url  = "<?=  base_url($uri_segment.'get_detail_penduduk') ?>";
        var data = {id : $(this).val()};
        let get  = $.get(url, data);
        get.done(function(respon){
            $('#hide_show_data').show();
            $('#tambah_nik').val(respon.nik);
            $('#tambah_no_kk').val(respon.no_kk);
        }, "json");
        get.fail(function(respon){
            toastr.error('Gagal!, Terjadi kesalahan.');
        }, "json");
    });

    $('#ubah_id_penduduk').change(function () { 
        var url  = "<?=  base_url($uri_segment.'get_detail_penduduk') ?>";
        var data = {id : $(this).val()};
        let get  = $.get(url, data);
        get.done(function(respon){
            $('#edit_nik').val(respon.nik);
            $('#edit_no_kk').val(respon.no_kk);
        }, "json");
        get.fail(function(respon){
            toastr.error('Gagal!, Terjadi kesalahan.');
        }, "json");
     })

</script>