<script type="text/javascript">
    let status = true;

    // Add the following code if you want the name of the file appear on select
    $(".custom-file-input").on("change", function() {
        let fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });

    $(document).on('change','#tambah_jenis_kelamin', function(){
        $('#jsk').val($(this).val());
    });
    
    $(document).on('change','#tambah_status_kerja', function(){
        $('#sttp').val($(this).val());
    });
    
    $('#form_tambah').submit(function(e) {
        e.preventDefault();

        tinyMCE.triggerSave();
        
        $('#buttom_tambah').text("proses...");
        $('#buttom_tambah').attr("disabled", true);

        let data = new FormData(this);
        var url  = $(this).attr('action');
        data.append(csrf.token_name, csrf.hash); 
        
        let post  = $.ajax({
            url         : url,
            type        : 'POST',
            data        : data,
            dataType    : 'json',
            processData : false,
            contentType : false,
            cache       : false,
            async       : false,
        });
        post.done(function(respon){
            if (respon.status == true) {
                $('#buttom_tambah').text("Simpan Data"); 
                if(respon.data.check == '0'){
                    toastr.success( 'Data berhasil ditambahkan', 'Berhasil', { timeOut: 2000, fadeOut: 2000 });
                    setTimeout(() => window.location.reload(), 2500);
                } else {
                    toastr.success( 'Data berhasil diubah', 'Berhasil', { timeOut: 2000, fadeOut: 2000 });
                    setTimeout(() => window.location.reload(), 2500);
                }
            } else { 
                $('#buttom_tambah').text("Simpan Data");
                $('#error_ktp').html(respon.ktp);
                $('#error_nim').html(respon.nim);
                $('#error_email').html(respon.email);
                $('#error_jenis_kelamin').html(respon.jsk);
                $('#error_hp').html(respon.hp);
                $('#error_status_kerja').html(respon.sttp);
                $('#error_pendidikan_terakhir').html(respon.pendidikan_terakhir);
                $('#error_bpjs').html(respon.bpjs);
                
                $('#buttom_tambah').attr("disabled", false);
                toastr.error( 'Periksa Inputan Anda', { timeOut: 2000, fadeOut: 2000 });
            }                  

            csrf.token_name = respon.csrf.token_name;
            csrf.hash = respon.csrf.hash; 
        });
        post.fail(function(respon){
            toastr.error( 'Ada inputan yang belum terisi', 'Gagal', { timeOut: 2000, fadeOut: 2000 });
            // location.reload();
        });
        post.always(function () {
            $('#buttom_tambah').html('Simpan Data');
        });
    });
    
//     $('#form_tambah').submit(function(e) {
//        e.preventDefault();
//
//        $('#buttom_tambah').text("proses...");
//        $('#buttom_tambah').attr("disabled", true);
//
//        let data = new FormData(this);
//        var url  = $(this).attr('action');
//        data.append(csrf.token_name, csrf.hash); 
//        
//        let post  = $.ajax({
//            url         : url,
//            type        : 'POST',
//            data        : data,
//            dataType    : 'json',
//            processData : false,
//            contentType : false,
//            cache       : false,
//            async       : false,
//        });
//        post.done(function(respon){
//            console.log(respon);
//        });
//       
//    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#tampil_kover').attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    function detail(id){
        let url  = "<?= base_url($uri_segment.'detail') ?>";
        let data = {id : id};
        let get  = $.get(url, data);
        get.done(function(respon){
            
            $('#id').val(id);
            $('#ubah_judul').val(respon.data.judul);
           
            // get_kategori(respon.data.kategori_id);
           // $('#npwp').val(respon.data.npwp);
           // $('#alamat').val(respon.data.alamat);
            //$('#deskripsi').val(respon.data.deskripsi);
            tinymce.get("deskripsi").setContent(respon.data.deskripsi);
        });
        get.fail(function(respon){
            toastr.error( 'Periksa koneksi internet', 'Gagal', { timeOut: 2000, fadeOut: 2000 });
            // location.reload();
        });
    }
    
    jQuery(function () {
        // Init page helpers (Summernote + CKEditor + SimpleMDE plugins)
        Codebase.helpers(['summernote']);
    });
</script>

<script src="<?= base_url() ?>plugins/tinymce/tinymce.min.js"></script>
<script>
    tinymce.init({
        selector: "#deskripsi",theme: "modern",height: 300,
        plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
            "table contextmenu directionality emoticons paste textcolor"
    ],
    toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
    toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code ",
    image_advtab: true ,
    external_filemanager_path:"<?= base_url() ?>plugins/filemanager/",
    filemanager_title:"Responsive Filemanager" ,
    external_plugins: { "filemanager" : "<?= base_url() ?>plugins/filemanager/plugin.min.js"}
    });
</script> 