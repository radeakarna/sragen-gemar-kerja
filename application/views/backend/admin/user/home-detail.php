<div class="col-11">
    <form id="form_tambah" method="post" action="<?= base_url($uri_segment . 'store_detail') ?>">
        <div class="block-content">
            <div class="form-group"> 
                <div class="row">
                    <input id="check" name="check" type="hidden" value="<?=$check?>" /> 
                    <div class="col-sm-12" hidden>
                        <label>Id</label>
                        <input type="text" class="form-control" name="id" id="ubah_id" value="<?=$id?>"> 
                    </div>
                    
                    <div class="col-sm-12">
                        <label>No KTP</label>
                        <input type="text" id="tambah_ktp" class="form-control" name="ktp" value="<?=$ktp?>" required/> 
                        <span id="error_ktp"></span>
                    </div>
                    
                     <div class="col-sm-12">
                        <label>NIM</label>
                        <input type="text" id="tambah_nim" class="form-control" name="nim" value="<?=$nim?>" required/> 
                        <span id="error_nim"></span>
                    </div>
                    
                    <div class="col-sm-12">
                        <label>Email</label>
                        <input type="email" id="tambah_email" class="form-control" name="email" value="<?=$email?>" required/> 
                        <span id="error_email"></span>
                    </div>
                    
                     <div class="col-sm-12">
                        <label>Jenis Kelamin</label>
                        <select id="tambah_jenis_kelamin" name=jenis_kelamin" class="form-control">
                            <?=$jenis_kelamin?>
                        </select>
                        <input type="hidden" name="jsk" id="jsk" value="<?=$jsk?>" />
                        <span id="error_jenis_kelamin"></span>
                    </div>
                    

                    <div class="col-sm-12">
                        <label>No HP</label>
                        <input type="text" id="tambah_hp" class="form-control" name="hp" value="<?=$hp?>" required/> 
                        <span id="error_hp"></span>
                    </div>
                    
                    <div class="col-sm-12">
                        <label>Status Pekerjaan</label>
                        <select class="form-control" name="status_kerja" id="tambah_status_kerja">
                            <?=$status?>
                        </select>
                        <input type="hidden" name="sttp" id="sttp" value="<?=$pilih_kerja?>" required />
                        <span id="error_status_kerja"></span>
                    </div>

                    <div class="col-sm-12">
                        <label>Pendidikan Terakhir</label>
                        <input type="text" id="tambah_hp" class="form-control" name="pendidikan_terakhir" value="<?=$pendidikan?>" required/> 
                        <span id="error_pendidikan_terakhir"></span>
                    </div>

                     <div class="col-sm-12">
                        <label>BPJS</label>
                        <input type="text" id="tambah_bpjs" class="form-control" name="bpjs" value="<?=$bpjs?>" required/> 
                        <span id="error_bpjs"></span>
                    </div>

                    
                </div> 
            </div> 
        </div>

        <div class="modal-footer">
            <?=$aksi?>
        </div>
    </form> 
</div>
