<div id="modal_ubah" class="modal" id="modal-normal" tabindex="-1" role="dialog" aria-labelledby="modal-small" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary">
                    <h3 class="block-title">Modal Ubah</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
                    </div>
                </div>
                <form id="form_ubah">
                    <div class="block-content">

                        <div class="form-group"> 
                            <div class="row">
                                <div class="col-sm-12" hidden>
                                    <label>Id</label>
                                    <input type="text" class="form-control" name="id" id="ubah_id"> 
                                </div>
                                <div class="col-sm-12">
                                    <label>Email</label>
                                    <input type="email" id="ubah_email" class="form-control" name="email"  required/> 
                                </div>
                                <div class="col-sm-12">
                                    <label>Username</label>
                                    <input type="text" id="ubah_username" class="form-control" name="username" required/> 
                                    <span id="error_edit_username"></span>
                                </div>
                                
                                <div class="col-sm-12" id="ubah_usaha_d">
                                    <label>Perusahaan</label>
                                    <select name="perusahaan" class="form-control" id="perusahaan_umum_ubah" required>
                                        <?=$perusahaan?>
                                    </select>
                                     <span id="error_perusahaan"></span>
                                     <input type="hidden" name="usaha" id="usaha_umum" />
                                </div>
                                
                                <div class="col-sm-12">
                                    <label>Pilih Role</label>
                                    <select id="ubah_role" class="form-control" name="role" required />
                                        <?=$role?>
                                    </select>
                                    <input type="hidden" id="ubah_role_dt" name="name_role" required />
                                    <span id="error_role"></span>
                                </div>
                                
                                <div class="col-sm-12">
                                    <label>Password (kosongkan jika tidak dirubah)</label>
                                    <input type="password" id="ubah_password" class="form-control" name="password"> 
                                </div>
                                <div class="col-sm-12">
                                    <label>Ulangi Password (kosongkan jika tidak dirubah)</label>
                                    <input type="password" id="ubah_confirm_password" class="form-control" name="confirm_password"> 
                                    <span id="error_edit_confirm_password"></span>
                                </div>
                            </div> 
                        </div> 
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id='buttom_ubah'>Ubah</button>
                    </div>
                </form> 
            </div> 
        </div>
    </div>
</div>