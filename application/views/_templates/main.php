<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$this->load->view('_templates/head');

// pembagian sidebar berdasarkan session
if($this->session->userdata('nama_role') == 'Administrator'){
    $this->load->view('_templates/sidebars/sidebar');
}elseif($this->session->userdata('nama_role') == 'Perusahaan'){
    $this->load->view('_templates/sidebars/perusahaan-sidebar');
}else{
    $this->load->view('_templates/sidebars/pencaker-sidebar');

}

$this->load->view('_templates/header');
if(is_file('./application/views/'.@$content.'.php')){
    $this->load->view($content);
} else{
    $this->load->view('_templates/blank');
}
$this->load->view('_templates/footer');
