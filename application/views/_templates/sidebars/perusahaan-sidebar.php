<style>
@media screen and (max-width: 600px) {
    #page-container.sidebar-o {
        padding-left: 0px;
    }
}

@media screen and (min-width: 600px) and (max-width: 990px) {
    #page-container.sidebar-inverse #sidebar {
        margin: 10px 0 10px 275px;
    }
}
</style>
<!-- BEGIN: Header-->
<div id="page-container" class="sidebar-o sidebar-inverse side-scroll page-header-fixed main-content-boxed">

    <nav id="sidebar">
        <!-- Sidebar Scroll Container -->
        <div id="sidebar-scroll">
            <!-- Sidebar Content -->
            <div class="sidebar-content">
                <!-- Side Header -->
                <div class="content-header content-header-fullrow px-15">
                    <!-- Mini Mode -->
                    <div class="content-header-section sidebar-mini-visible-b">
                        <!-- Logo -->
                        <span class="content-header-item font-w700 font-size-xl float-left animated fadeIn">
                            <span class="text-dual-primary-dark">c</span><span class="text-primary">b</span>
                        </span>
                        <!-- END Logo -->
                    </div>
                    <!-- END Mini Mode -->

                    <!-- Normal Mode -->
                    <div class="content-header-section text-center align-parent sidebar-mini-hidden">
                        <!-- Close Sidebar, Visible only on mobile screens -->
                        <!-- Layout API, functionality initialized in Codebase() -> uiApiLayout() -->
                        <button type="button" class="btn btn-circle btn-dual-secondary d-lg-none align-v-r" data-toggle="layout" data-action="sidebar_close">
                            <i class="fa fa-times text-danger"></i>
                        </button>
                        <!-- END Close Sidebar -->

                        <!-- Logo -->
                        <div class="content-header-item">
                            <a class="font-w700" href="<?= base_url() ?>">
                                <!-- <i class="si si-fire text-primary"></i> -->
                                <span class="font-size-xl text-dual-primary-dark">Gemar </span><span class="font-size-xl text-primary">Kerja</span>
                            </a>
                        </div>
                        <!-- END Logo -->
                    </div>
                    <!-- END Normal Mode -->
                </div>
                <!-- END Side Header -->

                <!-- Side User -->
                <div class="content-side content-side-full content-side-user px-10 align-parent">
                    <!-- Visible only in mini mode -->
                    <div class="sidebar-mini-visible-b align-v animated fadeIn">
                        <img class="img-avatar" src="https://ui-avatars.com/api/?name='<?= $this->session->userdata('username'); ?>'&rounded=true" alt="">
                    </div>
                    <!-- END Visible only in mini mode -->

                    <!-- Visible only in normal mode -->
                    <div class="sidebar-mini-hidden-b text-center">
                        <a class="img-link" href="<?= base_url('backend/dashboard') ?>">
                            <img class="img-avatar" src="<?= base_url('assets/') ?>logo.png" alt="">
                        </a>
                        <ul class="list-inline mt-10">
                            <li class="list-inline-item">
                                <a class="text-dual-primary-dark font-size-xs font-w600 text-uppercase" href="be_pages_generic_profile.html">Administrator</a>
                            </li>
                            <!-- <li class="list-inline-item">
                                <a class="link-effect text-dual-primary-dark" data-toggle="layout" data-action="sidebar_style_inverse_toggle" href="javascript:void(0)">
                                    <i class="si si-drop"></i>
                                </a>
                            </li> -->
                            <li class="list-inline-item">
                                <a class="link-effect text-dual-primary-dark" href="<?= base_url('login/logout') ?>">
                                    <i class="si si-logout"></i>
                                </a>
                            </li>
                        
                        </ul>
                    </div>
                    <!-- END Visible only in normal mode -->
                </div>
                <!-- END Side User -->

                <!-- Side Navigation -->
                <div class="content-side content-side-full">
                    <ul class="nav-main">
                        <li>
                            <a href="<?= base_url('backend/admin/dashboard/index') ?>" class="<?= @$li_active == "dashboard" ? 'active' : '' ?>"><i class="si si-cup"></i><span class="sidebar-mini-hide">Dashboard</span></a>
                        </li>

                        <li class="nav-main-heading"><span class="sidebar-mini-visible">UI</span><span class="sidebar-mini-hidden">App</span></li>
                        <li>
                            <a href="<?= base_url('backend/perusahaan/perusahaan') ?>" class="<?= @$li_active == "perusahaan" ? 'active' : '' ?>"><i class="fa fa-sliders"></i><span class="sidebar-mini-hide">Perusahaan</span></a>
                        </li>
                        <li>
                            <a href="<?= base_url('backend/perusahaan/jobfair') ?>" class="<?= @$li_active == "jobfair" ? 'active' : '' ?>"><i class="fa fa-sliders"></i><span class="sidebar-mini-hide">Job Fair</span></a>
                        </li>
                        <li>
                            <a href="<?= base_url('backend/perusahaan/loker') ?>" class="<?= @$li_active == "loker" ? 'active' : '' ?>"><i class="fa fa-sliders"></i><span class="sidebar-mini-hide">Loker</span></a>
                        </li>
                    </ul>
                </div>
                <!-- END Side Navigation -->
            </div>
            <!-- Sidebar Content -->
        </div>
        <!-- END Sidebar Scroll Container -->
    </nav>
    <!-- END Sidebar -->