<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!doctype html>
<!--[if lte IE 9]>     <html lang="en" class="no-focus lt-ie10 lt-ie10-msg"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en" class="no-focus"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

        <!-- <title><?= @$this->config->item('head_title')?></title> -->
        <title>Admin Panel Web </title>
        
        <meta name="description" content="<?= @$this->config->item('head_description')?>">
        <meta name="author" content="gemar kerja ">
        <meta name="robots" content="noindex, nofollow">

        <!-- Open Graph Meta -->
        <meta property="og:title" content="<?= @$this->config->item('head_title')?>">
        <meta property="og:site_name" content="<?= @$this->config->item('head_site_name')?>">
        <meta property="og:description" content="<?= @$this->config->item('head_description')?>">
        <meta property="og:type" content="website">
        <meta property="og:url" content="">
        <meta property="og:image" content="">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="<?= base_url('assets/') ?>logo.png">
        <link rel="icon" type="image/png" sizes="192x192" href="<?= base_url('assets/') ?>logo.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?= base_url('assets/') ?>logo.png">
        <!-- END Icons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.css" integrity="sha512-bYPO5jmStZ9WI2602V2zaivdAnbAhtfzmxnEGh9RwtlI00I9s8ulGe4oBa5XxiC6tCITJH/QG70jswBhbLkxPw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <!-- Stylesheets -->
        <!-- Codebase framework -->
        <!-- <link rel="stylesheet" id="css-main" href="<?= base_url() ?>assets/backend/css/codebase.css"> -->
        <!-- You can include a specific file from css/themes/ folder to alter the default color theme of the template. eg: -->
        <!-- <link rel="stylesheet" id="css-theme" href="assets/css/themes/flat.min.css"> -->
        <!-- END Stylesheets -->
        <?php @$select2 ? $this->load->view('_templates/css/select2'):''; ?> 
        <?php @$datatables_css ? $this->load->view('_templates/css/datatables'):''; ?> 
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/js/plugins/dropzonejs/min/dropzone.min.css">

        <?php @$toastr ? $this->load->view('_templates/css/toastr'):''; ?> 
        <?php @$sweet_alert ? $this->load->view('_templates/css/sweet-alert'):''; ?> 
        <?php @$leaflet ? $this->load->view('_templates/css/leaflet'):''; ?> 
        <?php @$dropzone ? $this->load->view('_templates/css/dropzone'):''; ?> 
        <?php @$sumernote ? $this->load->view('_templates/css/sumernote'):''; ?> 
        <?php @$slick ? $this->load->view('_templates/css/slick'):''; ?> 
        <link rel="stylesheet" id="css-main" href="<?= base_url() ?>assets/backend/css/codebase.min.css">
        <link rel="stylesheet" id="css-main" href="<?= base_url() ?>assets/backend/css/custom.css">
    </head>
    <body>
        <!-- Loader -->
        <div id="preloader" class="load-back" style="display:none">
            <div id="status">
                <div class="spinner">
                    <img src="https://appt.demoo.id/web-kemenpar-ap/assets/frontend/layouts/images/custom/kemenparekraf.png" style="width: 100%;" class="l-dark mover" alt="">
                </div>
            </div>
        </div>

        <img src="<?= base_url('assets/') ?>logo.png" class="o-all">