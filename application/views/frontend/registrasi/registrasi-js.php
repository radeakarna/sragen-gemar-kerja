<script type="text/javascript">
$(document).ready(function(){
    let status = true;
    $(document).on('change','#perusahaan_umum', function(){
        $('#usaha').val($(this).val());
    });
    
    $(document).on('change', '#tambah_role', function(){
        $('#name_role').val($(this).val());
    });
    
    function cek_required_tambah() {
        status = true;
        $('#form_tambah .required').each(function(){
            if( $(this).val() == ""){
              toastr.error( 'Ada inputan yang belum terisi', 'Gagal', { timeOut: 2000, fadeOut: 2000 });
              status = false;
            }
        });
        return status;
    }
    
      $(document).on('change','#tambah_jenis_kelamin', function(){
        $('#jsk').val($(this).val());
    });
    
    $(document).on('change','#tambah_status_kerja', function(){
        $('#sttp').val($(this).val());
    });
    
    
    $('#form_tambah').submit(function(e) {
        e.preventDefault();
        
        if (cek_required_tambah() == true) {
            let data = new FormData(this);
            var url  = "<?= base_url($uri_segment.'store') ?>";
            data.append(csrf.token_name, csrf.hash); 

            $.ajax({
                url: url,
                type: "POST",
                data: data,
                dataType : "JSON",
                processData: false,
                contentType: false,
                cache: false,
                async: false,
                beforeSend:function(){
                    $('#buttom_tambah').text("proses...");
                },
                success: function(data) {
                    
                    if (data.status === 'true') {
                    
                     
                        $('#email_umum').val("");
                        $('#perusahaan_umum').val("");
                        $('#password').val("");
                        $('#confirm_password').val("");
                        $('#buttom_tambah').text("Simpan"); 
                        
                        toastr.success( 'User berhasil ditambahkan', 'Berhasil', { timeOut: 2000, fadeOut: 2000 });
                    } else { 
                        $('#error_username').html(data.username);
                        $('#error_email').html(data.email);
                        $('#error_perusahaan').html(data.perusahaan_id);
                        $('#error_role').html(data.name_role);
                        $('#error_confirm_password').html(data.confirm_password);
                        
                        $('#error_ktp').html(respon.ktp);
                        $('#error_nim').html(respon.nim);
                       
                        $('#error_jenis_kelamin').html(respon.jsk);
                        $('#error_hp').html(respon.hp);
                        $('#error_status_kerja').html(respon.sttp);
                        $('#error_pendidikan_terakhir').html(respon.pendidikan_terakhir);
                        $('#error_bpjs').html(respon.bpjs);
                        $('#buttom_tambah').text("Simpan");
                        toastr.error( 'Periksa Inputan Anda', { timeOut: 2000, fadeOut: 2000 });
                    }                  

                    csrf.token_name = data.csrf.token_name;
                    csrf.hash = data.csrf.hash; 
                    
                },
                error: function () {
                    toastr.error( 'Periksa Inputan Anda', { timeOut: 2000, fadeOut: 2000 });
                    location.reload();
                }
            });
        } 
    });
    
});
</script>