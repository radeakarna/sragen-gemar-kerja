<!-- Title Header Start -->
<section class="inner-header-title" style="background-image:url(<?= base_url('assets/frontend/') ?>assets/img/bn2.jpg);">
    <div class="container">
        <h1>Pendaftaran</h1>
    </div>
</section>
<div class="clearfix"></div>
<!-- Title Header End -->

<!-- Tab Section Start -->
<section class="tab-sec gray">
    <div class="container">
        <form id="form_tambah">
            <div class="col-lg-8 col-md-8 col-sm-12 col-lg-offset-2 col-md-offset-2">
                <div class="new-logwrap">

                    <div class="form-group">
                        <label>Username</label>
                        <div class="input-with-icon">
                            <input type="text" id="tambah_username" name="username" class="form-control" placeholder="Enter Your Username">
                            <i class="theme-cl ti-user"></i>
                        </div>

                        <span id="error_username"></span>
                    </div>

                    <div class="form-group">
                        <label>Email</label>
                        <div class="input-with-icon">
                            <input type="email" id="tambah_email" name="email" class="form-control" placeholder="Enter Your Email">
                            <i class="theme-cl ti-email"></i>
                        </div>

                        <span id="error_email"></span>
                    </div>


                   
                    <input type="hidden" name="name_role" id="name_role" value="2" required />
                    <input type="hidden" name="usaha" id="usaha" value="0" />

                    <!-- get all user !-->

                    <div class="form-group">
                        <label>No KTP</label>
                        <div class="input-with-icon">
                            <input type="text" id="tambah_ktp" class="form-control" name="ktp" value="<?= $ktp ?>" required/> 
                            <i class="theme-cl ti-id-badge"></i>
                        </div>
                        <span id="error_ktp"></span>
                    </div>

                    <div class="form-group">
                        <label>NIM</label>
                        <div class="input-with-icon">
                            <input type="text" id="tambah_nim" class="form-control" name="nim" value="<?= $nim ?>" required/> 
                            <i class="theme-cl ti-pencil"></i>
                        </div>
                        <span id="error_nim"></span>
                    </div>

                    <div class="form-group">
                        <label>Jenis Kelamin</label>
                        <div class="input-with-icon">
                            <select id="tambah_jenis_kelamin" name=jenis_kelamin" class="form-control">
                                <?= $jenis_kelamin ?>
                            </select>

                            <i class="theme-cl fa-genderless"></i>
                        </div>
                        <input type="hidden" name="jsk" id="jsk" />
                        <span id="error_jenis_kelamin"></span>
                    </div>


                    <div class="form-group">
                        <label>No HP</label>
                        <div class="input-with-icon">
                            <input type="text" id="tambah_hp" class="form-control" name="hp" value="<?= $hp ?>" required/> 
                            <i class="theme-cl fa-phone"></i>
                        </div>
                        <span id="error_hp"></span>
                    </div>

                    <div class="form-group">
                        <label>Status Pekerjaan</label>
                        <div class="input-with-icon">
                            <select class="form-control" name="status_kerja" id="tambah_status_kerja">
                                <?= $status ?>
                            </select>
                            <i class="theme-cl"></i>
                        </div>
                        <input type="hidden" name="sttp" id="sttp" required />
                        <span id="error_status_kerja"></span>
                    </div>

                    <div class="form-group">
                        <label>Pendidikan Terakhir</label>
                        <div class="input-with-icon">
                            <input type="text" id="tambah_hp" class="form-control" name="pendidikan_terakhir" value="<?= $pendidikan ?>" required/> 
                            <i class="theme-cl"></i>
                        </div>
                        <span id="error_pendidikan_terakhir"></span>
                    </div>

                    <div class="form-group">
                        <label>BPJS</label>
                        <div class="input-with-icon">
                            <input type="text" id="tambah_bpjs" class="form-control" name="bpjs" value="<?= $bpjs ?>" required/> 
                            <i class="theme-cl"></i>
                        </div>
                        <span id="error_bpjs"></span>
                    </div>

                    <!-- get all user !-->

                    <div class="form-group">
                        <label>Password</label>
                        <div class="input-with-icon">
                            <input type="password" class="form-control" name="password" id="password" placeholder="Enter Your Password">
                            <i class="theme-cl ti-lock"></i>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Ulangi Password</label>
                        <div class="input-with-icon">
                            <input type="password" id="confirm_password" class="form-control" name="confirm_password" placeholder="Confirm Password" required/>                                                   
                            <i class="theme-cl ti-lock"></i>
                        </div>

                        <span id="error_confirm_password"></span>
                    </div>

                    <div class="register-account text-center">
                        By hitting the <span class="theme-cl">"Register"</span> button, you agree to the <a class="theme-cl" href="#">Terms conditions</a> and <a class="theme-cl" href="#">Privacy Policy</a>
                    </div>

                    <div class="form-groups">
                        <button type="submit" id="buttom_tambah" class="btn btn-primary theme-bg full-width">Daftar</button>

                    </div>

                </div>

            </div>
        </form>
    </div>
</div>
</div>
</section>
<!-- Tab section End -->
