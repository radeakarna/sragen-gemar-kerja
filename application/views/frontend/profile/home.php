<section class="bg-utama bg-half bg-light d-table w-100" style="background: url('<?= base_url('assets/frontend/') ?>layouts/images/custom/adli-wahid-oTa5NUwhj0c-unsplash.jpg') center center;">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12 text-center">
                <div class="page-next-level">
                    <h4 class="title text-white"> Tentang Kami </h4>
                    <div class="page-next">
                        <nav aria-label="breadcrumb" class="d-inline-block">
                            <ul class="breadcrumb bg-white rounded shadow mb-0">
                                <?= $li_active?>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>  <!--end col-->
        </div><!--end row-->
    </div> <!--end container-->
</section><!--end section-->
<!-- Hero End -->

<!-- Shape Start -->
<div class="position-relative">
    <div class="shape overflow-hidden text-white">
        <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
        </svg>
    </div>
</div>
<!--Shape End-->

<!-- About Start -->
<section class="section">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-5 col-md-5 mt-4 pt-2 mt-sm-0 pt-sm-0">
                <div class="position-relative">
                    <img src="<?= base_url('uploads/images/profile/'.$item->thumbnail) ?>" class="rounded img-fluid mx-auto d-block" alt="">
                    <div class="play-icon">
                        <a href="<?= $item->yt?>" class="play-btn video-play-icon">
                            <i class="mdi mdi-play text-primary rounded-circle bg-white shadow"></i>
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-lg-7 col-md-7 mt-4 pt-2 mt-sm-0 pt-sm-0">
                <div class="section-title ml-lg-4">
                    <h4 class="title mb-4"><?= $item->judul?></h4>
                    <p class="text-muted"><?=$item->deskripsi?></p>
                </div>
            </div>
        </div><!--end row-->
    </div><!--end container-->

    <!-- <div class="container mt-100 mt-60">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <div class="section-title mb-4 pb-2">
                    <h4 class="title mb-2">Sub Sektor</h4>
                    <p class="text-muted para-desc mx-auto mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                </div>
            </div>
        </div>

        <div class="row mx-md-n3 mx-0">
            <div class="col-lg-3 col-md-6 col-6 mt-md-4 mt-2 pt-2 px-md-3 px-2">
                <div class="media key-feature align-items-center p-3 rounded shadow">
                    <div class="icon text-center rounded-circle mr-md-3 mr-2">
                        <i data-feather="monitor" class="fea icon-ex-md text-primary"></i>
                    </div>
                    <div class="media-body">
                        <h4 class="title mb-0">Homestay</h4>
                    </div>
                </div>
            </div>
            
            <div class="col-lg-3 col-md-6 col-6 mt-md-4 mt-2 pt-2 px-md-3 px-2">
                <div class="media key-feature align-items-center p-3 rounded shadow">
                    <div class="icon text-center rounded-circle mr-md-3 mr-2">
                        <i data-feather="heart" class="fea icon-ex-md text-primary"></i>
                    </div>
                    <div class="media-body">
                        <h4 class="title mb-0">Usaha Wisata</h4>
                    </div>
                </div>
            </div>
            
            <div class="col-lg-3 col-md-6 col-6 mt-md-4 mt-2 pt-2 px-md-3 px-2">
                <div class="media key-feature align-items-center p-3 rounded shadow">
                    <div class="icon text-center rounded-circle mr-md-3 mr-2">
                        <i data-feather="eye" class="fea icon-ex-md text-primary"></i>
                    </div>
                    <div class="media-body">
                        <h4 class="title mb-0">Aplikasi</h4>
                    </div>
                </div>
            </div>
            
            <div class="col-lg-3 col-md-6 col-6 mt-md-4 mt-2 pt-2 px-md-3 px-2">
                <div class="media key-feature align-items-center p-3 rounded shadow">
                    <div class="icon text-center rounded-circle mr-md-3 mr-2">
                        <i data-feather="bold" class="fea icon-ex-md text-primary"></i>
                    </div>
                    <div class="media-body">
                        <h4 class="title mb-0">Game Developer</h4>
                    </div>
                </div>
            </div>
            
            <div class="col-lg-3 col-md-6 col-6 mt-md-4 mt-2 pt-2 px-md-3 px-2">
                <div class="media key-feature align-items-center p-3 rounded shadow">
                    <div class="icon text-center rounded-circle mr-md-3 mr-2">
                        <i data-feather="feather" class="fea icon-ex-md text-primary"></i>
                    </div>
                    <div class="media-body">
                        <h4 class="title mb-0">Kuliner</h4>
                    </div>
                </div>
            </div>
            
            <div class="col-lg-3 col-md-6 col-6 mt-md-4 mt-2 pt-2 px-md-3 px-2">
                <div class="media key-feature align-items-center p-3 rounded shadow">
                    <div class="icon text-center rounded-circle mr-md-3 mr-2">
                        <i data-feather="code" class="fea icon-ex-md text-primary"></i>
                    </div>
                    <div class="media-body">
                        <h4 class="title mb-0">Fashion</h4>
                    </div>
                </div>
            </div>
            
            <div class="col-lg-3 col-md-6 col-6 mt-md-4 mt-2 pt-2 px-md-3 px-2">
                <div class="media key-feature align-items-center p-3 rounded shadow">
                    <div class="icon text-center rounded-circle mr-md-3 mr-2">
                        <i data-feather="user-check" class="fea icon-ex-md text-primary"></i>
                    </div>
                    <div class="media-body">
                        <h4 class="title mb-0">Kriya</h4>
                    </div>
                </div>
            </div>
            
            <div class="col-lg-3 col-md-6 col-6 mt-md-4 mt-2 pt-2 px-md-3 px-2">
                <div class="media key-feature align-items-center p-3 rounded shadow">
                    <div class="icon text-center rounded-circle mr-md-3 mr-2">
                        <i data-feather="git-merge" class="fea icon-ex-md text-primary"></i>
                    </div>
                    <div class="media-body">
                        <h4 class="title mb-0">Film Animasi & Video</h4>
                    </div>
                </div>
            </div>

        </div>
    </div> -->
</section>