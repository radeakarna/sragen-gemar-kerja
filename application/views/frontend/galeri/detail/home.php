 <!-- Hero Start -->
 <section class="bg-utama bg-half bg-light d-table w-100" style="background: url('<?= base_url('assets/frontend/') ?>layouts/images/custom/adli-wahid-oTa5NUwhj0c-unsplash.jpg') center center;">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12 text-center">
                <div class="page-next-level">
                    <h4 class="title text-white"> <?= $page_title ?> </h4>
                    <div class="page-next">
                        <nav aria-label="breadcrumb" class="d-inline-block">
                            <ul class="breadcrumb bg-white rounded shadow mb-0">
                                <?= $li_active ?>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>  <!--end col-->
        </div><!--end row-->
    </div> <!--end container-->
</section><!--end section-->
<!-- Hero End -->

<!-- Shape Start -->
<div class="position-relative">
    <div class="shape overflow-hidden text-white">
        <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
        </svg>
    </div>
</div>
<!--Shape End-->

<!-- Start Works -->
<section class="section">
    <div class="container">
        <div class="row projects-wrapper">

            <?php
            foreach($galeri as $item):
            ?>

            <div class="col-lg-4 col-md-6 col-12 mb-4 pb-2 designing">
                <div class="card border-0 work-container work-grid position-relative d-block overflow-hidden rounded">
                    <div class="card-body p-0">
                        <a class="mfp-image d-inline-block" href="<?= @$item->foto_besar_path? base_url($item->foto_besar_path) : base_url('assets/img/no-camera.png') ?>" title="<?= $item->deskripsi ?>">
                        <img src="<?= @$item->foto_thumbnail_path? base_url($item->foto_thumbnail_path) : base_url('assets/img/no-camera.png') ?>" class="img-fluid rounded" alt="<?= $item->judul ?>">
                        </a>
                        <div class="content bg-white p-3">
                            <h5 class="mb-0"><a href="javascript:void(0)" class="text-dark title"><?= $item->deskripsi ?></a></h5>
                            <h6 class="text-muted tag mb-0"><?= $item->album ?></h6>
                        </div>
                    </div>
                </div>
            </div><!--end col-->
<!-- 
            <div class="col-lg-4 col-md-6 col-12 mb-4 pb-2">
                <a href="galeri-detail.html">
                <div class="card work-container work-modern position-relative overflow-hidden shadow rounded border-0">
                    <div class="card-body p-0">
                        <img src="<?= @$item->foto_thumbnail_path? base_url($item->foto_thumbnail_path) : base_url('assets/img/no-camera.png') ?>" class="img-fluid rounded" alt="<?= $item->judul ?>">
                        <div class="overlay-work bg-dark"></div>
                        <div class="content">
                            <a href="galeri-detail.html" class="title text-white d-block font-weight-bold"><?= $item->deskripsi ?></a>
                            <small class="text-light">Administrator</small>
                        </div>
                        <div class="client">
                            <small class="text-light user d-block"><i class="mdi mdi-account"></i> Admin</small>
                            <small class="text-light date"><i class="mdi mdi-calendar-check"></i> <?= tanggal_format($item->dibuat_pada) ?></small>
                        </div>
                    </div>
                </div>
                </a>
            </div> -->
            <?php
            endforeach;
            ?>
            
            <!-- PAGINATION START -->
            <div class="col-12">
                <ul class="pagination justify-content-center mb-0">
                    <?= $pagination ?>
                </ul>
            </div><!--end col-->
            <!-- PAGINATION END -->
        </div><!--end row-->
    </div><!--end container-->
</section><!--end section-->
<!-- End Works -->
