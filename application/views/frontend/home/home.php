<section class="swiper-slider-hero position-relative d-block vh-100" id="home">
    <div class="swiper-container">
        <div class="swiper-wrapper">
            <div class="swiper-slide d-flex align-items-center overflow-hidden">
                <div class="bg-utama slide-inner slide-bg-image d-flex align-items-center" style="background: center center;" data-background="<?= base_url() ?>assets/frontend/layouts/images/custom/mahmur-marganti-8Bg8N8HtiWI-unsplash.jpg">
                    <div class="bg-overlay"></div>
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-12">
                                <div class="title-heading">
                                    <div class="top-judul-utama title-dark">Selamat datang di website resmi</div>
                                    <hr class="hr-judul-utama">
                                    <div class="judul-utama mb-4 font-weight-bold text-white title-dark">Direktorat Akses Pembiayaan <br>Deputi Bidang Industri dan Investasi</div>

                                    <div class="mt-4 pt-2">
                                        <a href="#tentangkami" class="btn btn-primary">Tentang kami</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            foreach ($slider as $item) :
            ?>
                <div class="swiper-slide d-flex align-items-center overflow-hidden">
                    <div class="bg-utama slide-inner slide-bg-image d-flex align-items-center" style="background: center center;" data-background="<?= @$item->foto_path ? base_url($item->foto_path) : base_url('assets/img/no-camera.png') ?>" class="card-img-top rounded-top" alt="<?= $item->nama ?>">
                        <div class="bg-overlay"></div>
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-12">
                                    <div class="title-heading">
                                        <div class="top-judul-utama title-dark"><?= $item->nama ?></div>
                                        <hr class="hr-judul-utama">
                                        <div class="judul-utama mb-4 font-weight-bold text-white title-dark"><?= $item->deskripsi ?></div>

                                        <!-- <div class="mt-4 pt-2">
                                        <a href="#tentangkami" class="btn btn-primary">Tentang kami</a>
                                    </div> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
            endforeach;
            ?>
            <!-- <div class="swiper-slide d-flex align-items-center overflow-hidden">
                <div class="bg-utama slide-inner slide-bg-image d-flex align-items-center" style="background: center center;" data-background="<?= base_url('assets/frontend/') ?>layouts/images/custom/adli-wahid-oTa5NUwhj0c-unsplash.jpg">
                    <div class="bg-overlay"></div>
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-12">
                                <div class="title-heading text-center">
                                    <h1 class="heading text-white title-dark mb-4">Meet our brand <br> new solution</h1>
                                    <p class="para-desc mx-auto text-white-50">Launch your campaign and benefit from our expertise on designing and managing conversion centered bootstrap4 html page.</p>
                                    
                                    <div class="mt-4 pt-2">
                                        <a href="javascript:void(0)" class="btn btn-primary">Get Started</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
        </div>
        <div class="custom-shape-divider-bottom-1607944580">
            <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1200 120" preserveAspectRatio="none">
                <path d="M598.97 114.72L0 0 0 120 1200 120 1200 0 598.97 114.72z" class="shape-fill"></path>
            </svg>
        </div>
        <!-- end swiper-wrapper -->

        <!-- swipper controls -->
        <!-- <div class="swiper-pagination"></div> -->
        <div class="swiper-button-next border rounded-circle text-center"></div>
        <div class="swiper-button-prev border rounded-circle text-center"></div>
    </div>
</section>
<!--end Hero-->


<!-- Hero End -->

<section class="section bg-light pb-md-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="features-absolute rounded bg-white shadow mover">
                    <div class="row mx-0">
                        <div class="col-lg-12 col-md-12 my-0 p-2">
                            <div class="row mx-0">
                                <?php
                                foreach ($program as $item) :
                                ?>
                                    <div class="col-md-4 col-12 mx-auto">
                                        <div class="media features p-md-4 py-3">
                                            <div class="icon text-center rounded-circle text-primary mr-3 mt-2">
                                                <i class="uil uil-ruler-combined align-items-center h4 mb-0"></i>
                                            </div>
                                            <div class="media-body">
                                                <h4 class="title mb-0"><?= $item->judul ?></h4>
                                                <p class="text-muted para mb-0" style="text-align: justify;"><?= strtolower(substr(strip_tags($item->konten), 0, 200)) ?></p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end col-->
                                <?php
                                endforeach;
                                ?>
                            </div>
                            <!--end row-->
                        </div>

                        <!-- <div class="col-lg-4 d-none d-lg-block position-relative">
                            <img src="assets/layouts/images/construction/serveices.png" class="img-fluid mx-auto d-block construction-img" alt="">
                        </div> -->
                    </div>
                    <!--end row-->
                </div>
            </div>
            <!--end col-->

            <div class="col-12 mt-4 pt-2 text-center">
                <p class="text-muted mb-0" data-aos="fade-up" data-aos-duration="1400">Untuk informasi lebih lanjut mengenai program kami, silakan <a href="<?= base_url('program') ?>" class="text-primary h6">Klik disini</a> !</p>
            </div>
            <!--end col-->
        </div>
        <!--end row-->
    </div>
    <!--end container-->
</section>
<!--end section-->

<section class="section" id="tentangkami">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-5 col-md-6 col-12">
                <div class="position-relative" data-aos="fade-up" data-aos-duration="1400">
                    <img src="<?= @$profil->thumbnail ? base_url('uploads/images/profile/'.$profil->thumbnail) : base_url('assets/img/no-camera.png') ?>" class="rounded img-fluid mx-auto d-block" alt="">
                    <div class="play-icon">
                        <a href="<?= $profil->yt ?>" class="play-btn video-play-icon">
                            <i class="mdi mdi-play text-primary rounded-circle bg-white shadow"></i>
                        </a>
                    </div>
                </div>
            </div>
            <!--end col-->

            <div class="col-lg-7 col-md-6 col-12 mt-4 mt-sm-0 pt-2 pt-sm-0">
                <div class="ml-lg-5 ml-md-4">
                    <div class="section-title">
                        <span class="badge badge-pill badge-soft-primary" data-aos="fade-up" data-aos-duration="1000">Tentang Kami</span>
                        <h4 class="title mt-3 mb-4" data-aos="fade-up" data-aos-duration="1200"><?= $profil->judul ?></h4>
                        <p class="text-muted para-desc mx-auto" data-aos="fade-up" data-aos-duration="1400">
                            <!-- <?= strtolower(substr(strip_tags($profil->deskripsi), 0, 200)) ?> -->
                            <?= $profil->deskripsi; ?>
                        </p>
                        <div class="mt-4" data-aos="fade-up" data-aos-duration="1400">
                            <a href="<?= base_url('profile') ?>" class="btn btn-primary">Selengkapnya</a>
                        </div>
                    </div>
                </div>
            </div>
            <!--end col-->

        </div>
        <!--end row-->
    </div>
    <!--end container-->


    <div class="container mt-100 mt-60">
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="section-title sticky-bar position-sticky">
                    <span class="badge badge-pill badge-soft-primary">Artikel</span>
                    <?php
                    foreach ($berita_terbaru as $item) :
                    ?>
                        <h4 class="title mt-3 mb-2">Berita Terbaru</h4>
                        <p class="text-muted para-desc mb-0"><?= strtolower(substr(strip_tags($item->konten), 0, 140)) ?></p>
                        <div class="mt-4 d-none d-md-block">
                            <a href="<?= base_url('berita') ?>" class="btn btn-soft-primary">Lihat semua berita <i data-feather="arrow-right" class="fea icon-sm"></i></a>
                        </div>
                    <?php
                    endforeach;
                    ?>
                </div>
            </div>
            <!--end col-->

            <div class="col-lg-8 col-md-6 mt-4 pt-2 mt-sm-0 pt-sm-0">
                <div class="row">
                    <ul class="col container-filter list-unstyled categories-filter mb-0" id="filter" data-aos="fade-up" data-aos-duration="1000">
                        <li class="list-inline-item"><a class="categories-name border d-block text-dark rounded active" data-filter="*">Semua</a></li>
                        <?php
                        foreach ($kategori_berita as $item) :
                        ?>
                            <li class="list-inline-item"><a class="categories-name border d-block text-dark rounded" data-filter=".<?= $item->slug ?>"><?= ucfirst($item->nama) ?></a></li>
                        <?php
                        endforeach;
                        ?>
                        <!-- <li class="list-inline-item"><a class="categories-name border d-block text-dark rounded" data-filter=".buildings">Kategori B</a></li>
                        <li class="list-inline-item"><a class="categories-name border d-block text-dark rounded" data-filter=".roads">Kategori C</a></li> -->
                    </ul>
                </div>
                <!--end row-->

                <div class="row projects-wrapper" data-aos="fade-up" data-aos-duration="1400">

                    <?php
                    foreach ($berita as $item) :
                    ?>
                        <div class="col-lg-6 col-12 mt-4 pt-2 <?= $item->slug_kategori ?>">
                            <div class="card blog rounded border-0 shadow">
                                <div class="position-relative">
                                    <img src="<?= @$item->foto_thumbnail_path ? base_url($item->foto_thumbnail_path) : base_url('assets/img/no-camera.png') ?>" class="card-img-top rounded-top" alt="<?= $item->judul ?>">
                                    <div class="overlay rounded-top bg-dark"></div>
                                </div>
                                <div class="card-body content">
                                    <h5><a href="<?= base_url('berita/detail/' . $item->slug) ?>" class="card-title title text-dark"><?= $item->judul ?></a></h5>
                                    <div class="post-meta d-flex justify-content-between mt-3">
                                        <ul class="list-unstyled mb-0">
                                            <li class="list-inline-item"><a href="javascript:void(0)" class="text-muted comments"><i class="mdi mdi-calendar-check"></i> <?= tanggal_format($item->tanggal_pablis) ?></a></li>
                                        </ul>
                                        <a href="<?= base_url('berita/detail/' . $item->slug) ?>" class="text-muted readmore">Selengkapnya <i class="mdi mdi-chevron-right"></i></a>
                                    </div>
                                </div>
                                <div class="author">
                                    <small class="text-light user d-block"><i class="mdi mdi-account"></i> Admin</small>
                                </div>
                            </div>
                        </div>
                        <!--end col-->
                    <?php
                    endforeach;
                    ?>
                </div>
            </div>
        </div>
        <!--end row-->
    </div>
    <!--end container-->

</section>
<!--end section-->

<section class="section bg-light">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="section-title mb-4 pb-2 text-center">
                    <span class="badge badge-pill badge-soft-primary" data-aos="fade-up" data-aos-duration="1000">Artikel</span>
                    <h4 class="title mt-3 mb-2" data-aos="fade-up" data-aos-duration="1100">Pengumuman</h4>
                    <!-- <p class="text-muted mx-auto para-desc mb-0" data-aos="fade-up" data-aos-duration="1200">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p> -->
                </div>
            </div>
            <!--end col-->
        </div>
        <!--end row-->
        <div class="row">
            <div class="col-12 mt-4 pt-2">
                <div id="pengumuman" class="owl-carousel owl-theme" data-aos="fade-up" data-aos-duration="1400">
                    <?php
                    foreach ($pengumuman as $item) :
                    ?>
                        <div class="card customer-testi text-center border rounded mx-2">
                            <div class="card-body">
                                <img src="<?= base_url('assets/frontend/') ?>layouts/images/custom/pengumuman-grey.png" class="img-fluid avatar avatar-ex-sm mx-auto" alt="">
                                <h6 class="text-secondary mt-3"> <?= $item->judul ?> </h6>
                                <p class="text-muted">- <?= tanggal_format($item->tanggal_pablis) ?></p>
                                <a href="<?= base_url('pengumuman/detail/' . $item->slug) ?>" class="text-muted readmore">Selengkapnya <i class="mdi mdi-chevron-right"></i></a>
                            </div>
                        </div>
                    <?php
                    endforeach;
                    ?>
                </div>
            </div>
            <!--end col-->
            <div class="col-md-12 text-center mt-md-4 mt-3" data-aos="fade-up" data-aos-duration="1600">
                <a href="<?= base_url('pengumuman/index') ?>" class="btn btn-soft-primary">Lihat Semua Pengumuman <i data-feather="arrow-right" class="fea icon-sm"></i></a>
            </div>
            <!--end col-->
        </div>
        <!--end row-->

    </div>
    <!--end container-->
</section>
<!--end section-->