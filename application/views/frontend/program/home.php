<!-- Hero Start -->
<section class="bg-utama bg-half bg-light d-table w-100" style="background: url('<?= base_url('assets/frontend/') ?>layouts/images/custom/adli-wahid-oTa5NUwhj0c-unsplash.jpg') center center;">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12 text-center">
                <div class="page-next-level">
                    <h4 class="title text-white"> <?= $page_title ?> </h4>
                    <div class="page-next">
                        <nav aria-label="breadcrumb" class="d-inline-block">
                            <ul class="breadcrumb bg-white rounded shadow mb-0">
                                <?= $li_active ?>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
            <!--end col-->
        </div>
        <!--end row-->
    </div>
    <!--end container-->
</section>
<!--end section-->
<!-- Hero End -->

<!-- Shape Start -->
<div class="position-relative">
    <div class="shape overflow-hidden text-white">
        <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
        </svg>
    </div>
</div>
<!--Shape End-->

<section class="section">
    <div class="container">
        <div class="row align-items-center" id="counter">
            <div class="col-md-4">
                <img src="<?= $header_data->image_path ?>" class="img-fluid" alt="">
            </div>
            <!--end col-->

            <div class="col-md-8 mt-4 pt-2 mt-sm-0 pt-sm-0">
                <div class="ml-lg-4">
                    <div class="section-title">
                        <h4 class="title mb-4">Program Kami</h4>
                        <p class="text-muted"><?= $header_data->deskripsi ?></p>
                        <a href="#program" class="btn btn-primary mt-3">Lihat Program</a>
                    </div>
                </div>
            </div>
            <!--end col-->
        </div>
        <!--end row-->
    </div>
    <!--end container-->
</section>
<!--end section-->

<section class="section bg-light pt-md-5" id="program">

    <div class="container mt-100 mt-0">
        <div class="row align-items-end">
            <div class="col-md-12">
                <div class="section-title text-center text-md-left">
                    <h6 class="text-primary">Tentang</h6>
                    <h4 class="title">Program Kami</h4>
                </div>
            </div>

        </div>
        <!--end row-->

        <div class="row">
            <div class="col-md-4 mt-4 pt-2">
                <ul class="nav nav-pills nav-justified flex-column bg-white rounded shadow p-3 mb-0 sticky-bar" id="pills-tab" role="tablist">
                    <?php $i = 0; ?>
                    <?php foreach ($program_data as $program) : ?>
                        <li class="nav-item">
                            <a class="nav-link rounded <?= $i == 0 ? 'active' : '' ?>" id="<?= $program->slug ?>" data-toggle="pill" href="#tab-<?= $program->slug ?>" role="tab" aria-controls="tab-<?= $program->slug ?>" aria-selected="false">
                                <div class="text-center py-1">
                                    <h6 class="mb-0"><?= $program->judul ?></h6>
                                </div>
                            </a>
                            <!--end nav link-->
                        </li>
                        <!--end nav item-->
                        <?php $i++ ?>
                    <?php endforeach ?>

                </ul>
                <!--end nav pills-->
            </div>
            <!--end col-->

            <div class="col-md-8 col-12 mt-4 pt-2">
                <div class="tab-content" id="pills-tabContent">
                    <?php $i = 0; ?>
                    <?php foreach ($program_data as $program) : ?>
                        <div class="tab-pane fade bg-white <?= $i == 0 ? 'show active' : '' ?> p-4 rounded shadow" id="tab-<?= $program->slug ?>" role="tabpanel" aria-labelledby="<?= $program->slug ?>">
                            <img src="<?= base_url($program->icon_path) ?>" class="img-fluid rounded shadow" alt="">
                            <div class="mt-4">
                                <p class="text-muted"><?= $program->konten ?></p>
                            </div>
                        </div>
                        <!--end teb pane-->
                        <?php $i++ ?>
                    <?php endforeach ?>

                </div>
                <!--end tab content-->
            </div>
            <!--end col-->
        </div>
        <!--end row-->
    </div>
    <!--end container-->
</section>
<!--end section-->