<section class="bg-utama bg-half bg-light d-table w-100" style="background: url('<?= base_url('assets/frontend/') ?>layouts/images/custom/adli-wahid-oTa5NUwhj0c-unsplash.jpg') center center;">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12 text-center">
                <div class="page-next-level">
                    <h4 class="title text-white"> Kontak </h4>
                    <div class="page-next">
                        <nav aria-label="breadcrumb" class="d-inline-block">
                            <ul class="breadcrumb bg-white rounded shadow mb-0">
                               <?= $li_active ?>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>  <!--end col-->
        </div><!--end row-->
    </div> <!--end container-->
</section><!--end section-->
<!-- Hero End -->

<!-- Shape Start -->
<div class="position-relative">
    <div class="shape overflow-hidden text-white">
        <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
        </svg>
    </div>
</div>
<!--Shape End-->

<section class="section">
    <div class="container">
        <div class="row kontak-konten"> 
            <div class="col-md-4">
                <img src="<?= base_url('assets/frontend/') ?>layouts/images/custom/Illustrasi1.png" class="img-fluid w-100" alt="">
            </div>
            <div class="col-md-8 mt-md-0 mt-4">
                <div class="title-heading ml-lg-4">
                    <h4 class="mb-4">Hubungi Kami</h4>
                    <p class="text-muted"><?= $item->deskripsi?></p>
                    <div class="media contact-detail align-items-center mt-3">
                        <div class="icon">
                            <i data-feather="mail" class="fea icon-m-md text-dark mr-3"></i>
                        </div>
                        <div class="media-body content">
                            <h5 class="title font-weight-bold mb-0">Surel</h5>
                            <a href="mailto:<?= $item->email?>" class="text-primary"><?= $item->email?></a>
                        </div>
                    </div>
                    
                    <div class="media contact-detail align-items-center mt-3">
                        <div class="icon">
                            <i data-feather="phone" class="fea icon-m-md text-dark mr-3"></i>
                        </div>
                        <div class="media-body content">
                            <h5 class="title font-weight-bold mb-0">Telepon</h5>
                            <a href="tel:<?= $item->no_telp ?>" class="text-primary"><?= $item->no_telp?></a>
                        </div>
                    </div>
                    
                    <div class="media contact-detail align-items-center mt-3">
                        <div class="icon">
                            <i data-feather="map-pin" class="fea icon-m-md text-dark mr-3"></i>
                        </div>
                        <div class="media-body content">
                            <h5 class="title font-weight-bold mb-0">Lokasi</h5>
                            <a class="text-primary"><?= $item->alamat?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-6">
                <div class="card border-0 text-center features feature-clean">
                    <div class="icons text-primary text-center mx-auto">
                        <i class="uil uil-facebook d-block rounded h3 mb-0"></i>
                    </div>
                    <div class="content mt-3">
                        <h5 class="font-weight-bold">Facebook</h5>
                        <a href="javascript:void(0)" class="text-primary"><?= $item->fb?></a>
                    </div>
                </div>
            </div><!--end col-->
            
            <div class="col-md-3 col-6">
                <div class="card border-0 text-center features feature-clean">
                    <div class="icons text-primary text-center mx-auto">
                        <i class="uil uil-instagram d-block rounded h3 mb-0"></i>
                    </div>
                    <div class="content mt-3">
                        <h5 class="font-weight-bold">Instagram</h5>
                        <a href="javascript:void(0)" class="text-primary"><?= $item->ig?></a>
                    </div>
                </div>
            </div><!--end col-->
            
            <div class="col-md-3 col-6 mt-4 mt-sm-0 pt-2 pt-sm-0">
                <div class="card border-0 text-center features feature-clean">
                    <div class="icons text-primary text-center mx-auto">
                        <i class="uil uil-twitter d-block rounded h3 mb-0"></i>
                    </div>
                    <div class="content mt-3">
                        <h5 class="font-weight-bold">Twitter</h5>
                        <a href="javascript:void(0)" class="text-primary"><?= $item->twitter?></a>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-6 mt-4 mt-sm-0 pt-2 pt-sm-0">
                <div class="card border-0 text-center features feature-clean">
                    <div class="icons text-primary text-center mx-auto">
                        <i class="uil uil-youtube d-block rounded h3 mb-0"></i>
                    </div>
                    <div class="content mt-3">
                        <h5 class="font-weight-bold">YouTube</h5>
                        <a href="javascript:void(0)" class="text-primary"><?= $item->yt?></a>
                    </div>
                </div>
            </div>
        </div><!--end col-->
    </div><!--end row-->
</div><!--end container-->


</section>