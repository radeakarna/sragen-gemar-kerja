<!-- Hero Start -->
<section class="bg-utama bg-half bg-light d-table w-100" style="background: url('<?= base_url('assets/frontend/') ?>layouts/images/custom/adli-wahid-oTa5NUwhj0c-unsplash.jpg') center center;">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12 text-center">
                <div class="page-next-level">
                    <h4 class="title text-white"> <?= $page_title ?></h4>
                    <div class="page-next">
                        <nav aria-label="breadcrumb" class="d-inline-block">
                            <ul class="breadcrumb bg-white rounded shadow mb-0">
                                <?= $li_active ?>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>  <!--end col-->
        </div><!--end row-->
    </div> <!--end container-->
</section><!--end section-->
<!-- Hero End -->

<!-- Shape Start -->
<div class="position-relative">
    <div class="shape overflow-hidden text-white">
        <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
        </svg>
    </div>
</div>
<!--Shape End-->

<!-- Blog STart -->
<section class="section">
    <div class="container">
        <div class="row">
            <!-- BLog Start -->
            <div class="col-lg-8 col-md-6">
                <?= @$this->input->get('kunci') ? '<h5 class="widget-title">Hasil Pencarian Untuk '.ucwords($this->input->get('kunci')).'</h5><br>' : ''?>
                <div class="row">
                    
                    <?php
                    foreach($berita as $item):
                    ?>
                    
                    <div class="col-lg-6 col-md-12 mb-4 pb-2">
                            <div class="card blog rounded border-0 shadow">
                                <div class="position-relative">
                                    <img src="<?= @$item->foto_thumbnail_path? base_url($item->foto_thumbnail_path) : base_url('assets/img/no-camera.png') ?>" class="card-img-top rounded-top" alt="<?= $item->judul ?>">
                                <div class="overlay rounded-top bg-dark"></div>
                                </div>
                                <div class="card-body content">
                                    <h5><a href="<?= base_url('berita/detail/'.$item->slug) ?>" class="card-title title text-dark"><?= $item->judul ?></a></h5>
                                    <div class="post-meta d-flex justify-content-between mt-3">
                                        <ul class="list-unstyled mb-0">
                                            <li class="list-inline-item"><a href="javascript:void(0)" class="text-muted comments"><i class="mdi mdi-calendar-check"></i> <?= tanggal_format($item->tanggal_pablis) ?> </a></li>
                                        </ul>
                                        <a href="<?= base_url('berita/detail/'.$item->slug) ?>" class="text-muted readmore">Selengkapnya <i class="mdi mdi-chevron-right"></i></a>
                                    </div>
                                </div>
                                <div class="author">
                                    <small class="text-light user d-block"><i class="mdi mdi-account"></i> Admin</small>
                                </div>
                            </div>
                    </div><!--end col-->
                    
                    <?php 
                    endforeach;
                    ?>

                    <!-- PAGINATION START -->
                    <div class="col-12">
                    
                        <ul class="pagination justify-content-center mb-0">
                            <?= $pagination ?>        
                            <!-- <li class="page-item"><a class="page-link" href="javascript:void(0)" aria-label="Previous"><i class="mdi mdi-arrow-left"></i></a></li>
                            <li class="page-item active"><a class="page-link" href="javascript:void(0)">1</a></li>
                            <li class="page-item"><a class="page-link" href="javascript:void(0)">2</a></li>
                            <li class="page-item"><a class="page-link" href="javascript:void(0)">3</a></li>
                            <li class="page-item"><a class="page-link" href="javascript:void(0)" aria-label="Next"><i class="mdi mdi-arrow-right"></i></a></li> -->
                        </ul>
                    </div><!--end col-->
                    <!-- PAGINATION END -->
                </div><!--end row-->
            </div><!--end col-->
            <!-- BLog End -->

            <!-- START SIDEBAR -->
            <div class="col-lg-4 col-md-6 col-12 mt-4 mt-sm-0 pt-2 pt-sm-0">
                <div class="card border-0 sidebar sticky-bar rounded shadow">
                    <div class="card-body">
                        <!-- SEARCH -->
                        <div class="widget mb-4 pb-2">
                            <h5 class="widget-title">Cari Berita</h5>
                            <div id="search2" class="widget-search mt-4 mb-0">
                                <form role="search" method="get" id="searchform" class="searchform" action="<?= base_url('berita/cari') ?>">
                                    <div>
                                        <input type="text" class="border rounded" name="kunci" id="s" placeholder="Masukkan kata kunci...">
                                        <input type="submit" id="searchsubmit" value="Search">
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- SEARCH -->


                        <!-- Categories -->
                        <div class="widget mb-4 pb-2">
                            <h5 class="widget-title">Kategori</h5>
                            <ul class="list-unstyled mt-4 mb-0 blog-categories">
                                <?php
                                foreach($kategori as $item):
                                    $active = '';
                                    $active = @$this->uri->segment(3) == $item->slug ? $active = 'class="aktip-kategori"' : '';
                                ?>
                                <li><a href="<?= base_url('berita/kategori/'.$item->slug) ?>" <?= $active ?>><?= $item->nama ?></a> <span class="float-right"></span></li>
                                <?php 
                                endforeach;
                                ?>
                            </ul>
                        </div>
                        <!-- Categories -->

                        <!-- Postingan Terbaru -->
                        <div class="widget mb-4 pb-2">
                            <h5 class="widget-title">Postingan Terbaru</h5>
                            <div class="mt-4">
                                <?php
                                foreach($berita_terbaru as $item):
                                ?>
                                <div class="clearfix post-recent">
                                    <div class="post-recent-thumb float-left"> <a href="<?= base_url('berita/detail/'.$item->slug) ?>"> <img alt="<?= $item->judul ?>" src="<?= @$item->foto_thumbnail_path ? base_url($item->foto_thumbnail_path) : base_url('assets/img/no-camera.png') ?>" class="img-fluid rounded"></a></div>
                                    <div class="post-recent-content float-left"><a href="<?= base_url('berita/detail/'.$item->slug) ?>"><?= $item->judul ?></a><span class="text-muted mt-2"><?= date('d/m/y', strtotime($item->tanggal_pablis)) ?></span></div>
                                </div>
                                <?php 
                                endforeach;
                                ?>
                            </div>
                        </div>
                        <!-- Postingan Terbaru -->

                        
                        <!-- SOCIAL -->
                        <div class="widget">
                            <h5 class="widget-title">Ikuti Kami</h5>
                            <ul class="list-unstyled social-icon mb-0 mt-4">
                            <li class="list-inline-item"><a href="<?= kontak('fb') ?>" class="rounded"><i data-feather="facebook" class="fea icon-sm fea-social"></i></a></li>
                            <li class="list-inline-item"><a href="<?= kontak('ig') ?>" class="rounded"><i data-feather="instagram" class="fea icon-sm fea-social"></i></a></li>
                            <li class="list-inline-item"><a href="<?= kontak('twitter') ?>" class="rounded"><i data-feather="twitter" class="fea icon-sm fea-social"></i></a></li>
                            <li class="list-inline-item"><a href="<?= kontak('yt') ?>" class="rounded"><i data-feather="youtube" class="fea icon-sm fea-social"></i></a></li>
                            </ul><!--end icon-->
                        </div>
                        <!-- SOCIAL -->
                    </div>
                </div>
            </div><!--end col-->
            <!-- END SIDEBAR -->
        </div><!--end row-->
    </div><!--end container-->
</section><!--end section-->
<!-- Blog End -->