<section class="bg-utama bg-half bg-light d-table w-100" style="background: url('<?= base_url('assets/frontend/') ?>layouts/images/custom/adli-wahid-oTa5NUwhj0c-unsplash.jpg') center center;">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12 text-center">
                <div class="page-next-level">
                    <h4 class="title text-white"> Struktur Organisasi </h4>
                    <div class="page-next">
                        <nav aria-label="breadcrumb" class="d-inline-block">
                            <ul class="breadcrumb bg-white rounded shadow mb-0">
                               <?= $li_active ?>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>  <!--end col-->
        </div><!--end row-->
    </div> <!--end container-->
</section><!--end section-->
<!-- Hero End -->

<!-- Shape Start -->
<div class="position-relative">
    <div class="shape overflow-hidden text-white">
        <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
        </svg>
    </div>
</div>
<!--Shape End-->

<!-- Blog STart -->
<section class="section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-10 col-md-10">
                <div class="section-title">
                    <div class="text-center">
                        <h4 class="title mb-4"><?= $item->judul?></h4>
                        <img src="<?= base_url('uploads/images/struktur_organisasi/'.$item->bagan)?>" class="img-fluid rounded-md shadow-md" alt="">
                    </div>
                    <p class="text-muted mb-0 mt-4"><?= $item->deskripsi?></p>
                    
                    <!-- <h4 class="my-4">Challenges</h4>
                    <p class="text-muted">Due to its widespread use as filler text for layouts, non-readability is of great importance: human perception is tuned to recognize certain patterns and repetitions in texts.</p>
                    <p class="text-muted mb-0">For this reason, dummy text usually consists of a more or less random series of words or syllables. This prevents repetitive patterns from impairing the overall visual impression and facilitates the comparison of different typefaces.</p>

                    <h4 class="my-4">Solutions</h4>
                    <p class="text-muted">Furthermore, it is advantageous when the dummy text is relatively realistic so that the layout impression of the final publication is not compromised.</p>
                    <p class="text-muted mb-0">One disadvantage of Lorum Ipsum is that in Latin certain letters appear more frequently than others - which creates a distinct visual impression. Moreover, in Latin only words at the beginning of sentences are capitalized.</p> -->

                </div>
            </div><!--end col-->
        </div><!--end row-->
    </div><!--end container-->
</section>