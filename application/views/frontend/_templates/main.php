<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$this->load->view('frontend/_templates/head');
@$header ? $this->load->view('frontend/_templates/home-header'):
    $this->load->view('frontend/_templates/header');
if(is_file('./application/views/'.@$content.'.php')){
    $this->load->view($content);
} else{
    $this->load->view('frontend/_templates/blank');
}
$this->load->view('frontend/_templates/footer');
