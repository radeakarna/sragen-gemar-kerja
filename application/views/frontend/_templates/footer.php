<footer class="skin-dark-footer dark-footer">
    <div>
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-md-offset-1">
                    <div class="footer-widget">
                        <img
                            src="<?= base_url('assets/frontend/') ?>assets/img/gemar-kerja-dark.png"
                            class="img-footer"
                            alt=""
                            />
                        <div class="footer-add">
                            <p>
                                Collins Street West, Victoria,<br />
                                Australia (AU4578).
                            </p>
                            <p><strong>Email:</strong><br />hello@jobstock.com</p>
                            <p><strong>Call:</strong><br />91 855 742 62548</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2">
                    <div class="footer-widget">
                        <h4 class="widget-title">Navigations</h4>
                        <ul class="footer-menu">
                            <li><a href="home-6.html">New Home Design</a></li>
                            <li>
                                <a href="browse-candidate-list.html">Browse Candidates</a>
                            </li>
                            <li>
                                <a href="browse-employer-list.html">Browse Employers</a>
                            </li>
                            <li><a href="advance-search-2.html">Advance Search</a></li>
                            <li><a href="checkout.html">Job With Map</a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-2 col-md-2">
                    <div class="footer-widget">
                        <h4 class="widget-title">The Highlights</h4>
                        <ul class="footer-menu">
                            <li><a href="index-2.html">Home Page 2</a></li>
                            <li><a href="index-3.html">Home Page 3</a></li>
                            <li><a href="index-4.html">Home Page 4</a></li>
                            <li><a href="index-5.html">Home Page 5</a></li>
                            <li><a href="login.html">LogIn</a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-2 col-md-2">
                    <div class="footer-widget">
                        <h4 class="widget-title">My Account</h4>
                        <ul class="footer-menu">
                            <li><a href="candidate-dashboard.html">Dashboard</a></li>
                            <li><a href="applications.html">Applications</a></li>
                            <li><a href="packages.html">Packages</a></li>
                            <li><a href="candidate-resume.html">resume.html</a></li>
                            <li><a href="register.html">SignUp Page</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer-bottom">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 col-md-6">
                    <p class="mb-0">© 2021 Dinas Tenaga Kerja Sragen</p>
                </div>

                <div class="col-lg-6 col-md-6 text-right">
                    <ul class="footer-bottom-social">
                        <li>
                            <a href="#"><i class="ti-facebook"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="ti-twitter"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="ti-instagram"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="ti-linkedin"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- ============================ Footer End ================================== -->

<!-- Signin Window Code -->
<div class="modal fade" id="signup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">

                <div class="new-logwrap">

                    <div class="form-group">
                        <label>Username</label>
                        <div class="input-with-icon">
                            <input type="text" class="form-control" placeholder="Enter Your Username">
                            <i class="theme-cl ti-user"></i>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Email</label>
                        <div class="input-with-icon">
                            <input type="email" class="form-control" placeholder="Enter Your Email">
                            <i class="theme-cl ti-email"></i>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Password</label>
                        <div class="input-with-icon">
                            <input type="password" class="form-control" placeholder="Enter Your Password">
                            <i class="theme-cl ti-lock"></i>
                        </div>
                    </div>

                    <div class="form-groups">
                        <button type="submit" class="btn btn-primary theme-bg full-width">Register</button>
                    </div>

                    <div class="forget-account text-center">
                        <a class="theme-cl" href="#">Forget Password?</a>
                    </div>

                    <div class="social-devider">
                        <span class="line"></span>
                        <span class="circle">Or</span>
                    </div>

                    <div class="social-login row">

                        <div class="col-md-6">
                            <a href="#" class="jb-btn-icon social-login-facebook"><i class="fa fa-facebook"></i>Facebook</a>
                        </div>

                        <div class="col-md-6">
                            <a href="#" class="jb-btn-icon social-login-google"><i class="fa fa-google-plus"></i>Google</a>
                        </div>

                        <div class="col-md-6">
                            <a href="#" class="jb-btn-icon social-login-twitter"><i class="fa fa-twitter"></i>Twitter</a>
                        </div>

                        <div class="col-md-6">
                            <a href="#" class="jb-btn-icon social-login-linkedin"><i class="fa fa-linkedin"></i>Linkedin</a>
                        </div>

                    </div>

                    <div class="register-account text-center">
                        Don't have an account? <a class="theme-cl" href="register.html">Register</a>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>   
<!-- End Signin Window -->

<button class="w3-button w3-teal w3-xlarge w3-right" onclick="openRightMenu()"><i class="spin fa fa-cog" aria-hidden="true"></i></button>
<div class="w3-sidebar w3-bar-block w3-card-2 w3-animate-right" style="display:none;right:0;" id="rightMenu">
    <button onclick="closeRightMenu()" class="w3-bar-item w3-button w3-large">Close &times;</button>
    <ul id="styleOptions" title="switch styling">
        <li>
            <a href="javascript: void(0)" class="cl-box blue" data-theme="colors/blue-style"></a>
        </li>
        <li>
            <a href="javascript: void(0)" class="cl-box red" data-theme="colors/red-style"></a>
        </li>
        <li>
            <a href="javascript: void(0)" class="cl-box purple" data-theme="colors/purple-style"></a>
        </li>
        <li>
            <a href="javascript: void(0)" class="cl-box green" data-theme="colors/green-style"></a>
        </li>
        <li>
            <a href="javascript: void(0)" class="cl-box dark-red" data-theme="colors/dark-red-style"></a>
        </li>
        <li>
            <a href="javascript: void(0)" class="cl-box orange" data-theme="colors/orange-style"></a>
        </li>
        <li>
            <a href="javascript: void(0)" class="cl-box sea-blue" data-theme="colors/sea-blue-style "></a>
        </li>
        <li>
            <a href="javascript: void(0)" class="cl-box pink" data-theme="colors/pink-style"></a>
        </li>
    </ul>
</div>

<!-- Scripts
================================================== -->
<script type="text/javascript" src="<?= base_url('assets/frontend/') ?>assets/plugins/js/jquery.min.js"></script>
<script type="text/javascript" src="<?= base_url('assets/frontend/') ?>assets/plugins/js/viewportchecker.js"></script>
<script type="text/javascript" src="<?= base_url('assets/frontend/') ?>assets/plugins/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?= base_url('assets/frontend/') ?>assets/plugins/js/bootsnav.js"></script>
<script type="text/javascript" src="<?= base_url('assets/frontend/') ?>assets/plugins/js/select2.min.js"></script>
<script type="text/javascript" src="<?= base_url('assets/frontend/') ?>assets/plugins/js/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="<?= base_url('assets/frontend/') ?>assets/plugins/js/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="<?= base_url('assets/frontend/') ?>assets/plugins/js/datedropper.min.js"></script>
<script type="text/javascript" src="<?= base_url('assets/frontend/') ?>assets/plugins/js/dropzone.js"></script>
<script type="text/javascript" src="<?= base_url('assets/frontend/') ?>assets/plugins/js/loader.js"></script>
<script type="text/javascript" src="<?= base_url('assets/frontend/') ?>assets/plugins/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?= base_url('assets/frontend/') ?>assets/plugins/js/slick.min.js"></script>
<script type="text/javascript" src="<?= base_url('assets/frontend/') ?>assets/plugins/js/gmap3.min.js"></script>
<script type="text/javascript" src="<?= base_url('assets/frontend/') ?>assets/plugins/js/jquery.easy-autocomplete.min.js"></script>
<!-- Custom Js -->
<script src="<?= base_url('assets/frontend/') ?>assets/js/custom.js"></script><script type="text/javascript" src="<?= base_url('assets/frontend/') ?>assets/plugins/js/counterup.min.js"></script>

<script src="<?= base_url('assets/frontend/') ?>assets/js/jQuery.style.switcher.js"></script>
<script type="text/javascript">
            const csrf = {
                token_name: '<?=$this->security->get_csrf_token_name()?>',
                hash: '<?=$this->security->get_csrf_hash()?>'
            } 
        </script>
            <?php
            
                @$toastr ? $this->load->view('_templates/js/toastr'):'';
               @$sweet_alert ? $this->load->view('_templates/js/sweet-alert'):'';
               @$leaflet ? $this->load->view('_templates/js/leaflet'):'';
               @$select2 ? $this->load->view('_templates/js/select2'):'';
               @$dropzone ? $this->load->view('_templates/js/dropzone'):'';
               @$highchart ? $this->load->view('_templates/js/highchart'):'';
               @$datatables_js ? $this->load->view('_templates/js/datatables'):'';
               @$sumernote ? $this->load->view('_templates/js/sumernote'):'';
               @$slick ? $this->load->view('_templates/js/slick'):'';
               @$script ? $this->load->view($script):'';
               

             ?>
        
<script type="text/javascript">
            $(document).ready(function () {
                $('#styleOptions').styleSwitcher();
            });
</script>
<script>
    function openRightMenu() {
        document.getElementById("rightMenu").style.display = "block";
    }

    function closeRightMenu() {
        document.getElementById("rightMenu").style.display = "none";
    }
</script>

</div>
</body>

<!-- Mirrored from codeminifier.com/job-stock-5.4.1/job-stock/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 08 Oct 2021 01:48:28 GMT -->
</html>