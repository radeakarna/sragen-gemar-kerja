<!-- Navbar STart -->
<header id="topnav" class="defaultscroll sticky">
    <div class="container">
        <!-- Logo container-->
        <div>
            <a class="logo" href="<?= base_url() ?>">
                <img src="<?= base_url(get_logo()->logo_dark) ?>" class="l-dark logo-nav" alt="">
                <img src="<?= base_url(get_logo()->logo_white) ?>" class="l-light logo-nav" alt="">
                <!-- <img src="assets/layouts/images/custom/logo-color.png" class="l-dark logo-nav" alt="">
                <img src="assets/layouts/images/custom/logo-white.png" class="l-light logo-nav" alt=""> -->
            </a>
        </div>
        <!-- End Logo container-->
        <div class="menu-extras">
            <div class="menu-item">
                <!-- Mobile menu toggle-->
                <a class="navbar-toggle">
                    <div class="lines mr-1">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </a>
                <!-- End mobile menu toggle-->
            </div>
        </div>

        <div id="navigation">
            <!-- Navigation Menu-->
            <!-- Navigation Menu-->
            <ul class="navigation-menu nav-light">
                <li class="ml-md-auto"><a href="<?= base_url('home') ?>">Beranda</a></li>
                <li><a href="<?= base_url('berita') ?>">Berita</a></li>
                <li><a href="<?= base_url('pengumuman') ?>">Pengumuman</a></li>
                <li><a href="<?= base_url('galeri') ?>">Galeri</a></li>
                <li class="has-submenu">
                    <a href="javascript:void(0)">Profil</a><span class="menu-arrow"></span>
                    <ul class="submenu">
                        <li><a href="<?= base_url('profile') ?>">Tentang Kami</a></li>
                        <li><a href="<?= base_url('kontak') ?>">Kontak</a></li>
                        <li><a href="<?= base_url('strukturOrganisasi') ?>">Struktur Organisasi</a></li>
                    </ul>
                </li>
            </ul>
            <!--end navigation menu-->
        </div>
        <!--end navigation-->
    </div>
    <!--end container-->
</header>
<!--end header-->
<!-- Navbar End -->