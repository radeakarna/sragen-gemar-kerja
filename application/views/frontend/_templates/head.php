<!doctype html>
<html lang="en">
    <head>
        <!-- Basic Page Needs
        ================================================== -->
        <title>Gemar Kerja</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <!-- CSS
        ================================================== -->
        <link rel="stylesheet" href="<?= base_url('assets/frontend/') ?>assets/plugins/css/plugins.css">
        <link href="<?= base_url('assets/frontend/') ?>assets/css/style.css" rel="stylesheet">
        <link href="<?= base_url('assets/frontend/') ?>assets/css/custom.css" rel="stylesheet" />

        <link type="text/css" rel="stylesheet" id="jssDefault" href="<?= base_url('assets/frontend/') ?>assets/css/colors/green-style.css">
         <?php @$select2 ? $this->load->view('_templates/css/select2'):''; ?> 
        <?php @$datatables_css ? $this->load->view('_templates/css/datatables'):''; ?> 
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/js/plugins/dropzonejs/min/dropzone.min.css">

        <?php @$toastr ? $this->load->view('_templates/css/toastr'):''; ?> 
        <?php @$sweet_alert ? $this->load->view('_templates/css/sweet-alert'):''; ?> 
        <?php @$leaflet ? $this->load->view('_templates/css/leaflet'):''; ?> 
        <?php @$dropzone ? $this->load->view('_templates/css/dropzone'):''; ?> 
        <?php @$sumernote ? $this->load->view('_templates/css/sumernote'):''; ?> 
        <?php @$slick ? $this->load->view('_templates/css/slick'):''; ?> 
    </head>

   