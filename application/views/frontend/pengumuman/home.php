<!-- Hero Start -->
<section class="bg-utama bg-half bg-light d-table w-100" style="background: url('<?= base_url('assets/frontend/') ?>layouts/images/custom/adli-wahid-oTa5NUwhj0c-unsplash.jpg') center center;">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12 text-center">
                <div class="page-next-level">
                    <h4 class="title text-white"> <?= $page_title ?> </h4>
                    <div class="page-next">
                        <nav aria-label="breadcrumb" class="d-inline-block">
                            <ul class="breadcrumb bg-white rounded shadow mb-0">
                                <?= $li_active ?>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>  <!--end col-->
        </div><!--end row-->
    </div> <!--end container-->
</section><!--end section-->
<!-- Hero End -->

<!-- Shape Start -->
<div class="position-relative">
    <div class="shape overflow-hidden text-white">
        <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
        </svg>
    </div>
</div>
<!--Shape End-->


<!-- Start Forums -->
<section class="section">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 order-md-1 order-2">
                <div class="rounded shadow">
                    <div class="d-flex justify-content-between p-4 bg-light">
                        <?= @$this->input->get('kunci') ? '<h6 class="mb-0">Hasil Pencarian Untuk '.ucwords($this->input->get('kunci')).'</h6>' : '<h6 class="mb-0">Daftar Pengumuman</h6>'?>
                    </div>
                    
                    <?php
                    foreach($pengumuman as $item):
                    ?>
                    <div class="p-4">
                        <div class="d-md-flex d-block justify-content-between">
                            <div class="media align-items-center judul-peng">
                                <a class="pr-3 img-peng" href="<?= base_url('pengumuman/detail/'.$item->slug) ?>">
                                    <img src="<?= base_url('assets/frontend/') ?>layouts/images/custom/kemenparekraf.png" class="img-fluid avatar avatar-md-sm rounded-circle shadow" alt="img">
                                </a>
                                <div class="commentor-detail ket-peng">
                                    <h6 class="mb-0"><a href="<?= base_url('pengumuman/detail/'.$item->slug) ?>" class="media-heading text-dark"><?= $item->judul ?></a></h6>
                                    <small class="text-muted">oleh Admin </small>
                                </div>
                            </div>
                            <div class="d-md-block d-flex tgl-peng">
                                <div class="text-muted"><?= tanggal_format($item->tanggal_pablis) ?> </div> 
                                <!-- <div class="text-muted ml-md-0 ml-auto">03:44 WIB</div> -->
                            </div>
                        </div>
                        <div class="mt-3">
                            <p class="text-muted mb-0">
                            <?= strtolower(substr(strip_tags($item->konten), 0, 200)) ?>
                            </p>
                        </div>
                        <div class="mt-1 w-100 d-flex">
                            <a href="<?= base_url('pengumuman/detail/'.$item->slug) ?>" class="ml-auto text-muted readmore">Selengkapnya <i class="mdi mdi-chevron-right"></i></a>
                        </div>
                    </div>
                    <?php 
                    endforeach;
                    ?>

                </div>

                <!-- PAGINATION START -->
                <div class="w-100 mt-md-5 mt-5">
                    <ul class="pagination justify-content-center mb-0">
                        <?= $pagination ?>
                    </ul>
                </div><!--end col-->
                <!-- PAGINATION END -->

            </div><!--end col-->

            <div class="col-lg-4 mt-0 mt-lg-0 pt-2 pt-lg-0 order-md-2 order-1 mb-md-0 mb-4">
                <div class="card border-0 sidebar sticky-bar rounded shadow bg-light">
                    <div class="card-body">
                        <div class="widget">
                            <img class="w-100" src="<?= base_url('assets/frontend/') ?>layouts/images/custom/Illustrasi1.png" alt="">
                        </div>
                        
                        <!-- SEARCH -->
                        <div class="widget my-md-3 my-4 pb-2">
                            <div id="search2" class="widget-search mb-0">
                                <form role="search" method="get" id="searchform" class="searchform" action="<?= base_url('pengumuman/cari') ?>">
                                    <div>
                                        <input type="text" class="border rounded" name="kunci" id="s" placeholder="Masukkan kata kunci...">
                                        <input type="submit" id="searchsubmit" value="Search">
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- SEARCH -->
                        
                        <!-- SOCIAL -->
                        <div class="widget">
                            <h5 class="widget-title">Follow us</h5>
                            <ul class="list-unstyled social-icon social mb-0 mt-4">
                                <li class="list-inline-item"><a href="<?= kontak('fb') ?>" class="rounded"><i data-feather="facebook" class="fea icon-sm fea-social"></i></a></li>
                                <li class="list-inline-item"><a href="<?= kontak('ig') ?>" class="rounded"><i data-feather="instagram" class="fea icon-sm fea-social"></i></a></li>
                                <li class="list-inline-item"><a href="<?= kontak('twitter') ?>" class="rounded"><i data-feather="twitter" class="fea icon-sm fea-social"></i></a></li>
                                <li class="list-inline-item"><a href="<?= kontak('yt') ?>" class="rounded"><i data-feather="youtube" class="fea icon-sm fea-social"></i></a></li>
                            </ul><!--end icon-->
                        </div>
                        <!-- SOCIAL -->
                    </div>
                </div>
            </div><!--end col-->
        </div><!--end row-->
    </div><!--end container-->
</section><!--end section-->