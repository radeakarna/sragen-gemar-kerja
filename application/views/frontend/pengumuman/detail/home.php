 <!-- Hero Start -->
 <section class="bg-utama bg-half bg-light d-table w-100" style="background: url('<?= base_url('assets/frontend/') ?>layouts/images/custom/adli-wahid-oTa5NUwhj0c-unsplash.jpg') center center;">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12 text-center">
                <div class="page-next-level">
                    <h4 class="title text-white"> <?= $page_title ?> </h4>
                    <div class="page-next">
                        <nav aria-label="breadcrumb" class="d-inline-block">
                            <ul class="breadcrumb bg-white rounded shadow mb-0">
                                <?= $li_active ?>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>  <!--end col-->
        </div><!--end row-->
    </div> <!--end container-->
</section><!--end section-->
<!-- Hero End -->

<!-- Shape Start -->
<div class="position-relative">
    <div class="shape overflow-hidden text-white">
        <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
        </svg>
    </div>
</div>
<!--Shape End-->

 <!-- Blog STart -->
<!-- Start Forums -->
<section class="section">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-6">

                <div class="section-title kontennya">
                    <div class="text-left">
                        <h4 class="title mb-4"><?= $pengumuman->judul ?></h4>
                        <!-- <img src="assets/layouts/images/custom/struk-org.png" class="img-fluid rounded-md shadow-md" alt=""> -->
                    </div>
                    <p class="text-muted mb-0 mt-4">
                        <?php 
                            $konten = $pengumuman->konten ;
                            $konten = str_replace('&lt;', '<', $pengumuman->konten );
                            $konten = str_replace('&gt;', '>', $konten );
                            echo $konten;
                        ?>
                    </p>
                </div>
            </div><!--end col-->

            <!-- START SIDEBAR -->
            <div class="col-lg-4 col-md-6 col-12 mt-4 mt-sm-0 pt-2 pt-sm-0">
                <div class="card border-0 sidebar sticky-bar rounded shadow">
                    <div class="card-body">
                        <!-- SEARCH -->
                        <div class="widget mb-4 pb-2">
                            <h5 class="widget-title">Cari Pengumuman</h5>
                            <div id="search2" class="widget-search mt-4 mb-0">
                                <form role="search" method="get" id="searchform" class="searchform" action="<?= base_url('pengumuman/cari') ?>">
                                    <div>
                                        <input type="text" class="border rounded" name="kunci" id="s" placeholder="Masukkan kata kunci...">
                                        <input type="submit" id="searchsubmit" value="Search">
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- SEARCH -->

                        <!-- Categories -->
                        <div class="widget mb-4 pb-2">
                            <h5 class="widget-title">Kategori</h5>
                            <ul class="list-unstyled mt-4 mb-0 blog-categories">
                                <?php
                                foreach($kategori as $item):
                                ?>
                                <li><a href="<?= base_url('berita/kategori/'.$item->slug) ?>"><?= $item->nama ?></a> <span class="float-right"></span></li>
                                <?php 
                                endforeach;
                                ?>
                            </ul>
                        </div>
                        <!-- Categories -->

                        <!-- Postingan Terbaru -->
                        <div class="widget mb-4 pb-2">
                            <h5 class="widget-title">Postingan Terbaru</h5>
                            <div class="mt-4">
                                <?php
                                foreach($berita_terbaru as $item):
                                ?>
                                <div class="clearfix post-recent pengumumannya">
                                    <div class="post-recent-thumb float-left"> <a href="<?= base_url('pengumuman/detail/'.$item->slug) ?>"> <img alt="<?= $item->judul ?>" src="<?= base_url('assets/frontend/layouts/images/custom/pengumuman-grey.png') ?>" class="img-fluid"></a></div>
                                    <div class="post-recent-content float-left"><a href="<?= base_url('pengumuman/detail/'.$item->slug) ?>"><?= $item->judul ?></a><span class="text-muted mt-2"><?= date('d/m/y', strtotime($item->tanggal_pablis)) ?></span></div>
                                </div>
                                <?php 
                                endforeach;
                                ?>
                            </div>
                        </div>
                        <!-- Postingan Terbaru -->

                        
                        <!-- SOCIAL -->
                        <div class="widget">
                            <h5 class="widget-title">Ikuti Kami</h5>
                            <ul class="list-unstyled social-icon mb-0 mt-4">
                                <li class="list-inline-item"><a href="<?= kontak('fb') ?>" class="rounded"><i data-feather="facebook" class="fea icon-sm fea-social"></i></a></li>
                                <li class="list-inline-item"><a href="<?= kontak('ig') ?>" class="rounded"><i data-feather="instagram" class="fea icon-sm fea-social"></i></a></li>
                                <li class="list-inline-item"><a href="<?= kontak('twitter') ?>" class="rounded"><i data-feather="twitter" class="fea icon-sm fea-social"></i></a></li>
                                <li class="list-inline-item"><a href="<?= kontak('yt') ?>" class="rounded"><i data-feather="youtube" class="fea icon-sm fea-social"></i></a></li>
                            </ul><!--end icon-->
                        </div>
                        <!-- SOCIAL -->
                    </div>
                </div>
            </div><!--end col-->
            <!-- END SIDEBAR -->
        </div><!--end row-->
    </div><!--end container-->
</section><!--end section-->