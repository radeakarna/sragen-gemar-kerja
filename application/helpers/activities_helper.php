<?php
if (!function_exists('activities')) {
    function activities($class = NULL, $function = NULL) {
        $CI =& get_instance();
        $log = [];
    	$log['subject'] = 'LOG';
    	$log['url'] = $_SERVER['HTTP_REFERER'] ?? NULL;
    	$log['method'] = $_SERVER['REQUEST_METHOD'] ?? NULL;
    	$log['ip'] = $_SERVER['REMOTE_ADDR'] ?? NULL;
    	$log['agent'] = $_SERVER['HTTP_USER_AGENT'] ?? NULL;
        $log['user_id'] = $_SESSION['id'] ??NULL;
        $log['request_time'] = $_SERVER['REQUEST_TIME_FLOAT']?date('Y-m-d H:i:s',$_SERVER['REQUEST_TIME_FLOAT']):NULL;
        $log['class'] = $class;
        $log['function'] = $function;
        if (!@$CI->session->userdata($_SERVER['REMOTE_ADDR'])) {
                $log['details'] = file_get_contents('https://ipinfo.io/?token=f603ba50256063')??NULL;
                $CI->session->set_userdata($_SERVER['REMOTE_ADDR'],$log['details']);
        } else {
            if ($CI->session->userdata($_SERVER['REMOTE_ADDR']) == $_SERVER['REMOTE_ADDR']) {
                $log['details'] = $CI->session->userdata($_SERVER['REMOTE_ADDR']);
            } else {
                $log['details'] = file_get_contents('https://ipinfo.io/?token=f603ba50256063')??NULL;
                $CI->session->set_userdata($_SERVER['REMOTE_ADDR'],$log['details']);
            }
        }
        $CI->db->insert('log_activities', $log);
    }
}