<?php
function count_users()
{
    $return = 0;
    $CI = &get_instance();
    $get = $CI->db->select('count(*) as jumlah')->where(['is_active' => '1', 'deleted_at is NULL'])->get('users')->row()->jumlah;
    $return = $get;

    return $return;
}

function count_galeri()
{
    $return = 0;
    $CI = &get_instance();
    $get = $CI->db->select('count(*) as jumlah')->where(['dihapus_pada' => null,'dihapus_oleh' => null])->get('galeri')->row()->jumlah;
    $return = $get;

    return $return;
}

function count_kategori_berita()
{
    $return = 0;
    $CI = &get_instance();
    $get = $CI->db->select('count(*) as jumlah')->where(['dihapus_pada' => null,'dihapus_oleh' => null])->get('kategori')->row()->jumlah;
    $return = $get;

    return $return;
}

function count_kategori_berita_non_aktif()
{
    $return = 0;
    $CI = &get_instance();
    $get = $CI->db->select('count(*) as jumlah')->where(['status' => '0','dihapus_pada' => null,'dihapus_oleh' => null])->get('kategori')->row()->jumlah;
    $return = $get;

    return $return;
} 

function count_berita()
{
    $return = 0;
    $CI = &get_instance();
    $get = $CI->db->select('count(*) as jumlah')->where(['dihapus_pada' => null,'dihapus_oleh' => null])->get('berita')->row()->jumlah;
    $return = $get;

    return $return;
}

function count_berita_non_aktif()
{
    $return = 0;
    $CI = &get_instance();
    $get = $CI->db->select('count(*) as jumlah')->where(['status' => '0','dihapus_pada' => null,'dihapus_oleh' => null])->get('berita')->row()->jumlah;
    $return = $get;

    return $return;
} 

function count_album()
{
    $return = 0;
    $CI = &get_instance();
    $get = $CI->db->select('count(*) as jumlah')->where(['dihapus_pada' => null,'dihapus_oleh' => null])->get('album')->row()->jumlah;
    $return = $get;

    return $return;
}

function count_album_aktif()
{
    $return = 0;
    $CI = &get_instance();
    $get = $CI->db->select('count(*) as jumlah')->where(['status' => '1','dihapus_pada' => null,'dihapus_oleh' => null])->get('album')->row()->jumlah;
    $return = $get;

    return $return;
}

if (!function_exists('get_logo')) {
    function get_logo()
    {

        $CI = &get_instance();
        $logo = $CI->db->get('website', 1)->row();
        return $logo;
    }
}

function generate_csrf()
{
    $ci = &get_instance();
    $return['token_name'] = $ci->security->get_csrf_token_name();
    $return['hash'] = $ci->security->get_csrf_hash();
    return $return;
}

function generateRandomString($_length = null)
{
    if (@$_length && $_length > 5) {
        $length = $_length;
    } else {
        $length = 8;
    }
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

// function count_pengumuman()
// {
//     $return = 0;
//     $CI = &get_instance();
//     $get = $CI->db->select('count(*) as jumlah')->where('dihapus_pada', NULL)->get('pengumuman')->row()->jumlah;
//     $return = $get;

//     return $return;
// }

// function count_pengumuman_aktif()
// {
//     $return = 0;
//     $CI = &get_instance();
//     $get = $CI->db->select('count(*) as jumlah')->where(['status' => '1','dihapus_pada' => NULL,'dihapus_oleh' => NULL])->get('pengumuman')->row()->jumlah;
//     $return = $get;

//     return $return;
// } 

// function count_pengumuman_non_aktif()
// {
//     $return = 0;
//     $CI = &get_instance();
//     $get = $CI->db->select('count(*) as jumlah')->where(['status' => '0','dihapus_pada' => NULL,'dihapus_oleh' => NULL])->get('pengumuman')->row()->jumlah;
//     $return = $get;

//     return $return;
// } 

// function count_pengumuman()
// {
// $return = 1;
// $CI = &get_instance();
// $get = $CI->db->select('count(*) as jumlah')->where(['diubah_oleh is NULL'])->get('pengumuman')->row();
// if ($get->jumlah > 1) {
//     $return = $get->jumlah;
// }
// return $return;
// }


function count_role()
{
    $return = 1;
    $CI = &get_instance();
    $np = $CI->user_id;
    $get = $CI->db->select('count(id) as jumlah')->where(['no_pokok_karyawan' => $np, 'is_active' => '1'])->get('user_role')->row();
    if ($get->jumlah > 1) {
        $return = $get->jumlah;
    }
    return $return;
}

function role_name()
{
    $return = '-';
    $CI = &get_instance();
    $role = $CI->session->userdata('role');
    $get = $CI->db->select('nama')->where(['id' => $role])->get('ref_role');
    if ($get->num_rows() > 0) {
        $row = $get->result()[0];
        $return = $row->nama;
    }
    return $return;
}

function user_identity($field)
{
    $return = '-';
    $CI = &get_instance();
    $np = $CI->user_id;
    $tahun = $CI->tahun;
    $tb_name = 'mst_karyawan';
    $db_name = $CI->db->database;

    $count_db_name = $CI->db->select('COUNT(*) as jumlah')->where(['table_schema' => $db_name, 'table_name' => 'mst_karyawan_' . $tahun])->get('information_schema.tables')->row();
    if ($count_db_name->jumlah > 0) {
        $tb_name = 'mst_karyawan_' . $tahun;
    }

    $get = $CI->db->select('nama')->where(['no_pokok' => $np])->get($tb_name);
    if ($get->num_rows() > 0) {
        $row = $get->result()[0];
        $return = $row->$field;
    }
    return $return;
}

function get_menu()
{
    $CI = &get_instance();
    $role = $CI->session->userdata('role');
    $get = $CI->db->select('b.kode, b.nama, b.link')->where(['a.id_ref_role' => $role, 'a.is_active' => '1'])->from('menu_role a')->join('mst_menu b', 'a.id_menu=b.id', 'LEFT')->order_by('b.kode', 'ASC')->get();
    return $get->result_array();
}

function encode_id($id = null)
{
    $return = '';
    if (@$id) {
        require_once(APPPATH . '/third_party/hashids-1.0.5/lib/Hashids/HashGenerator.php');
        require_once(APPPATH . '/third_party/hashids-1.0.5/lib/Hashids/Hashids.php');
        $hashids = new Hashids\Hashids('this is my salt', 32);
        $hash = $hashids->encode($id);
        $return = $hash;
    }
    return $return;
}

function decode_id($hash = null)
{
    $return = [];
    if (@$hash) {
        require_once(APPPATH . '/third_party/hashids-1.0.5/lib/Hashids/HashGenerator.php');
        require_once(APPPATH . '/third_party/hashids-1.0.5/lib/Hashids/Hashids.php');
        $hashids = new Hashids\Hashids('this is my salt', 32);
        $id = $hashids->decode($hash);
        $return = $id;
    }
    return @$return;
}

function day_to_hari($day)
{
    $return = '';
    switch ($day) {
        case "Sun":
            $return = 'Minggu';
            break;
        case "Mon":
            $return = 'Senin';
            break;
        case "Tue":
            $return = 'Selasa';
            break;
        case "Wed":
            $return = 'Rabu';
            break;
        case "Thu":
            $return = 'Kamis';
            break;
        case "Fri":
            $return = 'Jumat';
            break;
        case "Sat":
            $return = 'Sabtu';
            break;
        default:
            $return = '';
    }
    return $return;
}


function strToHex($string)
{
    $hex = '';
    for ($i = 0; $i < strlen($string); $i++) {
        $ord = ord($string[$i]);
        $hexCode = dechex($ord);
        $hex .= substr('0' . $hexCode, -2);
    }
    return $hex;
}

function hexToStr($hex)
{
    $string = '';
    for ($i = 0; $i < strlen($hex) - 1; $i += 2) {
        $string .= chr(hexdec($hex[$i] . $hex[$i + 1]));
    }
    return $string;
}

function get_column($number = '')
{
    $number = $number + 1;
    $huruf = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ', 'BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU', 'BV', 'BW', 'BX', 'BY', 'BZ', 'CA', 'CB', 'CC', 'CD', 'CE', 'CF', 'CG', 'CH', 'CI', 'CJ', 'CK', 'CL', 'CM', 'CN', 'CO', 'CP', 'CQ', 'CR', 'CS', 'CT', 'CU', 'CV', 'CW', 'CX', 'CY', 'CZ', 'DA', 'DB', 'DC', 'DD', 'DE', 'DF', 'DG', 'DH', 'DI', 'DJ', 'DK', 'DL', 'DM', 'DN', 'DO', 'DP', 'DQ', 'DR', 'DS', 'DT', 'DU', 'DV', 'DW', 'DX', 'DY', 'DZ');
    return $huruf[$number];
}

function tanggal_format($date = null)
{
    $result = '-';
    if (@$date) {
        $BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");

        $tahun = substr($date, 0, 4);
        $bulan = substr($date, 5, 2);
        $tgl   = substr($date, 8, 2);
        $result = $tgl . " " . $BulanIndo[(int)$bulan - 1] . " " . $tahun;
    }
    return $result;
}

function rp_format($angka)
{
    $hasil_rupiah = number_format($angka, 0, ',', '.');
    return $hasil_rupiah;
}

function nama_bulan($angka)
{
    $bulan = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
    return $bulan[$angka - 1];
}

function judulseo($string)
{
    $c = array(' ');
    $d = array('-', '/', '\\', ',', '.', '#', ':', ';', '\'', '"', '[', ']', '{', '}', ')', '(', '|', '`', '~', '!', '@', '%', '$', '^', '&', '*', '=', '?', '+');
    $string = str_replace($d, '', $string); // Hilangkan karakter yang telah disebutkan di array $d
    $string = strtolower(str_replace($c, '-', $string)); // Ganti spasi dengan tanda - dan ubah hurufnya menjadi kecil semua
    return $string;
}

function validateDateTime($date, $_format = NULL)
{
    $format = 'Y-m-d H:i:s';
    if (@$_format) {
        $format = $_format;
    }
    $d = DateTime::createFromFormat($format, $date);
    // The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.
    return $d && $d->format($format) === $date;
}

function validateDate($date, $_format = NULL)
{
    $format = 'Y-m-d';
    if (@$_format) {
        $format = $_format;
    }
    $d = DateTime::createFromFormat($format, $date);
    // The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.
    return $d && $d->format($format) === $date;
}

function validateMonth($date, $_format = NULL)
{
    $format = 'Y-m';
    if (@$_format) {
        $format = $_format;
    }
    $d = DateTime::createFromFormat($format, $date);
    // The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.
    return $d && $d->format($format) === $date;
}

function demo_data()
{
    return $array = array(
        array(
            'kode' => 1,
            'nama' => 'menu 1',
        ),
        array(
            'kode' => 10,
            'nama' => 'menu 1-0',
        ),
        array(
            'kode' => 101,
            'link' => 'data1',
            'nama' => 'menu 1-0-1',
        ),
        array(
            'kode' => 102,
            'link' => 'data2',
            'nama' => 'menu 1-0-2',
        ),
        array(
            'kode' => 11,
            'nama' => 'menu 1-1',
        ),
        array(
            'kode' => 2,
            'link' => 'data 3',
            'nama' => 'menu 2',
        ),
        array(
            'kode' => 3,
            'nama' => 'menu 3',
        ),
        array(
            'kode' => 30,
            'link' => 'data4',
            'nama' => 'menu 3-0',
        ),
        array(
            'kode' => 4,
            'link' => NULL,
            'nama' => 'menu 4',
        ),
        array(
            'kode' => 5,
            'link' => NULL,
            'nama' => 'menu 5',
        ),
    );
}

if (!function_exists('slugify')) {
    function slugify($string)
    {
        // Get an instance of $this
        $CI = &get_instance();

        $CI->load->helper('text');
        $CI->load->helper('url');

        // Replace unsupported characters (add your owns if necessary)
        $string = str_replace("'", '-', $string);
        $string = str_replace(".", '-', $string);
        $string = str_replace("²", '2', $string);

        // Slugify and return the string
        return url_title(convert_accented_characters($string), 'dash', true);
    }
}

// if ( ! function_exists('count_pengumuman'))
// {
//     function count_pengumuman()
//     {

//         return 1;
//     }
// }



if (!function_exists('berita_terkait')) {
    function berita_terkait($slug = null, $not_data, $limit = null, $where = null)
    {
        $CI = &get_instance();
        $CI->db->select('t1.judul, t1.konten, t1.foto_thumbnail_path, t1.tanggal_pablis, t1.slug, t2.slug as slug_kategori');
        $CI->db->from('berita t1');
        $CI->db->join('kategori t2', 't2.id = t1.kategori_id', 'left');
        $CI->db->where(['t1.dihapus_pada' => NULL, 't1.status' => '1']);
        $CI->db->where('t2.status', '1');
        $CI->db->where('t2.jenis !=', 'halaman');
        $CI->db->where('t1.slug !=', $not_data);
        $CI->db->where('t2.dihapus_pada is NULL');
        $CI->db->order_by('t1.id', 'DESC');

        if ($limit != null) {
            $CI->db->limit($limit);
        }

        if ($where != null) {
            $CI->db->where($where);
        }

        if ($slug != null) {
            $CI->db->where(['t2.slug' => $slug]);
        }

        return $CI->db->get();
    }
}

if (!function_exists('kontak')) {
    function kontak($kolom)
    {
        $CI = &get_instance();
        $CI->db->select('t1.' . $kolom);
        $CI->db->from('kontak t1');
        $CI->db->where('id', 1);
        return $CI->db->get()->row()->$kolom;
    }
}

if (!function_exists('tautan_terkait')) {
    function tautan_terkait()
    {
        $CI = &get_instance();
        $CI->db->select('t1.judul, t1.link');
        $CI->db->from('link t1');
        $CI->db->where('is_active', '1');
        return $CI->db->get()->result();
    }
}
