<?php

function check_permission($id_menu_hash=null, $action=null){
    $CI =& get_instance();
    if(!$id_menu_hash || !$action){
        $CI->session->set_flashdata('failed', 'Missing parameters.');
        redirect('login');
    } else if(@$id_menu_hash && decode_id($id_menu_hash)==[]){
        $CI->session->set_flashdata('failed', 'Invalid code.');
        redirect('login');
    } else if(@$id_menu_hash && decode_id($id_menu_hash)!=[] && @$action){
        $get_act = $CI->db->select('id_ref_action')->where(['id_menu'=>decode_id($id_menu_hash)[0], 'id_ref_role'=>$CI->session->userdata('role'), 'is_active'=>'1'])->get('menu_role')->result_array();
        $arr_act = array_map(function($value){return $value['id_ref_action'];}, $get_act);
        if(!in_array($action, $arr_act)){
            $CI->session->set_flashdata('failed', 'No permission.');
            redirect('login');
        } else{
            return true;
        }
    }
}

function check_access($id_menu_hash=null, $action=null){
    $CI =& get_instance();
    if(!$id_menu_hash || !$action){
        return false;
    } else if(@$id_menu_hash && decode_id($id_menu_hash)==[]){
        return false;
    } else if(@$id_menu_hash && decode_id($id_menu_hash)!=[] && @$action){
        $get_act = $CI->db->select('id_ref_action')->where(['id_menu'=>decode_id($id_menu_hash)[0], 'id_ref_role'=>$CI->session->userdata('role'), 'is_active'=>'1'])->get('menu_role')->result_array();
        $arr_act = array_map(function($value){return $value['id_ref_action'];}, $get_act);
        if(!in_array($action, $arr_act)){
            return false;
        } else{
            return true;
        }
    }
}

function check_hash($hash=null){
    $CI =& get_instance();
    if(!$hash){
        $CI->session->set_flashdata('failed', 'Missing parameters.');
        redirect('welcome');
    } else if(@$hash && decode_id($hash)==[]){
        $CI->session->set_flashdata('failed', 'Invalid code.');
        redirect('welcome');
    }
}

function get_ref($table=null, $primary_field=null, $search_field=null, $id=null){
    $return = '-';
    $CI =& get_instance();
    if(@$table && @$primary_field && @$search_field && @$id){
        $get = $CI->db->select($search_field)->where($primary_field, $id)->get($table)->row();
        $return = $get->$search_field;
    }
    return $return;
}

function get_icons(){
    return $array = ["bx bx-search","bx bx-show","bx bx-power-off","bx bx-toggle-left","bx bx-toggle-right","bx bx-zoom-in","bx bx-slider-alt","bx bx-menu-alt-right","bx bx-notification","bx bxs-x-square","bx bxs-checkbox","bx bx-zoom-out","bx bx-x-circle","bx bx-dislike","bx bx-reset","bx bx-filter","bx bx-adjust","bx bx-extension","bx bx-area","bx bx-space-bar","bx bx-radio-circle","bx bx-radio-circle-marked","bx bx-menu","bx bxs-filter-alt","bx bx-rotate-right","bx bxs-collection","bx bxs-detail","bx bxs-copy","bx bxs-cog","bx bx-rotate-left","bx bx-hide","bx bx-trash-alt","bx bx-detail","bx bx-block","bx bxs-x-circle","bx bx-check-double","bx bx-show-alt","bx bxs-checkbox-checked","bx bxs-copy-alt","bx bx-dots-horizontal-rounded","bx bx-minus-circle","bx bxs-area","bx bx-dots-horizontal","bx bx-dots-vertical-rounded","bx bx-dots-vertical","bx bx-tag","bx bxs-categories","bx bx-x","bx bx-copy","bx bx-check","bx bxs-extension","bx bx-label","bx bxs-zoom-out","bx bxs-plus-square","bx bx-selection","bx bx-exit-fullscreen","bx bx-fullscreen","bx bx-collection","bx bx-notification-off","bx bx-cut","bx bx-expand","bx bx-collapse","bx bx-plus","bx bx-crop","bx bx-select-multiple","bx bx-menu-alt-left","bx bx-screenshot","bx bx-trash","bx bx-filter-alt","bx bxs-add-to-queue","bx bx-customize","bx bxs-zoom-in","bx bx-slider","bx bx-poll","bx bx-paste","bx bxs-adjust","bx bx-history","bx bx-add-to-queue","bx bx-clipboard","bx bx-windows","bx bx-like","bx bx-columns","bx bx-cog","bx bxs-search","bx bx-window-close","bx bxs-minus-square","bx bx-checkbox-checked","bx bx-checkbox","bx bx-revision","bx bx-checkbox-square","bx bx-repost","bx bxs-label","bx bxs-dislike","bx bx-search-alt-2","bx bxs-notification","bx bxs-customize","bx bx-window","bx bx-window-open","bx bxs-toggle-right","bx bxs-toggle-left","bx bx-table","bx bx-crosshair","bx bx-plus-circle","bx bx-copy-alt","bx bxs-paste","bx bxs-hide","bx bxs-select-multiple","bx bxs-show","bx bxs-duplicate","bx bx-duplicate","bx bxs-minus-circle","bx bxs-plus-circle","bx bxs-widget","bx bxs-trash","bx bxs-tag","bx bxs-trash-alt","bx bxs-to-top","bx bx-check-square","bx bxs-like","bx bxs-search-alt-2","bx bxs-notification-off","bx bxs-tag-x","bx bx-check-circle","bx bx-exit","bx bxs-adjust-alt","bx bxs-exit","bx bxs-check-circle","bx bxs-check-square","bx bx-minus","bx bx-search-alt"];
}
?>
