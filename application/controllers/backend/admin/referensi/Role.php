<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH.'third_party/phpSpreadsheet/autoload.php');

use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

use PhpOffice\PhpSpreadsheet\Reader\Csv;

class Role extends MY_Controller {

    public function __construct(){
        parent::__construct();
        if(!$this->id){
            redirect('login/logout');
        } 
        $this->load->model('backend/admin/M_role', 'm_main');
        $this->load->model(['All_crud']);
    }

    function index() {  
        $data = [ 
            'page_title'=>'Referensi Role',
            'detail_page_title' => 
            '
                List Data
            ',
            'li_active'=>'Referensi Role',
            'uri_segment'=>'backend/admin/referensi/role/',
            'content'=>'backend/admin/referensi/role/home',
            'script'=>'backend/admin/referensi/role/home-js',
            'datatables_js'=>TRUE,
            'datatables_css'=>TRUE,
            'toastr' => TRUE,
            'sweet_alert' => TRUE,
            'btn_kembali' => '<a href="'.@$_SERVER['HTTP_REFERER'].'" class="btn btn-sm btn-rounded btn-outline-danger"> <i class="si si-arrow-left"></i> Kembali</a>',
            'btn_tambah'=> '<button type="button" href="javascript:;" class="btn btn-sm btn-rounded btn-outline-primary" id="tombol_tambah"> <i class="si si-plus"></i> Tambah</button>',
            'btn_excel' => '<div class="btn-group" role="group">
                                <button id="btnGroupDrop1" type="button" class="btn btn-outline-success btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fa fa-file-excel-o"></i> Excel
                                </button>
                                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                  <a class="dropdown-item" href="'.base_url('backend/admin/referensi/role/eksport_excel').'">Export Template</a>
                                  <a class="dropdown-item" href="javascript:;" id="tombol_import">Import</a>
                                </div>
                            </div>',
            'modal'         => array(
                'backend/admin/referensi/role/modal-tambah', 
                'backend/admin/referensi/role/modal-ubah',
                'backend/admin/referensi/role/modal-import',
            )
        ];

        $this->load->view('_templates/main', $data);
    }

    function get_data(){
        $list = $this->m_main->get_datatables(); 
        $data = array();
        $no = $_GET['start'];
        foreach($list as $field){
            // $struktur = '';
            $hapus = ''; 
            $ubah = ''; 
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = ucfirst($field->nama);
            $ubah = '<button href="#" class="btn btn-sm btn-rounded btn-outline-primary tombol_ubah" data="'.strToHex($field->id).'"> <i class="fa fa-pencil"></i> Edit</button> '; 
            $hapus = '<button href="#" class="btn btn-sm btn-rounded btn-outline-danger tombol_hapus" data="'.strToHex($field->id).'"> <i class="fa fa-trash"></i> Hapus</button>';
            $row[] = $ubah.$hapus;

            $data[] = $row;
        }

        $output = array(
            "draw" => $_GET['draw'],
            "recordsTotal" => $this->m_main->count_all(),
            "recordsFiltered" => $this->m_main->count_filtered(),
            "data" => $data,
            "csrf" => generate_csrf(),
        );
        //output dalam format JSON
        echo json_encode($output);
    }

    function store() {
        $id = $this->input->post('id');
        $nama = $this->input->post('nama');

        $status = ""; $json = array(); $data = array();

        $this->form_validation->set_rules('nama', 'nama ', 'required');
        if ($this->form_validation->run() == FALSE) {
        	$json = array(
                'status' => false,
                'csrf' => generate_csrf()
            );
            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($json)); 
        } else{
        	if (empty($id)) {
                $data = [ 
                    'nama' => $this->input->post('nama'),

                    'is_active' => '1',

                    'created_at' => date("Y-m-d H:i:s"),
                    'created_by' => $this->session->userdata('id'),
                ]; 
                $this->db->insert('ref_role', $data); 
            } else {
                $data = [ 
                    'nama' => $this->input->post('nama'),

                    'is_active' => '1',
                    
                    'updated_at' => date("Y-m-d H:i:s"),
                    'updated_by' => $this->session->userdata('id'),
                ]; 
                $this->db->where('id',$id);
                $this->db->update('ref_role', $data); 
            }
	        echo json_encode(array('status' => true,'data' => $data,'csrf' => generate_csrf()));
        }
    }

    function detail()
    { 
        $id = hexToStr($this->input->post('id'));
        $data = $this->db->where(['deleted_at is NULL','id' => $id])->get('ref_role')->row();
        $return = ""; $status = false;
        if (@$data) { 
            $status = true;
            $return = $data;
        }
        echo json_encode(array('status' => $status,'data' => $return,'csrf' => generate_csrf()));
    } 
    
    function delete($id) { 
        $id = hexToStr($id); 
    	$this->All_crud->update_by_id(
            'ref_role',
            'id',
            $id,
            [
                'is_active' => '2',
                'deleted_at' => date("Y-m-d H:i:s"),
                'deleted_by' => $this->session->userdata('id'),

            ]
    
        ); 
        if($this->db->affected_rows()>0){
        	$status = true;            
        } else {
            $status = false; 
        }
        echo json_encode(array('status' => $status,"csrf" => generate_csrf())); 
    }

    function cek_table($nama=null){
        if($nama != null) {
            $nama = urldecode($nama);
            $cek = $this->m_main->get_nama($nama);
            if ($cek->ada==0) {
                echo "<span style='color:green;'>Tabel Tersedia</span>";
            }else {
                echo "<span style='color:red;'>Tabel Sudah Digunakan</span>";
            }         

        }
    }

    function eksport_excel($explode_id = '')
    { 
        $spreadsheet = new Spreadsheet();
        
        $spreadsheet->getActiveSheet()->getStyle('A')->getAlignment()->setHorizontal('center');
        $spreadsheet->getActiveSheet()->mergeCells('A1:B1');
        $spreadsheet->getActiveSheet()->getStyle('A1')->getFont()->setBold( true );
        $spreadsheet->setActiveSheetIndex(0)->setCellValue('A1', 'Eksport Referensi Role');
        
        $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('A3', 'No')
        ->setCellValue('B3', 'Nama')
        ;

        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(7);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(30);

        $no=1;
        $awal = 4;
        $data = $this->db->where('a.is_active','1')->get('ref_role a')->result();
        foreach ($data as $row) {
            $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue('A'.$awal, $no++)
                ->setCellValue('B'.$awal, $row->nama)
            ;
            $awal++;
        }

        $spreadsheet->setActiveSheetIndex(0);

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="eksport_ref_role_'.date('Ymd').'.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit; 
        
    }

    function import_excel()
    {  
        $file_mimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        if(isset($_FILES['upload_file']['name']) && in_array($_FILES['upload_file']['type'], $file_mimes)) {
            $arr_file = explode('.', $_FILES['upload_file']['name']);
            $extension = end($arr_file);
            if('csv' == $extension){
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
            } else {
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
            }

            $spreadsheet = $reader->load($_FILES['upload_file']['tmp_name']);
            $sheetData = $spreadsheet->getActiveSheet()->toArray(); 

            $data = $this->db->where('a.is_active','1')->get('ref_role a')->result();
            foreach ($data as $value) {

                $this->All_crud->delete_by_params(
                    'ref_role',
                    [
                        'nama' => $value->nama

                    ]                
                );
            } 
            $status = false; $data = "";

            for($i = 3;$i < count($sheetData);$i++)
            { 
                $data_insert = array(
                    'nama'       => $sheetData[$i]['1'], 
                    'is_active'  => '1',
                    'created_at' => date("Y-m-d H:i:s"),
                    'created_by' => $this->session->userdata('id'),
                );
                $this->All_crud->insert('ref_role', $data_insert);
                $data = $data_insert;

                $status = true; 
            } 
            echo json_encode(array('status' => $status,"csrf" => generate_csrf())); 
        } 
        
    }
}