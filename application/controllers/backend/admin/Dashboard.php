<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        //$this->load->helper(array('Cookie', 'String'));
        // if(!$this->user_id){
        //     redirect('login/logout');
        // }
    }

    function index()
    {
        $jumberita = $this->db->select('count(id) as jumberita')->where(array('status', '1'))->get('berita')->row()->jumberita;
        $jumgaleri = $this->db->select('count(id) as jumgaleri')->where(array('status', '1'))->get('galeri')->row()->jumgaleri;
        $jumusers = $this->db->select('count(id) as jumusers')->where(array('is_active', '1'))->get('users')->row()->jumusers;

        $data = [
            'csrf' => [
                'name' => $this->security->get_csrf_token_name(),
                'hash' => $this->security->get_csrf_hash()
            ],
            'jumberita' => $jumberita,
            'jumgaleri' => $jumgaleri,
            'jumusers' => $jumusers,

            'page_title'    => 'Dashboard',
            'li_active'     => 'dashboard',
            'content'       => 'backend/admin/dashboard/home',
            'script'        => 'backend/admin/dashboard/home-js',
            'toastr'        => TRUE,
            'chart_bundle'  => TRUE,
            'modal'         => array()
        ];
        $this->load->view('_templates/main', $data);

        // $this->load->view('_templates/main', $data);
        // $data = [
        //     'page_title'=>'Dashboard',
        //     'li_active'=>'Dashboard',
        //     'content'=>'backend/admin/dashboard/home',
        //     // 'script'=>'backend/admin/dashboard/home_js',
        //     'amcharts_js'=>TRUE,
        // ];
        // $this->load->view('_templates/main', $data);
    }
}
