<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Album extends MY_Controller {

    public function __construct(){
        parent::__construct();
        if(!$this->id){
            redirect('login/logout');
        } 
        $this->load->model('backend/admin/M_album', 'm_main');
        $this->load->model(['All_crud']);
    }

    function index() {  
        $data = [ 
            'page_title'        => 'Album',
            'detail_page_title' => 'List Data',
            'li_active'         => 'album',
            'uri_segment'       => 'backend/admin/album/',
            'content'           => 'backend/admin/album/home',
            'script'            => 'backend/admin/album/home-js',
            'datatables_js'     => TRUE,
            'datatables_css'    => TRUE,
            'toastr'            => TRUE,
            'sweet_alert'       => TRUE,
            'btn_kembali'       => '<a href="'.@$_SERVER['HTTP_REFERER'].'" class="btn btn-sm btn-rounded btn-outline-danger"> <i class="si si-arrow-left"></i> Kembali</a>',
            'btn_tambah'        => '<button type="button" href="javascript:;" class="btn btn-sm btn-rounded btn-outline-primary" id="tombol_tambah"> <i class="si si-plus"></i> Tambah</button>',
            'modal'         => array(
                'backend/admin/album/modal-tambah', 
                'backend/admin/album/modal-ubah',
            )
        ];

        $this->load->view('_templates/main', $data);
    }

    function get_data(){
        $res = $list = $this->m_main->list_data(); 
        $res['csrf'] = generate_csrf();
        echo json_encode($res);
    }

    function store() {
        $json = array(); $data = array();

        $this->form_validation->set_rules('nama','nama kategori tidak boleh kosong','required|trim|is_unique[album.nama]');
        $this->form_validation->set_rules('deskripsi','deskripsi tidak boleh kosong','required');
        $this->form_validation->set_rules('status','Status tidak boleh kosong','required');
        $this->form_validation->set_message('required', 'Anda melewatkan input, {field}!');
        $this->form_validation->set_message('is_unique', 'Nama kategori sudah terdaftar, kategori tidak boleh sama');
        if ($this->form_validation->run() == FALSE) {
        	$json['status'] = false;
        } else{
            
        	$data['nama']          = $this->input->post('nama', TRUE);
            $data['slug']          = slugify($this->input->post('nama', TRUE));
            $data['deskripsi']     = $this->input->post('deskripsi', TRUE);
            $data['status']        = $this->input->post('status', TRUE);
            $data['dibuat_pada']   = date("Y-m-d H:i:s");
            $data['diubah_pada']   = date("Y-m-d H:i:s");
            $data['dibuat_oleh']   = $this->session->userdata('id');
            // menyimpan data pada database
            $this->db->insert('album', $data);
            $json['status'] = true;

            // kebutuhan push notifikasi
            $this->load->view('../../vendor/autoload.php');
            $options = array(
                'cluster' => 'ap1',
                'useTLS' => true
            );
            $pusher = new Pusher\Pusher(
                '4b7d59c739af7d7ca6d2',
                '1dcbdcfd5a68151cc94d',
                '1125954',
                $options
            );
            $count_sidebar['album'] = count_album();
            $pusher->trigger('web-kemenpar-ap', 'count-sidebar', $count_sidebar);
            // kebutuhan push notifikasi

            // kebutuhan push notifikasi
            $this->load->view('../../vendor/autoload.php');
            $options = array(
                'cluster' => 'ap1',
                'useTLS' => true
            );
            $pusher = new Pusher\Pusher(
                '4b7d59c739af7d7ca6d2',
                '1dcbdcfd5a68151cc94d',
                '1125954',
                $options
            );
            $table_data['table_data_reload'] = 'table_reload';
            $pusher->trigger('web-kemenpar-ap', 'table-data', $table_data);
            // kebutuhan push notifikasi   
        }
        $json['csrf'] = generate_csrf();
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($json)); 
    }

    function update() {
        $json = array(); $data = array();

        $this->form_validation->set_rules('id','id tidak boleh kosong','required|trim');
        $this->form_validation->set_rules('nama','nama kategori tidak boleh kosong','required|trim|callback_check_uniq_nama');
        $this->form_validation->set_rules('deskripsi','deskripsi tidak boleh kosong','required');
        $this->form_validation->set_rules('status','Status tidak boleh kosong','required');
        $this->form_validation->set_message('required', 'Anda melewatkan input, {field}!');
        $this->form_validation->set_message('is_unique', 'Nama kategori sudah terdaftar, kategori tidak boleh sama');
        if ($this->form_validation->run() == FALSE) {
        	$json['status'] = false;
        } else{
            
            $data['nama']          = $this->input->post('nama', TRUE);
            $data['slug']          = slugify($this->input->post('nama', TRUE));
            $data['deskripsi']     = $this->input->post('deskripsi', TRUE);
            $data['status']        = $this->input->post('status', TRUE);
            $data['diubah_oleh']   = $this->session->userdata('id');
            $data['diubah_pada']   = date("Y-m-d H:i:s");
            // mengubah data pada database
            $this->db->where('id', $this->input->post('id', TRUE));
            $this->db->update('album', $data); 
            $json['status'] = true;

            // kebutuhan push notifikasi
            $this->load->view('../../vendor/autoload.php');
            $options = array(
                'cluster' => 'ap1',
                'useTLS' => true
            );
            $pusher = new Pusher\Pusher(
                '4b7d59c739af7d7ca6d2',
                '1dcbdcfd5a68151cc94d',
                '1125954',
                $options
            );
            $table_data['table_data_reload'] = 'table_reload';
            $pusher->trigger('web-kemenpar-ap', 'table-data', $table_data);
            // kebutuhan push notifikasi  
        }
        $json['csrf'] = generate_csrf();
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($json)); 
    }

    function detail()
    { 
        $id = hexToStr($this->input->get('id'));
        $data = $this->db->select('id, nama, deskripsi, status')->where(['dihapus_pada is NULL', 'id' => $id])->get('album')->row();
        $return = ""; $status = false;
        if (@$data) { 
            $status = true;
            $return = $data;
        }
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(
                array('status' => $status,'data' => $return,'csrf' => generate_csrf())
            )); 
    } 
    
    function delete($id) { 
        $id = hexToStr($id); 
    	$this->All_crud->update_by_id(
            'album',
            'id',
            $id,
            [
                'dihapus_pada' => date("Y-m-d H:i:s"),
                'dihapus_oleh' => $this->session->userdata('id'),

            ]
    
        ); 
        if($this->db->affected_rows()>0){
        	$status = true;

            // kebutuhan push notifikasi
            $this->load->view('../../vendor/autoload.php');
            $options = array(
                'cluster' => 'ap1',
                'useTLS' => true
            );
            $pusher = new Pusher\Pusher(
                '4b7d59c739af7d7ca6d2',
                '1dcbdcfd5a68151cc94d',
                '1125954',
                $options
            );
            $count_sidebar['album'] = count_album();
            $pusher->trigger('web-kemenpar-ap', 'count-sidebar', $count_sidebar);
            // kebutuhan push notifikasi

            // kebutuhan push notifikasi
            $this->load->view('../../vendor/autoload.php');
            $options = array(
                'cluster' => 'ap1',
                'useTLS' => true
            );
            $pusher = new Pusher\Pusher(
                '4b7d59c739af7d7ca6d2',
                '1dcbdcfd5a68151cc94d',
                '1125954',
                $options
            );
            $table_data['table_data_reload'] = 'table_reload';
            $pusher->trigger('web-kemenpar-ap', 'table-data', $table_data);
            // kebutuhan push notifikasi    
        } else {
            $status = false; 
        }
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(array('status' => $status,"csrf" => generate_csrf())));
    }

    public function show()
    {
        $data  = $this->db->select('a.id, a.nama as nama')
                        ->where('a.dihapus_pada', null)
                        ->where('status', '1')
                        ->get('album a')->result();
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data)); 
    }

    public function check_uniq_nama()
    {
        $id = @$this->input->post('id');

        if($id == ''){
            // insert data
            $data = $this->db->where(array('nama' => $this->input->post('nama'), 'dihapus_pada' => null))->get('album')->num_rows();
        }else{
            // update data
            $data = $this->db->where(array('nama' => $this->input->post('nama'), 'dihapus_pada' => null, 'id !=' => $this->input->post('id')))->get('album')->num_rows();
        }
        
        if ($data == 0) {
            return TRUE;
        }else{
            $this->form_validation->set_message('check_uniq_nama', 'Nama kategori telah digunakan');
            return FALSE;
        }
    }

    public function ubahStatus($id)
    {
        $id = hexToStr($id); 
        $data['status']        = $this->input->post('status', TRUE);
        $data['diubah_oleh']   = $this->session->userdata('id');
        $data['diubah_pada']   = date("Y-m-d H:i:s");
        // mengubah data pada database
        $this->db->where('id', $id);
        $update = $this->db->update('album', $data); 
        if($update){ 
            // kebutuhan push notifikasi
            $this->load->view('../../vendor/autoload.php');
            $options = array(
                'cluster' => 'ap1',
                'useTLS' => true
            );
            $pusher = new Pusher\Pusher(
                '4b7d59c739af7d7ca6d2',
                '1dcbdcfd5a68151cc94d',
                '1125954',
                $options
            );
            $count_sidebar['album_aktif'] = count_album_aktif();
            $pusher->trigger('web-kemenpar-ap', 'count-sidebar', $count_sidebar);
            // kebutuhan push notifikasi  

            // kebutuhan push notifikasi
            $this->load->view('../../vendor/autoload.php');
            $options = array(
                'cluster' => 'ap1',
                'useTLS' => true
            );
            $pusher = new Pusher\Pusher(
                '4b7d59c739af7d7ca6d2',
                '1dcbdcfd5a68151cc94d',
                '1125954',
                $options
            );
            $table_data['table_data_reload'] = 'table_reload';
            $pusher->trigger('web-kemenpar-ap', 'table-data', $table_data);
            // kebutuhan push notifikasi  
            
            $json['status'] = true;
        }else{
            $json['status'] = false;
        }

        $json['csrf'] = generate_csrf();
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($json)); 
    }
}