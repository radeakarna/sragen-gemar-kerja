<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class User extends MY_Controller {

    public function __construct(){
        parent::__construct();
        if(!$this->id){
            redirect('login/logout');
        } 
        $this->load->model('backend/admin/M_user', 'm_main');
        $this->load->model(['All_crud']);
    }

    function arrRole(){
        $arrRole = array();
        
        $get_role = $this->db->select("id, nama")->get("ref_role")->result();                                                                                     
        foreach($get_role as $roles){
                $arrRole[$roles->id] = $roles->nama;
        }
        
        return $arrRole;
    }
    
    function get_perusahaan(){
        $arrperusahaan = array();
        $perusahaan = $this->db->select('id,nama')
            ->where('dihapus_pada is NULL')
            ->get('perusahaan')
            ->result();
        foreach ($perusahaan as $perusahaanq) {
            $arrperusahaan[$perusahaanq->id] = $perusahaanq->nama;
        }
        return $arrperusahaan;

    }
    
    function index() {  
         
        $arr_roles = $this->arrRole();
        
        $get_roles = '';
        
        $get_roles .= '<option value="' . 'none' . '">' . 'Pilih Role' . '</option>';
        foreach ($arr_roles as $key => $value) {
            $get_roles .= '<option value="' . $key . '">' . $value . '</option>';
        }
        
        $arrperusahaan = $this->get_perusahaan();
        
        $pil_perusahaan = '';
        $pil_perusahaan .= '<option value="' ."none" . '">' . "--- Pilih Perusahaan ---" . '</option>';
        foreach ($arrperusahaan as $key => $value) {
            $pil_perusahaan .= '<option value="' . $key . '">' . $value . '</option>';
        }
        
              
        $data = [ 
            'page_title'        => 'Manajemen User',
            'detail_page_title' => 
            '
                List Data
            ',
            'li_active'         => 'manajemen user',
            'uri_segment'       => 'backend/admin/user/',
            'role'              => $get_roles,
            'perusahaan'        => $pil_perusahaan,
            'content'           => 'backend/admin/user/home',
            'script'            => 'backend/admin/user/home-js',
            'datatables_js'     => TRUE,
            'datatables_css'    => TRUE,
            'toastr'            => TRUE,
            'sweet_alert'       => TRUE,
            'select2'           => TRUE,
            'btn_kembali'       => '<a href="'.@$_SERVER['HTTP_REFERER'].'" class="btn btn-sm btn-rounded btn-outline-danger"> <i class="si si-arrow-left"></i> Kembali</a>',
            'btn_tambah'        => '<button type="button" href="javascript:;" class="btn btn-sm btn-rounded btn-outline-primary" id="tombol_tambah"> <i class="si si-plus"></i> Tambah</button>',
            'modal'             => array(
                'backend/admin/user/modal-tambah', 
                'backend/admin/user/modal-ubah',
            )
        ];

        $this->load->view('_templates/main', $data);
    }

    function approve($id){
        $data  = [
            'approve' => 1
        ];
        
        $this->db->where('id',$id);
        $this->db->update('users', $data);
        header("Location: ".base_url('backend/admin/user'));
    }
    
    function get_data(){
        $list = $this->m_main->get_datatables(); 
        $data = array();
        $no = $_GET['start'];
        foreach($list as $field){
            // $struktur = '';
            $hapus = ''; 
            $ubah = ''; 
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->email;
            $row[] = ucfirst($field->username);
            if($field->id_role == 3 || $field->id_role == 2){
                $row[] = "<a class='btn btn-primary' href=".base_url('backend/admin/user/detail_user/').$field->id.">".ucfirst($field->nama)."</a>";
            } else {
                $row[] = ucfirst($field->nama);
            }
            
            if($field->id_role == 1){ 
                
                $row[] = "<a class='btn btn-secondary' disabled>"."Tidak perlu Approve"."</a>";
            } else {
                 $row[] = "<a class='btn btn-success' href=".base_url('backend/admin/user/approve/').$field->id.">"."Approve"."</a>";
            }
            
            
            $ubah = '<button href="#" class="btn btn-sm btn-rounded btn-outline-primary tombol_ubah" data="'.strToHex($field->id).'"> <i class="fa fa-pencil"></i> Edit</button> '; 
            $hapus = '<button href="#" class="btn btn-sm btn-rounded btn-outline-danger tombol_hapus" data="'.strToHex($field->id).'"> <i class="fa fa-trash"></i> Hapus</button>';
            $row[] = $ubah.$hapus;

            $data[] = $row;
        }

        $output = array(
            "draw"              => $_GET['draw'],
            "recordsTotal"      => $this->m_main->count_all(),
            "recordsFiltered"   => $this->m_main->count_filtered(),
            "data"              => $data,
            "csrf"              => generate_csrf(),
        );
        //output dalam format JSON
        echo json_encode($output);
    }

    function store() {
        $id = $this->input->post('id');
        $status = ""; $json = array(); $data = array();

        // $this->form_validation->set_rules('id_penduduk', 'Penduduk ', 'required');
        $this->form_validation->set_rules('email', 'Email ', 'required|valid_email');
        $this->form_validation->set_rules('username', 'Username ', 'required|is_unique[users.username]');
        $this->form_validation->set_rules('name_role', 'Role', 'required');
        $this->form_validation->set_rules('password', 'Password ', 'required');
        $this->form_validation->set_rules('usaha', 'usaha', 'required');
        $this->form_validation->set_rules('confirm_password', 'confirm_password ', 'required|matches[password]');
        $this->form_validation->set_message('is_unique', 'Username sudah terdaftar, username tidak boleh sama');
        $this->form_validation->set_message('confirm_password', 'Password tidak sama');

        if ($this->form_validation->run() == FALSE) {
        	$json = array(
                'status'             => false,
                'message'            => 'inputan tidak boleh kosong',
                // 'id_penduduk'        => form_error('id_penduduk', '<p class="text-danger">', '</p>'),
                'username'           => form_error('username', '<p class="text-danger">', '</p>'),
                'role'           => form_error('name_role', '<p class="text-danger">', '</p>'),
                'email'              => form_error('email', '<p class="text-danger">', '</p>'),
                'usaha'         => form_error('usaha', '<p class="text-danger">', '</p>'),
                'password'           => form_error('password', '<p class="text-danger">', '</p>'),
                'confirm_password'   => form_error('confirm_password', '<p class="text-danger">', '</p>'),
                'csrf'               => generate_csrf()
            );
            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($json)); 
        } else{
            
        	$data = [ 
                'email'      => $this->input->post('email'),
                'username'   => $this->input->post('username'),
                'password'   => $this->input->post('password'),
                'perusahaan_id' => $this->input->post('usaha'),
                'id_role'    => $this->input->post('role'),
                'is_active'  => '1',
                'created_at' => date("Y-m-d H:i:s"),
                'created_by' => $this->session->userdata('id'),
            ]; 
            $this->db->insert('users', $data);
            $id_user = $this->db->insert_id();
            // if($id_user){
            //     $data_penduduk = [];
            //     $data_penduduk['id_user'] = $id_user; 
            //     $this->db->where('id', $this->input->post('id_penduduk'));
            //     $this->db->update('penduduk', $data_penduduk);
            // }

	        echo json_encode(array('status' => true,'data' => $data,'csrf' => generate_csrf()));
        }
    }
    
    function store_detail(){
        $id = $this->input->post('id');
        $check = $this->input->post('check');
        $status = ""; 
        $json = array(); 
        $data = array();

        $this->form_validation->set_rules('ktp', 'ktp ', 'required');
        $this->form_validation->set_rules('nim', 'nim ', 'required');
        $this->form_validation->set_rules('email','email','required');       
        $this->form_validation->set_rules('jsk', 'jsk ', 'required');
        $this->form_validation->set_rules('hp', 'hp ', 'required');
        $this->form_validation->set_rules('sttp', 'sttp ', 'required');
        $this->form_validation->set_rules('pendidikan_terakhir', 'pendidikan_terakhir ', 'required');
        $this->form_validation->set_rules('bpjs', 'bpjs ', 'required');
        

        if ($this->form_validation->run() == FALSE) {
        	$json = array(
                'status'             => false,
                'ktp'                => form_error('ktp', '<p class="text-danger">', '</p>'),
                'nim'                => form_error('nim', '<p class="text-danger">', '</p>'),
                'email'              => form_error('email', '<p class="text-danger">', '</p>'),
                'jsk'                => form_error('jsk', '<p class="text-danger">', '</p>'),
                'hp'                 => form_error('hp', '<p class="text-danger">', '</p>'),
                'sttp'               => form_error('sttp', '<p class="text-danger">', '</p>'),
                'pendidikan_terakhir' => form_error('pendidikan_terakhir', '<p class="text-danger">', '</p>'),
                'bpjs'               => form_error('bpjs', '<p class="text-danger">'), 
                'csrf'               => generate_csrf()
            );
                
            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($json)); 
        } else{
            
            $data = [ 
                'user_id'      => $id,
                'nim'          => $this->input->post('nim'),
                'status_pekerjaan_id' => $this->input->post('sttp'),
                'email'        => $this->input->post('email'),
                'no_hp'        => $this->input->post('hp'),
                'pendidikan_terakhir' => $this->input->post('pendidikan_terakhir'),
                'no_ktp'       => $this->input->post('ktp'),
                'bpjs'         => $this->input->post('bpjs'),
                'jenis_kelamin_id' => $this->input->post('jsk')
            ]; 
            
            if($check == 0){                
                $this->db->insert('detail_user', $data);
                $id_user = $this->db->insert_id();
                $data['check'] = 0;
            } else {
               $this->db->where('user_id',$id);
               $this->db->update('detail_user', $data);
               $data['check'] = 1;
            }
            // if($id_user){
            //     $data_penduduk = [];
            //     $data_penduduk['id_user'] = $id_user; 
            //     $this->db->where('id', $this->input->post('id_penduduk'));
            //     $this->db->update('penduduk', $data_penduduk);
            // }

	        echo json_encode(array('status' => true,'data' => $data,'csrf' => generate_csrf()));
        }
    }
    
    public function jenisKelamin(){
        $arr_jk = array();
        
        $get_jenis_kelamin = $this->db->select('id,name')->get('ref_jenis_kelamin')->result();
        foreach($get_jenis_kelamin as $key){
            $arr_jk[$key->id] = $key->name;
        }
        
        return $arr_jk;
    }
    
    public function ref_status_kerja(){
        $arr_kerja = array();
        
        $get_stker = $this->db->select('id,nama')->get('ref_status_kerja')->result();
        foreach($get_stker as $key){
            $arr_kerja[$key->id] = $key->nama;
        }
        
        return $arr_kerja;
    }
    
    public function detail_user($id){
        
        $get_jenis_kelamin = $this->jenisKelamin();
        $get_stt_kerja = $this->ref_status_kerja();
        
        $cek_dari_detail = $this->db->select("*")->where('user_id', $id)->get("detail_user")->result();
       
        $ktp = '';
        $nim = '';
        $email = '';
        $jenis_pilih = '';
        $hp = '';
        $pendidikan = '';
        $bpjs = '';
        $status = '';
        $aksi = '';
        $check = '';
        if(count(array($cek_dari_detail)) > 0){
            foreach($cek_dari_detail as $k){
                $ktp = $k->no_ktp;
                $nim = $k->nim;
                $email = $k->email;
                $jenis_pilih = $k->jenis_kelamin_id;
                $hp = $k->no_hp;
                $pendidikan = $k->pendidikan_terakhir;
                $bpjs = $k->bpjs;
                $status = $k->status_pekerjaan_id;
                $check = 1;
                $aksi = "<button type='submit' class='btn btn-warning float-left' id='buttom_ubah'>Ubah</button>";
            }
        } 
        
        if(empty($cek_dari_detail)){
            $check = 0;
            $aksi = "<button type='submit' class='btn btn-primary float-left' id='buttom_tambah'>Simpan</button>";
        }
        
        
        
        $pilih_jenis_kelamin = '';

        $pilih_jenis_kelamin .= '<option value="' . 'none' . '">' . 'Pilih Jenis Kelamin' . '</option>';
        foreach ($get_jenis_kelamin as $key => $value) {
            if($jenis_pilih == $key){
                $pilih_jenis_kelamin .= '<option value="' . $key . '" selected>' . $value . '</option>';
            } else {
                $pilih_jenis_kelamin .= '<option value="' . $key . '">' . $value . '</option>';
            }
        }
        
        $pilih_status_kerja = '';
        
        $pilih_status_kerja .= '<option value="' . 'none' . '">' . 'Pilih Status Kerja' . '</option>';
        foreach ($get_stt_kerja as $key => $value) {
            if($status == $key){
                $pilih_status_kerja .= '<option value="' . $key . '" selected>' . $value . '</option>';
            } else {
                $pilih_status_kerja .= '<option value="' . $key . '">' . $value . '</option>';
            }
        }
        
        
        
        $data = [ 
            'page_title'        => 'Tambah Data Detail',
            'detail_page_title' => 'Tambah Data',
            'li_active'         => 'tambah berita',
            'jenis_kelamin'     => $pilih_jenis_kelamin,
            'id'                => $id,
            'ktp'               => $ktp,
            'nim'               => $nim,
            'email'             => $email,
            'hp'                => $hp,
            'pendidikan'        => $pendidikan,
            'bpjs'              => $bpjs,
            'data'              => $cek_dari_detail,
            'status'            => $pilih_status_kerja,
            'aksi'              => $aksi,
            'check'             => $check,
            'arr'               => $cek_dari_detail,
            'jsk'               => $jenis_pilih,
            'pilih_kerja'       => $status,
            'uri_segment'       => 'backend/admin/user/',
            'content'           => 'backend/admin/user/home-detail',
            'script'            => 'backend/admin/user/home-detail-js',
            'toastr'            => TRUE,
            'sweet_alert'       => TRUE,
            // 'sumernote'         => TRUE,
            'btn_kembali'       => '<a href="'.@$_SERVER['HTTP_REFERER'].'" class="btn btn-sm btn-rounded btn-outline-danger"> <i class="si si-arrow-left"></i> Kembali</a>',
        ];

        $this->load->view('_templates/main', $data);
    }
    
    
    function detail_user_check($id){        
        $cek_dari_detail = $this->db->select("user_id")->where('user_id', $id)->get("detail_user")->result(); 
    }

    public function update()
    {
        $id = $this->input->post('id');
        $status = ""; $json = array(); $data = array();

        // $this->form_validation->set_rules('id_penduduk', 'Penduduk ', 'required');
        $this->form_validation->set_rules('email', 'Email ', 'required|valid_email');
        $this->form_validation->set_rules('name_role', 'Role ', 'required');
        $this->form_validation->set_rules('username', 'Username ', 'required|callback_check_uniq_username['.$this->input->post('username').']');
        $this->form_validation->set_rules('usaha', 'usaha', 'required');
        if ($this->form_validation->run() == FALSE) {
        	$json = array(
                'status'             => false,
                'message'            => 'inputan tidak boleh kosong',
                // 'id_penduduk'        => form_error('id_penduduk', '<p class="text-danger">', '</p>'),
                'username'           => form_error('username', '<p class="text-danger">', '</p>'),
                'email'              => form_error('email', '<p class="text-danger">', '</p>'),
                'usaha'             => form_error('usaha', '<p class="text-danger">', '</p>'),
                'name_role'              => form_error('name_role', '<p class="text-danger">', '</p>'),
                'csrf'               => generate_csrf()
            );
            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($json)); 
        } else{

            $data = [
                'email'      => $this->input->post('email'),
                'username'   => $this->input->post('username'),
                'id_role' => $this->input->post('role'),
                'perusahaan_id' => $this->input->post('usaha'),
                'updated_at' => date("Y-m-d H:i:s"),
                'updated_by' => $this->session->userdata('id'),
            ]; 

            if($this->input->post('password') != ''){

                if ($this->input->post('password') != $this->input->post('confirm_password')) {

                    $json = array(
                        'status'             => false,
                        'confirm_password'   => '<p class="text-danger">Password tidak sama</p>',
                        'csrf'               => generate_csrf()
                    );
                    echo json_encode($json);
                    exit();

                }else{

                    $data['password'] = sha1($this->input->post('password'));

                }
                    
            }
            
            // update data user
            $this->db->where('id',$id);
            $this->db->update('users', $data);

            // update data penduduk
            // $data_penduduk = [];
            // $data_penduduk['id_user'] = $id; 
            // $this->db->where('id', $this->input->post('id_penduduk'));
            // $this->db->update('penduduk', $data_penduduk);

	        echo json_encode(array('status' => true,'data' => $data,'csrf' => generate_csrf()));
        }
    }

    function detail()
    { 
        $id                = hexToStr($this->input->post('id'));
        $data              = $this->db->select('id, username, email, id_role, perusahaan_id')->where(['deleted_at is NULL','id' => $id])->get('users')->row();
        // $data_penduduk     = $this->db->select('id, nik, no_kk')->where(['deleted_at is NULL','id_user' => $id])->get('penduduk')->row();
        // $data->id_penduduk = '';
        // $data->nik         = '';
        // $data->no_kk       = '';

        // if($data_penduduk){
        //     $data->id_penduduk = $data_penduduk->id;
        //     $data->nik         = $data_penduduk->nik;
        //     $data->no_kk       = $data_penduduk->no_kk;
        // }
        
        $return = ""; $status = false;
        if (@$data) { 
            $status = true;
            $return = $data;
        }
        echo json_encode(array('status' => $status,'data' => $return,'csrf' => generate_csrf()));
    } 
    
    function delete($id) { 
        $id = hexToStr($id); 
    	$this->All_crud->update_by_id(
            'users',
            'id',
            $id,
            [
                'is_active' => '2',
                'deleted_at' => date("Y-m-d H:i:s"),
                'deleted_by' => $this->session->userdata('id'),

            ]
    
        ); 
        if($this->db->affected_rows()>0){
        	$status = true;            
        } else {
            $status = false; 
        }
        echo json_encode(array('status' => $status,"csrf" => generate_csrf())); 
    }

    // public function get_penduduk()
    // {
    //     $id = $this->input->get('id_penduduk');
    //     if ($id != ''){
    //         // kondisi ketika user di edit -> menampikan data penduduk yang tidak memiliki akun user
    //         // menampilkan data penduduk milik dia sendiri
    //         $get_penduduk           = $this->db->select('id, nama_lengkap as nama')->where('id_user is null and deleted_at is null')->get('penduduk')->result();
    //         $get_penduduk_selectted = $this->db->select('id, nama_lengkap as nama')->where('deleted_at is null')->where('id', $id)->get('penduduk')->row();
    //         if($get_penduduk_selectted){
    //             $get_penduduk[]         = $get_penduduk_selectted;
    //         }
    //     }else{
    //         // kondisi ketika user melakukan tambah data
    //         // menampilkan data penduduk yang belum mepunyai akun user
    //         $get_penduduk = $this->db->select('id, nama_lengkap as nama')->where('id_user is null and deleted_at is null')->get('penduduk')->result();
    //     }
    //     $this->output->set_content_type('application/json')->set_output(json_encode($get_penduduk));
    // }

    // public function get_detail_penduduk()
    // {
    //     $id           = $this->input->get('id');
    //     $get_penduduk = $this->db->select('nik, no_kk')->where('deleted_at is null')->where('id', $id)->get('penduduk')->row();
    //     $this->output->set_content_type('application/json')->set_output(json_encode($get_penduduk));
    // }

    public function check_uniq_username($username)
    {
        $data = $this->db->where(array('username' => $username, 'id !=' => $this->input->post('id')))->get('users')->num_rows();
        if ($data == 0) {
            return TRUE;
        }else{
            $this->form_validation->set_message('check_uniq_username', 'Username telah digunakan');
            return FALSE;
        }
    }

}