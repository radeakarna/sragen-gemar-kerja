<?php

ini_set('memory_limit', '512M');

defined('BASEPATH') or exit('No direct script access allowed');

class Perusahaan extends MY_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->id) {
            redirect('login/logout');
        }
        $this->load->model('backend/admin/M_perusahaan', 'm_slider');
        $this->load->model(['All_crud']);
        // $this->load->library('Image');
    }

    function index() {
        $data = [
            'page_title' => 'Perusahaan',
            'detail_page_title' => 'List Data',
            'li_active' => 'perusahaan',
            'uri_segment' => 'backend/admin/perusahaan/',
            'content' => 'backend/admin/perusahaan/perusahaan',
            'script' => 'backend/admin/perusahaan/perusahaan_js',
            'datatables_js' => TRUE,
            'datatables_css' => TRUE,
            'toastr' => TRUE,
            'sweet_alert' => TRUE,
            'btn_kembali' => '<a href="' . @$_SERVER['HTTP_REFERER'] . '" class="btn btn-sm btn-rounded btn-outline-danger"> <i class="si si-arrow-left"></i> Kembali</a>',
            'btn_tambah' => '<a href="' . base_url('backend/admin/perusahaan/create') . '"><button type="button" href="javascript:;" class="btn btn-sm btn-rounded btn-outline-primary" id="tombol_tambah"> <i class="si si-plus"></i> Tambah</button></a>',
            'modal' => array()
        ];

        $this->load->view('_templates/main', $data);
    }

    function get_data() {
        $res = $this->m_slider->list_data();
        $res['csrf'] = generate_csrf();
        echo json_encode($res);
    }
    
    function arrprovinsi(){
        $arrprovinsi = array();
        
        $prov = $this->db->select('id,nama')->get('ref_provinsi')->result();                                                                                        
        foreach($prov as $provin){
                $arrprovinsi[$provin->id] = $provin->nama;
        }
        return $arrprovinsi;
    }
    
    function arrkabupaten($idprov=null){
        $arrkabupaten = array();
        if($idprov!=null){
                $kabupaten = $this->db->select('id,nama')
                                    ->where('provinsi_id',$idprov)
                                    ->get('ref_kabupaten')
                                    ->result();
        }else{
               $kabupaten = array('id' => 0, 'nama' => 'Kabupaten belum didaftarkan');
        }

        $arrkabupaten = $kabupaten;
        return $arrkabupaten;
    }
    
    function arrkec($idkec=null){
        $arrkec = array();
        if($idkec!=null){
                $kecamatan = $this->db->select('id,kecamatan_nama')
                                        ->where('kabupaten_id',$idkec)
                                        ->get('ref_kecamatan')
                                        ->result();
        }else{
               $kecamatan = array('id' => 0, 'nama' => 'Kecamatan belum didaftarkan');
        }

        $arrkec = $kecamatan;
        return $arrkec;
    }
    
        
    function loadpilkab(){
        $idprov = $this->input->get('prov');

        $arrkabupaten = $this->arrkabupaten($idprov);
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($arrkabupaten)); 

    }
    
    public function loadpilkec(){
        $idkec = $this->input->get('kab');
        
        $arrkecamatan = $this->arrkec($idkec);
        
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($arrkecamatan)); 
    }

    public function create() {
        
        $arrprov = $this->arrprovinsi();
        $pilprovinsi = '';
        
        $pilprovinsi .= '<option value="' . 'none' . '">' . 'Pilih Provinsi' . '</option>';
        foreach ($arrprov as $key => $value) {
            $pilprovinsi .= '<option value="' . $key . '">' . $value . '</option>';
        }
        
        $data = [
            'page_title' => 'Tambah Data Perusahaan',
            'detail_page_title' => 'Tambah Data',
            'provinsi' => $pilprovinsi,
            'li_active' => 'perusahaan',
            'uri_segment' => 'backend/admin/perusahaan/',
            'content' => 'backend/admin/perusahaan/tambah/perusahaan',
            'script' => 'backend/admin/perusahaan/tambah/perusahaan_js',
            'toastr' => TRUE,
            'sweet_alert' => TRUE,
            'sumernote' => TRUE,
            'btn_kembali' => '<a href="' . @$_SERVER['HTTP_REFERER'] . '" class="btn btn-sm btn-rounded btn-outline-danger"> <i class="si si-arrow-left"></i> Kembali</a>',
            'modal' => array(
            // 'backend/admin/slider_utama/tambah/modal_tambah', 
            )
        ];

        $this->load->view('_templates/main', $data);
    }

    public function edit($id = null) {
        $arrprov = $this->arrprovinsi();
        $pilprovinsi = '';
        
        $pilprovinsi .= '<option value="' . 'none' . '">' . 'Pilih Provinsi' . '</option>';
        foreach ($arrprov as $key => $value) {
            $pilprovinsi .= '<option value="' . $key . '">' . $value . '</option>';
        }
        
        $data = [
            'page_title' => 'Edit Data Perusahaan',
            'detail_page_title' => 'Edit Data',
            'li_active' => 'perusahaan',
            'provinsi' => $pilprovinsi,
            'uri_segment' => 'backend/admin/perusahaan/',
            'content' => 'backend/admin/perusahaan/edit/home',
            'script' => 'backend/admin/perusahaan/edit/home-js',
            'toastr' => TRUE,
            'sweet_alert' => TRUE,
            'sumernote' => TRUE,
            'id' => $id,
            'btn_kembali' => '<a href="' . @$_SERVER['HTTP_REFERER'] . '" class="btn btn-sm btn-rounded btn-outline-danger"> <i class="si si-arrow-left"></i> Kembali</a>',
            'modal' => array()
        ];

        $this->load->view('_templates/main', $data);
    }

    function store() {
        $json = array();
        $data = array();

        $this->form_validation->set_rules('nama', 'nama tidak boleh kosong', 'required|trim|callback_check_uniq_judul');
        $this->form_validation->set_rules('alamat', 'alamat tidak boleh kosong', 'required');
        $this->form_validation->set_rules('npwp', 'npwp tidak boleh kosong', 'required');
        $this->form_validation->set_rules('deskripsi', 'deskripsi tidak boleh kosong', 'required');
        $this->form_validation->set_rules('prov_id', 'Provinsi tidak boleh kosong', 'required');
        $this->form_validation->set_rules('kab_id', 'Kabupaten tidak boleh kosong', 'required');
        $this->form_validation->set_rules('kec_id', 'Kecamatan tidak boleh kosong', 'required');
        $this->form_validation->set_rules('latitude', 'latitude tidak boleh kosong', 'required');
        $this->form_validation->set_rules('longitude', 'longitude tidak boleh kosong', 'required');
        $this->form_validation->set_message('required', 'Anda melewatkan input, {field}!');
        
        if ($_FILES['file']['name'] != '' or $_FILES['file']['name'] != null) {
            $this->form_validation->set_rules('file', 'file ', 'callback_check_file');
        }
       // $this->form_validation->set_rules('file', 'file ', 'callback_check_file');
        // $this->form_validation->set_message('is_unique', 'judul sudah terdaftar, judul tidak boleh sama');
        if ($this->form_validation->run() != FALSE) {

            $data['nama'] = $this->input->post('nama', TRUE);
            $data['npwp'] = preg_replace('/\D/', '', $this->input->post('npwp'));
            $data['alamat'] = $this->input->post('alamat');
            $data['provinsi_id'] = $this->input->post('prov_id');
            $data['kabupaten_id'] = $this->input->post('kab_id');
            $data['kecamatan_id'] = $this->input->post('kec_id');
            $data['deskripsi'] = $this->input->post('deskripsi', TRUE);
            $data['dibuat_pada'] = date("Y-m-d H:i:s");
            
           
            $new_name = date("H-i-s_dmY");
            $config['encrypt_name'] = TRUE;
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['upload_path'] = 'uploads/images/perusahaan';
            $this->load->library('upload', $config);
            
            $upload_data = $this->upload->do_upload('file');
            $foto = $this->upload->data();
            $file_name = $foto['raw_name'] . $foto['file_ext'];
            $data['foto'] = $file_name;
            $data['foto_thumbnail_path'] = "uploads/images/perusahaan/".$file_name;
            $data['status'] = 0;
            $data['latitude'] = $this->input->post('latitude');
            $data['longitude'] = $this->input->post('longitude');
           // $this->db->trans_begin();
            try{
                $this->db->trans_start();
                $this->db->insert('perusahaan', $data);
                $this->db->trans_commit();
                
                $json['status'] = true;
            } catch(Exception $e){
                 $json['status'] = false;
                $this->db->trans_rollback();
            
            }
            
          
        } else {

            $json['status'] = false;
            
            if ($_FILES['file']['name'] != '' or $_FILES['file']['name'] != null) {
                $json['file'] = form_error('file', '<p class="text-danger">', '</p>');
            }
            
            $json['nama'] = form_error('nama', '<p class="text-danger">', '</p>');
            $json['npwp'] = form_error('npwp', '<p class="text-danger">', '</p>');
            $json['alamat'] = form_error('alamat', '<p class="text-danger">', '</p>');
            $json['deskripsi'] = form_error('deskripsi', '<p class="text-danger">', '</p>');
            $json['status'] = form_error('status', '<p class="text-danger">', '</p>');
            $json['provinsi_id'] = form_error('provinsi_id', '<p class="text-danger">', '</p>');
            $json['kabupaten_id'] = form_error('kabupaten_id', '<p class="text-danger">', '</p>');
            $json['kecamatan_id'] = form_error('kecamatan_id', '<p class="text-danger">', '</p>');
            $json['latitude'] = form_error('latitude', '<p class="text-danger">', '</p>');
            $json['longitude'] = form_error('longitude', '<p class="text-danger">', '</p>');
        }
        $json['csrf'] = generate_csrf();
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($json));
    }

    function update() {
        $id = @hexToStr($this->input->post('id'));
        $json = array();
        $data = array();

        $this->form_validation->set_rules('id', 'id tidak boleh kosong', 'required|trim');
        $this->form_validation->set_rules('nama', 'nama tidak boleh kosong', 'required|trim|callback_check_uniq_judul');
        $this->form_validation->set_rules('npwp', 'npwp tidak boleh kosong', 'required');
        $this->form_validation->set_rules('alamat', 'alamat tidak boleh kosong', 'required');
        $this->form_validation->set_rules('prov_id', 'Provinsi tidak boleh kosong', 'required');
        $this->form_validation->set_rules('kab_id', 'Kabupaten tidak boleh kosong', 'required');
        $this->form_validation->set_rules('kec_id', 'Kecamatan tidak boleh kosong', 'required');
        $this->form_validation->set_rules('latitude', 'latitude tidak boleh kosong', 'required');
        $this->form_validation->set_rules('longitude', 'longitude tidak boleh kosong', 'required');
        $this->form_validation->set_rules('deskripsi', 'deskripsi tidak boleh kosong', 'required');
       
        $this->form_validation->set_message('required', 'Anda melewatkan input, {field}!');
        if ($this->form_validation->run() != FALSE) {
            $data['nama'] = $this->input->post('nama', TRUE);
            $data['npwp'] = preg_replace('/\D/', '', $this->input->post('npwp'));
            $data['alamat'] = $this->input->post('alamat');
            $data['deskripsi'] = $this->input->post('deskripsi', TRUE);
            $data['provinsi_id'] = $this->input->post('prov_id');
            $data['kabupaten_id'] = $this->input->post('kab_id');
            $data['kecamatan_id'] = $this->input->post('kec_id');
            $data['diubah_pada'] = date("Y-m-d H:i:s");
            $data['diubah_oleh'] = $this->session->userdata('id');
            
            $config['encrypt_name'] = TRUE;
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['upload_path'] = 'uploads/images/perusahaan';
            $this->load->library('upload', $config);
            
            $upload_data = $this->upload->do_upload('file');
            $foto = $this->upload->data();
            $file_name = $foto['raw_name'] . $foto['file_ext'];
            $data['foto'] = $file_name;
            $data['foto_thumbnail_path'] = "uploads/images/perusahaan/".$file_name;
            $data['latitude'] = $this->input->post('latitude');
            $data['longitude'] = $this->input->post('longitude');
            
             try{
                $this->db->trans_start();
                $this->db->where('id', $id);
                $this->db->update('perusahaan', $data);
                $this->db->trans_commit();
                
                $json['status'] = true;
            } catch(Exception $e){
                 $json['status'] = false;
                $this->db->trans_rollback();
            
            }
        } else {

            $json['status'] = false;
            
             if ($_FILES['file']['name'] != '' or $_FILES['file']['name'] != null) {
                $json['file'] = form_error('file', '<p class="text-danger">', '</p>');
            }
            
            $json['nama'] = form_error('nama', '<p class="text-danger">', '</p>');
            $json['npwp'] = form_error('npwp', '<p class="text-danger">', '</p>');
            $json['alamat'] = form_error('alamat', '<p class="text-danger">', '</p>');
            $json['deskripsi'] = form_error('deskripsi', '<p class="text-danger">', '</p>');
            $json['status'] = form_error('status', '<p class="text-danger">', '</p>');
            $json['provinsi_id'] = form_error('provinsi_id', '<p class="text-danger">', '</p>');
            $json['kabupaten_id'] = form_error('kabupaten_id', '<p class="text-danger">', '</p>');
            $json['kecamatan_id'] = form_error('kecamatan_id', '<p class="text-danger">', '</p>');
            $json['latitude'] = form_error('latitude', '<p class="text-danger">', '</p>');
            $json['longitude'] = form_error('longitude', '<p class="text-danger">', '</p>');
        }

        $json['csrf'] = generate_csrf();
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($json));
    }

    function detail() {
        $id = hexToStr($this->input->get('id'));
        $data = $this->m_slider->detail($id);
        $return = "";
        $status = false;
        if (@$data) {
            $status = true;
            $return = $data;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(
                                array('status' => $status, 'data' => $return, 'q' => $this->db->last_query())
        ));
    }

    public function check_uniq_judul() {
        $id = @hexToStr($this->input->post('id'));

        if ($id == '') {
            // insert data
            $data = $this->db->where(array('judul' => $this->input->post('judul'), 'dihapus_pada' => null))->get('berita')->num_rows();
        } else {
            // update data
            $data = $this->db->where(array('judul' => $this->input->post('judul'), 'dihapus_pada' => null, 'id !=' => $id))->get('berita')->num_rows();
        }

        if ($data == 0) {
            return TRUE;
        } else {
            $this->form_validation->set_message('check_uniq_judul', 'Judul telah digunakan');
            return FALSE;
        }
    }

    public function delete() {
        $this->db->trans_begin();
        $id = hexToStr($this->input->post('id'));
        $data = ['dihapus_pada' => date("Y-m-d H:i:s"), 'dihapus_oleh' => $this->session->userdata('id')];
        //update data berita
        $this->db->update('perusahaan', $data, ['id' => $id]);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            $json['status'] = false;
            $json['message'] = 'Gagal menghapus perusahaan';
        } else {
            $this->db->trans_commit();
            $json['status'] = true;
        }

        $json['csrf'] = generate_csrf();
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($json));
    }

    public function destroy($id) {
        echo $this->m_slider->remdir($id);
    }

    public function check_file() {

        $allowedExts = array("gif", "jpeg", "jpg", "png", "JPG", "JPEG", "GIF", "PNG");
        $extension = pathinfo($_FILES["file"]["name"], PATHINFO_EXTENSION);

        if ($_FILES['file']['size'] < 40000000 and in_array($extension, $allowedExts)) {
            return TRUE;
        } elseif (!in_array($extension, $allowedExts)) {
            $this->form_validation->set_message('check_file', 'Format file tidak sesuai');
            return FALSE;
        } elseif ($_FILES['file']['size'] > 40000000) {
            $this->form_validation->set_message('check_file', 'File terlalu besar');
            return FALSE;
        } else {
            $this->form_validation->set_message('check_file', 'Periksa kembali inputan anda');
            return FALSE;
        }
    }

    public function ubahStatus($id) {
        $id = hexToStr($id);
        $data['status'] = $this->input->post('status', TRUE);
        $data['diubah_oleh'] = $this->session->userdata('id');
        $data['diubah_pada'] = date("Y-m-d H:i:s");
        // mengubah data pada database
        $this->db->where('id', $id);
        $update = $this->db->update('perusahaan', $data);
        if ($update) {
            $json['status'] = true;
        } else {
            $json['status'] = false;
        }

        $json['csrf'] = generate_csrf();
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($json));
    }

}
