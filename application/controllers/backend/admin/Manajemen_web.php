<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Manajemen_web extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->id) {
            redirect('login/logout');
        }
        $this->load->model('backend/admin/M_manajemen_web', 'm_manajemen');
        $this->load->model(['All_crud']);
        // $this->load->library('Image');
    }

    function index()
    {
        $data = [
            'page_title'        => 'Manajemen Web',
            'detail_page_title' => 'List Data',
            'li_active'         => 'manajemen web',
            'uri_segment'       => 'backend/admin/manajemen_web/',
            'content'           => 'backend/admin/manajemen_web/edit/home',
            'script'            => 'backend/admin/manajemen_web/edit/home-js',
            'datatables_js'     => TRUE,
            'datatables_css'    => TRUE,
            'toastr'            => TRUE,
            'sweet_alert'       => TRUE,
            'id'                => 5,
            'btn_kembali'       => '<a href="' . @$_SERVER['HTTP_REFERER'] . '" class="btn btn-sm btn-rounded btn-outline-danger"> <i class="si si-arrow-left"></i> Kembali</a>',
            'btn_tambah'        => '<a href="' . base_url('backend/admin/manajemen_web/create') . '"><button type="button" href="javascript:;" class="btn btn-sm btn-rounded btn-outline-primary" id="tombol_tambah"> <i class="si si-plus"></i> Tambah</button></a>',
            'modal'             => array()
        ];

        $this->load->view('_templates/main', $data);
    }

    function get_data()
    {
        $res = $list = $this->m_manajemen->list_data();
        $res['csrf'] = generate_csrf();
        echo json_encode($res);
    }

    public function create()
    {
        $data = [
            'page_title'        => 'Tambah Data Manajemen Web',
            'detail_page_title' => 'Tambah Data',
            'li_active'         => 'tambah manajemen web',
            'uri_segment'       => 'backend/admin/manajemen_web/',
            'content'           => 'backend/admin/manajemen_web/tambah/home',
            'script'            => 'backend/admin/manajemen_web/tambah/home-js',
            'toastr'            => TRUE,
            'sweet_alert'       => TRUE,
            'sumernote'         => TRUE,
            'btn_kembali'       => '<a href="' . @$_SERVER['HTTP_REFERER'] . '" class="btn btn-sm btn-rounded btn-outline-danger"> <i class="si si-arrow-left"></i> Kembali</a>',
            'modal'             => array()
        ];

        $this->load->view('_templates/main', $data);
    }

    public function edit($id = null)
    {
        $data = [
            'page_title'        => 'Edit Data Manajemen Web',
            'detail_page_title' => 'Edit Data',
            'li_active'         => 'edit manajemen web',
            'uri_segment'       => 'backend/admin/manajemen_web/',
            'content'           => 'backend/admin/manajemen_web/edit/home',
            'script'            => 'backend/admin/manajemen_web/edit/home-js',
            'toastr'            => TRUE,
            'sweet_alert'       => TRUE,
            'sumernote'         => TRUE,
            'id'                => $id,
            'btn_kembali'       => '<a href="' . @$_SERVER['HTTP_REFERER'] . '" class="btn btn-sm btn-rounded btn-outline-danger"> <i class="si si-arrow-left"></i> Kembali</a>',
            'modal'             => array()
        ];

        $this->load->view('_templates/main', $data);
    }

    function store()
    {
        $json = array();
        $data = array();

        $this->form_validation->set_rules('judul', 'Judul tidak boleh kosong', 'required|trim|callback_check_uniq_judul');
        // $this->form_validation->set_rules('logo_dark','Status tidak boleh kosong','required');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi tidak boleh kosong', 'required');
        $this->form_validation->set_message('required', 'Anda melewatkan input, {field}!');
        $this->form_validation->set_rules('logo_dark', 'logo_dark ', 'callback_check_file_logo_dark');
        $this->form_validation->set_rules('logo_white', 'logo_white ', 'callback_check_file_logo_white');
        $this->form_validation->set_rules('logo_footer', 'logo_footer ', 'callback_check_file_logo_footer');
        // $this->form_validation->set_message('is_unique', 'judul sudah terdaftar, judul tidak boleh sama');
        if ($this->form_validation->run() != FALSE and ($_FILES['logo_dark']['name'] != '' or $_FILES['logo_dark']['name'] != null) and ($_FILES['logo_white']['name'] != '' or $_FILES['logo_white']['name'] != null) and ($_FILES['logo_footer']['name'] != '' or $_FILES['logo_footer']['name'] != null)) {

            $data['judul']          = $this->input->post('judul', TRUE);
            $data['deskripsi']         = $this->input->post('deskripsi', TRUE);
            $data['dibuat_pada']    = date("Y-m-d H:i:s");
            $data['diubah_pada']    = date("Y-m-d H:i:s");
            $data['diubah_oleh']    = $this->session->userdata('id');

            // $this->db->trans_begin();
            $this->db->insert('website', $data);
            $website_id   = $this->db->insert_id();

            $upload = $this->m_manajemen->fileUpload($website_id);

            if ($upload['status'] != false) {
                $json['status']                 = true;
                $data["logo_dark"]             = $upload['logo_dark']['location'] . $upload["logo_dark"]['file_large'];
                $data["logo_white"]             = $upload['logo_dark']['location'] . $upload["logo_white"]['file_large'];
                $data["logo_footer"]             = $upload['logo_dark']['location'] . $upload["logo_footer"]['file_large'];
                // update data di table
                $this->db->where('id', $website_id);
                $this->db->update('website', $data);
            } else {
                $json['status']  = false;
                $json['message'] = $upload['message'];
            }
        } else {

            $json['status']      = false;
            $json['judul']       = form_error('judul', '<p class="text-danger">', '</p>');
            $json['logo_dark']        = form_error('logo_dark', '<p class="text-danger">', '</p>');
            $json['logo_white']        = form_error('logo_white', '<p class="text-danger">', '</p>');
            $json['logo_footer']        = form_error('logo_footer', '<p class="text-danger">', '</p>');
            $json['deskripsi']      = form_error('deskripsi', '<p class="text-danger">', '</p>');
        }
        $json['csrf'] = generate_csrf();
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($json));
    }

    function update()
    {
        $id     = $this->input->post('id');
        $json = array();
        $data = array();

        $this->form_validation->set_rules('id', 'id tidak boleh kosong', 'required|trim');
        $this->form_validation->set_rules('judul', 'judul tidak boleh kosong', 'required|trim');
        $this->form_validation->set_rules('deskripsi', 'deskripsi tidak boleh kosong', 'required');
        if ($_FILES['logo_dark']['name'] != '' or $_FILES['logo_dark']['name'] != null) {
            $this->form_validation->set_rules('logo_dark', 'logo_dark', 'callback_check_file_logo_dark');
        }
        if ($_FILES['logo_white']['name'] != '' or $_FILES['logo_white']['name'] != null) {
            $this->form_validation->set_rules('logo_white', 'logo_white', 'callback_check_file_logo_white');
        }
        if ($_FILES['logo_footer']['name'] != '' or $_FILES['logo_footer']['name'] != null) {
            $this->form_validation->set_rules('logo_footer', 'logo_footer', 'callback_check_file_logo_footer');
        }
        $this->form_validation->set_message('required', 'Anda melewatkan input, {field}!');
        if ($this->form_validation->run() != FALSE) {

            $data['judul']         = $this->input->post('judul', TRUE);
            $data['judul_pendek']  = $this->input->post('judul_pendek', TRUE);
            $data['facebook']      = $this->input->post('facebook', TRUE);
            $data['twitter']       = $this->input->post('twitter', TRUE);
            $data['instagram']     = $this->input->post('instagram', TRUE);
            $data['no_hp']         = $this->input->post('no_hp', TRUE);
            $data['copyright']     = $this->input->post('copyright', TRUE);
            $data['deskripsi']     = $this->input->post('deskripsi', TRUE);
            $data['alamat']        = $this->input->post('alamat', TRUE);
            $data['diubah_oleh']   = $this->session->userdata('id');
            $data['diubah_pada']   = date("Y-m-d H:i:s");

            $this->db->where('id',  $id);
            $this->db->update('website', $data);


            $json['status']   = true;
            if (($_FILES['logo_dark']['name'] != '' or $_FILES['logo_dark']['name'] != null) or ($_FILES['logo_white']['name'] != '' or $_FILES['logo_white']['name'] != null) or ($_FILES['logo_footer']['name'] != '' or $_FILES['logo_footer']['name'] != null)) {
                // $this->m_manajemen->remdir($id);
                $upload = $this->m_manajemen->fileUpload($id);

                if ($upload['status'] != false) {
                    $json['status']   = true;

                    if ($upload['logo_dark']['status']) {
                        # code...
                        $data["logo_dark"]             = $upload['logo_dark']['location'] . $upload["logo_dark"]['file_name'];
                    }
                    if ($upload['logo_white']['status']) {
                        # code...
                        $data["logo_white"]             = $upload['logo_white']['location'] . $upload["logo_white"]['file_name'];
                    }
                    if ($upload['logo_footer']['status']) {
                        # code...
                        $data["logo_footer"]             = $upload['logo_footer']['location'] . $upload["logo_footer"]['file_name'];
                    }
                    // update data di table
                    $this->db->where('id', $id);
                    $this->db->update('website', $data);
                } else {
                    $json['status']  = false;
                    $json['message'] = $upload['message'];
                }
            }
        } else {

            $json['status']      = false;
            $json['id']          = form_error('id', '<p class="text-danger">', '</p>');
            $json['judul']       = form_error('judul', '<p class="text-danger">', '</p>');

            if ($_FILES['logo_dark']['name'] != '' or $_FILES['logo_dark']['name'] != null) {
                $json['logo_dark']        = form_error('logo_dark', '<p class="text-danger">', '</p>');
            }
            if ($_FILES['logo_white']['name'] != '' or $_FILES['logo_white']['name'] != null) {
                $json['logo_white']        = form_error('logo_white', '<p class="text-danger">', '</p>');
            }
            if ($_FILES['logo_footer']['name'] != '' or $_FILES['logo_footer']['name'] != null) {
                $json['logo_footer']        = form_error('logo_footer', '<p class="text-danger">', '</p>');
            }

            $json['deskripsi']      = form_error('deskripsi', '<p class="text-danger">', '</p>');
        }

        $json['csrf'] = generate_csrf();
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($json));
    }

    function detail()
    {
        $id = 5;
        $data = $this->m_manajemen->detail($id);
        $return = "";
        $status = false;
        if (@$data) {
            $status = true;
            $return = $data;
        }
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(
                array('status' => $status, 'data' => $return)
            ));
    }

    public function check_uniq_judul()
    {
        $id = @hexToStr($this->input->post('id'));

        if ($id == '') {
            // insert data
            $data = $this->db->where(array('judul' => $this->input->post('judul'), 'dihapus_pada' => null))->get('website')->num_rows();
        } else {
            // update data
            $data = $this->db->where(array('judul' => $this->input->post('judul'), 'dihapus_pada' => null, 'id !=' => $id))->get('website')->num_rows();
        }

        if ($data == 0) {
            return TRUE;
        } else {
            $this->form_validation->set_message('check_uniq_judul', 'Judul telah digunakan');
            return FALSE;
        }
    }

    public function delete()
    {
        $this->db->trans_begin();
        $id = hexToStr($this->input->post('id'));
        $data = ['dihapus_pada' => date("Y-m-d H:i:s"), 'dihapus_oleh' => $this->session->userdata('id')];
        //update data berita
        $this->db->update('program', $data, ['id' => $id]);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            $json['status']  = false;
            $json['message'] = 'Gagal menghapus berita';
        } else {
            $this->db->trans_commit();
            $json['status']  = true;
        }

        $json['csrf'] = generate_csrf();
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($json));
    }

    public function destroy($id)
    {
        echo $this->m_program->remdir($id);
    }

    public function check_file_logo_dark()
    {

        $allowedExts = array("gif", "jpeg", "jpg", "png", "JPG", "JPEG", "GIF", "PNG");
        $extension   = pathinfo($_FILES["logo_dark"]["name"], PATHINFO_EXTENSION);

        if ($_FILES['logo_dark']['size'] < 2000000 and in_array($extension, $allowedExts)) {
            return TRUE;
        } elseif (!in_array($extension, $allowedExts)) {
            $this->form_validation->set_message('check_file', 'Format file tidak sesuai');
            return FALSE;
        } elseif ($_FILES['logo_dark']['size'] > 2000000) {
            $this->form_validation->set_message('check_file', 'File terlalu besar');
            return FALSE;
        } else {
            $this->form_validation->set_message('check_file', 'Periksa kembali inputan anda');
            return FALSE;
        }
    }

    public function check_file_logo_white()
    {

        $allowedExts = array("gif", "jpeg", "jpg", "png", "JPG", "JPEG", "GIF", "PNG");
        $extension   = pathinfo($_FILES["logo_white"]["name"], PATHINFO_EXTENSION);

        if ($_FILES['logo_white']['size'] < 2000000 and in_array($extension, $allowedExts)) {
            return TRUE;
        } elseif (!in_array($extension, $allowedExts)) {
            $this->form_validation->set_message('check_file', 'Format file tidak sesuai');
            return FALSE;
        } elseif ($_FILES['logo_white']['size'] > 2000000) {
            $this->form_validation->set_message('check_file', 'File terlalu besar');
            return FALSE;
        } else {
            $this->form_validation->set_message('check_file', 'Periksa kembali inputan anda');
            return FALSE;
        }
    }

    public function check_file_logo_footer()
    {

        $allowedExts = array("gif", "jpeg", "jpg", "png", "JPG", "JPEG", "GIF", "PNG");
        $extension   = pathinfo($_FILES["logo_footer"]["name"], PATHINFO_EXTENSION);

        if ($_FILES['logo_footer']['size'] < 2000000 and in_array($extension, $allowedExts)) {
            return TRUE;
        } elseif (!in_array($extension, $allowedExts)) {
            $this->form_validation->set_message('check_file', 'Format file tidak sesuai');
            return FALSE;
        } elseif ($_FILES['logo_footer']['size'] > 2000000) {
            $this->form_validation->set_message('check_file', 'File terlalu besar');
            return FALSE;
        } else {
            $this->form_validation->set_message('check_file', 'Periksa kembali inputan anda');
            return FALSE;
        }
    }
}
