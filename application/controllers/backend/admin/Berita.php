<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Berita extends MY_Controller {

    public function __construct(){
        parent::__construct();
        if(!$this->id){
            redirect('login/logout');
        } 
        $this->load->model('backend/admin/M_berita', 'm_main');
        $this->load->model(['All_crud']);
        // $this->load->library('Image');
    }

    function index() {  
        $data = [ 
            'page_title'        => 'Berita',
            'detail_page_title' => 'List Data',
            'li_active'         => 'berita',
            'uri_segment'       => 'backend/admin/berita/',
            'content'           => 'backend/admin/berita/home',
            'script'            => 'backend/admin/berita/home-js',
            'datatables_js'     => TRUE,
            'datatables_css'    => TRUE,
            'toastr'            => TRUE,
            'sweet_alert'       => TRUE,
            'btn_kembali'       => '<a href="'.@$_SERVER['HTTP_REFERER'].'" class="btn btn-sm btn-rounded btn-outline-danger"> <i class="si si-arrow-left"></i> Kembali</a>',
            'btn_tambah'        => '<a href="'.base_url('backend/admin/berita/create').'"><button type="button" href="javascript:;" class="btn btn-sm btn-rounded btn-outline-primary" id="tombol_tambah"> <i class="si si-plus"></i> Tambah</button></a>',
            'modal'             => array(
            )
        ];

        $this->load->view('_templates/main', $data);
    }

    function get_data(){
        $res = $list = $this->m_main->list_data(); 
        $res['csrf'] = generate_csrf();
        echo json_encode($res);
    }

    public function create()
    {
        $data = [ 
            'page_title'        => 'Tambah Data Berita',
            'detail_page_title' => 'Tambah Data',
            'li_active'         => 'tambah berita',
            'uri_segment'       => 'backend/admin/berita/',
            'content'           => 'backend/admin/berita/tambah/home',
            'script'            => 'backend/admin/berita/tambah/home-js',
            'toastr'            => TRUE,
            'sweet_alert'       => TRUE,
            // 'sumernote'         => TRUE,
            'btn_kembali'       => '<a href="'.@$_SERVER['HTTP_REFERER'].'" class="btn btn-sm btn-rounded btn-outline-danger"> <i class="si si-arrow-left"></i> Kembali</a>',
            'modal'         => array(
                'backend/admin/berita/tambah/modal-tambah', 
            )
        ];

        $this->load->view('_templates/main', $data);
    }

    public function edit($id = null)
    {
        $data = [ 
            'page_title'        => 'Edit Data Berita',
            'detail_page_title' => 'Edit Data',
            'li_active'         => 'berita',
            'berita'            => $this->db->select('konten')->where([ 'dihapus_pada' => null, 'id' => @hexToStr($id)])->get('berita')->row(),
            'uri_segment'       => 'backend/admin/berita/',
            'content'           => 'backend/admin/berita/edit/home',
            'script'            => 'backend/admin/berita/edit/home-js',
            'toastr'            => TRUE,
            'sweet_alert'       => TRUE,
            // 'sumernote'         => TRUE,
            'id'                => $id,
            'btn_kembali'       => '<a href="'.@$_SERVER['HTTP_REFERER'].'" class="btn btn-sm btn-rounded btn-outline-danger"> <i class="si si-arrow-left"></i> Kembali</a>',
            'modal'             => array(
                'backend/admin/berita/edit/modal-tambah', 
            )
        ];

        $this->load->view('_templates/main', $data);
    }

    function store() {
        $json = array(); $data = array();

        $this->form_validation->set_rules('judul','judul tidak boleh kosong','required|trim|callback_check_uniq_judul');
        $this->form_validation->set_rules('kategori_id','kategori tidak boleh kosong','required');
        $this->form_validation->set_rules('konten','konten tidak boleh kosong','required');
        $this->form_validation->set_rules('status','Status tidak boleh kosong','required');
        $this->form_validation->set_message('required', 'Anda melewatkan input, {field}!');
        $this->form_validation->set_rules('file', 'file ', 'callback_check_file');
        // $this->form_validation->set_message('is_unique', 'judul sudah terdaftar, judul tidak boleh sama');
        if ($this->form_validation->run() != FALSE AND ($_FILES['file']['name'] != '' OR $_FILES['file']['name'] != null) ){

            $data['judul']          = $this->input->post('judul', TRUE);
            $data['kategori_id']    = $this->input->post('kategori_id', TRUE);
            $data['user_id']        = $this->session->userdata('id');
            $data['slug']           = slugify($this->input->post('judul', TRUE));
            $data['konten']         = $this->input->post('konten', TRUE);
            $data['status']         = $this->input->post('status', TRUE);
            $data['tanggal_pablis'] = date("Y-m-d H:i:s");
            $data['dibuat_pada']    = date("Y-m-d H:i:s");
            $data['diubah_pada']    = date("Y-m-d H:i:s");
            $data['diubah_oleh']    = $this->session->userdata('id');
            
            // $this->db->trans_begin();
            $this->db->insert('berita', $data);
            $berita_id   = $this->db->insert_id();

            // kebutuhan push notifikasi
            $this->load->view('../../vendor/autoload.php');
            $options = array(
                'cluster' => 'ap1',
                'useTLS' => true
            );
            $pusher = new Pusher\Pusher(
                '4b7d59c739af7d7ca6d2',
                '1dcbdcfd5a68151cc94d',
                '1125954',
                $options
            );
            $count_sidebar['berita'] = count_berita();
            $pusher->trigger('web-kemenpar-ap', 'count-sidebar', $count_sidebar);
            // kebutuhan push notifikasi  
    
            $upload = $this->m_main->fileUpload($berita_id);

            if($upload['status'] != false){
                $json['status']                 = true;
                $data["foto_besar"]             = $upload['file_large'];
                $data["foto_besar_path"]        = $upload['location'].$upload['file_large'];
                $data["foto_thumbnail"]         = $upload['file_thumb'];
                $data["foto_thumbnail_path"]    = $upload['location'].$upload['file_thumb'];
                // update data di table
                $this->db->where('id', $berita_id);
                $this->db->update('berita', $data);
            }else{
                $json['status']  = false;
                $json['message'] = $upload['message'];
            }
            
        } else{

            $json['status']      = false;
            $json['judul']       = form_error('judul', '<p class="text-danger">', '</p>');
            $json['file']        = form_error('file', '<p class="text-danger">', '</p>');
            $json['konten']      = form_error('konten', '<p class="text-danger">', '</p>');
            $json['kategori_id'] = form_error('kategori_id', '<p class="text-danger">', '</p>');
            $json['status']      = form_error('status', '<p class="text-danger">', '</p>');
        	
        }
        $json['csrf'] = generate_csrf();
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($json)); 
    }

    function update() {
        $id     = @hexToStr($this->input->post('id'));
        $json = array(); $data = array();
        // print_r($this->input->post('konten', TRUE));exit;

        $this->form_validation->set_rules('id','id tidak boleh kosong','required|trim');
        $this->form_validation->set_rules('judul','judul tidak boleh kosong','required|trim|callback_check_uniq_judul');
        $this->form_validation->set_rules('kategori_id','kategori tidak boleh kosong','required');
        $this->form_validation->set_rules('konten','konten tidak boleh kosong','required');
        $this->form_validation->set_rules('status','Status tidak boleh kosong','required');
        if($_FILES['file']['name'] != '' OR $_FILES['file']['name'] != null){
            $this->form_validation->set_rules('file', 'file ', 'callback_check_file');
        }
        $this->form_validation->set_message('required', 'Anda melewatkan input, {field}!');
        if ($this->form_validation->run() != FALSE) {
            
            $data['judul']         = $this->input->post('judul', TRUE);
            $data['kategori_id']   = $this->input->post('kategori_id', TRUE);
            $data['slug']          = slugify($this->input->post('judul', TRUE));
            $data['konten']        = $this->input->post('konten', TRUE);
            $data['status']        = $this->input->post('status', TRUE);
            $data['diubah_oleh']   = $this->session->userdata('id');
            $data['diubah_pada']   = date("Y-m-d H:i:s");

            $this->db->where('id',  $id);
            $this->db->update('berita', $data); 

            
            $json['status']   = true;
            if($_FILES['file']['name'] != '' OR $_FILES['file']['name'] != null){
                $this->m_main->remdir($id);
                $upload = $this->m_main->fileUpload($id);

                if($upload['status'] != false){
                    $json['status']   = true;
                    $data["foto_besar"]             = $upload['file_large'];
                    $data["foto_besar_path"]        = $upload['location'].$upload['file_large'];
                    $data["foto_thumbnail"]         = $upload['file_thumb'];
                    $data["foto_thumbnail_path"]    = $upload['location'].$upload['file_thumb'];
                    // update data di table
                    $this->db->where('id', $id);
                    $this->db->update('berita', $data);
                }else{
                    $json['status']  = false;
                    $json['message'] = $upload['message'];
                }
            }

        } else{
            
            $json['status']      = false;
            $json['id']          = form_error('id', '<p class="text-danger">', '</p>');
            $json['judul']       = form_error('judul', '<p class="text-danger">', '</p>');

            if($_FILES['file']['name'] != '' OR $_FILES['file']['name'] != null){
                $json['file']        = form_error('file', '<p class="text-danger">', '</p>');
            }
            
            $json['konten']      = form_error('konten', '<p class="text-danger">', '</p>');
            $json['kategori_id'] = form_error('kategori_id', '<p class="text-danger">', '</p>');
            $json['status']      = form_error('status', '<p class="text-danger">', '</p>');

        }

        $json['csrf'] = generate_csrf();
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($json)); 
    }

    function detail()
    { 
        $id = hexToStr($this->input->get('id'));
        $data = $this->m_main->detail($id);
        $return = ""; $status = false;
        if (@$data) { 
            $status = true;
            $return = $data;
        }
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(
                array('status' => $status,'data' => $return)
            )); 
    }

    public function check_uniq_judul()
    {
        $id = @hexToStr($this->input->post('id'));

        if($id == ''){
            // insert data
            $data = $this->db->where(array('judul' => $this->input->post('judul'), 'dihapus_pada' => null))->get('berita')->num_rows();
        }else{
            // update data
            $data = $this->db->where(array('judul' => $this->input->post('judul'), 'dihapus_pada' => null, 'id !=' => $id))->get('berita')->num_rows();
        }
        
        if ($data == 0) {
            return TRUE;
        }else{
            $this->form_validation->set_message('check_uniq_judul', 'Judul telah digunakan');
            return FALSE;
        }
    }

    public function delete()
    {
        $this->db->trans_begin();
        $id = hexToStr($this->input->post('id'));
        $data = ['dihapus_pada' => date("Y-m-d H:i:s"), 'dihapus_oleh' => $this->session->userdata('id')];
        //update data berita
        $this->db->update('berita', $data, ['id' => $id]);

        if($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            $json['status']  = false;
            $json['message'] = 'Gagal menghapus berita';
        } else {
            $this->db->trans_commit();
            $json['status']  = true;

            // kebutuhan push notifikasi
            $this->load->view('../../vendor/autoload.php');
            $options = array(
                'cluster' => 'ap1',
                'useTLS' => true
            );
            $pusher = new Pusher\Pusher(
                '4b7d59c739af7d7ca6d2',
                '1dcbdcfd5a68151cc94d',
                '1125954',
                $options
            );
            $count_sidebar['berita'] = count_berita();
            $pusher->trigger('web-kemenpar-ap', 'count-sidebar', $count_sidebar);
            // kebutuhan push notifikasi   

        }

        $json['csrf'] = generate_csrf();
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($json)); 
    }

    public function destroy($id)
    {
        echo $this->m_main->remdir($id);
    }

    public function check_file()
    {

        $allowedExts = array("gif", "jpeg", "jpg", "png", "JPG", "JPEG", "GIF", "PNG");
        $extension   = pathinfo($_FILES["file"]["name"], PATHINFO_EXTENSION);

        if($_FILES['file']['size'] < 2000000 AND in_array($extension, $allowedExts)){
            return TRUE;
        }elseif(!in_array($extension, $allowedExts)){
            $this->form_validation->set_message('check_file', 'Format file tidak sesuai');
            return FALSE;
        }elseif($_FILES['file']['size'] > 2000000){
            $this->form_validation->set_message('check_file', 'File terlalu besar');
            return FALSE;
        }else{
            $this->form_validation->set_message('check_file', 'Periksa kembali inputan anda');
            return FALSE;
        }
    }

    public function ubahStatus($id)
    {
        $id = hexToStr($id); 
        $data['status']        = $this->input->post('status', TRUE);
        $data['diubah_oleh']   = $this->session->userdata('id');
        $data['diubah_pada']   = date("Y-m-d H:i:s");
        // mengubah data pada database
        $this->db->where('id', $id);
        $update = $this->db->update('berita', $data); 
        if($update){
            $json['status'] = true;

            // kebutuhan push notifikasi
            $this->load->view('../../vendor/autoload.php');
            $options = array(
                'cluster' => 'ap1',
                'useTLS' => true
            );
            $pusher = new Pusher\Pusher(
                '4b7d59c739af7d7ca6d2',
                '1dcbdcfd5a68151cc94d',
                '1125954',
                $options
            );
            $count_sidebar['berita'] = count_berita();
            $pusher->trigger('web-kemenpar-ap', 'count-sidebar', $count_sidebar);
            // kebutuhan push notifikasi 

            // kebutuhan push notifikasi
            $this->load->view('../../vendor/autoload.php');
            $options = array(
                'cluster' => 'ap1',
                'useTLS' => true
            );
            $pusher = new Pusher\Pusher(
                '4b7d59c739af7d7ca6d2',
                '1dcbdcfd5a68151cc94d',
                '1125954',
                $options
            );
            $count_sidebar['berita_non_aktif'] = count_berita_non_aktif();
            $pusher->trigger('web-kemenpar-ap', 'count-sidebar', $count_sidebar);
            // kebutuhan push notifikasi  

        }else{
            $json['status'] = false;
        }

        $json['csrf'] = generate_csrf();
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($json)); 
    }
}