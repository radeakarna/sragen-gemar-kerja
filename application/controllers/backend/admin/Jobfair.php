<?php

ini_set('memory_limit', '512M');

defined('BASEPATH') or exit('No direct script access allowed');

class Jobfair extends MY_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->id) {
            redirect('login/logout');
        }
        $this->load->model('backend/admin/M_jobfair', 'm_jobfair');
        $this->load->model(['All_crud']);
    }

    function index() {
        $data = [
            'page_title' => 'Job fair',
            'detail_page_title' => 'List Data',
            'li_active' => 'jobfair',
            'uri_segment' => 'backend/admin/jobfair/',
            'content' => 'backend/admin/jobfair/jobfair',
            'script' => 'backend/admin/jobfair/jobfair_js',
            'datatables_js' => TRUE,
            'datatables_css' => TRUE,
            'toastr' => TRUE,
            'sweet_alert' => TRUE,
            'btn_kembali' => '<a href="' . @$_SERVER['HTTP_REFERER'] . '" class="btn btn-sm btn-rounded btn-outline-danger"> <i class="si si-arrow-left"></i> Kembali</a>',
            'btn_tambah' => '<a href="' . base_url('backend/admin/jobfair/create') . '"><button type="button" href="javascript:;" class="btn btn-sm btn-rounded btn-outline-primary" id="tombol_tambah"> <i class="si si-plus"></i> Tambah</button></a>',
            'modal' => array()
        ];

        $this->load->view('_templates/main', $data);
    }

    function get_data() {
        $res = $this->m_jobfair->list_data();
        $res['csrf'] = generate_csrf();
        echo json_encode($res);
    }
    
    function arrkelurahan($idkec=null){
		$arrkelurahan = array();
		if($idkec!=null){
			$kelurahan = $this->db->select('id,kelurahan_nama')
								->where('kelurahan_kecamatan_id',$idkec)
								->where('aktif','1')
								->get('ref_kelurahan')
								->result();
		}else{
			$kelurahan = $this->db->select('id,kelurahan_nama')
								->where('aktif','1')
								->get('ref_kelurahan')
								->result();
		}
		foreach($kelurahan as $kelurahanq){
			$arrkelurahan[$kelurahanq->id] = $kelurahanq->kelurahan_nama;
		}
		return $arrkelurahan;
    }

    function arrprovinsi(){
        $arrprovinsi = array();
        
        $prov = $this->db->select('id,nama')->get('ref_provinsi')->result();                                                                                        
        foreach($prov as $provin){
                $arrprovinsi[$provin->id] = $provin->nama;
        }
        return $arrprovinsi;
    }
    
    function arrkabupaten($idprov=null){
        $arrkabupaten = array();
        if($idprov!=null){
            $kabupaten = $this->db->select('id,nama')
                                ->where('provinsi_id',$idprov)
                                ->get('ref_kabupaten')
                                ->result();
        }else{
            $kabupaten = array('id' => 0, 'nama' => 'Kabupaten belum didaftarkan');
        }

        $arrkabupaten = $kabupaten;
        return $arrkabupaten;
    }
    
    function arrkec($idkec=null){
        $arrkec = array();
        if($idkec!=null){
                $kecamatan = $this->db->select('id,kecamatan_nama')
                                                        ->where('kabupaten_id',$idkec)
                                                        ->get('ref_kecamatan')
                                                        ->result();
        }else{
               $kecamatan = array('id' => 0, 'nama' => 'Kecamatan belum didaftarkan');
        }
//        foreach($kecamatan as $kec){
//                $arrkec[$kec->id] = $kec->kecamatan_nama;
//        }
        $arrkec = $kecamatan;
        return $arrkec;
    }
    
        
    function loadpilkab(){
        $idprov = $this->input->get('prov');
      
        
        $arrkabupaten = $this->arrkabupaten($idprov);
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($arrkabupaten)); 
    }
    
    public function loadpilkec(){
        $idkec = $this->input->get('kab');
        $arrkecamatan = $this->arrkec($idkec);
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($arrkecamatan)); 
    }
    
    function loadpilkelurahan($idkecamatan,$idkelurahan='0'){
		$arraykelurahan = $this->arrkelurahan($idkecamatan);
		$printpil = '<div class="col-sm-12 row">
                        <label class="col-12" for="kel_id">Kelurahan</label>
                        <div class="col-md-12">
                            <select class="js-select2 form-control" id="tambah_kel_id" name="kel_id" style="width:100%;">
                            <option value="">- Silahkan pilih -</option>';
		foreach($arraykelurahan as $key => $value){
            if($idkelurahan == '0'){
                $printpil .= '<option value="'.$key.'"> '.$value.' </option>';
            }else{
                $printpil .= '<option value="'.$key.'" '.(($key==$idkelurahan)?'selected="selected"':"").'> '.$value.' </option>';
            }
		}
		$printpil .= '</select>
                        </div>
                                </div>
                                <script>
                                $("#tambah_kel_id").select2();
                                </script>
                                ';
		echo $printpil;
    }

    public function create() {

        $arrprov = $this->arrprovinsi();
        $pilprovinsi = '';
        
        $pilprovinsi .= '<option value="' . 'none' . '">' . 'Pilih Provinsi' . '</option>';
        foreach ($arrprov as $key => $value) {
            $pilprovinsi .= '<option value="' . $key . '">' . $value . '</option>';
        }
      
        $data = [
            'page_title' => 'Tambah Data Jobfair',
            'detail_page_title' => 'Tambah Data',
            'li_active' => 'jobfair',
            'provinsi' => $pilprovinsi,
            'uri_segment' => 'backend/admin/jobfair/',
            'content' => 'backend/admin/jobfair/tambah/jobfair',
            'script' => 'backend/admin/jobfair/tambah/jobfair_js',
            'toastr' => TRUE,
            'sweet_alert' => TRUE,
            'sumernote' => TRUE,
            'btn_kembali' => '<a href="' . @$_SERVER['HTTP_REFERER'] . '" class="btn btn-sm btn-rounded btn-outline-danger"> <i class="si si-arrow-left"></i> Kembali</a>',
            'modal' => array(
            // 'backend/admin/slider_utama/tambah/modal_tambah', 
            )
        ];

        $this->load->view('_templates/main', $data);
    }

    public function edit($id = null) {
        
        $arrprov = $this->arrprovinsi();
        $arrs_kab_tempat = array();
        $arrs_kec_tempat = array();
        //$arrkab = $this->arrkabupaten();
        
        $pilprovinsi = '';
       
        $data_get = $this->db->select("provinsi_id")->where('id', hexToStr($id))->get('job_fair')->result_array();
        $pilprovinsi .= '<option value="' . 'none' . '">' . 'Pilih Provinsi' . '</option>';
        foreach ($arrprov as $key => $value) {
            if($data_get[0]['provinsi_id'] == $key){
                $pilprovinsi .= '<option value="' . $key . '" selected>' . $value . '</option>';
            } else {
                $pilprovinsi .= '<option value="' . $key . '">' . $value . '</option>';
            }
        }
        
        $data_get_kab = $this->db->select("kabupaten_id")->where('id', hexToStr($id))->get('job_fair')->result_array();
        $arrkab = $this->db->select("id,nama")->where('id', $data_get_kab[0]['kabupaten_id'])->get('ref_kabupaten')->result();
        
        foreach($arrkab as $kab){
                $arrs_kab_tempat[$kab->id] = $kab->nama;
        }
        $pilkabupaten = '';
        foreach ($arrs_kab_tempat as $key => $value) {
            if($data_get_kab[0]['kabupaten_id'] == $key){
                $pilkabupaten .= '<option value="' . $key . '" selected>' . $value . '</option>';
            } else {
                $pilkabupaten .= '<option value="' . $key . '">' . $value . '</option>';
            }
        }
  
        $data_get_kec = $this->db->select("kecamatan_id")->where('id', hexToStr($id))->get('job_fair')->result_array();
        $arrkec = $this->db->select("id,kecamatan_nama")->where('id', $data_get_kec[0]['kecamatan_id'])->get('ref_kecamatan')->result();
        $pil_kecamatan = '';
        foreach($arrkec as $kec){
                $arrs_kec_tempat[$kec->id] = $kec->kecamatan_nama;
        }
        
        foreach ($arrs_kec_tempat as $key => $value) {
            if($data_get_kec[0]['kecamatan_id'] == $key){
                $pil_kecamatan .= '<option value="' . $key . '" selected>' . $value . '</option>';
            } else {
                $pil_kecamatan .= '<option value="' . $key . '">' . $value . '</option>';
            }
        }
       
        
        $data = [
            'page_title' => 'Edit Data Perusahaan',
            'detail_page_title' => 'Edit Data',
            'li_active' => 'perusahaan',
            'kabupaten' => $pilkabupaten,
            'provinsi' => $pilprovinsi,
            'kecamatan' => $pil_kecamatan,
            'uri_segment' => 'backend/admin/jobfair/',
            'content' => 'backend/admin/jobfair/edit/jobfair',
            'script' => 'backend/admin/jobfair/edit/jobfair_js',
            'toastr' => TRUE,
            'sweet_alert' => TRUE,
            'sumernote' => TRUE,
            'id' => $id,
            'btn_kembali' => '<a href="' . @$_SERVER['HTTP_REFERER'] . '" class="btn btn-sm btn-rounded btn-outline-danger"> <i class="si si-arrow-left"></i> Kembali</a>',
            'modal' => array()
        ];

        $this->load->view('_templates/main', $data);
    }

    function store() {
        $json = array();
        $data = array();

        $this->form_validation->set_rules('judul', 'judul tidak boleh kosong', 'required|trim|callback_check_uniq_judul');
        $this->form_validation->set_rules('mulai_sewa', 'mulai sewa tidak boleh kosong', 'required');
        $this->form_validation->set_rules('jatuh_tempo', 'jatuh tempo tidak boleh kosong', 'required');
        $this->form_validation->set_rules('prov_id', 'Provinsi tidak boleh kosong', 'required');
        $this->form_validation->set_rules('kab_id', 'Kabupaten tidak boleh kosong', 'required');
        $this->form_validation->set_rules('kec_id', 'Kecamatan tidak boleh kosong', 'required');
        $this->form_validation->set_rules('alamat', 'alamat tidak boleh kosong', 'required');
        $this->form_validation->set_rules('deskripsi', 'deskripsi tidak boleh kosong', 'required');
        
        $this->form_validation->set_message('required', 'Anda melewatkan input, {field}!');
      
        if ($this->form_validation->run() != FALSE) {

            $data['judul'] = $this->input->post('judul', TRUE);
            $data['tanggal_mulai'] = date("Y-m-d H:i:s", strtotime($this->input->post('mulai_sewa')));
            $data['tanggal_selesai'] = date("Y-m-d H:i:s", strtotime($this->input->post('jatuh_tempo')));
           
            $data['deskripsi'] = $this->input->post('deskripsi', TRUE);
            $data['status'] = 0;
            $data['provinsi_id'] = $this->input->post('prov_id');
            $data['kabupaten_id'] = $this->input->post('kab_id');
            $data['kecamatan_id'] = $this->input->post('kec_id');
            $data['alamat'] = $this->input->post('alamat');
            $data['jangka_waktu'] = $this->input->post('jangka_waktu');
            $data['dibuat_pada'] = date("Y-m-d H:i:s");

            // $this->db->trans_begin();
            try{
                $this->db->trans_start();
                $this->db->insert('job_fair', $data);
                $this->db->trans_commit();
                
                $json['status'] = true;
            } catch(Exception $e){
                 $json['status'] = false;
                $this->db->trans_rollback();
            
            }
        } else {

            $json['status'] = false;
            $json['judul'] = form_error('judul', '<p class="text-danger">', '</p>');
            $json['tanggal_mulai'] = form_error('tanggal_mulai', '<p class="text-danger">', '</p>');
            $json['tanggal_selesai'] = form_error('tanggal_selesai', '<p class="text-danger">', '</p>');
            
            $json['alamat'] = form_error('alamat', '<p class="text-danger">', '</p>');
            $json['provinsi_id'] = form_error('provinsi_id', '<p class="text-danger">', '</p>');
            $json['kabupaten_id'] = form_error('kabupaten_id', '<p class="text-danger">', '</p>');
            $json['kecamatan_id'] = form_error('kecamatan_id', '<p class="text-danger">', '</p>');
            $json['deskripsi'] = form_error('deskripsi', '<p class="text-danger">', '</p>');
            $json['status'] = form_error('status', '<p class="text-danger">', '</p>');
        }
        $json['csrf'] = generate_csrf();
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($json));
    }

    function update() {
        $id = @hexToStr($this->input->post('id'));
        $json = array();
        $data = array();

        $this->form_validation->set_rules('judul', 'judul tidak boleh kosong', 'required|trim|callback_check_uniq_judul');
        $this->form_validation->set_rules('mulai_sewa', 'mulai sewa tidak boleh kosong', 'required');
        $this->form_validation->set_rules('jatuh_tempo', 'jatuh tempo tidak boleh kosong', 'required');
        $this->form_validation->set_rules('prov_id', 'Provinsi tidak boleh kosong', 'required');
        $this->form_validation->set_rules('kab_id', 'Kabupaten tidak boleh kosong', 'required');
        $this->form_validation->set_rules('kec_id', 'Kecamatan tidak boleh kosong', 'required');
        $this->form_validation->set_rules('alamat', 'alamat tidak boleh kosong', 'required');
        $this->form_validation->set_rules('deskripsi', 'deskripsi tidak boleh kosong', 'required');
        
        $this->form_validation->set_message('required', 'Anda melewatkan input, {field}!');
      
        if ($this->form_validation->run() != FALSE) {

            $data['judul'] = $this->input->post('judul', TRUE);
            $data['tanggal_mulai'] = date("Y-m-d H:i:s", strtotime($this->input->post('mulai_sewa')));
            $data['tanggal_selesai'] = date("Y-m-d H:i:s", strtotime($this->input->post('jatuh_tempo')));
           
            $data['deskripsi'] = $this->input->post('deskripsi', TRUE);
            $data['status'] = 0;
            $data['provinsi_id'] = $this->input->post('prov_id');
            $data['kabupaten_id'] = $this->input->post('kab_id');
            $data['kecamatan_id'] = $this->input->post('kec_id');
            $data['alamat'] = $this->input->post('alamat');
            $data['deskripsi'] = $this->input->post('deskripsi');
            $data['jangka_waktu'] = $this->input->post('jangka_waktu');
            $data['dibuat_pada'] = date("Y-m-d H:i:s");

            // $this->db->trans_begin();
            try{
                $this->db->trans_start();
               $this->db->where('id', $id);
                $this->db->update('job_fair', $data);

                $this->db->trans_commit();
                
                $json['status'] = true;
            } catch(Exception $e){
                 $json['status'] = false;
                $this->db->trans_rollback();
            
            }
        } else {

            $json['status'] = false;
            $json['judul'] = form_error('judul', '<p class="text-danger">', '</p>');
            $json['tanggal_mulai'] = form_error('tanggal_mulai', '<p class="text-danger">', '</p>');
            $json['tanggal_selesai'] = form_error('tanggal_selesai', '<p class="text-danger">', '</p>');
            
            $json['alamat'] = form_error('alamat', '<p class="text-danger">', '</p>');
            $json['provinsi_id'] = form_error('provinsi_id', '<p class="text-danger">', '</p>');
            $json['kabupaten_id'] = form_error('kabupaten_id', '<p class="text-danger">', '</p>');
            $json['kecamatan_id'] = form_error('kecamatan_id', '<p class="text-danger">', '</p>');
            $json['deskripsi'] = form_error('deskripsi', '<p class="text-danger">', '</p>');
            $json['status'] = form_error('status', '<p class="text-danger">', '</p>');
        }
        $json['csrf'] = generate_csrf();
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($json));
    }

    function detail() {
        $id = hexToStr($this->input->get('id'));
        $data = $this->m_jobfair->detail($id);
        $return = "";
        $status = false;
        if (@$data) {
            $status = true;
            $return = $data;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(
                                array('status' => $status, 'data' => $return, 'q' => $this->db->last_query())
        ));
    }

    public function check_uniq_judul() {
        $id = @hexToStr($this->input->post('id'));

        if ($id == '') {
            // insert data
            $data = $this->db->where(array('judul' => $this->input->post('judul'), 'dihapus_pada' => null))->get('berita')->num_rows();
        } else {
            // update data
            $data = $this->db->where(array('judul' => $this->input->post('judul'), 'dihapus_pada' => null, 'id !=' => $id))->get('berita')->num_rows();
        }

        if ($data == 0) {
            return TRUE;
        } else {
            $this->form_validation->set_message('check_uniq_judul', 'Judul telah digunakan');
            return FALSE;
        }
    }

    public function delete() {
        $this->db->trans_begin();
        $id = hexToStr($this->input->post('id'));
        $data = ['dihapus_pada' => date("Y-m-d H:i:s"), 'dihapus_oleh' => $this->session->userdata('id')];
        //update data berita
        $this->db->update('job_fair', $data, ['id' => $id]);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            $json['status'] = false;
            $json['message'] = 'Gagal menghapus perusahaan';
        } else {
            $this->db->trans_commit();
            $json['status'] = true;
        }

        $json['csrf'] = generate_csrf();
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($json));
    }

    public function destroy($id) {
        echo $this->m_jobfair->remdir($id);
    }

    public function check_file() {

        $allowedExts = array("gif", "jpeg", "jpg", "png", "JPG", "JPEG", "GIF", "PNG");
        $extension = pathinfo($_FILES["file"]["name"], PATHINFO_EXTENSION);

        if ($_FILES['file']['size'] < 40000000 and in_array($extension, $allowedExts)) {
            return TRUE;
        } elseif (!in_array($extension, $allowedExts)) {
            $this->form_validation->set_message('check_file', 'Format file tidak sesuai');
            return FALSE;
        } elseif ($_FILES['file']['size'] > 40000000) {
            $this->form_validation->set_message('check_file', 'File terlalu besar');
            return FALSE;
        } else {
            $this->form_validation->set_message('check_file', 'Periksa kembali inputan anda');
            return FALSE;
        }
    }

    public function ubahStatus($id) {
        $id = hexToStr($id);
        $data['status'] = $this->input->post('status', TRUE);
        $data['diubah_oleh'] = $this->session->userdata('id');
        $data['diubah_pada'] = date("Y-m-d H:i:s");
        // mengubah data pada database
        $this->db->where('id', $id);
        $update = $this->db->update('job_fair', $data);
        if ($update) {
            $json['status'] = true;
        } else {
            $json['status'] = false;
        }

        $json['csrf'] = generate_csrf();
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($json));
    }

}
