<?php
ini_set('memory_limit', '512M');

defined('BASEPATH') or exit('No direct script access allowed');

class Slider_utama extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->id) {
            redirect('login/logout');
        }
        $this->load->model('backend/admin/M_slider_utama', 'm_slider');
        $this->load->model(['All_crud']);
        // $this->load->library('Image');
    }

    function index()
    {
        $data = [
            'page_title'        => 'Slider Utama',
            'detail_page_title' => 'List Data',
            'li_active'         => 'slider_utama',
            'uri_segment'       => 'backend/admin/slider_utama/',
            'content'           => 'backend/admin/slider/slider_utama',
            'script'            => 'backend/admin/slider/slider_utama_js',
            'datatables_js'     => TRUE,
            'datatables_css'    => TRUE,
            'toastr'            => TRUE,
            'sweet_alert'       => TRUE,
            'btn_kembali'       => '<a href="' . @$_SERVER['HTTP_REFERER'] . '" class="btn btn-sm btn-rounded btn-outline-danger"> <i class="si si-arrow-left"></i> Kembali</a>',
            'btn_tambah'        => '<a href="' . base_url('backend/admin/slider_utama/create') . '"><button type="button" href="javascript:;" class="btn btn-sm btn-rounded btn-outline-primary" id="tombol_tambah"> <i class="si si-plus"></i> Tambah</button></a>',
            'modal'             => array()
        ];

        $this->load->view('_templates/main', $data);
    }

    function get_data()
    {
        $res = $list = $this->m_slider->list_data();
        $res['csrf'] = generate_csrf();
        echo json_encode($res);
    }

    public function create()
    {
        $data = [
            'page_title'        => 'Tambah Data Slider',
            'detail_page_title' => 'Tambah Data',
            'li_active'         => 'slider_utama',
            'uri_segment'       => 'backend/admin/slider_utama/',
            'content'           => 'backend/admin/slider/tambah/slider_utama',
            'script'            => 'backend/admin/slider/tambah/slider_utama_js',
            'toastr'            => TRUE,
            'sweet_alert'       => TRUE,
            'sumernote'         => TRUE,
            'btn_kembali'       => '<a href="' . @$_SERVER['HTTP_REFERER'] . '" class="btn btn-sm btn-rounded btn-outline-danger"> <i class="si si-arrow-left"></i> Kembali</a>',
            'modal'         => array(
                // 'backend/admin/slider_utama/tambah/modal_tambah', 
            )
        ];

        $this->load->view('_templates/main', $data);
    }

    public function edit($id = null)
    {
        $data = [
            'page_title'        => 'Edit Data Slider',
            'detail_page_title' => 'Edit Data',
            'li_active'         => 'slider_utama',
            'uri_segment'       => 'backend/admin/slider_utama/',
            'content'           => 'backend/admin/slider/edit/home',
            'script'            => 'backend/admin/slider/edit/home-js',
            'toastr'            => TRUE,
            'sweet_alert'       => TRUE,
            'sumernote'         => TRUE,
            'id'                => $id,
            'btn_kembali'       => '<a href="' . @$_SERVER['HTTP_REFERER'] . '" class="btn btn-sm btn-rounded btn-outline-danger"> <i class="si si-arrow-left"></i> Kembali</a>',
            'modal'             => array()
        ];

        $this->load->view('_templates/main', $data);
    }

    function store()
    {
        $json = array();
        $data = array();

        $this->form_validation->set_rules('nama', 'nama tidak boleh kosong', 'required|trim|callback_check_uniq_judul');
        $this->form_validation->set_rules('deskripsi', 'deskripsi tidak boleh kosong', 'required');
        $this->form_validation->set_message('required', 'Anda melewatkan input, {field}!');
        $this->form_validation->set_rules('file', 'file ', 'callback_check_file');
        // $this->form_validation->set_message('is_unique', 'judul sudah terdaftar, judul tidak boleh sama');
        if ($this->form_validation->run() != FALSE and ($_FILES['file']['name'] != '' or $_FILES['file']['name'] != null)) {

            $data['nama']          = $this->input->post('nama', TRUE);
            $data['slug']           = slugify($this->input->post('nama', TRUE));
            $data['deskripsi']         = $this->input->post('deskripsi', TRUE);
            $data['status']         = 0;
            $data['dibuat_pada']    = date("Y-m-d H:i:s");
            $data['diubah_pada']    = date("Y-m-d H:i:s");
            $data['diubah_oleh']    = $this->session->userdata('id');

            // $this->db->trans_begin();
            $this->db->insert('slider', $data);
            $slider_id   = $this->db->insert_id();

            $upload = $this->m_slider->fileUpload($slider_id);

            if ($upload['status'] != false) {
                $json['status']                 = true;
                $data["foto"]             = $upload['file_large'];
                $data["foto_path"]        = $upload['location'] . $upload['file_large'];
                // update data di table
                $this->db->where('id', $slider_id);
                $this->db->update('slider', $data);
            } else {
                $json['status']  = false;
                $json['message'] = $upload['message'];
            }
        } else {

            $json['status']      = false;
            $json['nama']       = form_error('nama', '<p class="text-danger">', '</p>');
            $json['file']        = form_error('file', '<p class="text-danger">', '</p>');
            $json['deskripsi']      = form_error('deskripsi', '<p class="text-danger">', '</p>');
            $json['status']      = form_error('status', '<p class="text-danger">', '</p>');
        }
        $json['csrf'] = generate_csrf();
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($json));
    }

    function update()
    {
        $id     = @hexToStr($this->input->post('id'));
        $json = array();
        $data = array();

        $this->form_validation->set_rules('id', 'id tidak boleh kosong', 'required|trim');
        $this->form_validation->set_rules('nama', 'nama tidak boleh kosong', 'required|trim|callback_check_uniq_judul');
        $this->form_validation->set_rules('deskripsi', 'deskripsi tidak boleh kosong', 'required');
        if ($_FILES['file']['name'] != '' or $_FILES['file']['name'] != null) {
            $this->form_validation->set_rules('file', 'file ', 'callback_check_file');
        }
        $this->form_validation->set_message('required', 'Anda melewatkan input, {field}!');
        if ($this->form_validation->run() != FALSE) {

            $data['nama']         = $this->input->post('nama', TRUE);
            $data['slug']          = slugify($this->input->post('nama', TRUE));
            $data['deskripsi']        = $this->input->post('deskripsi', TRUE);
            $data['status']        = 0;
            $data['diubah_oleh']   = $this->session->userdata('id');
            $data['diubah_pada']   = date("Y-m-d H:i:s");

            $this->db->where('id',  $id);
            $this->db->update('slider', $data);


            $json['status']   = true;
            if ($_FILES['file']['name'] != '' or $_FILES['file']['name'] != null) {
                $this->m_slider->remdir($id);
                $upload = $this->m_slider->fileUpload($id);

                if ($upload['status'] != false) {
                    $json['status']   = true;
                    $data["foto"]             = $upload['file_large'];
                    $data["foto_path"]        = $upload['location'] . $upload['file_large'];
                    // update data di table
                    $this->db->where('id', $id);
                    $this->db->update('slider', $data);
                } else {
                    $json['status']  = false;
                    $json['message'] = $upload['message'];
                }
            }
        } else {

            $json['status']      = false;
            $json['id']          = form_error('id', '<p class="text-danger">', '</p>');
            $json['nama']       = form_error('nama', '<p class="text-danger">', '</p>');

            if ($_FILES['file']['name'] != '' or $_FILES['file']['name'] != null) {
                $json['file']        = form_error('file', '<p class="text-danger">', '</p>');
            }

            $json['konten']      = form_error('konten', '<p class="text-danger">', '</p>');
            $json['status']      = form_error('status', '<p class="text-danger">', '</p>');
        }

        $json['csrf'] = generate_csrf();
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($json));
    }

    function detail()
    {
        $id = hexToStr($this->input->get('id'));
        $data = $this->m_slider->detail($id);
        $return = "";
        $status = false;
        if (@$data) {
            $status = true;
            $return = $data;
        }
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(
                array('status' => $status, 'data' => $return, 'q' => $this->db->last_query())
            ));
    }

    public function check_uniq_judul()
    {
        $id = @hexToStr($this->input->post('id'));

        if ($id == '') {
            // insert data
            $data = $this->db->where(array('judul' => $this->input->post('judul'), 'dihapus_pada' => null))->get('berita')->num_rows();
        } else {
            // update data
            $data = $this->db->where(array('judul' => $this->input->post('judul'), 'dihapus_pada' => null, 'id !=' => $id))->get('berita')->num_rows();
        }

        if ($data == 0) {
            return TRUE;
        } else {
            $this->form_validation->set_message('check_uniq_judul', 'Judul telah digunakan');
            return FALSE;
        }
    }

    public function delete()
    {
        $this->db->trans_begin();
        $id = hexToStr($this->input->post('id'));
        $data = ['dihapus_pada' => date("Y-m-d H:i:s"), 'dihapus_oleh' => $this->session->userdata('id')];
        //update data berita
        $this->db->update('slider', $data, ['id' => $id]);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            $json['status']  = false;
            $json['message'] = 'Gagal menghapus slider';
        } else {
            $this->db->trans_commit();
            $json['status']  = true;
        }

        $json['csrf'] = generate_csrf();
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($json));
    }

    public function destroy($id)
    {
        echo $this->m_slider->remdir($id);
    }

    public function check_file()
    {

        $allowedExts = array("gif", "jpeg", "jpg", "png", "JPG", "JPEG", "GIF", "PNG");
        $extension   = pathinfo($_FILES["file"]["name"], PATHINFO_EXTENSION);

        if ($_FILES['file']['size'] < 40000000 and in_array($extension, $allowedExts)) {
            return TRUE;
        } elseif (!in_array($extension, $allowedExts)) {
            $this->form_validation->set_message('check_file', 'Format file tidak sesuai');
            return FALSE;
        } elseif ($_FILES['file']['size'] > 40000000) {
            $this->form_validation->set_message('check_file', 'File terlalu besar');
            return FALSE;
        } else {
            $this->form_validation->set_message('check_file', 'Periksa kembali inputan anda');
            return FALSE;
        }
    }

    public function ubahStatus($id)
    {
        $id = hexToStr($id);
        $data['status']        = $this->input->post('status', TRUE);
        $data['diubah_oleh']   = $this->session->userdata('id');
        $data['diubah_pada']   = date("Y-m-d H:i:s");
        // mengubah data pada database
        $this->db->where('id', $id);
        $update = $this->db->update('slider', $data);
        if ($update) {
            $json['status'] = true;
        } else {
            $json['status'] = false;
        }

        $json['csrf'] = generate_csrf();
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($json));
    }
}
