<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori_unit extends MY_Controller {

    public function __construct(){
        parent::__construct();
        if(!$this->id){
            redirect('login/logout');
        } 
        $this->load->model('backend/admin/M_kategori_pelaksana', 'm_main');
        $this->load->model(['All_crud']);
    }

    function index() {  
        $data = [ 
            'page_title'        => 'Kategori Unit',
            'detail_page_title' => 'List Data',
            'li_active'         => 'kategori',
            'uri_segment'       => 'backend/admin/kategori_unit/',
            'content'           => 'backend/admin/kategori_unit/home',
            'script'            => 'backend/admin/kategori_unit/home-js',
            'datatables_js'     => TRUE,
            'datatables_css'    => TRUE,
            'toastr'            => TRUE,
            'sweet_alert'       => TRUE,
            'btn_kembali'       => '<a href="'.@$_SERVER['HTTP_REFERER'].'" class="btn btn-sm btn-rounded btn-outline-danger"> <i class="si si-arrow-left"></i> Kembali</a>',
            'btn_tambah'        => '<button type="button" href="javascript:;" class="btn btn-sm btn-rounded btn-outline-primary" id="tombol_tambah"> <i class="si si-plus"></i> Tambah</button>',
            'modal'         => array(
                'backend/admin/kategori_unit/modal-tambah', 
                'backend/admin/kategori_unit/modal-ubah',
            )
        ];

        $this->load->view('_templates/main', $data);
    }

    function get_data(){
        $res = $list = $this->m_main->list_data(); 
        $res['csrf'] = generate_csrf();
        echo json_encode($res);
    }

    function store() {
        $json = array(); $data = array();

        $this->form_validation->set_rules('nama','nama kategori tidak boleh kosong','required|trim|is_unique[kategori.nama]');
        $this->form_validation->set_rules('deskripsi','deskripsi tidak boleh kosong','required');
        $this->form_validation->set_rules('status','Status tidak boleh kosong','required');
        $this->form_validation->set_message('required', 'Anda melewatkan input, {field}!');
        $this->form_validation->set_message('is_unique', 'Nama kategori sudah terdaftar, kategori tidak boleh sama');
        if ($this->form_validation->run() == FALSE) {
        	$json['status'] = false;
        } else{
            
        	$data['nama']          = $this->input->post('nama', TRUE);
        	$data['jenis']         = $this->input->post('jenis', TRUE);
            $data['slug']          = slugify($this->input->post('nama', TRUE));
            $data['deskripsi']     = $this->input->post('deskripsi', TRUE);
            $data['status']        = $this->input->post('status', TRUE);
            $data['dibuat_pada']   = date("Y-m-d H:i:s");
            $data['diubah_pada']   = date("Y-m-d H:i:s");
            $data['dibuat_oleh']   = $this->session->userdata('id');
            // menyimpan data pada database
            $this->db->insert('kategori_unit', $data);
            $json['status'] = true;

            // kebutuhan push notifikasi
            $this->load->view('../../vendor/autoload.php');
            $options = array(
                'cluster' => 'ap1',
                'useTLS' => true
            );
            $pusher = new Pusher\Pusher(
                '4b7d59c739af7d7ca6d2',
                '1dcbdcfd5a68151cc94d',
                '1125954',
                $options
            );
            $count_sidebar['kategori'] = count_kategori_berita();
            $pusher->trigger('web-kemenpar-ap', 'count-sidebar', $count_sidebar);
            // kebutuhan push notifikasi

            // kebutuhan push notifikasi
            $this->load->view('../../vendor/autoload.php');
            $options = array(
                'cluster' => 'ap1',
                'useTLS' => true
            );
            $pusher = new Pusher\Pusher(
                '4b7d59c739af7d7ca6d2',
                '1dcbdcfd5a68151cc94d',
                '1125954',
                $options
            );
            $table_data['table_data_reload'] = 'table_reload';
            $pusher->trigger('web-kemenpar-ap', 'table-data', $table_data);
            // kebutuhan push notifikasi
        }
        $json['csrf'] = generate_csrf();
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($json)); 
    }

    function update() {
        $json = array(); $data = array();

        $this->form_validation->set_rules('id','id tidak boleh kosong','required|trim');
        $this->form_validation->set_rules('nama','nama kategori tidak boleh kosong','required|trim|callback_check_uniq_nama');
        $this->form_validation->set_rules('deskripsi','deskripsi tidak boleh kosong','required');
        $this->form_validation->set_rules('status','Status tidak boleh kosong','required');
        $this->form_validation->set_message('required', 'Anda melewatkan input, {field}!');
        $this->form_validation->set_message('is_unique', 'Nama kategori sudah terdaftar, kategori tidak boleh sama');
        if ($this->form_validation->run() == FALSE) {
        	$json['status'] = false;
        } else{
            
            $data['nama']          = $this->input->post('nama', TRUE);
            $data['jenis']         = $this->input->post('jenis', TRUE);
            $data['slug']          = slugify($this->input->post('nama', TRUE));
            $data['deskripsi']     = $this->input->post('deskripsi', TRUE);
            $data['status']        = $this->input->post('status', TRUE);
            $data['diubah_oleh']   = $this->session->userdata('id');
            $data['diubah_pada']   = date("Y-m-d H:i:s");
            // mengubah data pada database
            $this->db->where('id', $this->input->post('id', TRUE));
            $this->db->update('kategori_unit', $data); 
            $json['status'] = true;

            // kebutuhan push notifikasi
            $this->load->view('../../vendor/autoload.php');
            $options = array(
                'cluster' => 'ap1',
                'useTLS' => true
            );
            $pusher = new Pusher\Pusher(
                '4b7d59c739af7d7ca6d2',
                '1dcbdcfd5a68151cc94d',
                '1125954',
                $options
            );
            $table_data['table_data_reload'] = 'table_reload';
            $pusher->trigger('web-kemenpar-ap', 'table-data', $table_data);
            // kebutuhan push notifikasi  
        }
        $json['csrf'] = generate_csrf();
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($json)); 
    }

    function detail()
    { 
        $id = hexToStr($this->input->get('id'));
        $data = $this->db->select('id, nama, deskripsi, status, jenis')->where(['dihapus_pada is NULL', 'id' => $id])->get('kategori_unit')->row();
        $return = ""; $status = false;
        if (@$data) { 
            $status = true;
            $return = $data;
        }
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(
                array('status' => $status,'data' => $return,'csrf' => generate_csrf())
            )); 
    } 
    
    function delete($id) { 
        $id = hexToStr($id); 
    	$this->All_crud->update_by_id(
            'kategori_unit',
            'id',
            $id,
            [
                'dihapus_pada' => date("Y-m-d H:i:s"),
                'dihapus_oleh' => $this->session->userdata('id'),

            ]
    
        ); 
        if($this->db->affected_rows()>0){
        	$status = true;    
            // kebutuhan push notifikasi
            $this->load->view('../../vendor/autoload.php');
            $options = array(
                'cluster' => 'ap1',
                'useTLS' => true
            );
            $pusher = new Pusher\Pusher(
                '4b7d59c739af7d7ca6d2',
                '1dcbdcfd5a68151cc94d',
                '1125954',
                $options
            );
            $count_sidebar['kategori'] = count_kategori_berita();
            $pusher->trigger('web-kemenpar-ap', 'count-sidebar', $count_sidebar);
            // kebutuhan push notifikasi 

            // kebutuhan push notifikasi
            $this->load->view('../../vendor/autoload.php');
            $options = array(
                'cluster' => 'ap1',
                'useTLS' => true
            );
            $pusher = new Pusher\Pusher(
                '4b7d59c739af7d7ca6d2',
                '1dcbdcfd5a68151cc94d',
                '1125954',
                $options
            );
            $table_data['table_data_reload'] = 'table_reload';
            $pusher->trigger('web-kemenpar-ap', 'table-data', $table_data);
            // kebutuhan push notifikasi          
        } else {
            $status = false; 
        }
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(array('status' => $status,"csrf" => generate_csrf())));
    }

    public function show()
    {
        $jenis = $this->input->get('jenis');
        $data  = $this->db->select('a.id, a.nama as nama')
                        ->where('a.dihapus_pada', null)
                        ->where('jenis', $jenis)
                        ->where('status', '1')
                        ->get('kategori_unit a')->result();
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data)); 
    }
    
    public function show_id()
    {
        $id = hexToStr($id);
        $jenis = $this->input->get('jenis');
        $data  = $this->db->select('a.id, a.nama as nama')
                        ->where('a.dihapus_pada', null)
                        ->where('jenis', $jenis)
                        ->where('status', '1')
                        ->get('kategori_unit a')->result();
        
        echo '<select id="kategori_id" name="kategori_id">';
         foreach($data as $k){
            if($id == $v->id){
               echo "<option value='{$k->id}' selected>{$k->nama}</option>";
            } else {
               echo "<option value='{$k->id}'>{$k->nama}</option>";
            }
         }
         echo '</select>';
//        $html = '<select id="kategori_id" name="kategori_id">';
//        foreach($data as $k){
//            if($id == $v->id){
//                $html += 
//            } else {
//                $html += "<option value='{$k->id}'>{$k->nama}</option>";
//            }
//        } 
        
//        $html += "</select>";
//        echo $html;
//        $this->output
//            ->set_content_type('application/json')
//            ->set_output(json_encode($html)); 
//        
        
    }
    
    public function check_uniq_nama()
    {
        $id = @$this->input->post('id');

        if($id == ''){
            // insert data
            $data = $this->db->where(array('nama' => $this->input->post('nama'), 'dihapus_pada' => null))->get('kategori')->num_rows();
        }else{
            // update data
            $data = $this->db->where(array('nama' => $this->input->post('nama'), 'dihapus_pada' => null, 'id !=' => $this->input->post('id')))->get('kategori')->num_rows();
        }
        
        if ($data == 0) {
            return TRUE;
        }else{
            $this->form_validation->set_message('check_uniq_nama', 'Nama kategori telah digunakan');
            return FALSE;
        }
    }

    public function ubahStatus($id)
    {
        $id = hexToStr($id); 
        $data['status']        = $this->input->post('status', TRUE);
        $data['diubah_oleh']   = $this->session->userdata('id');
        $data['diubah_pada']   = date("Y-m-d H:i:s");
        // mengubah data pada database
        $this->db->where('id', $id);
        $update = $this->db->update('kategori_unit', $data); 
        if($update){
            $json['status'] = true;

            // kebutuhan push notifikasi
            $this->load->view('../../vendor/autoload.php');
            $options = array(
                'cluster' => 'ap1',
                'useTLS' => true
            );
            $pusher = new Pusher\Pusher(
                '4b7d59c739af7d7ca6d2',
                '1dcbdcfd5a68151cc94d',
                '1125954',
                $options
            );
            $count_sidebar['kategori'] = count_kategori_berita();
            $pusher->trigger('web-kemenpar-ap', 'count-sidebar', $count_sidebar);
            // kebutuhan push notifikasi 

            // kebutuhan push notifikasi
            $this->load->view('../../vendor/autoload.php');
            $options = array(
                'cluster' => 'ap1',
                'useTLS' => true
            );
            $pusher = new Pusher\Pusher(
                '4b7d59c739af7d7ca6d2',
                '1dcbdcfd5a68151cc94d',
                '1125954',
                $options
            );
            $count_sidebar['kategori_berita_non_aktif'] = count_kategori_berita_non_aktif();
            $pusher->trigger('web-kemenpar-ap', 'count-sidebar', $count_sidebar);
            // kebutuhan push notifikasi

            // kebutuhan push notifikasi
            $this->load->view('../../vendor/autoload.php');
            $options = array(
                'cluster' => 'ap1',
                'useTLS' => true
            );
            $pusher = new Pusher\Pusher(
                '4b7d59c739af7d7ca6d2',
                '1dcbdcfd5a68151cc94d',
                '1125954',
                $options
            );
            $table_data['table_data_reload'] = 'table_reload';
            $pusher->trigger('web-kemenpar-ap', 'table-data', $table_data);
            // kebutuhan push notifikasi
        }else{
            $json['status'] = false;
        }

        $json['csrf'] = generate_csrf();
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($json)); 
    }
}