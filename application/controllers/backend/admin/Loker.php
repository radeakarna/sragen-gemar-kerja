<?php

ini_set('memory_limit', '512M');

defined('BASEPATH') or exit('No direct script access allowed');

class Loker extends MY_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->id) {
            redirect('login/logout');
        }
        $this->load->model('backend/admin/M_loker', 'm_loker');
        $this->load->model(['All_crud']);
        // $this->load->library('Image');
    }

    function index() {
        $data = [
            'page_title' => 'Loker',
            'detail_page_title' => 'List Data Loker',
            'li_active' => 'loker',
            'uri_segment' => 'backend/admin/loker/',
            'content' => 'backend/admin/loker/loker_utama',
            'script' => 'backend/admin/loker/loker_utama_js',
            'datatables_js' => TRUE,
            'datatables_css' => TRUE,
            'toastr' => TRUE,
            'sweet_alert' => TRUE,
            'btn_kembali' => '<a href="' . @$_SERVER['HTTP_REFERER'] . '" class="btn btn-sm btn-rounded btn-outline-danger"> <i class="si si-arrow-left"></i> Kembali</a>',
            'btn_tambah' => '<a href="' . base_url('backend/admin/loker/create') . '"><button type="button" href="javascript:;" class="btn btn-sm btn-rounded btn-outline-primary" id="tombol_tambah"> <i class="si si-plus"></i> Tambah</button></a>',
            'modal' => array()
        ];

        $this->load->view('_templates/main', $data);
    }

    function get_data() {
        $res = $list = $this->m_loker->list_data();
        $res['csrf'] = generate_csrf();
        echo json_encode($res);
    }
    
    function get_perusahaan(){
        $arrperusahaan = array();
        $perusahaan = $this->db->select('id,nama')
            ->where('dihapus_pada is NULL')
            ->get('perusahaan')
            ->result();
        foreach ($perusahaan as $perusahaanq) {
            $arrperusahaan[$perusahaanq->id] = $perusahaanq->nama;
        }
        return $arrperusahaan;

    }

    public function create() {
        
        $arrperusahaan = $this->get_perusahaan();
        $pil_perusahaan = '';
        $pil_perusahaan .= '<option value="' ."none" . '">' . "--- Pilih Perusahaan ---" . '</option>';
        foreach ($arrperusahaan as $key => $value) {
            $pil_perusahaan .= '<option value="' . $key . '">' . $value . '</option>';
        }

        
        $data = [
            'page_title' => 'Tambah Data Loker',
            'detail_page_title' => 'Tambah Data',
            'li_active' => 'loker',
            'uri_segment' => 'backend/admin/loker/',
            'content' => 'backend/admin/loker/tambah/loker',
            'pil_perusahaan' => $pil_perusahaan,
            'script' => 'backend/admin/loker/tambah/loker_js',
            'toastr' => TRUE,
            'sweet_alert' => TRUE,
            'sumernote' => TRUE,
            'btn_kembali' => '<a href="' . @$_SERVER['HTTP_REFERER'] . '" class="btn btn-sm btn-rounded btn-outline-danger"> <i class="si si-arrow-left"></i> Kembali</a>',
            'modal' => array(
            // 'backend/admin/slider_utama/tambah/modal_tambah', 
            )
        ];

        $this->load->view('_templates/main', $data);
    }

    public function edit($id = null) {
        
        $arrperusahaan = array();
        
        $perusahaan = $this->db->select('id,judul,perusahaan_id')
            ->where('dihapus_pada is NULL')
            ->get('loker')
            ->result();  
        
        $arrperusahaan = $this->get_perusahaan();
        $pil_perusahaan = '';
        $pil_perusahaan .= '<option value="' ."none" . '">' . "--- Pilih Perusahaan ---" . '</option>';
        foreach ($arrperusahaan as $key => $value) {
            if($perusahaan[0]->perusahaan_id == $key){
                $pil_perusahaan .= '<option value="' . $key . '" selected="selected">' . $value . '</option>';
            } else {
                $pil_perusahaan .= '<option value="' . $key . '">' . $value . '</option>';
            }
        }
        
        $data = [
            'page_title' => 'Edit Data Loker',
            'detail_page_title' => 'Edit Data',
            'li_active' => 'loker',
            'pil_perusahaan' => $pil_perusahaan,
            'uri_segment' => 'backend/admin/loker/',
            'content' => 'backend/admin/loker/edit/loker',
            'script' => 'backend/admin/loker/edit/loker_js',
            'toastr' => TRUE,
            'sweet_alert' => TRUE,
            'sumernote' => TRUE,
            'id' => $id,
            'btn_kembali' => '<a href="' . @$_SERVER['HTTP_REFERER'] . '" class="btn btn-sm btn-rounded btn-outline-danger"> <i class="si si-arrow-left"></i> Kembali</a>',
            'modal' => array()
        ];

        $this->load->view('_templates/main', $data);
    }

    function store() {
        $json = array();
        $data = array();

        $this->form_validation->set_rules('tambah_judul', 'judul tidak boleh kosong', 'required|trim|callback_check_uniq_judul');
        $this->form_validation->set_rules('tambah_perusahaan', 'perusahaan tidak boleh kosong', 'required');
        $this->form_validation->set_rules('tambah_deskripsi', 'deskripsi tidak boleh kosong', 'required');
       $this->form_validation->set_rules('mulai_sewa', 'mulai sewa tidak boleh kosong', 'required');
        $this->form_validation->set_rules('jatuh_tempo', 'jatuh tempo tidak boleh kosong', 'required');
        
        $this->form_validation->set_message('required', 'Anda melewatkan input, {field}!');
       // $this->form_validation->set_rules('file', 'file ', 'callback_check_file');
        // $this->form_validation->set_message('is_unique', 'judul sudah terdaftar, judul tidak boleh sama');
        if ($this->form_validation->run() != FALSE) {

            $data['perusahaan_id'] = $this->input->post('tambah_perusahaan');
            $data['judul'] = $this->input->post('tambah_judul', TRUE);
            $data['slug'] = slugify($this->input->post('tambah_judul', TRUE));
            $data['deskripsi'] = $this->input->post('tambah_deskripsi', TRUE);
            $data['dibuat_pada'] = date("Y-m-d H:i:s");
            $data['tanggal_mulai'] = date("Y-m-d H:i:s", strtotime($this->input->post('mulai_sewa')));
            $data['tanggal_selesai'] = date("Y-m-d H:i:s", strtotime($this->input->post('jatuh_tempo')));
            $data['jangka_waktu'] = $this->input->post('jangka_waktu');
            $data['dibuat_oleh'] = $this->session->userdata('id');

            // $this->db->trans_begin();
            try{
                $this->db->trans_start();
                $this->db->insert('loker', $data);
                $this->db->trans_commit();
                
                $json['status'] = true;
            } catch(Exception $e){
                 $json['status'] = false;
                $this->db->trans_rollback();
            
            }
        } else {

            $json['status'] = false;
            $json['tambah_perusahaan'] = form_error('tambah_perusahaan', '<p class="text-danger">', '</p>');
            $json['tambah_nama'] = form_error('tambah_nama', '<p class="text-danger">', '</p>');
            $json['tanggal_mulai'] = form_error('tanggal_mulai', '<p class="text-danger">', '</p>');
            $json['tanggal_selesai'] = form_error('tanggal_selesai', '<p class="text-danger">', '</p>');
            
            $json['tambah_deskripsi'] = form_error('tambah_deskripsi', '<p class="text-danger">', '</p>');
            $json['status'] = form_error('status', '<p class="text-danger">', '</p>');
        }
        $json['csrf'] = generate_csrf();
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($json));
    }

    function update() {
        $id = @hexToStr($this->input->post('id'));
        $json = array();
        $data = array();

        $this->form_validation->set_rules('ubah_judul', 'judul tidak boleh kosong', 'required|trim|callback_check_uniq_judul');
        $this->form_validation->set_rules('ubah_perusahaan', 'perusahaan tidak boleh kosong', 'required');
        $this->form_validation->set_rules('ubah_deskripsi', 'deskripsi tidak boleh kosong', 'required');
        $this->form_validation->set_rules('mulai_sewa', 'mulai sewa tidak boleh kosong', 'required');
        $this->form_validation->set_rules('jatuh_tempo', 'jatuh tempo tidak boleh kosong', 'required');
        $this->form_validation->set_message('required', 'Anda melewatkan input, {field}!');
        if ($this->form_validation->run() != FALSE) {

            
            $data['perusahaan_id'] = $this->input->post('ubah_perusahaan');
            $data['judul'] = $this->input->post('ubah_judul', TRUE);
            $data['slug'] = slugify($this->input->post('ubah_judul', TRUE));
             $data['tanggal_mulai'] = date("Y-m-d H:i:s", strtotime($this->input->post('mulai_sewa')));
            $data['tanggal_selesai'] = date("Y-m-d H:i:s", strtotime($this->input->post('jatuh_tempo')));
            $data['jangka_waktu'] = $this->input->post('jangka_waktu');
            $data['deskripsi'] = $this->input->post('ubah_deskripsi', TRUE);
            $data['diubah_pada'] = date("Y-m-d H:i:s");
            $data['diubah_oleh'] = $this->session->userdata('id');

             try{
                $this->db->trans_start();
                $this->db->where('id', $id);
                $this->db->update('loker', $data);
                $this->db->trans_commit();
                
                $json['status'] = true;
            } catch(Exception $e){
                 $json['status'] = false;
                $this->db->trans_rollback();
            
            }
        } else {

            $json['status'] = false;
            $json['ubah_judul'] = form_error('ubah_judul', '<p class="text-danger">', '</p>');
            $json['ubah_perusahaan'] = form_error('ubah_perusahaan', '<p class="text-danger">', '</p>');
             $json['tanggal_mulai'] = form_error('tanggal_mulai', '<p class="text-danger">', '</p>');
            $json['tanggal_selesai'] = form_error('tanggal_selesai', '<p class="text-danger">', '</p>');
           
            $json['ubah_deskripsi'] = form_error('ubah_deskripsi', '<p class="text-danger">', '</p>');
            $json['status'] = form_error('status', '<p class="text-danger">', '</p>');
        }

        $json['csrf'] = generate_csrf();
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($json));
    }

    function detail() {
        $id = hexToStr($this->input->get('id'));
        $data = $this->m_loker->detail($id);
        $return = "";
        $status = false;
        if (@$data) {
            $status = true;
            $return = $data;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(
                                array('status' => $status, 'data' => $return, 'q' => $this->db->last_query())
        ));
    }

    public function check_uniq_judul() {
        $id = @hexToStr($this->input->post('id'));

        if ($id == '') {
            // insert data
            $data = $this->db->where(array('judul' => $this->input->post('judul'), 'dihapus_pada' => null))->get('berita')->num_rows();
        } else {
            // update data
            $data = $this->db->where(array('judul' => $this->input->post('judul'), 'dihapus_pada' => null, 'id !=' => $id))->get('berita')->num_rows();
        }

        if ($data == 0) {
            return TRUE;
        } else {
            $this->form_validation->set_message('check_uniq_judul', 'Judul telah digunakan');
            return FALSE;
        }
    }

    public function delete() {
        $this->db->trans_begin();
        $id = hexToStr($this->input->post('id'));
        $data = ['dihapus_pada' => date("Y-m-d H:i:s"), 'dihapus_oleh' => $this->session->userdata('id')];
        //update data berita
        $this->db->update('loker', $data, ['id' => $id]);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            $json['status'] = false;
            $json['message'] = 'Gagal menghapus perusahaan';
        } else {
            $this->db->trans_commit();
            $json['status'] = true;
        }

        $json['csrf'] = generate_csrf();
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($json));
    }

    public function destroy($id) {
        echo $this->m_slider->remdir($id);
    }

    public function check_file() {

        $allowedExts = array("gif", "jpeg", "jpg", "png", "JPG", "JPEG", "GIF", "PNG");
        $extension = pathinfo($_FILES["file"]["name"], PATHINFO_EXTENSION);

        if ($_FILES['file']['size'] < 40000000 and in_array($extension, $allowedExts)) {
            return TRUE;
        } elseif (!in_array($extension, $allowedExts)) {
            $this->form_validation->set_message('check_file', 'Format file tidak sesuai');
            return FALSE;
        } elseif ($_FILES['file']['size'] > 40000000) {
            $this->form_validation->set_message('check_file', 'File terlalu besar');
            return FALSE;
        } else {
            $this->form_validation->set_message('check_file', 'Periksa kembali inputan anda');
            return FALSE;
        }
    }

    public function ubahStatus($id) {
        $id = hexToStr($id);
        $data['status'] = $this->input->post('status', TRUE);
        $data['diubah_oleh'] = $this->session->userdata('id');
        $data['diubah_pada'] = date("Y-m-d H:i:s");
        // mengubah data pada database
        $this->db->where('id', $id);
        $update = $this->db->update('loker', $data);
        if ($update) {
            $json['status'] = true;
        } else {
            $json['status'] = false;
        }

        $json['csrf'] = generate_csrf();
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($json));
    }

}
