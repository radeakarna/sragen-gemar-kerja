<?php
class Registrasi extends CI_Controller {
    
    public function __construct(){
        parent::__construct();

        $this->load->model('backend/admin/M_user', 'm_main');
        $this->load->model(['All_crud']);
    }
    
    function get_perusahaan(){
        $arrperusahaan = array();
        $perusahaan = $this->db->select('id,nama')
            ->where('dihapus_pada is NULL')
            ->get('perusahaan')
            ->result();
        foreach ($perusahaan as $perusahaanq) {
            $arrperusahaan[$perusahaanq->id] = $perusahaanq->nama;
        }
        return $arrperusahaan;

    }
    
    function arrRole(){
        $arrRole = array();
        
        $get_role = $this->db->select("id, nama")->where('nama = "Pencaker" OR nama = "umum"')->get("ref_role")->result();                                                                                     
        foreach($get_role as $roles){
                $arrRole[$roles->id] = $roles->nama;
        }
        
        return $arrRole;
    }
    
    public function jenisKelamin(){
        $arr_jk = array();
        
        $get_jenis_kelamin = $this->db->select('id,name')->get('ref_jenis_kelamin')->result();
        foreach($get_jenis_kelamin as $key){
            $arr_jk[$key->id] = $key->name;
        }
        
        return $arr_jk;
    }
    
    public function ref_status_kerja(){
        $arr_kerja = array();
        
        $get_stker = $this->db->select('id,nama')->get('ref_status_kerja')->result();
        foreach($get_stker as $key){
            $arr_kerja[$key->id] = $key->nama;
        }
        
        return $arr_kerja;
    }
    
    public function index(){
        $arrperusahaan = $this->get_perusahaan();
        $get_jenis_kelamin = $this->jenisKelamin();
        $get_stt_kerja = $this->ref_status_kerja();
        
        $pil_perusahaan = '';
        $pil_perusahaan .= '<option value="' ."none" . '">' . "--- Pilih Perusahaan ---" . '</option>';
        foreach ($arrperusahaan as $key => $value) {
            $pil_perusahaan .= '<option value="' . $key . '">' . $value . '</option>';
        }
        
        $arr_roles = $this->arrRole();
        
        $get_roles = '';
        
        $get_roles .= '<option value="' . 'none' . '">' . 'Pilih Role' . '</option>';
        foreach ($arr_roles as $key => $value) {
            $get_roles .= '<option value="' . $key . '">' . $value . '</option>';
        }
        
        
         $pilih_jenis_kelamin = '';

        $pilih_jenis_kelamin .= '<option value="' . 'none' . '">' . 'Pilih Jenis Kelamin' . '</option>';
        foreach ($get_jenis_kelamin as $key => $value) {
            if($jenis_pilih == $key){
                $pilih_jenis_kelamin .= '<option value="' . $key . '" selected>' . $value . '</option>';
            } else {
                $pilih_jenis_kelamin .= '<option value="' . $key . '">' . $value . '</option>';
            }
        }
        
        $pilih_status_kerja = '';
        
        $pilih_status_kerja .= '<option value="' . 'none' . '">' . 'Pilih Status Kerja' . '</option>';
        foreach ($get_stt_kerja as $key => $value) {
            if($status == $key){
                $pilih_status_kerja .= '<option value="' . $key . '" selected>' . $value . '</option>';
            } else {
                $pilih_status_kerja .= '<option value="' . $key . '">' . $value . '</option>';
            }
        }
        
        $data = [ 
            'page_title'        => 'Registrasi User',
            'detail_page_title' => 'Registrasi User',
            'role'              => $get_roles,
            'perusahaan'        => $pil_perusahaan,
            'jenis_kelamin'     => $pilih_jenis_kelamin,
            'status'            => $pilih_status_kerja,
            'uri_segment'       => 'registrasi/',
            'content'           => 'frontend/registrasi/registrasi',
            'script'            => 'frontend/registrasi/registrasi-js',
            'toastr'            => TRUE,
            'sweet_alert'       => TRUE,
            'select2'           => TRUE
        ];

        $this->load->view('frontend/_templates/main', $data);
    }
    
    function store() {
        $id = $this->input->post('id');
        $status = ""; $json = array(); $data = array();

        // $this->form_validation->set_rules('id_penduduk', 'Penduduk ', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('username', 'Username', 'required|is_unique[users.username]');
        $this->form_validation->set_rules('name_role', 'Role', 'required');
        $this->form_validation->set_rules('usaha', 'usaha', 'required');
        $this->form_validation->set_rules('password', 'Password ', 'required');
        $this->form_validation->set_rules('confirm_password', 'confirm_password ', 'required|matches[password]');
        $this->form_validation->set_message('is_unique', 'Username sudah terdaftar, username tidak boleh sama');
        $this->form_validation->set_message('confirm_password', 'Password tidak sama');
        
        $this->form_validation->set_rules('ktp', 'ktp ', 'required');
        $this->form_validation->set_rules('nim', 'nim ', 'required');
      
        $this->form_validation->set_rules('jsk', 'jsk ', 'required');
        $this->form_validation->set_rules('hp', 'hp ', 'required');
        $this->form_validation->set_rules('sttp', 'sttp ', 'required');
        $this->form_validation->set_rules('pendidikan_terakhir', 'pendidikan_terakhir ', 'required');
        $this->form_validation->set_rules('bpjs', 'bpjs ', 'required');

        if ($this->form_validation->run() == FALSE) {
        	$json = array(
                'status'             => false,
                'message'            => 'inputan tidak boleh kosong',
                // 'id_penduduk'        => form_error('id_penduduk', '<p class="text-danger">', '</p>'),
                'username'           => form_error('username', '<p class="text-danger">', '</p>'),
                'name_role'           => form_error('name_role', '<p class="text-danger">', '</p>'),
                'email'              => form_error('email', '<p class="text-danger">', '</p>'),
                'password'           => form_error('password', '<p class="text-danger">', '</p>'),
                'usaha'         => form_error('usaha', '<p class="text-danger">', '</p>'),
                'confirm_password'   => form_error('confirm_password', '<p class="text-danger">', '</p>'),
                'ktp'                => form_error('ktp', '<p class="text-danger">', '</p>'),
                'nim'                => form_error('nim', '<p class="text-danger">', '</p>'),
                'jsk'                => form_error('jsk', '<p class="text-danger">', '</p>'),
                'hp'                 => form_error('hp', '<p class="text-danger">', '</p>'),
                'sttp'               => form_error('sttp', '<p class="text-danger">', '</p>'),
                'pendidikan_terakhir' => form_error('pendidikan_terakhir', '<p class="text-danger">', '</p>'),
                'bpjs'               => form_error('bpjs', '<p class="text-danger">'), 
                'csrf'               => generate_csrf()
            );
                
         
            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($json)); 
        } else{
            
            $data = [ 
                'email'      => $this->input->post('email'),
                'username'   => $this->input->post('username'),
                'password'   => $this->input->post('password'),
                'perusahaan_id' => $this->input->post('usaha'),
                'id_role'    => $this->input->post('name_role'),
                'is_active'  => '1',
                'created_at' => date("Y-m-d H:i:s"),
                'created_by' => $this->session->userdata('id'),
            ]; 
            
            $data_detail = [
                'nim'          => $this->input->post('nim'),
                'status_pekerjaan_id' => $this->input->post('sttp'),
                'email'        => $this->input->post('email'),
                'no_hp'        => $this->input->post('hp'),
                'pendidikan_terakhir' => $this->input->post('pendidikan_terakhir'),
                'no_ktp'       => $this->input->post('ktp'),
                'bpjs'         => $this->input->post('bpjs'),
                'jenis_kelamin_id' => $this->input->post('jsk')
            ];
       
            $this->db->trans_start();
        
            if($this->db->insert('users', $data)){
                $insert_id = $this->db->insert_id();
                
                $data_detail['user_id'] = $insert_id;
                $this->db->insert('detail_user', $data_detail);

            }

            $this->db->trans_complete();

            if($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $data['status'] = 'false';
            } else {
                $this->db->trans_commit();
                
                $data['status'] = 'true';
              
            }
        

	        $data['csrf'] = generate_csrf();
                $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data)); 
        }
    }
}