<?php
class Home extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('frontend/KategoriModel','kategori_berita');
        $this->load->model('frontend/ProgramModel','program');
        $this->load->model('frontend/PengumumanModel','pengumuman');
        $this->load->model('frontend/BeritaModel','berita');
        // $this->load->model('frontend/BaseModel','base');
    }
    
    public function index()
    {

        $data = [ 
            'page_title'        => 'Beranda',
            'detail_page_title' => 'beranda',
            'slider'            => $this->db->select('t1.deskripsi, t1.nama, t1.foto_path')->where(['t1.status' => '1', 't1.dihapus_pada' => null])->get('slider t1')->result(),
            'profil'            => $this->db->get('profile t1', 1)->row(),
            'kategori_berita'   => $this->kategori_berita->tampil(),
            'program'           => $this->program->tampil(3),
            'pengumuman'        => $this->pengumuman->tampil(10),
            'berita'            => $this->berita->tampil(10),
            'berita_terbaru'    => $this->berita->tampil(1),
            'li_active'         => 'beranda',
            'uri_segment'       => 'home',
            'content'           => 'frontend/home/home',
            'script'            => 'frontend/home/home-js',
            'header'            => true,
            // 'highchart'         => true,
            // 'leaflet'           => true, 
        ];

        $this->load->view('frontend/_templates/main', $data);

    }

    public function cek()
    {
        print_r($this->pengumuman->tampil(5));
    }
}