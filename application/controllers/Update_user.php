<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Update_user extends MY_Controller {

    public function __construct(){
        parent::__construct();
        //$this->load->helper(array('Cookie', 'String'));
        // if(!$this->user_id){
        //     redirect('login/logout');
        // }
    }

    public function index()
    {
        $id = $this->input->post('id');
        if($this->session->userdata('nama_role') == 'Administrator'){
            $this->update_adminitrator($id);
        }else if($this->session->userdata('nama_role') == 'Verifikator'){
            $this->update_adminitrator($id);
        }else if($this->session->userdata('nama_role') == 'Pengawas'){
            $this->update_pengawas($id);
        }else{
            $json['status']  = false;
            $json['message'] = 'Role tidak ditemukan';
            $json['csrf']    = generate_csrf();
            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($json)); 
        }
    }

    public function update_adminitrator($id)
    {
        $status = ""; $json = array(); $data = array();
        $this->form_validation->set_rules('old_password', 'old_password ', 'required|callback_check_old_password');
        $this->form_validation->set_rules('new_password', 'new_password ', 'required');
        $this->form_validation->set_rules('confirm_password', 'confirm_password ', 'required|matches[new_password]');
        $this->form_validation->set_message('matches', 'Password tidak sama dengan password baru');
        if ($this->form_validation->run() == FALSE) {
        	$json = array(
                'status'             => false,
                'message'            => 'inputan tidak boleh kosong',
                'old_password'       => form_error('old_password', '<p class="text-danger">', '</p>'),
                'new_password'       => form_error('new_password', '<p class="text-danger">', '</p>'),
                'confirm_password'   => form_error('confirm_password', '<p class="text-danger">', '</p>'),
            );
        } else{

            $data = [
                'password'   => sha1($this->input->post('new_password')),
                'updated_at' => date("Y-m-d H:i:s"),
                'updated_by' => $this->session->userdata('id'),
            ]; 

            // update data user
            $this->db->where('id',$id);
            $this->db->update('users', $data);

            $json = array(
                'status'   => true,
            );

        }
        
        $json['csrf'] = generate_csrf();
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($json)); 
    }

    public function update_pengawas($id)
    {
        $status = ""; $json = array(); $data = array();
        $this->form_validation->set_rules('username', 'username ', 'required|callback_check_uniq_username');
        $this->form_validation->set_rules('old_password', 'old_password ', 'required|callback_check_old_password');
        $this->form_validation->set_rules('new_password', 'new_password ', 'required');
        $this->form_validation->set_rules('confirm_password', 'confirm_password ', 'required|matches[new_password]');
        $this->form_validation->set_message('matches', 'Password tidak sama dengan password baru');
        if ($this->form_validation->run() == FALSE) {
        	$json = array(
                'status'             => false,
                'message'            => 'inputan tidak boleh kosong',
                'username'       => form_error('username', '<p class="text-danger">', '</p>'),
                'old_password'       => form_error('old_password', '<p class="text-danger">', '</p>'),
                'new_password'       => form_error('new_password', '<p class="text-danger">', '</p>'),
                'confirm_password'   => form_error('confirm_password', '<p class="text-danger">', '</p>'),
            );
        } else{

            $data = [
                'username'   => $this->input->post('username'),
                'password'   => sha1($this->input->post('new_password')),
                'updated_at' => date("Y-m-d H:i:s"),
                'updated_by' => $this->session->userdata('id'),
            ]; 

            // update data user
            $this->db->where('id',$id);
            $this->db->update('users', $data);

            $json = array(
                'status'   => true,
            );

        }
        
        $json['csrf'] = generate_csrf();
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($json)); 
    }

    public function check_uniq_username()
    {
        $where['username']      = $this->input->post('username');
        $where['id !=']         = $this->input->post('id');
        $where['deleted_at']    = null;

        $data = $this->db->where($where)->get('users')->num_rows();
        if ($data == 0) {
            return TRUE;
        }else{
            $this->form_validation->set_message('check_uniq_username', 'Username telah digunakan');
            return FALSE;
        }
    }

    public function check_old_password()
    {
        
        $where['password'] = sha1($this->input->post('old_password'));
        $where['id']       = $this->input->post('id');
        
        $check_password = $this->db->where($where)->get('users')->num_rows();
        if($check_password == 1){
            return TRUE;
        }else{
            $this->form_validation->set_message('check_old_password', 'Password lama tidak sesuai');
            return FALSE;
        }
    }
}
