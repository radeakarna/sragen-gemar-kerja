<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once APPPATH . 'third_party/html2pdf/html2pdf.class.php';

class html_pdf extends HTML2PDF {

	public function __construct($orientation = 'P', $format = 'A4', $langue='fr', $unicode=true, $encoding='UTF-8', $marges = array(5, 5, 5, 8)){
		parent::__construct($orientation, $format, $langue, $unicode, $encoding, $marges);
	}

}